/*
| Parses users newsletter info.
*/
//import * as level from 'level';

import * as fs from 'node:fs/promises';

const txt = ( await fs.readFile( 'users' ) ) + '';
const lines = txt.split( '\n' );

for( let a = 1; a < lines.length; a += 2 )
{
	const line = lines[ a ];
	const obj = JSON.parse( line );
	if( obj.news !== true || obj.mail === '' ) continue;
	console.log( obj.mail, obj.name );
}
