/*
| Iterates over a db.
*/
import * as level from 'level';

/*
| Prints out usage info.
*/
const usage =
	function( )
{
	const argv = process.argv;
	console.log( 'USAGE: node ' + argv[ 0 ] + ' ' + argv[ 1 ] + ' DBNAME [greater-than] [less-than]' );
};


const run =
	async function( )
{
	const argv = process.argv;
	if( argv.length < 3 )
	{
		usage( );
		return;
	}

	const db = new level.Level( argv[ 2 ] );
	await db.open( );

	let gt;
	let lt;

	if( argv.length >= 4 )
	{
		gt = argv[ 3 ];
		if( argv.length >= 5 )
		{
			lt = argv[ 4 ];
		}
		else
		{
			lt =
				gt.substring( 0, gt.length - 1 )
				+ String.fromCharCode( gt.charCodeAt( gt.length - 1 ) + 1 );
		}
	}

	console.log( 'lt:', lt );
	console.log( 'gt:', gt );

	console.log( '>>>>' );

	const it =
		gt
		? db.iterator( { gt: gt, lt: lt } )
		: db.iterator( );

	for await ( const [ key, value ] of it )
	{
		console.log( key );
		console.log( '    ', value );
	}

	console.log( '<<<<' );

};

run( );
