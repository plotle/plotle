/*
| Starts the test suite.
*/
Error.stackTraceLimit = 15;

global.NODE = true;
global.CHECK = true;

import * as util from 'node:util';
util.inspect.defaultOptions.depth = null;

await import( 'ti2c' );
await import( 'ti2c-gleam' );

const pkg =
	await ti2c.register(
		'name',    'plotle',
		'meta',    import.meta,
		'source',  'src/',
		'relPath', 'Test/Start',
		'codegen', 'codegen/'
	);
const Root = await pkg.import( 'Test/Root' );
Root.init( pkg.rootDir );
