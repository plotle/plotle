/*
| The tests creates all basic items on plotle:home
*/
def.abstract = true;

import timers from 'node:timers/promises';

import { Self as Control        } from '{Test/Control}';
import { Self as FabricDocPath  } from '{Shared/Fabric/Item/DocPath/Self}';
import { Self as FabricLabel    } from '{Shared/Fabric/Item/Label}';
import { Self as FabricNote     } from '{Shared/Fabric/Item/Note}';
import { Self as FabricStroke   } from '{Shared/Fabric/Item/Stroke/Self}';
import { Self as FabricSubspace } from '{Shared/Fabric/Item/Subspace}';
import { Self as Plan           } from '{Shared/Trace/Plan}';
import { Self as Point          } from '{gleam:Point}';
import { Self as Rect           } from '{gleam:Rect}';
import { Self as Trace          } from '{ti2c:Trace}';

/*
| Runs the test.
*/
def.static.run =
	async function( uidPlotleHome )
{
	const traceRoot  = Trace.root( Plan.root );
	const traceView0 = traceRoot.add( 'view', 0 );

	const keys = { };

	// -- note 1 --
	{
		await Control.click( traceView0, Point.XY( 200, 100 ) );
		await Control.click(
			traceView0
			.add( 'space', uidPlotleHome )
			.add( 'bench' ).add( 'newItem' ).add( 'note' )
		);
		await Control.type( 'This is a note.' );
		await Control.press( 'Enter' );
		await Control.type( 'line two;' );
		await Control.press( 'Enter' );
		await Control.type( 'and a three' );

		const glintSpace = await Control.getGlint( traceView0 );
		console.log( glintSpace );
	}

	// -- note 2 --
	{
		await Control.click( traceView0, Point.XY( 600, 150 ) );
		await Control.click(
			traceView0
			.add( 'space', uidPlotleHome )
			.add( 'bench' ).add( 'newItem' ).add( 'note' )
		);
		await Control.type( 'Another note.' );
	}

	// -- label --
	{
		await Control.click( traceView0, Point.XY( 400, 350 ) );
		await Control.click(
			traceView0
			.add( 'space', uidPlotleHome )
			.add( 'bench' ).add( 'newItem' ).add( 'label' )
		);
	}

	{
	}

	/*
	// -- line --
	await Control.click(
		Trace.root( Plan.root )
		.add( 'panels', 'create' )
		.add( 'widgets', 'createLine' )
	);

	const rectNote1 = rectLabel1; // XXX
	await Control.drag( rectNote1.pc, Point.XY( 680,  70 ) );

	// -- arrow --
	await Control.click(
		Trace.root( Plan.root )
		.add( 'panels', 'create' )
		.add( 'widgets', 'createArrow' )
	);

	await Control.drag( rectNote1.pc, Point.XY( 680,  20 ) );

	// -- relation --
	await Control.click(
		Trace.root( Plan.root )
		.add( 'panels', 'create' )
		.add( 'widgets', 'createRelation' )
	);

	await Control.drag( rectNote1.pc, rectLabel1.pc );

	// -- doc path --
	await Control.click(
		Trace.root( Plan.root )
		.add( 'panels', 'create' )
		.add( 'widgets', 'createDocPath' )
	);

	const rectDocPath1 =
		Rect.PosPse(
			Point.XY( 180,  50 ),
			Point.XY( 380, 175 ),
		);
	await Control.drag( rectDocPath1.pos, rectDocPath1.pse );

	{
		const glintH0A =
			await Control.getGlint(
				Trace.root( Plan.root )
				.add( 'view',  0 )
				.add( 'space', uidPlotleHome )
				.add( 'bench' )
				.add( 'docPath' )
				.add( 'handle', 0 )
				.add( 'add' )
			);
		const figH0A = glintH0A.figure;
		const posH0A = figH0A.pos;
		const pcH0A =
			Point.XY(
				posH0A.x + figH0A.width / 2,
				posH0A.y + figH0A.height / 2,
			);

		await Control.drag( pcH0A, rectNote1.pc );
	}

	{
		const glintH1A =
			await Control.getGlint(
				Trace.root( Plan.root )
				.add( 'view', 0 )
				.add( 'space', uidPlotleHome )
				.add( 'bench' )
				.add( 'docPath' )
				.add( 'handle', 1 )
				.add( 'add' )
			);
		const figH1A = glintH1A.figure;
		const posH1A = figH1A.pos;
		const pcH1A =
			Point.XY(
				posH1A.x + figH1A.width / 2,
				posH1A.y + figH1A.height / 2,
			);

		await Control.drag( pcH1A, rectNote2.pc );
	}

	// -- subspace --
	await Control.click(
		Trace.root( Plan.root )
		.add( 'panels', 'create' )
		.add( 'widgets', 'createSubspace' )
	);

	const rectSubspace1 =
		Rect.PosPse(
			Point.XY( 750, 150 ),
			Point.XY( 900, 250 ),
		);

	await Control.drag( rectSubspace1.pos, rectSubspace1.pse );

	await timers.setTimeout( 250 );
	const fabricSpace = await Control.getSpaceFabric( 'plotle:home' );

	const items = fabricSpace.items;
	if( fabricSpace.items.length !== 10 )
	{
		throw new Error ();
	}

	// transforms the random keys to known keys

	let keyDocPath1;
	let keyLabel1;
	let keyLabelR;
	let keyNote1;
	let keyNote2;
	let keySubspace1;

	for( let key of items.keys )
	{
		const item = items.get( key );

		switch( item.ti2ctype )
		{
			case FabricDocPath:
				keyDocPath1 = key;
				break;

			case FabricLabel:
				if( !keyLabel1 )
				{
					keyLabel1 = key;
				}
				else if( items.get( keyLabel1 ).zone.pos.y > item.zone.pos.y )
				{
					keyLabelR = keyLabel1;
					keyLabel1 = key;
				}
				else
				{
					keyLabelR = key;
				}

				break;

			case FabricNote:
				if( !keyNote1 )
				{
					keyNote1 = key;
				}
				else if( items.get( keyNote1 ).zone.pos.y > item.zone.pos.y )
				{
					keyNote2 = keyNote1;
					keyNote1 = key;
				}
				else
				{
					keyNote2 = key;
				}
				break;

			case FabricStroke:
				break;

			case FabricSubspace:
				keySubspace1 = key;
				break;

			default:
				throw new Error( );
		}
	}

	return( {
		keyDocPath1:  keyDocPath1,
		keyLabel1:    keyLabel1,
		keyLabelR:    keyLabelR,
		keyNote1:     keyNote1,
		keyNote2:     keyNote2,
		keySubspace1: keySubspace1,
		fabricSpace:  fabricSpace,
	} );
	*/
	throw new Error( );
};
