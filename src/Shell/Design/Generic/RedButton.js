/*
| A red button.
*/
def.abstract = true;

import { Self as Color           } from '{gleam:Color}';
import { Self as Facet           } from '{Shell/Facet/Self}';
import { Self as FacetBorder     } from '{Shell/Facet/Border}';
import { Self as FacetList       } from '{Shell/Facet/List}';
import { Self as GroupBoolean    } from '{group@boolean}';
import { Self as ListFacetBorder } from '{list@Shell/Facet/Border}';

def.staticLazy.facets = ( ) =>
	FacetList.Elements(
		// default state.
		Facet.create(
			'border', FacetBorder.ColorDistanceWidth( Color.RGBA( 196, 94, 44, 0.7 ), -1, 2 ),
			'fill', Color.RGBA( 255, 186, 157, 0.7 ),
		),
		// hover
		Facet.create(
			'fill', Color.RGBA( 251, 78, 78, 0.7 ),
			'border', FacetBorder.ColorDistanceWidth( Color.RGBA( 196, 94, 44, 0.7 ), -1, 2 ),
			'specs', GroupBoolean.Table( { hover: true } ),
		),
		// focus
		Facet.create(
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -2, 1.5 ),
					FacetBorder.ColorDistanceWidth( Color.RGBA( 196, 94, 44, 0.7 ), -1, 2 )
			),
			'fill', Color.RGBA( 255, 186, 157, 0.7 ),
			'specs', GroupBoolean.Table( { focus: true } ),
		),
		// focus and hover
		Facet.create(
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -2, 1.5 ),
					FacetBorder.ColorDistanceWidth( Color.RGBA( 196, 94, 44, 0.7 ), -1, 2 )
				),
			'fill', Color.RGBA( 251, 78, 78, 0.7 ),
			'specs', GroupBoolean.Table( { focus: true, hover: true } ),
		)
	);
