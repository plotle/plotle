/*
| Default input field.
*/
def.abstract = true;

import { Self as Color           } from '{gleam:Color}';
import { Self as Facet           } from '{Shell/Facet/Self}';
import { Self as FacetBorder     } from '{Shell/Facet/Border}';
import { Self as FacetList       } from '{Shell/Facet/List}';
import { Self as GroupBoolean    } from '{group@boolean}';
import { Self as ListFacetBorder } from '{list@Shell/Facet/Border}';

def.staticLazy.facets =
	( ) =>
	FacetList.Elements(
		// default state.
		Facet.create(
			'fill', Color.white,
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistanceWidth( Color.RGB( 255, 188, 87 ), -1, 1.5 ),
					FacetBorder.simpleBlack
				)
		),
		// focus
		Facet.create(
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 2 ),
					FacetBorder.simpleBlack
				),
			'fill', Color.white,
			'specs', GroupBoolean.Table( { focus: true } ),
		),
		// hover
		Facet.create(
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistanceWidth( Color.RGB( 255, 188, 87 ), -1, 1.5 ),
					FacetBorder.simpleBlack
				),
			'fill', Color.lightGray,
			'specs', GroupBoolean.Table( { hover: true } ),
		),
		// focus and hover
		Facet.create(
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 2 ),
					FacetBorder.simpleBlack
				),
			'fill', Color.lightGray,
			'specs', GroupBoolean.Table( { focus: true, hover: true } ),
		)
	);
