/*
| Selection facets.
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';

/*
| Text selection.
*/
def.staticLazyFunc.facetSelectText =
	( light ) =>
	light
	? Facet.create(
		'fill', Color.RGBA( 243, 203, 255, 0.9 ),
		'border', FacetBorder.simpleBlack
	)
	: Facet.create(
		'fill', Color.RGBA( 100, 100, 255, 0.5 ),
		'border', FacetBorder.simpleBlack
	);
