/*
| A select field design.
*/
def.attributes =
{
	// the arrow
	facetArrow: { type: 'Shell/Facet/Self' },

	// the options popup
	facetOptions: { type: 'Shell/Facet/Self' },

	// style facets
	facets: { type: 'Shell/Facet/List' },

	// designed font
	font: { type: 'gleam:Font/Color' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};
