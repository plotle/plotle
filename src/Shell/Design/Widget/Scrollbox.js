/*
| A scrollbox design.
*/
def.attributes =
{
	// widgets
	widgets: { type: 'twig@<Shell/Design/Widget/Types' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};
