/*
| An input field design.
*/
def.attributes =
{
	// style facets
	facets: { type: 'Shell/Facet/List' },

	// designed font
	font: { type: 'gleam:Font/Color' },

	// maximum input length
	maxlen: { type: [ 'undefined', 'integer' ] },

	// true for password input
	password: { type: 'boolean', defaultValue: 'false' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};
