/*
| The zoom in icon.
|
|
|       **
|       **
|   **********
|   **********
|       **
|       **
|
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignIcon  } from '{Shell/Design/Icon/Self}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const c = Point.zero;

	return DesignIcon.create(
		'facet',
		Facet.create(
			'fill', Color.black,
			'border', FacetBorder.Color( Color.darkRed )
		),

		'shape',
		Path.Plan(
			'pc', c,
			'start', c.add( -1, -7 ),
			'line', c.add(  1, -7 ),
			'line', c.add(  1, -1 ),
			'line', c.add(  7, -1 ),
			'line', c.add(  7,  1 ),
			'line', c.add(  1,  1 ),
			'line', c.add(  1,  7 ),
			'line', c.add( -1,  7 ),
			'line', c.add( -1,  1 ),
			'line', c.add( -7,  1 ),
			'line', c.add( -7, -1 ),
			'line', c.add( -1, -1 ),
			'line', 'close'
		)
	);
};
