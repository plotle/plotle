/*
| An icon.
*/
def.attributes =
{
	facet: { type: 'Shell/Facet/Self'    },
	shape: { type: '<gleam:Figure/Types' },
};
