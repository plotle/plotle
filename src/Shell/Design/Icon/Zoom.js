/*
| The zoom icon.
|
|       .----.
|     .'      '.
|    .          .
|    '          '
|    '          '
|    '         '
|     '.     .'
|     /  /'''
|    /  /
|   /  /
|   '.'
|
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignIcon  } from '{Shell/Design/Icon/Self}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';
import { Self as FigureList  } from '{gleam:Figure/List}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const c = Point.XY( 0, -3 );

	return DesignIcon.create(
		'facet',
		Facet.create(
			'fill', Color.black,
			'border', FacetBorder.Color( Color.darkRed )
		),
		'shape',
		FigureList.Elements(
			Path.Plan(
				'pc', c,
				'start', c.add( -11.0, 11.0 ),
				'line',  c.add(  -6.5,  5.0 ),
				'qbend', c.add(  -7.0,  0.0 ),
				'qbend', c.add(   0.0, -7.0 ),
				'qbend', c.add(   7.0,  0.0 ),
				'qbend', c.add(   0.0,  7.0 ),
				'qbend', c.add(  -5.0,  8.0 ),
				'line',  c.add(  -9.0, 12.0 ),
				'qbend', 'close'
			),
			Path.Plan(
				'pc', c,
				'start',        c.add( -6.5,  0.0 ),
				'qbend', 'ccw', c.add(  0.0,  6.5 ),
				'qbend', 'ccw', c.add(  6.5,  0.0 ),
				'qbend', 'ccw', c.add(  0.0, -6.5 ),
				'qbend', 'ccw', 'close'
			)
		)
	);
};
