/*
| The XSymbol, used as "remove" icon, but also close.
|
|  A**B   D**E
|  P***   ***F
|    ***C***
|     O***G
|    ***K***
|  N***   ***H
|  M**L   J**I
|
*/
def.abstract = true;

import { Self as Color       } from '{gleam:Color}';
import { Self as DesignIcon  } from '{Shell/Design/Icon/Self}';
import { Self as Facet       } from '{Shell/Facet/Self}';
import { Self as FacetBorder } from '{Shell/Facet/Border}';
import { Self as Path        } from '{gleam:Path}';
import { Self as Point       } from '{gleam:Point}';

/*
| Design.
*/
def.staticLazyFunc.design =
	( light ) =>
{
	const pc = Point.zero;
	const pnw = pc.add( -5, -5 );
	const pse = pc.add(  5,  5 );
	const pne = pc.add( pse.x, pnw.y );
	const psw = pc.add( pnw.x, pse.y );

	// arm with and height
	const aw = 1.75;
	const ah = 1.75;

	// center point width/height
	const cw = 2;
	const ch = 2;

	return DesignIcon.create(
		'facet',
			light
			?  Facet.create(
				'fill', Color.RGB( 196, 0, 0 ),
				'border', FacetBorder.Color( Color.RGB( 168, 0, 0 ) ),
			)
			: Facet.create(
				'fill', Color.RGB( 168, 0, 0 ),
				'border', FacetBorder.Color( Color.RGB( 128, 0, 0 ) ),
			),

		'shape',
		Path.Plan(
			'pc', pc,
			'start', pnw,                // A
			'line', pnw.add(  aw,   0 ), // B
			'line', pc.add(    0, -ch ), // C
			'line', pne.add( -aw,   0 ), // D
			'line', pne,                 // E
			'line', pne.add(   0,  ah ), // F
			'line', pc.add(   cw,   0 ), // G
			'line', pse.add(   0, -ah ), // H
			'line', pse,                 // I
			'line', pse.add( -aw,   0 ), // J
			'line', pc.add(    0,  ch ), // K
			'line', psw.add(  aw,   0 ), // L
			'line', psw,                 // M
			'line', psw.add(   0, -ah ), // N
			'line', pc.add(  -cw,   0 ), // O
			'line', pnw.add(   0,  ah ), // P
			'line', 'close'              // A
		),
	);
};
