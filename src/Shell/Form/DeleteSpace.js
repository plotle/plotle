/*
| User is asked if they want to delete a space.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// the reference of current space
	refSpace: { type: [ 'undefined', 'Shared/Ref/Space' ] },

	// form seam
	seam: { type: 'Shell/Seam/Form/DeleteSpace' },

	// the uid of current space
	uidSpace: { type: [ 'undefined', 'string' ] },
};

import { Self as Color               } from '{gleam:Color}';
import { Self as DesignButton        } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont          } from '{Shell/Design/Font}';
import { Self as DesignInput         } from '{Shell/Design/Widget/Input}';
import { Self as DesignLabel         } from '{Shell/Design/Widget/Label}';
import { Self as GenericButton       } from '{Shell/Design/Generic/Button}';
import { Self as GenericInput        } from '{Shell/Design/Generic/Input}';
import { Self as GenericRedButton    } from '{Shell/Design/Generic/RedButton}';
import { Self as Point               } from '{gleam:Point}';
import { Self as Rect                } from '{gleam:Rect}';
import { Self as SeamFormDeleteSpace } from '{Shell/Seam/Form/DeleteSpace}';
import { Self as Shell               } from '{Shell/Self}';
import { Self as TwigDesignWidget    } from '{twig@<Shell/Design/Widget/Types}';


/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const confirmation = this.widgets.get( 'confirmInput' ).value;

	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'cancelButton':
		{
			Shell.setModeNormal( );
			break;
		}

		case 'buttonDelete':
		{
			if( confirmation !== this.refSpace.fullname )
			{
				Shell.alter(
					SeamFormDeleteSpace.traceSeam.add( 'errorMessage' ),
					'confirmation does not match'
				);

				return;
			}

			Shell.deleteSpace( this.uidSpace, confirmation );
			break;
		}

		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 22, light ),
			'pos',     Point.XY( 0, -95 ),
			'string', '', // do you want to delete [user:tag]?
		),
		'message1',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 16, light ),
			'pos',     Point.XY( 0, -50 ),
			'string', 'This cannot be undone!',
		),
		'errorMessage',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.SizeColor( 14, Color.red ),
			'pos',     Point.XY( 0, -20 ),
			'string', '',
		),
		'typeMessage',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 16, light ),
			'pos',     Point.XY( 0, 10 ),
			'string', '', // type user:tag to confirm
		),
		'confirmInput',
		DesignInput.create(
			'facets', GenericInput.facets,
			'font',   DesignFont.Size( 14 ),
			'zone',   Rect.PosWidthHeight( Point.XY( -105, 25 ), 210, 25 ),
		),
		'buttonDelete',
		DesignButton.create(
			'facets',  GenericRedButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'Delete',
			'zone',    Rect.PosWidthHeight( Point.XY( -40, 70 ), 80, 40 ),
		),
		'cancelButton',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'Cancel',
			'zone',    Rect.PosWidthHeight( Point.XY( 125, 70 ), 75, 40 ),
		)
	);

/*
| Sets the seam trace of an input widget.
|
| TODO replace by input widget insert/remove functions.
*/
def.proto._widgetTraceSeam =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'confirmInput':
			return SeamFormDeleteSpace.traceSeam.add( 'confirmation' );

		default: throw new Error( );
	}
};

/*
| Sets the value of an input widget.
*/
def.proto._widgetInputValue =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'confirmInput': return this.seam.confirmation;
		default: throw new Error( );
	}
};

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	switch( key )
	{
		case 'errorMessage':
			return this.seam.errorMessage;

		case 'headline':
			return 'Do you want do delete ' + this.refSpace.fullname + '?';

		case 'typeMessage':
			return 'Type ' + this.refSpace.fullname + ' to confirm:';

		default:
			// keep it as in layout
			return undefined;
	}
};

