/*
| The moveTo or 'go' form.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// distance to left side of screen.
	distanceLeft: { type: 'number' },

	// form seam
	seam: { type: 'Shell/Seam/Form/MoveTo' },

	// user info
	userInfo: { type: [ 'undefined', 'Shared/User/Info' ] },
};

import { Self as Color            } from '{gleam:Color}';
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as DesignScrollbar  } from '{Shell/Design/Scrollbar}';
import { Self as DesignScrollbox  } from '{Shell/Design/Widget/Scrollbox}';
import { Self as Facet            } from '{Shell/Facet/Self}';
import { Self as FacetBorder      } from '{Shell/Facet/Border}';
import { Self as FacetList        } from '{Shell/Facet/List}';
import { Self as GroupBoolean     } from '{group@boolean}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as RefSpace         } from '{Shared/Ref/Space}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';
import { Self as TwigWidget       } from '{twig@<Shell/Widget/Types}';
import { Self as WidgetButton     } from '{Shell/Widget/Button}';
import { Self as WidgetBase       } from '{Shell/Widget/Base}';
import { Self as WidgetScrollbox  } from '{Shell/Widget/Scrollbox}';

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const buttonKey = traceV.backward( 'widgets' ).last.key;
	const parts = buttonKey.split( ':' );

	Shell.openSpace(
		0,
		RefSpace.create( 'username', parts[ 0 ], 'tag',  parts[ 1 ] ),
		'addHistory',
	);
};

/*
| Builds the widgets.
*/
def.lazy.widgets =
	function( )
{
	const light             = this.light;
	const designSpaceButton = Self._designSpaceButton( light );
	const designWidgets     = Self._designWidgets( light );
	const resolution        = this.resolution;
	const sizeDisplay       = this.sizeDisplay;
	const systemFocus       = this.systemFocus;
	const transform         = this.transform;

	let twig = { };
	let headline;

	{
		const traceV = this.traceV.add( 'widgets', 'headline' );
		headline =
			WidgetBase.FromDesign(
				designWidgets.get( 'headline' ),
				light, traceV, resolution, systemFocus, transform,
			);
		const uis = transform.scale;
		const size = headline.fontColor.size;
		// content height
		const ch = size * 2 + 160 + this._rows * 160;
		// if below minimum content is no longer vertical centered and scrolling is needed.
		const x = this.distanceLeft / uis + ( this._cols - 0.5 ) * 80 + 30;
		const y = Math.max( sizeDisplay.height / ( 2 * uis ) - ch / 2, 10 + size );
		headline = headline.create( 'pos', Point.XY( x, y ) );
		twig.headline = headline;
	}

	{
		const traceV = this.traceV.add( 'widgets', 'scrollbox' );
		let sb =
			WidgetBase.FromDesign(
				designWidgets.get( 'scrollbox' ),
				light, traceV, resolution, systemFocus, transform,
			);
		const sbRanks = [ 'plotle:home', 'plotle:sandbox' ];
		const sbTwig =
		{
			'plotle:home'   : sb.widgets.get( 'plotle:home' ),
			'plotle:sandbox': sb.widgets.get( 'plotle:sandbox' )
		};

		// cols in current row
		let cLen = this._cols;

		// buttons are in the scrollbox
		sbTwig[ 'plotle:home' ] =
			this._buildButton(
				RefSpace.PlotleHome,
				Point.XY( 160 * ( this._cols - 2 ) / 2, 1 )
			);

		sbTwig[ 'plotle:sandbox' ] =
			this._buildButton(
				RefSpace.PlotleSandbox,
				Point.XY( 160 * this._cols / 2, 1 )
			);

		const listSpacesOwned = this.userInfo?.listSpacesOwned;
		if( listSpacesOwned )
		{
			let c = 0; // current column
			let cOff = 0; // column offset (for last row)
			let r = 1; // current row
			const lenLSO = listSpacesOwned.length;
			for( let refSpace of listSpacesOwned )
			{
				if( r >= this._rows )
				{
					cLen = lenLSO % this._cols;
					if( cLen === 0 ) cLen = this._cols;
					cOff = ( this._cols - cLen ) / 2;
				}

				const fullname = refSpace.fullname;
				sbRanks.push( fullname );
				sbTwig[ fullname ] =
					this._buildButton(
						refSpace,
						Point.XY( 2 + 160 * ( cOff + c ), 1 + 160 * r )
					);

				if( ++c >= this._cols ) { c = 0; r++; }
			}
		}

		const cy = headline.pos.y + 50;
		const uis = transform.scale;
		const zone =
			Rect.PosWidthHeight(
				Point.XY( this.distanceLeft / uis, cy ),
				( this._availableWidth ) / uis,
				sizeDisplay.height / uis - cy - 1
			);

		// TODO make a global seam rectify data walkthrough to fix scrollPositions
		const innerHeight = 1 + 160 * this._rows + designSpaceButton.zone.height;

		twig.scrollbox =
			sb.create(
				'scrollPos',
					WidgetScrollbox.prepareScrollPos( this.seam.scrollPos, innerHeight, zone ),
				'widgets', TwigWidget.create( 'twig:init', sbTwig, sbRanks ),
				'zone', zone
			);
	}

	return TwigWidget.create( 'twig:init', twig, designWidgets.keys );
};

/*
| Width available to the form.
*/
def.lazy._availableWidth =
	function( )
{
	return(
		this.sizeDisplay.width
		- this.distanceLeft
		- DesignScrollbar.strength
	);
};

/*
| Builds a button.
*/
def.proto._buildButton =
	function( refSpace, pos )
{
	const light = this.light;
	const name = refSpace.fullname;
	const designSpaceButton = Self._designSpaceButton( this.light );
	const traceV = this._sbTraceV.add( 'widgets', name );

	let wHover = this.hover;
	if( wHover && !wHover.hasTrace( traceV ) )
	{
		wHover = undefined;
	}

	let wMark = this.mark;
	if( wMark && !wMark.encompasses( traceV ) )
	{
		wMark = undefined;
	}

	let button =
		WidgetButton.FromDesign(
			designSpaceButton,
			light, traceV,
			this.resolution,
			this.systemFocus,
			this.transform,
		);

	button =
		button.create(
			'hover', wHover,
			'mark', wMark,
			'string', refSpace.username + '\n' + refSpace.tag,
			'zone',
				designSpaceButton.zone
				.create( 'pos', pos )
		);

	return button;
};

/*
| Number of columns used.
*/
def.lazy._cols =
	function( )
{
	const uis = this.transform.scale;
	return(
		Math.floor(
			( this._availableWidth - 2 * uis + 30 * uis ) / ( 160 * uis )
		)
	);
};

/*
| Design of the space buttons.
*/
def.staticLazyFunc._designSpaceButton =
	( light ) =>
	DesignButton.create(
		'facets',
			FacetList.Elements(
				// default state.
				Facet.create(
					'fill', Color.RGBA( 255, 237, 210, 0.5 ),
					'border', FacetBorder.Color( Color.RGB( 255, 141, 66 ) )
				),
				// hover
				Facet.create(
					'border', FacetBorder.ColorWidth( Color.RGB( 255, 141, 66 ), 1.5 ),
					'fill', Color.RGBA( 255, 188, 88, 0.7 ),
					'specs', GroupBoolean.Table( { hover: true } ),
				),
				// focus
				Facet.create(
					'border', FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 1.5 ),
					'fill', Color.RGBA( 255, 237, 210, 0.5 ),
					'specs', GroupBoolean.Table( { focus: true } ),
				),
				// focus and hover
				Facet.create(
					'border', FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 1.5 ),
					'fill', Color.RGBA( 255, 188, 88, 0.7 ),
					'specs', GroupBoolean.Table( { focus: true, hover: true } ),
				)
			),
		'font', DesignFont.Size( 14 ),
		'shape', 'RectRound',
		'stringNewline', 25,
		'zone',
			Rect.PosWidthHeight(
				Point.zero, // dummy
				130, 130
			),
	);

/*
| Design.
*/
def.staticLazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.NameSizeLight( 'form', 22, light ),
			'pos', Point.zero,
			'string', 'go to another space',
		),
		'scrollbox',
		DesignScrollbox.create(
			// this are all dummy values overridden, by moveTo initializer
			'widgets', TwigDesignWidget.Empty,
			'zone', Rect.PosWidthHeight( Point.zero, 0, 0 ),
		)
	);

/*
| Number of rows used.
*/
def.lazy._rows =
	function( )
{
	const listSpacesOwned = this.userInfo?.listSpacesOwned;

	return(
		listSpacesOwned
		? Math.ceil( listSpacesOwned.length / this._cols )
		: 0
	);
};

/*
| traceV of the scrollbox.
*/
def.lazy._sbTraceV =
	function( )
{
	return this.traceV.add( 'widgets', 'scrollbox' );
};

