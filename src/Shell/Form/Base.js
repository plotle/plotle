/*
| Abstract parent of forms.
*/
def.attributes =
{
	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// rectangle of view window
	rectView: { type: 'gleam:Rect' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// current display size
	sizeDisplay: { type: 'gleam:Size' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// transform
	transform: { type: '<gleam:Transform/Types' },

	// traceV of the form
	traceV: { type: 'ti2c:Trace' },
};

import { Self as ActionScrolly     } from '{Shell/Action/Scrolly}';
import { Self as Color             } from '{gleam:Color}';
import { Self as Facet             } from '{Shell/Facet/Self}';
import { Self as GradientAskew     } from '{gleam:Gradient/Askew}';
import { Self as GradientColorStop } from '{gleam:Gradient/ColorStop}';
import { Self as Hover             } from '{Shell/Result/Hover}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as MarkWidget        } from '{Shared/Mark/Widget}';
import { Self as SeamFormRoot      } from '{Shell/Seam/Form/Root}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as SysMode           } from '{Shell/Result/SysMode}';
import { Self as TwigWidget        } from '{twig@<Shell/Widget/Types}';
import { Self as WidgetBase        } from '{Shell/Widget/Base}';
import { Self as WidgetButton      } from '{Shell/Widget/Button}';
import { Self as WidgetCheckbox    } from '{Shell/Widget/Checkbox}';
import { Self as WidgetInput       } from '{Shell/Widget/Input}';
import { Self as WidgetLabel       } from '{Shell/Widget/Label}';
import { Self as WidgetSelect      } from '{Shell/Widget/Select}';

/*
| Cycles the focus.
*/
def.proto.cycleFocus =
	function( dir )
{
	const widgets = this.widgets;
	const length = widgets.length;
	const traceV = this.mark.traceVWidget;

	if( !traceV ) return;

	let rank = widgets.rankOf( traceV.last.key );
	const rs = rank;

	for(;;)
	{
		rank = ( rank + dir + length ) % length;
		if( rank === rs ) break;
		const ve = widgets.atRank( rank );

		if( ve.focusable && ve.visible !== false )
		{
			Shell.alter(
				'mark',
					ve.caretable
					? MarkCaret.TraceV( ve.traceV.add( 'value' ).add( 'offset', 0 ) )
					: MarkWidget.create( 'traceV', ve.traceV )
			);
			break;
		}
	}
};

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	const widgets = this.widgets;
	for( let w of widgets )
	{
		const bubble = w.click( p, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	return false;
};

/*
| Defaults clipboard to be empty.
*/
def.proto.clipboard = '';

/*
| Common background design of all forms.
*/
def.staticLazyFunc.designBackground =
	( light ) =>
	light
	?  Facet.create(
		'fill',
			GradientAskew.Elements(
				GradientColorStop.OffsetColor( 0, Color.RGB( 255, 255, 248 ) ),
				GradientColorStop.OffsetColor( 1, Color.RGB( 255, 255, 210 ) )
			)
	)
	: Facet.create(
		'fill',
			GradientAskew.Elements(
				GradientColorStop.OffsetColor( 0, Color.RGB( 60, 57, 51 ) ),
				GradientColorStop.OffsetColor( 0, Color.RGB( 60, 55, 44 ) ),
			)
	);

/*
| Drag moves.
|
| ~p:      point, viewbased point of stop
| ~shift:  true if shift key was pressed
| ~ctrl:   true if ctrl key was pressed
| ~action: current action
*/
def.proto.dragMove =
	function( p, shift, ctrl, action )
{
	switch( action.ti2ctype )
	{
		case ActionScrolly:
		{
			const traceV = action.traceV;
			const formName = traceV.forward( 'forms' ).last.key;
			const widgetName = traceV.forward( 'widgets' ).last.key;

/**/		if( CHECK )
/**/		{
/**/			if( formName !== this.traceV.forward( 'forms' ).last.key ) throw new Error( );
/**/		}

			// FUTURE currently cannot handle nested widgets
/**/		if( CHECK && traceV.length !== 3 ) throw new Error( );

			const widget = this.widgets.get( widgetName );

			const ratio = this.resolution.ratio;
			const dy = ( p.y - action.pointStart.y ) / ratio;
			const sbary = widget.scrollbarY;

			let sy = action.startPos + sbary.scale( dy );
			if( sy < 0 ) sy = 0;

			Shell.alter(
				SeamFormRoot.traceSeam.add( formName ).add( 'scrollPos' ),
				widget.scrollPos.create( 'y', sy )
			);

			break;
		}
	}
};

/*
| Starts an operation with the pointing device held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	const widgets = this.widgets;
	for( let w of widgets )
	{
		const bubble = w.dragStart( p, shift, ctrl );
		if( bubble ) return bubble;
	}

	return false;
};

/*
| Stops an operation with the poiting device button held down.
*/
def.proto.dragStop =
	function( p, shift, ctrl )
{
	Shell.alter( 'action', undefined );
};

/*
| Returns the focused widget.
*/
def.lazy.focusedWidget =
	function( )
{
	const mark = this.mark;
	if( !mark )
	{
		return undefined;
	}

	const tvw = mark.traceVWidget;
	if( !tvw )
	{
		return undefined;
	}

	return this.widgets.get( tvw.last.key );
};

/*
| Returns the glint.
*/
def.lazy.glint =
	function( )
{
	const list =
	[
		Self.designBackground( this.light ).glint( this.rectView.zeroPos )
	];

	const widgets = this.widgets;

	for( let w of widgets.reverse( ) )
	{
		const wg = w.glint;
		if( wg ) list.push( wg );
	}

	return ListGlint.Array( list );
};

/*
| User is inputing characters.
*/
def.proto.input =
	function( string )
{
	const widget = this.focusedWidget;
	if( widget )
	{
		widget.input( string );
	}
};

/*
| Mouse wheel is being turned.
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	const widgets = this.widgets;
	for( let w of widgets )
	{
		const bubble = w.mousewheel( p, dir, shift, ctrl );
		if( bubble ) return bubble;
	}
	return false;
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.proto.paste =
	function( type, data )
{
	this.input( data );
};

/*
| If point is on the form returns its hovering state.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const widgets = this.widgets;
	for( let w of widgets )
	{
		const bubble = w.pointingHover( p, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}
	return Hover.CursorDefault;
};

/*
| Scrolls current mark into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	// nothing
};

/*
| Sets a widget.
*/
def.proto.setWidget =
	function( name, widget )
{
	return this.create( 'widgets', this.widgets.set( name, widget ) );
};

/*
| User is pressing a special key.
*/
def.static.specialKey =
def.proto.specialKey =
	function( key, shift, ctrl )
{
	const widget = this.focusedWidget;
	if( !widget ) return;

	if( key === 'tab' )
	{
		this.cycleFocus( shift ? -1 : 1 );
		return;
	}

	widget.specialKey( key, shift, ctrl );
};

/*
| Returns the sysMode.
*/
def.lazy.sysMode =
	function( )
{
	const focus = this.focusedWidget;
	return(
		focus
		? focus.sysMode
		: SysMode.Blank
	);
};

/*
| Transforms a mark.
*/
def.proto.transformMark =
	function( mark, changeX )
{
	return mark;
};

/*
| Builds the widgets.
*/
def.lazy.widgets =
	function( )
{
	const light         = this.light;
	const hover         = this.hover;
	const designWidgets = this._designWidgets( light );
	const mark          = this.mark;
	const resolution    = this.resolution;
	const systemFocus   = this.systemFocus;
	const transform     = this.transform;
	const traceV        = this.traceV;

	let twig = { };
	for( let key of designWidgets.keys )
	{
		const traceVWidget = traceV.add( 'widgets', key );

		let widget =
			WidgetBase.FromDesign(
				designWidgets.get( key ),
				light, traceVWidget, resolution, systemFocus, transform,
			);

		const visible = this._widgetVisible( key ) ?? pass;

		switch( widget.ti2ctype )
		{
			case WidgetButton:
			{
				let wHover = hover;
				if( wHover && !wHover.hasTrace( traceVWidget ) ) wHover = undefined;

				let wMark = mark;
				if( wMark && !wMark.encompasses( traceVWidget ) ) wMark = undefined;

				twig[ key ] =
					widget.create(
						'hover', wHover,
						'mark', wMark,
						'resolution', resolution,
						'systemFocus', systemFocus,
						'transform', transform,
						'visible', visible,
						'traceV', traceVWidget,
					);
				break;
			}

			case WidgetCheckbox:
			{
				let wHover = hover;
				if( wHover && !wHover.hasTrace( traceVWidget ) ) wHover = undefined;

				let wMark = mark;
				if( wMark && !wMark.encompasses( traceVWidget ) ) wMark = undefined;

				twig[ key ] =
					widget.create(
						'checked', this._widgetCheckboxChecked( key ),
						'hover', wHover,
						'mark', wMark,
						'resolution', resolution,
						'systemFocus', systemFocus,
						'transform', transform,
						'visible', visible,
						'traceV', traceVWidget,
					);
				break;
			}

			case WidgetInput:
			{
				let wHover = hover;
				if( wHover && !wHover.hasTrace( traceVWidget ) ) wHover = undefined;

				let wMark = mark;
				if( wMark && !wMark.encompasses( traceVWidget ) ) wMark = undefined;

				twig[ key ] =
					widget.create(
						'hover', wHover,
						'mark', wMark,
						'systemFocus', systemFocus,
						'traceSeam', this._widgetTraceSeam( key ),
						'transform', transform,
						'value', this._widgetInputValue( key ),
						'visible', visible,
						'traceV', traceVWidget,
					);
				break;
			}

			case WidgetLabel:
			{
				const string = this._widgetLabelString( key ) ?? pass;

				twig[ key ] =
					widget.create(
						'resolution', resolution,
						'string', string,
						'transform', transform,
						'visible',     visible,
						'traceV', traceVWidget,
					);
				break;
			}

			case WidgetSelect:
			{
				let wHover = hover;
				if( wHover && !wHover.hasTrace( traceVWidget ) ) wHover = undefined;

				let wMark = mark;
				if( wMark && !wMark.encompasses( traceVWidget ) ) wMark = undefined;

				const seamWidget = this.seam.widgets.get( key );

				twig[ key ] =
					widget.create(
						'hover',       wHover,
						'mark',        wMark,
						'open',        seamWidget.open,
						'options',     this._widgetSelectOptions( key ),
						'systemFocus', systemFocus,
						'traceSeam',   this._widgetTraceSeam( key ),
						'transform',   transform,
						'value',       this._widgetSelectValue( key ),
						'visible',     visible,
						'traceV',      traceVWidget,
					);
				break;
			}

			default: throw new Error( );
		}
	}

	return TwigWidget.create( 'twig:init', twig, designWidgets.keys );
};

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( )
{
	// keep it as in design
};

/*
| Sets the visibility of a widget.
*/
def.proto._widgetVisible =
	function( key )
{
	// keep it as in design
};

