/*
| The login form.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// login form seam
	seam: { type: 'Shell/Seam/Form/Login' },
};

import { Self as Base             } from '{Shell/Form/Base}';
import { Self as Color            } from '{gleam:Color}';
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignInput      } from '{Shell/Design/Widget/Input}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as GenericButton    } from '{Shell/Design/Generic/Button}';
import { Self as GenericInput     } from '{Shell/Design/Generic/Input}';
import { Self as MarkCaret        } from '{Shared/Mark/Caret}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as RefSpace         } from '{Shared/Ref/Space}';
import { Self as ReplyAuth        } from '{Shared/Reply/Auth}';
import { Self as SeamFormLogin    } from '{Shell/Seam/Form/Login}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';
import { Self as UserCreds        } from '{Shared/User/Creds}';
import { Self as UserPasshash     } from '{Shared/User/PassHash}';

/*
| Clears all fields.
*/
def.proto.clear =
	function( )
{
	// FUTURE set the seam directly
	Shell.alter(
		SeamFormLogin.traceSeam.add( 'password' ), '',
		SeamFormLogin.traceSeam.add( 'username' ), '',
		SeamFormLogin.traceSeam.add( 'errorMessage' ), '',
		'mark', undefined,
	);
};

/*
| Logins the user
*/
def.proto.login =
	function( )
{
	const widgets = this.widgets;
	const passInput = widgets.get( 'inputPassword' );
	const inputUsername = widgets.get( 'inputUsername' );
	const username = inputUsername.value;
	const pass = passInput.value;

	if( username.length < 4 )
	{
		Shell.alter(
			SeamFormLogin.traceSeam.add( 'errorMessage' ),
				'Username too short, min. 4 characters',
			'mark',
				MarkCaret.TraceV( inputUsername.offsetTraceV( username.length ) )
		);
		return;
	}

	if( username.substr( 0, 5 ) === 'visit' )
	{
		Shell.alter(
			SeamFormLogin.traceSeam.add( 'errorMessage' ),
				'Username must not start with "visit"',
			'mark',
				MarkCaret.TraceV( inputUsername.offsetTraceV( 0 ) )
		);
		return;
	}

	if( pass.length < 5 )
	{
		Shell.alter(
			SeamFormLogin.traceSeam.add( 'errorMessage' ),
				'Password too short, min. 5 characters',
			'mark',
				MarkCaret.TraceV( passInput.offsetTraceV( pass.length ) )
		);
		return;
	}

	Shell.auth(
		UserCreds.create(
			'name', username,
			'passhash', UserPasshash.calc( pass )
		)
	);
};

/*
| An auth (login) operation completed.
*/
def.proto.onAuth =
	function( reply )
{
	if( reply.ti2ctype !== ReplyAuth )
	{
		const message = reply.message;

		if( message.search( /Username/ ) >= 0 )
		{
			const inputUsername = this.widgets.get( 'inputUsername' );
			Shell.alter(
				SeamFormLogin.traceSeam.add( 'errorMessage' ),
					message,
				'mark',
					MarkCaret.TraceV( inputUsername.offsetTraceV( inputUsername.value.length ) )
			);
		}
		else
		{
			const passInput = this.widgets.get( 'inputPassword' );
			Shell.alter(
				SeamFormLogin.traceSeam.add( 'errorMessage' ),
					message,
				'mark',
					MarkCaret.TraceV( passInput.offsetTraceV( passInput.value.length ) )
			);
		}
		return;
	}

	reply.userCreds.saveToLocalStorage( );
	Shell.alter(
		'action',    undefined,
		'userCreds', reply.userCreds,
	);
	this.clear( );
	Shell.openSpace( 0, RefSpace.PlotleHome, 'addHistory' );
};

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'buttonLogin':
			this.login( );
			break;

		case 'buttonClose':
			Shell.setModeNormal( );
			break;

		default: throw new Error( );
	}
};

/*
| User is pressing a special key.
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	// a return in the password field is made
	// to be a login command right away
	if( key === 'enter' )
	{
		const tvc = this.mark.traceVCaret;
		if( tvc && tvc.backward( 'widgets' ).last.key === 'inputPassword' )
		{
			this.login( );
			return;
		}
	}

	Base.specialKey.call( this, key, shift, ctrl );
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 22, light ),
			'string', 'Log In',
			'pos', Point.XY( -225, -112 )
		),
		'labelUsername',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -175, -49 ),
			'string', 'username',
		),
		'inputUsername',
		DesignInput.create(
			'facets', GenericInput.facets,
			'font',   DesignFont.Size( 14 ),
			'maxlen', 100,
			'zone',   Rect.PosWidthHeight( Point.XY( -80, -67 ), 210, 25 )
		),
		'labelPassword',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -175, -9 ),
			'string', 'password',
		),
		'inputPassword',
		DesignInput.create(
			'facets',   GenericInput.facets,
			'font',     DesignFont.Size( 14 ),
			'maxlen',   100,
			'password', true,
			'zone',     Rect.PosWidthHeight( Point.XY( -80, -27 ), 210, 25 )
		),
		'errorMessage',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.SizeColor( 14, Color.red ),
			'pos', Point.XY( -20, -83 ),
			'string', '',
		),
		'buttonLogin',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'log in',
			'zone',    Rect.PosWidthHeight( Point.XY( 95, 28 ), 70, 70 ),
		),
		'buttonClose',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'close',
			'zone',    Rect.PosWidthHeight( Point.XY( 180, 38 ), 50, 50 ),
		)
	);

/*
| Sets the seam trace of an input widget.
|
| TODO replace by input widget insert/remove functions.
*/
def.proto._widgetTraceSeam =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'inputPassword': return SeamFormLogin.traceSeam.add( 'password' );
		case 'inputUsername': return SeamFormLogin.traceSeam.add( 'username' );
		default: throw new Error( );
	}
};

/*
| Sets the value of an input widget.
*/
def.proto._widgetInputValue =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'inputPassword':
			return this.seam.password;

		case 'inputUsername':
			return this.seam.username;

		default:
			throw new Error( );
	}
};

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	switch( key )
	{
		case 'errorMessage': return this.seam.errorMessage;

		// otherwise keep it as in layout
		default: return undefined;
	}
};

