/*
| The space form.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// users access to current space
	access: { type: [ 'undefined', 'string' ] },

	// space is public readable
	publicReadable: { type: [ 'undefined', 'boolean' ] },

	// the reference of current space
	refSpace: { type: [ 'undefined', 'Shared/Ref/Space' ] },

	// form seam
	seam: { type: 'Shell/Seam/Form/Space' },

	// the window the form is shown in
	window: { type: 'Shell/Window/Space' },
};

import { Self as ChangeTreeAssign    } from '{ot:Change/Tree/Assign}';
import { Self as DesignButton        } from '{Shell/Design/Widget/Button}';
import { Self as DesignCheckbox      } from '{Shell/Design/Widget/Checkbox}';
import { Self as DesignFont          } from '{Shell/Design/Font}';
import { Self as DesignLabel         } from '{Shell/Design/Widget/Label}';
import { Self as GenericButton       } from '{Shell/Design/Generic/Button}';
import { Self as GenericCheckbox     } from '{Shell/Design/Generic/Checkbox}';
import { Self as GenericRedButton    } from '{Shell/Design/Generic/RedButton}';
import { Self as IconCheck           } from '{Shell/Design/Icon/Check}';
import { Self as ModeForm            } from '{Shell/Mode/Form}';
import { Self as Point               } from '{gleam:Point}';
import { Self as Rect                } from '{gleam:Rect}';
import { Self as RefSpace            } from '{Shared/Ref/Space}';
import { Self as SeamFormDeleteSpace } from '{Shell/Seam/Form/DeleteSpace}';
import { Self as Shell               } from '{Shell/Self}';
import { Self as TraceRoot           } from '{Shared/Trace/Root}';
import { Self as TwigDesignWidget    } from '{twig@<Shell/Design/Widget/Types}';

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'buttonClose':
		{
			Shell.setModeNormal( );
			return;
		}

		case 'buttonDelete':
		{
			Shell.alter(
				// clean in one go
				SeamFormDeleteSpace.traceSeam.add( 'confirmation' ), '',
				SeamFormDeleteSpace.traceSeam.add( 'errorMessage' ), '',
				'mode', ModeForm.deleteSpace
			);
			return;
		}

		case 'buttonGo':
		{
			Shell.alter( 'mode', ModeForm.moveTo );
			return;
		}

		default:
			throw new Error( );
	}
};

/*
| A checkbox has been toggled.
*/
def.proto.toggleCheckbox =
	function( traceV )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'checkboxPublic':
		{
			const uid = this.window.uid;
			const prev = this.publicReadable;
			const change =
				ChangeTreeAssign.create(
					'trace', TraceRoot.space( uid ).add( 'publicReadable' ),
					'val', !prev,
					'prev', prev
				);
			Shell.change( uid, change );
			return;
		}

		default: throw new Error( );
	}
};

/*
| A select widget changed.
|
| ~traceV: of the select widget pushed
| ~value:  value to change it to.
*/
def.proto.widgetSelectChanged =
	function( traceV, value )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.NameSizeLight( 'form', 22, light ),
			'pos', Point.XY( 0, -40 ),
			'string', ''
		),
		'labelPublic',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -43, 2 ),
			'string', 'public readable',
		),
		'checkboxPublic',
		DesignCheckbox.create(
			'facets',      GenericCheckbox.facets,
			'iconChecked', IconCheck.design( light ),
			'zone',        Rect.PosWidthHeight( Point.XY( -70, -12 ), 16, 15 )
		),
		'buttonGo',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'go to space',
			'zone',    Rect.PosWidthHeight( Point.XY( -68, 40 ), 136, 40 ),
		),
		'buttonDelete',
		DesignButton.create(
			'facets',  GenericRedButton.facets,
			'font',    DesignFont.NameSizeLight( 'form', 14, light ),
			'shape',  'RectRound',
			'string', 'delete this space',
			'zone',    Rect.PosWidthHeight( Point.XY( -68, 100 ), 136, 40 ),
		),
		'buttonClose',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'close',
			'zone',    Rect.PosWidthHeight( Point.XY( 180, 40 ), 55, 40 ),
		)
	);

/*
| Sets the checked of a checkbox widget.
*/
def.proto._widgetCheckboxChecked =
	function( key )
{
	switch( key )
	{
		case 'checkboxPublic':   return this.publicReadable;
		default: throw new Error( );
	}
};

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	switch( key )
	{
		case 'headline': return this.refSpace.fullname;
		// otherwise keep it as in layout
		default: return undefined;
	}
};

/*
| Sets the visibility of a widget.
*/
def.proto._widgetVisible =
	function( key )
{
	switch( key )
	{
		case 'buttonDelete':
		{
			const refSpace = this.refSpace;

			return(
				refSpace !== RefSpace.PlotleSandbox
				&& refSpace !== RefSpace.PlotleHome
				&& refSpace.tag !== 'home'
				&& this.window.isOwner
			);
		}

		case 'checkboxGuides':
		case 'checkboxGrid':
		case 'checkboxSnapping':
		case 'labelGuides':
		case 'labelGrid':
		case 'labelSnapping':
			return this.access === 'rw';

		case 'checkboxPublic':
		case 'labelPublic':
		{
			return this.window.isOwner;
		}

		default:
			return undefined;
	}
};
