/*
| A form to notify the user their current space has been deleted.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// the window the form is shown in
	window: { type: '<Shell/Window/Types' },
};
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as GenericButton    } from '{Shell/Design/Generic/Button}';
import { Self as ModeForm         } from '{Shell/Mode/Form}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'buttonGo':
		{
			Shell.alter( 'mode', ModeForm.moveTo );
			return;
		}

		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align', 'center',
			'font',   DesignFont.NameSizeLight( 'form', 16, light ),
			'pos',    Point.XY( 0, -15 ),
			'string', '',
		),
		'buttonGo',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'go to another space',
			'zone',    Rect.PosWidthHeight( Point.XY( -78, 40 ), 156, 40 ),
		),
	);

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	if( key === 'headline' )
	{
		return 'the space ' + this.window.ref.fullname + ' has been deleted.';
	}
	else
	{
		// keep it as in layout
		return undefined;
	}
};
