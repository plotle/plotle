/*
| The change password form.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// changePwd form seam
	seam: { type: 'Shell/Seam/Form/ChangePwd' },

	// currently logged in user
	userCreds: { type: 'Shared/User/Creds' },
};


import { Self as Color             } from '{gleam:Color}';
import { Self as DesignButton      } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont        } from '{Shell/Design/Font}';
import { Self as DesignInput       } from '{Shell/Design/Widget/Input}';
import { Self as DesignLabel       } from '{Shell/Design/Widget/Label}';
import { Self as GenericButton     } from '{Shell/Design/Generic/Button}';
import { Self as GenericInput      } from '{Shell/Design/Generic/Input}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as ModeForm          } from '{Shell/Mode/Form}';
import { Self as Passhash          } from '{Shared/User/PassHash}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as SeamFormChangePwd } from '{Shell/Seam/Form/ChangePwd}';
import { Self as TwigDesignWidget  } from '{twig@<Shell/Design/Widget/Types}';

/*
| A change password request got a reply.
*/
def.proto.onChangePwd =
	function( request, reply )
{
	// errors would have resulted in a fail screen

	Shell.alter(
		SeamFormChangePwd.traceSeam, SeamFormChangePwd.Clean,
		'mode', ModeForm.done,
	);
};

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'buttonChangePwd':
			this._changePwd( );
			break;

		case 'buttonClose':
			Shell.setModeNormal( );
			break;

		default: throw new Error( );
	}
};

/*
| Handles the change password button press.
*/
def.proto._changePwd =
	function( )
{
	const inputPasswordOld = this.widgets.get( 'inputPasswordOld' );
	const passwordOld = inputPasswordOld.value;

	if( this.userCreds.passhash !== Passhash.calc( passwordOld ) )
	{
		Shell.alter(
			SeamFormChangePwd.traceSeam.add( 'errorMessage' ),
				'invalid old password',
			'mark',
				MarkCaret.TraceV( inputPasswordOld.offsetTraceV( passwordOld.length ) )
		);
		return;
	}

	const inputPasswordNew1 = this.widgets.get( 'inputPasswordNew1' );
	const inputPasswordNew2 = this.widgets.get( 'inputPasswordNew2' );
	const passwordNew1 = inputPasswordNew1.value;
	const passwordNew2 = inputPasswordNew2.value;

	if( passwordNew1.length < 5 )
	{
		Shell.alter(
			SeamFormChangePwd.traceSeam.add( 'errorMessage' ),
				'Password too short, min. 5 characters',
			'mark',
				MarkCaret.TraceV( inputPasswordNew1.offsetTraceV( passwordNew1.length ) ),
		);
		return;
	}

	if( passwordNew1 !== passwordNew2 )
	{
		Shell.alter(
			SeamFormChangePwd.traceSeam.add( 'errorMessage' ),
				'Passwords do not match',
			'mark',
				MarkCaret.TraceV( inputPasswordNew2.offsetTraceV( passwordNew2.length ) ),
		);
		return;
	}

	Shell.changePwd( Passhash.calc( passwordNew1 ) );
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.NameSizeLight( 'form', 22, light ),
			'pos', Point.XY( 0, -165 ),
			'string', 'Change password?',
		),
		'labelPasswordOld',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, -62 ),
			'string', 'old password',
		),
		'labelPasswordNew1',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, -22 ),
			'string', 'new password',
		),
		'labelPasswordNew2',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, 18 ),
			'string', 'repeat new password',
		),
		'errorMessage',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.SizeColor( 14, Color.red ),
			'pos', Point.XY( -20, -116 ),
			'string', ''
		),
		'inputPasswordOld',
		DesignInput.create(
			'facets',   GenericInput.facets,
			'font',     DesignFont.Size( 14 ),
			'maxlen',   100,
			'password', true,
			'zone',     Rect.PosWidthHeight( Point.XY( -80, -80 ), 210, 25 ),
		),
		'inputPasswordNew1',
		DesignInput.create(
			'facets',   GenericInput.facets,
			'font',     DesignFont.Size( 14 ),
			'maxlen',   100,
			'password', true,
			'zone',     Rect.PosWidthHeight( Point.XY( -80, -40 ), 210, 25 ),
		),
		'inputPasswordNew2',
		DesignInput.create(
			'facets',   GenericInput.facets,
			'font',     DesignFont.Size( 14 ),
			'maxlen',   100,
			'password', true,
			'zone',     Rect.PosWidthHeight( Point.XY( -80, 0 ), 210, 25 ),
		),
		'buttonChangePwd',
		DesignButton.create(
			'facets',   GenericButton.facets,
			'font',     DesignFont.Size( 14 ),
			'shape',   'RectRound',
			'string',  'change',
			'zone',     Rect.PosWidthHeight( Point.XY( 95, 95 ), 70, 70 ),
		),
		'buttonClose',
		DesignButton.create(
			'facets',   GenericButton.facets,
			'font',     DesignFont.Size( 14 ),
			'shape',   'RectRound',
			'string',  'cancel',
			'zone',     Rect.PosWidthHeight( Point.XY( 180, 105 ), 50, 50 ),
		)
	);

/*
| Sets the seam trace of an input widget.
|
| TODO replace by input widget insert/remove functions.
*/
def.proto._widgetTraceSeam =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'inputPasswordNew1': return SeamFormChangePwd.traceSeam.add( 'passwordNew1' );
		case 'inputPasswordNew2': return SeamFormChangePwd.traceSeam.add( 'passwordNew2' );
		case 'inputPasswordOld':  return SeamFormChangePwd.traceSeam.add( 'passwordOld' );
		default: throw new Error( );
	}
};

/*
| Sets the value of an input widget.
*/
def.proto._widgetInputValue =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'inputPasswordNew1': return this.seam.passwordNew1;
		case 'inputPasswordNew2': return this.seam.passwordNew2;
		case 'inputPasswordOld':  return this.seam.passwordOld;
		default: throw new Error( );
	}
};

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	switch( key )
	{
		case 'errorMessage':
			return this.seam.errorMessage;

		case 'headline':
		{
			const userCreds = this.userCreds;
			return(
				'Change password of '
				+ ( userCreds ? userCreds.name : '' )
				+ '?'
			);
		}

		default:
			// keep it as in layout
			return undefined;
	}
};

/*
| Sets the visibility of a widget.
*/
def.proto._widgetVisible =
	function( key )
{
	return undefined;
};

