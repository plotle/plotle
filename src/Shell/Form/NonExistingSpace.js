/*
| The space space the user tried to port to does not exist.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// the window this is at
	atWindow: { type: 'number' },

	// the window the form is shown in
	window: { type: 'Shell/Window/NonExisting' },
};

import { Self as GenericButton    } from '{Shell/Design/Generic/Button}';
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as RefSpace         } from '{Shared/Ref/Space}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const atWindow = this.atWindow;

	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'noButton':
		{
			if( atWindow > 0 )
			{
				Shell.closeWindow( atWindow );
			}
			else
			{
				let refFallback = this.window.refFallback;
				if( !refFallback )
				{
					refFallback = RefSpace.PlotleHome;
				}
				Shell.openSpace( 0, refFallback );
			}

			Shell.setModeNormal( );
			break;
		}

		case 'yesButton':
		{
			Shell.openSpace(
				atWindow, this.window.ref,
				'addHistory',
				'createMissing',
			);
			break;
		}

		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 22, light ),
			'pos',     Point.XY( 0, -120 ),
			'string', '',
		),
		'message1',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 16, light ),
			'pos',     Point.XY( 0, -50 ),
			'string', 'Do you want to create it?',
		),
		'noButton',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'No',
			'zone',    Rect.PosWidthHeight( Point.XY( -100, 28 ), 75, 75 ),
		),
		'yesButton',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'Yes',
			'zone',    Rect.PosWidthHeight( Point.XY( 25, 28 ), 75, 75 ),
		)
	);

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	if( key === 'headline' )
	{
		return this.window.ref.fullname + ' does not exist.';
	}
	// keep it as in layout
	else
	{
		return undefined;
	}
};

