/*
| The user form.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// "system", "light" or "dark"
	colorScheme: { type: 'string' },

	// form seam
	seam: { type: 'Shell/Seam/Form/Opts' },

	// show space grid
	showGrid: { type: 'boolean' },

	// show selection guides
	showGuides: { type: 'boolean' },

	// spaces have snapping
	snapping: { type: 'boolean' },

	// currently logged in user
	userCreds: { type: 'Shared/User/Creds' },

	// true if current user opted in to newsletter
	userInfo: { type: [ 'undefined', 'Shared/User/Info' ] },
};

import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignCheckbox   } from '{Shell/Design/Widget/Checkbox}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as DesignSelect     } from '{Shell/Design/Widget/Select}';
import { Self as GenericButton    } from '{Shell/Design/Generic/Button}';
import { Self as GenericCheckbox  } from '{Shell/Design/Generic/Checkbox}';
import { Self as GenericSelect    } from '{Shell/Design/Generic/Select}';
import { Self as IconCheck        } from '{Shell/Design/Icon/Check}';
import { Self as ListString       } from '{list@string}';
import { Self as ModeForm         } from '{Shell/Mode/Form}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as SeamFormOpts     } from '{Shell/Seam/Form/Opts}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'buttonClose':
			Shell.setModeNormal( );
			break;

		case 'buttonChangePwd':
			Shell.alter( 'mode', ModeForm.changePwd );
			break;

		case 'buttonLogin':
			Shell.alter( 'mode', ModeForm.login );
			break;

		case 'buttonLogout':
			Shell.logout( );
			break;

		case 'buttonSignup':
			Shell.alter( 'mode', ModeForm.signUp );
			break;

		default:
			throw new Error( );
	}
};

/*
| A checkbox has been toggled.
*/
def.proto.toggleCheckbox =
	function( traceV )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'checkboxGrid':
		{
			Shell.setShowGrid( !this.showGrid );
			return;
		}

		case 'checkboxGuides':
		{
			Shell.setShowGuides( !this.showGuides );
			return;
		}

		case 'checkboxNewsletter':
		{
			Shell.changeUserNewsletter( !this.userInfo.newsletter );
			return;
		}

		case 'checkboxSnapping':
		{
			Shell.setSnapping( !this.snapping );
			return;
		}

		default: throw new Error( );
	}
};

/*
| A select widget changed.
|
| ~traceV: of the select widget pushed
| ~value:  value to change it to.
*/
def.proto.widgetSelectChanged =
	function( traceV, value )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'selectColorScheme':
		{
			let colorScheme;
			switch( value )
			{
				case 0: colorScheme = 'system'; break;
				case 1: colorScheme = 'light'; break;
				case 2: colorScheme = 'dark'; break;
				default: throw new Error( );
			}

			Shell.setColorScheme( colorScheme );
			return;
		}

		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 22, light ),
			'pos',     Point.XY( 0, -150 ),
			'string', 'Hello'
		),

		'checkboxGrid',
		DesignCheckbox.create(
			'facets',      GenericCheckbox.facets,
			'iconChecked', IconCheck.design( light ),
			'zone',        Rect.PosWidthHeight( Point.XY( -55, -124 ), 16, 15 )
		),
		'labelGrid',
		DesignLabel.create(
			'font',    DesignFont.NameSizeLight( 'form', 16, light ),
			'pos',     Point.XY( -28, -110 ),
			'string', 'show grid',
		),

		'checkboxSnapping',
		DesignCheckbox.create(
			'facets',      GenericCheckbox.facets,
			'iconChecked', IconCheck.design( light ),
			'zone',        Rect.PosWidthHeight( Point.XY( -55, -94 ), 16, 15 )
		),
		'labelSnapping',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -28, -80 ),
			'string', 'snap to grid',
		),

		'checkboxGuides',
		DesignCheckbox.create(
			'facets',      GenericCheckbox.facets,
			'iconChecked', IconCheck.design( light ),
			'zone',        Rect.PosWidthHeight( Point.XY( -55, -64 ), 16, 15 )
		),
		'labelGuides',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -28, -50 ),
			'string', 'selection guides',
		),

		'labelColorScheme',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -28, -20 ),
			'string', 'color scheme',
		),
		'selectColorScheme',
		DesignSelect.create(
			'facetArrow',   GenericSelect.facetArrow,
			'facetOptions', GenericSelect.facetOptions,
			'facets',       GenericSelect.facets,
			'font',         DesignFont.Size( 14 ),
			'zone',         Rect.PosWidthHeight( Point.XY( 80, -38 ), 130, 25 ),
		),

		'checkboxNewsletter',
		DesignCheckbox.create(
			'facets',      GenericCheckbox.facets,
			'iconChecked', IconCheck.design( light ),
			'zone',        Rect.PosWidthHeight( Point.XY( -55, 27 ), 16, 15 ),
		),
		'labelNewsletter',
		DesignLabel.create(
			'align', 'left',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -28, 40 ),
			'string', 'newsletter'
		),
		'visitor1',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( 0, 22 ),
			'string', 'You\'re currently an anonymous visitor!'
		),
		'visitor2',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( 0, 47 ),
			'string', 'Click on "sign up" or "log in"'
		),


		'buttonChangePwd',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'change password',
			'zone',    Rect.PosWidthHeight( Point.XY( -60, 70 ), 135, 40 )
		),
		'buttonSignup',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'sign up',
			'zone',    Rect.PosWidthHeight( Point.XY( -60, 70 ), 135, 40 )
		),
		'buttonLogin',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'log in',
			'zone',    Rect.PosWidthHeight( Point.XY( -60, 118 ), 135, 40 )
		),
		'buttonLogout',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'log out',
			'zone',    Rect.PosWidthHeight( Point.XY( -60, 118 ), 135, 40 )
		),
		'buttonClose',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'close',
			'zone',    Rect.PosWidthHeight( Point.XY( 180, 118 ), 55, 40 )
		),
	);

/*
| Sets the checked of a checkbox widget.
*/
def.proto._widgetCheckboxChecked =
	function( key )
{
	switch( key )
	{
		case 'checkboxNewsletter': return this.userInfo?.newsletter;
		case 'checkboxGrid':       return this.showGrid;
		case 'checkboxGuides':     return this.showGuides;
		case 'checkboxSnapping':   return this.snapping;
		default: throw new Error( );
	}
};

/*
| Sets the seam trace of a widget.
|
| TODO make this generic, it should always be the same (also with Input).
*/
def.proto._widgetTraceSeam =
	function( key )
{
	switch( key )
	{
		case 'selectColorScheme':
			return SeamFormOpts.traceSeam.add( 'widgets', 'selectColorScheme' );

		default: throw new Error( );
	}
};

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	if( key === 'headline' )
	{
		const userCreds = this.userCreds;
		return(
			'Hello '
			+ ( userCreds ? userCreds.name : '' )
		);
	}
	else
	{
		// keep it as in layout
		return undefined;
	}
};

/*
| Sets the visibility of a widget.
*/
def.proto._widgetSelectOptions =
	function( key )
{
	switch( key )
	{
		case 'selectColorScheme':
			return ListString.Elements( 'system setting', 'override light', 'override dark' );

		default: throw new Error( );
	}
};

/*
| Sets the value of a select widget.
*/
def.proto._widgetSelectValue =
	function( key )
{
	switch( key )
	{
		case 'selectColorScheme':
		{
			switch( this.colorScheme )
			{
				case 'system': return 0;
				case 'light':  return 1;
				case 'dark':   return 2;
				default: throw new Error( );
			}
		}
		default: throw new Error( );
	}
};

/*
| Sets the visibility of a widget.
*/
def.proto._widgetVisible =
	function( key )
{
	const isVisitor = this.userCreds ? this.userCreds.isVisitor : true;

	switch( key )
	{
		case 'visitor1':
		case 'visitor2':
		case 'buttonLogin':
		case 'buttonSignup':
			return isVisitor;

		case 'buttonChangePwd':
		case 'buttonLogout':
		case 'checkboxNewsletter':
		case 'labelNewsletter':
			return !isVisitor;

		default: return undefined;
	}
};

