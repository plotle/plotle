/*
| The welcome form.
|
| Shown only after successfully signing up.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// currently logged in user
	userCreds: { type: [ 'undefined', 'Shared/User/Creds' ] },
};

import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as GenericButton    } from '{Shell/Design/Generic/Button}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	if( key === 'headline' )
	{
		return 'Welcome ' + this.userCreds.name;
	}
	else
	{
		// keep it as in layout
		return undefined;
	}
};

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const buttonKey = traceV.backward( 'widgets' ).last.key;
	switch( buttonKey )
	{
		case 'buttonClose':
			Shell.setModeNormal( );
			break;

		default:
			throw new Error( );
	}
};

/*
| Design widgets.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 22, light ),
			'pos',     Point.XY( 0, -120 ),
			'string', 'Welcome',
		),
		'message1',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 16, light ),
			'pos',     Point.XY( 0, -50 ),
			'string', 'Your registration was successful :-)',
		),
		'buttonClose',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'close',
			'zone',    Rect.PosWidthHeight( Point.XY( 180, 54 ), 55, 36 ),
		)
	);
