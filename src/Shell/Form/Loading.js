/*
| The loading form.
|
| Shown when loading a space.
*/
def.attributes =
{
	// reference of loading space
	refSpace: { type: 'Shared/Ref/Space' },
};

def.extend = 'Shell/Form/Base';

import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as Point            } from '{gleam:Point}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	if( key === 'spaceText' )
	{
		return this.refSpace.fullname;
	}
	else
	{
		// keep it as in layout
		return undefined;
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.NameSizeLight( 'form', 28, light ),
			'pos', Point.XY( 0, -56 ),
			'string', 'loading',
		),
		'spaceText',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.NameSizeLight( 'form', 28, light ),
			'pos', Point.zero,
			'string', 'plotle:home',
		)
	);
