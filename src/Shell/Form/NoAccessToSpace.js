/*
| User has no access to a space s/he tried to port to.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// the window this is at
	atWindow: { type: 'number' },

	// the window the form is shown in
	window: { type: 'Shell/Window/NoAccess' },
};

import { Self as GenericButton    } from '{Shell/Design/Generic/Button}';
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'buttonClose':
		{
			const refFallback = this.window.refFallback;
			if( refFallback )
			{
				Shell.openSpace( this.atWindow, refFallback );
			}
			else
			{
				Shell.closeWindow( this.atWindow );
			}
			break;
		}

		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 22, light ),
			'pos',     Point.XY( 0, -120 ),
			'string', '',
		),
		'message1',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 16, light ),
			'pos',     Point.XY( 0, -50 ),
			'string', 'Sorry, you cannot port to this space or create it.',
		),
		'buttonClose',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'close',
			'zone',    Rect.PosWidthHeight( Point.XY( 180, 34 ), 55, 40 ),
		)
	);

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	if( key === 'headline' )
	{
		const ref = this.window.ref;
		return 'No access to ' + ref.fullname;
	}
	else
	{
		// keep it as in layout
		return undefined;
	}
};
