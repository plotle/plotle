/*
| The signup form.
*/
def.extend = 'Shell/Form/Base';

def.attributes =
{
	// login form seam
	seam: { type: 'Shell/Seam/Form/SignUp' },
};

import { Self as Color             } from '{gleam:Color}';
import { Self as DesignButton      } from '{Shell/Design/Widget/Button}';
import { Self as DesignCheckbox    } from '{Shell/Design/Widget/Checkbox}';
import { Self as DesignFont        } from '{Shell/Design/Font}';
import { Self as DesignInput       } from '{Shell/Design/Widget/Input}';
import { Self as DesignLabel       } from '{Shell/Design/Widget/Label}';
import { Self as GenericButton     } from '{Shell/Design/Generic/Button}';
import { Self as GenericCheckbox   } from '{Shell/Design/Generic/Checkbox}';
import { Self as GenericInput      } from '{Shell/Design/Generic/Input}';
import { Self as IconCheck         } from '{Shell/Design/Icon/Check}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as Passhash          } from '{Shared/User/PassHash}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as ReplyError        } from '{Shared/Reply/Error}';
import { Self as SeamFormSignUp    } from '{Shell/Seam/Form/SignUp}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as TransactionSignUp } from '{Shell/Transaction/SignUp}';
import { Self as TwigDesignWidget  } from '{twig@<Shell/Design/Widget/Types}';
import { Self as UserCreds         } from '{Shared/User/Creds}';

/*
| Clears all fields.
*/
def.proto.clear =
	function( )
{
	Shell.alter(
		SeamFormSignUp.traceSeam, SeamFormSignUp.Clean,
		'mark', undefined
	);
};

/*
| A signUp request got a reply.
*/
def.proto.onSignUp =
	function( request, reply )
{
	if( reply.ti2ctype === ReplyError )
	{
		const message = reply.message;
		const inputUsername = this.widgets.get( 'inputUsername' );
		if( message.search( /Username/ ) >= 0 )
		{
			Shell.alter(
				SeamFormSignUp.traceSeam.add( 'errorMessage' ),
					message,
				'mark',
					MarkCaret.TraceV( inputUsername.offsetTraceV( inputUsername.value.length ) )
			);
		}
		else
		{
			Shell.alter( SeamFormSignUp.traceSeam.add( 'errorMessage' ), message );
		}
	}
	else
	{
		this.clear( );
	}
};

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'buttonSignup':
			this.signup( );
			break;

		case 'buttonClose':
			Shell.setModeNormal( );
			break;

		default:
			throw new Error( );
	}
};

/*
| Signs a new user up
*/
def.proto.signup =
	function( )
{
	const widgets = this.widgets;
	const inputUsername = widgets.get( 'inputUsername' );
	const username      = inputUsername.value;
	const email         = widgets.get( 'inputEmail' ).value;
	const passInput     = widgets.get( 'inputPassword' );
	const pass2Input    = widgets.get( 'inputPassword2' );
	const pass          = passInput.value;
	const pass2         = pass2Input.value;
	const newsletter    = widgets.get( 'checkboxNewsletter' ).checked;

	if( username.length < 4 )
	{
		Shell.alter(
			SeamFormSignUp.traceSeam.add( 'errorMessage' ),
				'Username too short, min. 4 characters',
			'mark',
				MarkCaret.TraceV( inputUsername.offsetTraceV( username.length ) ),
		);
		return;
	}

	if( username.substr( 0, 5 ) === 'visit' )
	{
		Shell.alter(
			SeamFormSignUp.traceSeam.add( 'errorMessage' ),
				'Username must not start with "visit"',
			'mark',
				MarkCaret.TraceV( inputUsername.offsetTraceV( 0 ) ),
		);
		return;
	}

	if( pass.length < 5 )
	{
		Shell.alter(
			SeamFormSignUp.traceSeam.add( 'errorMessage' ),
				'Password too short, min. 5 characters',
			'mark',
				MarkCaret.TraceV( passInput.offsetTraceV( pass.length ) ),
		);
		return;
	}

	if( pass !== pass2 )
	{
		Shell.alter(
			SeamFormSignUp.traceSeam.add( 'errorMessage' ),
				'Passwords do not match',
			'mark',
				MarkCaret.TraceV( pass2Input.offsetTraceV( pass2.length ) ),
		);
		return;
	}

	TransactionSignUp.start(
		UserCreds.create(
			'name', username,
			'passhash', Passhash.calc( pass )
		),
		email,
		newsletter
	);
};

/*
| A checkbox has been toggled.
*/
def.proto.toggleCheckbox =
	function( traceV )
{
	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'checkboxNewsletter':
		{
			Shell.alter( SeamFormSignUp.traceSeam.add( 'newsletter' ), !this.seam.newsletter );
			return;
		}

		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 22, light ),
			'pos', Point.XY( -245, -165 ),
			'string', 'Sign Up',
		),
		'labelUsername',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, -102 ),
			'string', 'username',
		),
		'labelEmail',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, -62 ),
			'string', 'email',
		),
		'labelPassword',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, -22 ),
			'string', 'password',
		),
		'labelPassword2',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, 18 ),
			'string', 'repeat password',
		),
		'labelNewsletter',
		DesignLabel.create(
			'align', 'right',
			'font', DesignFont.NameSizeLight( 'form', 16, light ),
			'pos', Point.XY( -98, 58 ),
			'string', 'newsletter',
		),
		'errorMessage',
		DesignLabel.create(
			'align', 'center',
			'font', DesignFont.SizeColor( 14, Color.red ),
			'pos', Point.XY( -20, -136 ),
			'string', ''
		),
		'inputUsername',
		DesignInput.create(
			'facets', GenericInput.facets,
			'font',   DesignFont.Size( 14 ),
			'maxlen', 100,
			'zone',   Rect.PosWidthHeight( Point.XY( -80, -120 ), 210, 25 ),
		),
		'inputEmail',
		DesignInput.create(
			'facets', GenericInput.facets,
			'font',   DesignFont.Size( 14 ),
			'maxlen', 100,
			'zone',   Rect.PosWidthHeight( Point.XY( -80, -80 ), 210, 25 ),
		),
		'inputPassword',
		DesignInput.create(
			'facets',   GenericInput.facets,
			'font',     DesignFont.Size( 14 ),
			'maxlen',   100,
			'password', true,
			'zone',     Rect.PosWidthHeight( Point.XY( -80, -40 ), 210, 25 ),
		),
		'inputPassword2',
		DesignInput.create(
			'facets',   GenericInput.facets,
			'password', true,
			'font',     DesignFont.Size( 14 ),
			'maxlen',   100,
			'zone',     Rect.PosWidthHeight( Point.XY( -80, 0 ), 210, 25 ),
		),
		'checkboxNewsletter',
		DesignCheckbox.create(
			'facets',      GenericCheckbox.facets,
			'iconChecked', IconCheck.design( light ),
			'zone',        Rect.PosWidthHeight( Point.XY( -75, 45 ), 16, 15 ),
		),
		'labelNewsletter2',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 12, light ),
			'pos', Point.XY( -45, 57 ),
			'string', 'Updates and News',
		),
		'labelNewsletter3',
		DesignLabel.create(
			'font', DesignFont.NameSizeLight( 'form', 12, light ),
			'pos', Point.XY( -45, 77 ),
			'string', 'Not going to be more than an email a month.',
		),
		'buttonSignup',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'sign up',
			'zone',    Rect.PosWidthHeight( Point.XY( 95, 95 ), 70, 70 ),
		),
		'buttonClose',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'close',
			'zone',    Rect.PosWidthHeight( Point.XY( 180, 105 ), 50, 50 ),
		)
	);

/*
| Sets the checked of a checkbox widget.
*/
def.proto._widgetCheckboxChecked =
	function( key )
{
	switch( key )
	{
		case 'checkboxNewsletter':
			return this.seam.newsletter;

		default:
			throw new Error( );
	}
};

/*
| Sets the seam trace of an input widget.
|
| TODO replace by input widget insert/remove functions.
*/
def.proto._widgetTraceSeam =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'inputEmail':     return SeamFormSignUp.traceSeam.add( 'email' );
		case 'inputPassword':  return SeamFormSignUp.traceSeam.add( 'password' );
		case 'inputPassword2': return SeamFormSignUp.traceSeam.add( 'password2' );
		case 'inputUsername':  return SeamFormSignUp.traceSeam.add( 'username' );
		default: throw new Error( );
	}
};

/*
| Sets the value of an input widget.
*/
def.proto._widgetInputValue =
	function( key )
{
	// keep it as in layout
	switch( key )
	{
		case 'inputEmail':     return this.seam.email;
		case 'inputPassword':  return this.seam.password;
		case 'inputPassword2': return this.seam.password2;
		case 'inputUsername':  return this.seam.username;
		default: throw new Error( );
	}
};

/*
| Sets the string of a label widget.
*/
def.proto._widgetLabelString =
	function( key )
{
	return(
		key === 'errorMessage'
		? this.seam.errorMessage
		// otherwise keep it as in layout
		: undefined
	);
};
