/*
| A form showing "Done".
|
| Shown after successfully changeing password.
*/
def.extend = 'Shell/Form/Base';

import { Self as GenericButton    } from '{Shell/Design/Generic/Button}';
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignLabel      } from '{Shell/Design/Widget/Label}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';

/*
| A button of the form has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const buttonKey = traceV.backward( 'widgets' ).last.key;
	switch( buttonKey )
	{
		case 'buttonClose':
			Shell.setModeNormal( );
			break;

		default: throw new Error( );
	}
};

/*
| Design.
*/
def.lazyFunc._designWidgets =
	( light ) =>
	TwigDesignWidget.Grow(
		'headline',
		DesignLabel.create(
			'align',  'center',
			'font',    DesignFont.NameSizeLight( 'form', 22, light ),
			'pos',     Point.XY( 0, -54 ),
			'string', 'Done!',
		),
		'buttonClose',
		DesignButton.create(
			'facets',  GenericButton.facets,
			'font',    DesignFont.Size( 14 ),
			'shape',  'RectRound',
			'string', 'close',
			'zone',    Rect.PosWidthHeight( Point.XY( 180, 54 ), 55, 36 ),
		)
	);
