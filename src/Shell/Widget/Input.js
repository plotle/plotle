/*
| An input field.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// style facets
	facets: { type: 'Shell/Facet/List' },

	// font of the value
	fontColor: { type: 'gleam:Font/Color' },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// maximum input length
	maxlen: { type: 'integer', defaultValue: '0' },

	// true for password input
	password: { type: 'boolean', defaultValue: 'false' },

	// if true the input is readonly.
	readonly: { type: 'boolean', defaultValue: 'false' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// seam trace
	traceSeam: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the value in the input box
	value: { type: 'string', defaultValue: '""' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};

import { Self as Color       } from '{gleam:Color}';
import { Self as Ellipse     } from '{gleam:Ellipse}';
import { Self as Font        } from '{gleam:Font/Root}';
import { Self as GlintFigure } from '{gleam:Glint/Figure}';
import { Self as GlintPane   } from '{gleam:Glint/Pane}';
import { Self as GlintString } from '{gleam:Glint/String}';
import { Self as GlintWindow } from '{gleam:Glint/Window}';
import { Self as Hover       } from '{Shell/Result/Hover}';
import { Self as DesignInput } from '{Shell/Design/Widget/Input}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret   } from '{Shared/Mark/Caret}';
import { Self as Point       } from '{gleam:Point}';
import { Self as Rect        } from '{gleam:Rect}';
import { Self as RectRound   } from '{gleam:RectRound}';
import { Self as Shell       } from '{Shell/Self}';
import { Self as SysMode     } from '{Shell/Result/SysMode}';

/*
| Inputs can hold a caret.
*/
def.proto.caretable = true;

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	if( !p || !this._tZone.within( p ) )
	{
		return undefined;
	}

	const pp = p.sub( this._tZone.pos );

	if( !this._tzShape.within( pp ) )
	{
		return undefined;
	}

	if( this.readonly )
	{
		return false;
	}

	const offset = this._getOffsetAtP( pp );
	Shell.alter(
		'action', undefined,
		'mark', MarkCaret.TraceV( this.offsetTraceV( offset ) ),
	);

	return true;
};

/*
| Creates an actual widget from a design.
|
| ~design:      widget design
| ~light:       true if light color scheme
| ~traceV:      traceV of the widget
| ~resolution:  current screen resolution
| ~systemFocus: true if having the system focus
| ~transform:   transform of the widget
*/
def.static.FromDesign =
	function( design, light, traceV, resolution, systemFocus, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 6 ) throw new Error( );
/**/	if( design.ti2ctype !== DesignInput ) throw new Error( );
/**/}

	return(
		Self.create(
			'facets',      design.facets,
			'fontColor',   design.font,
			'maxlen',      design.maxlen,
			'password',    design.password,
			'resolution',  resolution,
			'systemFocus', systemFocus,
			'transform',   transform,
			'traceV',      traceV,
			'zone',        design.zone,
		)
	);
};

/*
| Inputs are focusable
*/
def.proto.focusable = true;

/*
| The widget's glint.
*/
def.lazy.glint =
	function( )
{
	if( !this.visible ) return undefined;

	const zone = this._tZone.envelope( 1 );

	return(
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane',
				GlintPane.create(
					'glint', this._glint,
					'resolution', this.resolution,
					'size', zone.size,
				),
			'pos', zone.pos
		)
	);
};

/*
| User input.
|
| ~string: string inputed
*/
def.proto.input =
	function( string )
{
	const mark = this.mark;
	const value = this.value;
	const at = mark.traceVCaret.last.at;
	const maxlen = this.maxlen;
	// cuts of string if larger than this maxlen
	if( maxlen > 0 && value.length + value.length > maxlen )
	{
		string = string.substring( 0, maxlen - value.length );
	}

	this.insert( at, string );
};

/*
| Performs an insertion.
|
| Default implementation changes the value of the widget itself.
| To be overloaded in widgets on items.
|
| ~at: position to insert at.
| ~str: string to insert.
*/
def.proto.insert =
	function( at, str )
{
	const value = this.value;

	Shell.alter(
		this.traceSeam, value.substring( 0, at ) + str + value.substring( at ),
		'mark', MarkCaret.TraceV( this.offsetTraceV( at + str.length ) )
	);
};

/*
| Returns the point of a given offset.
|
| ~offset: the offset to get the point from.
*/
def.lazyFunc.locateOffsetPoint =
	function( offset )
{
	const fontColor = this._fontColor;
	const pitch = this._tPitch;
	const value = this.value.substring( 0, offset );
	if( this.password )
	{
		return(
			Point.XY(
				pitch.x + ( this._maskWidth + this._maskKern ) * offset - 1,
				Math.round( pitch.y + fontColor.size )
			)
		);
	}
	else
	{
		return(
			Point.XY(
				Math.round( pitch.x + fontColor.StringNormal( value ).advanceWidth ),
				Math.round( pitch.y + fontColor.size )
			)
		);
	}
};

/*
| Returns an offset trace into the value.
*/
def.proto.offsetTraceV =
	function( at )
{
	return this.traceV.add( 'value' ).add( 'offset', at );
};

/*
| Default distance of characters.
*/
def.staticLazy.pitch = ( ) =>
	Point.XY( 8, 3 );

/*
| Mouse hover
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	if(
		!this._tZone.within( p )
		|| !this._tzShape.within( p.sub( this._tZone.pos ) )
	) return undefined;

	return Hover.CursorText.create( 'traceV', this.traceV );
};

/*
| Performs a string removal.
|
| Default implementation changed the value of the widget itself.
| To be overloaded in widgets on items.
|
| ~at: position to remove one character.
*/
def.proto.remove =
	function( at )
{
	const value = this.value;
	const mark = this.mark;
	const pat = mark.traceVCaret.last.at;

	Shell.alter(
		this.traceSeam, value.substring( 0, at ) + value.substring( at + 1 ),
		'mark', pat >= at + 1 ? mark.backward : pass
	);
};

/*
| User pressed a special key
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	switch( key )
	{
		case 'backspace': this._keyBackspace( ); return;
		case 'del': this._keyDel( ); return;
		case 'down': this._keyDown( ); return;
		case 'end': this._keyEnd( ); return;
		case 'enter': this._keyEnter( ); return;
		case 'left': this._keyLeft( ); return;
		case 'pos1': this._keyPos1( ); return;
		case 'right': this._keyRight( ); return;
		case 'up': this._keyUp( ); return;
	}
};

/*
| SysMode, see shell/root.
*/
def.lazy.sysMode =
	function( )
{
	const fs = this.fontColor.size;
	const descend = fs * Font.bottomBox;
	const mark = this.mark;

	if( !mark ) return SysMode.Blank;

	const p = this.locateOffsetPoint( mark.traceVCaret.last.at );
	const s = Math.round( p.y + descend + 1 );

	return(
		SysMode.create(
			'attention', this._tZone.pos.y + s - Math.round( fs + descend ),
			'keyboard', true,
			'password', this.password,
		)
	);
};

/*
| Glint for the caret.
*/
def.lazy._caretGlint =
	function( )
{
	const fs = this._fontColor.size;
	const descend = fs * Font.bottomBox;
	const p = this.locateOffsetPoint( this.mark.traceVCaret.last.at );

	return(
		GlintFigure.FigureFill(
			Rect.PosWidthHeight( p.add( 0, 1 - fs ), 1, fs + descend ),
			Color.black
		)
	);
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const traceV = this.traceV;
/**/
/**/	// if hover is defined it has to affect this widget.
/**/	const hover = this.hover;
/**/	if( hover && !hover.hasTrace( traceV ) ) throw new Error( );
/**/
/**/	// if mark is defined it has to affect this widget.
/**/	const mark = this.mark;
/**/	if( mark && !mark.encompasses( traceV ) ) throw new Error( );
/**/}
};

/*
| The widget's facet.
*/
def.lazy._facet =
	function( )
{
	return(
		this.facets.getFacet(
			'hover', !!this.hover,
			'focus', !!this.mark && this.systemFocus
		)
	);
};

/*
| Transformed font size.
*/
def.lazy._fontColor =
	function( )
{
	return this.fontColor.transform( this.transform );
};

/*
| Returns the offset nearest to point p.
*/
def.proto._getOffsetAtP =
	function( p )
{
	let mw;
	const pitch = this._tPitch;
	const dx = p.x - pitch.x;
	const value = this.value;
	let x1 = 0, x2 = 0;
	const password = this.password;
	const fontColor = this._fontColor;
	if( password ) mw = this._maskWidth + this._maskKern;
	let a;
	for( a = 0; a < value.length; a++ )
	{
		x1 = x2;
		x2 = password ? a * mw : fontColor.StringNormal( value.substr( 0, a ) ).advanceWidth;
		if( x2 >= dx ) break;
	}
	if( dx - x1 < x2 - dx && a > 0 ) a--;
	return a;
};

/*
| Returns the inner glint of the input field.
*/
def.lazy._glint =
	function( )
{
	const pitch = this._tPitch;
	const value = this.value;
	const mark = this.mark;
	const resolution = this.resolution;
	const list = [ this._facet.glintFill( this._tzShape ) ];
	const fontColor = this._fontColor;

	if( this.password )
	{
		const black = Color.black;
		for( let shape of this._passMask )
		{
			list.push( GlintFigure.FigureFill( shape, black ) );
		}
	}
	else
	{
		list.push(
			GlintString.create(
				'fontColor', fontColor,
				'resolution', resolution,
				'string', value,
				'p', Point.XY( pitch.x, fontColor.size + pitch.y )
			)
		);
	}

	if( mark && mark.ti2ctype === MarkCaret && this.systemFocus )
	{
		list.push( this._caretGlint );
	}

	list.push( this._facet.glintBorder( this._tzShape ) );
	return ListGlint.Array( list );
};

/*
| User pressed backspace.
*/
def.proto._keyBackspace =
	function( )
{
	const mark = this.mark;
	const at = mark.traceVCaret.last.at;
	if( at <= 0 ) return;
	this.remove( at - 1 );
};

/*
| User pressed del.
*/
def.proto._keyDel =
	function( )
{
	const at = this.mark.traceVCaret.last.at;
	const value = this.value;
	if( at >= value.length ) return;
	this.remove( at );
};

/*
| User pressed return key.
| User pressed down key.
*/
def.proto._keyEnter =
def.proto._keyDown =
	function( )
{
	Shell.cycleFocus( 1 );
};

/*
| User pressed end key.
*/
def.proto._keyEnd =
	function( )
{
	const mark = this.mark;
	const at = mark.traceVCaret.last.at;

	if( at >= this.value.length ) return;

	Shell.alter( 'mark', MarkCaret.TraceV( this.offsetTraceV( this.value.length ) ) );
};

/*
| User pressed left key.
*/
def.proto._keyLeft =
	function( )
{
	const mark = this.mark;
	if( mark.traceVCaret.last.at <= 0 ) return;

	Shell.alter( 'mark', mark.backward );
};

/*
| User pressed pos1 key
*/
def.proto._keyPos1 =
	function( )
{
	const mark = this.mark;
	if( mark.traceVCaret.last.at <= 0 ) return;

	Shell.alter( 'mark', mark.zero );
};

/*
| User pressed right key
*/
def.proto._keyRight =
	function( )
{
	const mark = this.mark;
	if( mark.traceVCaret.last.at >= this.value.length ) return;

	Shell.alter( 'mark', mark.forward );
};

/*
| User pressed up key.
*/
def.proto._keyUp =
	function( )
{
	Shell.cycleFocus( -1 );
};

/*
| Returns the kerning of characters for password masks.
*/
def.lazy._maskKern =
	function( )
{
	return this._fontColor.size * 0.15;
};

/*
| Returns the width of a character for password masks.
*/
def.lazy._maskWidth =
	function( )
{
	return this._fontColor.size * 0.5;
};

/*
| Returns an array of ellipses
| representing the password mask.
*/
def.lazy._passMask =
	function( )
{
	const value = this.value;
	const size = this._fontColor.size;
	const pm = [ ];
	const pitch = this._tPitch;
	let x = pitch.x;
	const y = pitch.y + Math.round( size * 0.7 );
	const w = this._maskWidth;
	//h = size * 0.32,
	const h = w;
	const k = this._maskKern;
	for( let a = 0, al = value.length; a < al; a++, x += w + k )
	{
		pm.push( Ellipse.PosWidthHeight( Point.XY( x, y - h / 2 ), w, h ) );
	}
	return pm;
};

/*
| Transformed pitch.
*/
def.lazy._tPitch =
	function( )
{
	const s = this.transform.scale;
	const p = Self.pitch;

	return Point.XY( p.x * s, p.y * s );
};

/*
| The transformed zone of the button.
*/
def.lazy._tZone =
	function( )
{
	return this.zone.transform( this.transform );
};

/*
| The transformed shape of the button
| positioned at zero.
*/
def.lazy._tzShape =
	function( )
{
	const tZone = this._tZone;
	const t = this.transform;

	return(
		RectRound.create(
			'pos', Point.zero,
			'width', tZone.width,
			'height', tZone.height,
			'a', 7 * t.scale,
			'b', 3 * t.scale,
		)
	);
};
