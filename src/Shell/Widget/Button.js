/*
| A button.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// true if the button is down
	down: { type: 'boolean', defaultValue: 'false' },

	// style facets
	facets: { type: 'Shell/Facet/List' },

	// font familiy, size and color of the string
	fontColor: { type: [ 'undefined', 'gleam:Font/Color' ] },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// icon facet
	iconFacet: { type: [ 'undefined', 'Shell/Facet/Self' ] },

	// true if the icon rendering is to be not grid fitted
	iconNoGrid: { type: [ 'undefined', 'boolean' ] },

	// icon shape
	iconShape: { type: [ '<gleam:Figure/Types', 'undefined' ] },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types' ] },

	// shape of the button
	shape:
	{
		type:
		[
			'gleam:Ellipse',
			'gleam:RectRound',
			'gleam:RectRoundExt',
			'string',
		],
	},

	// the string written in the button
	string: { type: 'string', defaultValue: '""' },

	// vertical distance of newline
	stringNewline: { type: [ 'undefined', 'number' ] },

	// rotation of the string
	stringRotation: { type: [ 'undefined', '<gleam:Angle/Types' ] },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};

import { Self as Ellipse         } from '{gleam:Ellipse}';
import { Self as GlintPane       } from '{gleam:Glint/Pane}';
import { Self as GlintString     } from '{gleam:Glint/String}';
import { Self as GlintWindow     } from '{gleam:Glint/Window}';
import { Self as Hover           } from '{Shell/Result/Hover}';
import { Self as DesignButton    } from '{Shell/Design/Widget/Button}';
import { Self as ListGlint       } from '{list@<gleam:Glint/Types}';
import { Self as Point           } from '{gleam:Point}';
import { Self as Rect            } from '{gleam:Rect}';
import { Self as RectRound       } from '{gleam:RectRound}';
import { Self as Shell           } from '{Shell/Self}';
import { Self as TransformNormal } from '{gleam:Transform/Normal}';

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	if( !this.within( p ) )
	{
		return undefined;
	}

	Shell.pushButton( this.traceV, shift, ctrl );

	return true;
};

/*
| Start an operation with the poiting device button held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	if( !this.within( p ) )
	{
		return undefined;
	}

	Shell.dragStartButton( this.traceV );

	return true;
};

/*
| Stops an operation with the poiting device button held down.
*/
def.proto.dragStop =
	function( )
{
	Shell.alter( 'action', undefined );
};

/*
| Creates an actual widget from a design.
|
| ~design:       widget design
| ~light:       true if light color scheme
| ~traceV:       traceV of the widget
| ~resolution:   current screen resolution
| ~systemFocus:  true if having the system focus
| ~transform:    transform of the widget
*/
def.static.FromDesign =
	function( design, light, traceV, resolution, systemFocus, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 6 ) throw new Error( );
/**/	if( design.ti2ctype !== DesignButton ) throw new Error( );
/**/}

	const icon = design.icon;

	return(
		Self.create(
			'facets',         design.facets,
			'fontColor',      design.font,
			'iconNoGrid',     design.iconNoGrid,
			'iconShape',      icon?.shape,
			'iconFacet',      icon?.facet,
			'resolution',     resolution,
			'shape',          design.shape,
			'string',         design.string,
			'stringNewline',  design.stringNewline,
			'stringRotation', design.stringRotation,
			'systemFocus',    systemFocus,
			'transform',      transform,
			'traceV',         traceV,
			'visible',        design.visible,
			'zone',           design.zone,
		)
	);
};

/*
| Buttons are focusable.
*/
def.proto.focusable = true;

/*
| The widget's glint.
*/
def.lazy.glint =
	function( )
{
	if( !this.visible )
	{
		return undefined;
	}

	const zone = this._tZone.envelope( 1 );
	return(
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane',
				GlintPane.create(
					'glint', this._glint,
					'resolution', this.resolution,
					'size', zone.size,
				),
			'pos', zone.pos,
		)
	);
};

/*
| Any normal key for a button having focus triggers a push.
*/
def.proto.input =
	function( string )
{
	Shell.pushButton( this.traceV );

	return true;
};

/*
| Mouse wheel is being turned.
*/
def.proto.mousewheel =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| Mouse hover.
*/
def.proto.pointingHover =
	function( p )
{
	if( !this.within( p ) ) return undefined;

	return Hover.CursorPointer.create( 'traceV', this.traceV );
};

/*
| Special keys for buttons having focus
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	switch( key )
	{
		case 'down':
			Shell.cycleFocus( 1 );
			return;

		case 'up':
			Shell.cycleFocus( -1 );
			return;

		case 'enter':
			Shell.pushButton( this.traceV, false, false );
			return;
	}
};

/*
| Returns true if p is within the button.
*/
def.proto.within =
	function( p )
{
	return(
		this.visible
		&& this._tZone.within( p )
		&& this._shapeTZ.within( p.sub( this._tZone.pos ) )
	);
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const traceV = this.traceV;
/**/
/**/	// if hover is defined it has to affect this widget.
/**/	const hover = this.hover;
/**/	if( hover && !hover.hasTrace( traceV ) ) throw new Error( );
/**/
/**/	// if mark is defined it has to affect this widget.
/**/	const mark = this.mark;
/**/	if( mark && !mark.encompasses( traceV ) ) throw new Error( );
/**/}
};

/*
| The button's inner glint.
*/
def.lazy._glint =
	function( )
{
	const facet =
		this.facets.getFacet(
			'down', this.down,
			'focus', !!this.mark,
			'hover', !!this.hover,
		);

	const list = [ facet.glint( this._shapeTZ ) ];

	let string = this.string;
	if( string )
	{
		let newline = this.stringNewline;
		const fontColor = this._tFontColor;

		if( newline === undefined )
		{
			list.push(
				GlintString.create(
					'align', 'center',
					'base', 'middle',
					'fontColor', fontColor,
					'p', this._pc,
					'resolution', this.resolution,
					'rotation', this.stringRotation,
					'string', string
				)
			);
		}
		else
		{
			newline = this.transform.d( newline );
			string = string.split( '\n' );
			const tlen = string.length;
			let y = -( tlen - 1 ) / 2 * newline;
			for( let t = 0; t < tlen; t++, y += newline )
			{
				list.push(
					GlintString.create(
						'align', 'center',
						'base', 'middle',
						'fontColor', fontColor,
						'p', this._pc.add( 0, y ),
						'resolution', this.resolution,
						'string', string[ t ]
					)
				);
			}
		}
	}

	if( this.iconShape )
	{
		list.push( this.iconFacet.glint( this._iconShape, this.iconNoGrid ) );
	}

	return ListGlint.Array( list );
};

/*
| The transformed iconShape.
*/
def.lazy._iconShape =
	function( )
{
	return(
		this.iconShape
		.transform( TransformNormal.setOffset( this.zone.zeroPos.pc ) )
		.transform( this.transform.scaleOnly )
	);
};

/*
| The transformed center point of this widget.
*/
def.lazy._pc =
	function( )
{
	return this._tZone.zeroPos.pc;
};

/*
| The font of the button label.
*/
def.lazy._tFontColor =
	function( )
{
	return this.fontColor.transform( this.transform );
};

/*
| The transformed zone of the button.
*/
def.lazy._tZone =
	function( )
{
	return this.zone.transform( this.transform );
};

/*
| The shape of the button
| transformed and zero positioned.
*/
def.lazy._shapeTZ =
	function( )
{
	switch( this.shape )
	{
		case 'Ellipse':
		{
			const tZone = this._tZone;
			return Ellipse.PosWidthHeight( Point.zero, tZone.width - 1, tZone.height - 1 );
		}

		case 'Rect':
		{
			const tZone = this._tZone;
			return(
				Rect.create(
					'pos', Point.zero,
					'width', tZone.width - 1,
					'height', tZone.height - 1,
				)
			);
		}

		case 'RectRound':
		{
			const tZone = this._tZone;
			const ab = ( tZone.height - 1 ) / 3;
			return(
				RectRound.create(
					'pos', Point.zero,
					'width', tZone.width - 1,
					'height', tZone.height - 1,
					'a', ab,
					'b', ab
				)
			);
		}

		default:
		{
			return this.shape.transform( this.transform.scaleOnly );
		}
	}
};
