/*
| A checkbox.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// true if the checkbox is checked
	checked: { type: 'boolean', defaultValue: 'false' },

	// widget design
	design: { type: 'Shell/Design/Widget/Checkbox' },

	// style facets
	facets: { type: 'Shell/Facet/List' },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types' ] },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};

import { Self as Hover          } from '{Shell/Result/Hover}';
import { Self as DesignCheckbox } from '{Shell/Design/Widget/Checkbox}';
import { Self as ListGlint      } from '{list@<gleam:Glint/Types}';
import { Self as Shell          } from '{Shell/Self}';

/*
| Creates an actual widget from a design.
|
| ~design:      widget design
| ~light:       true if light color scheme
| ~traceV:      traceV of the widget
| ~resolution:  current screen resolution
| ~systemFocus: true if having the system focus
| ~transform:   transform of the widget
*/
def.static.FromDesign =
	function( design, light, traceV, resolution, systemFocus, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 6 ) throw new Error( );
/**/	if( design.ti2ctype !== DesignCheckbox ) throw new Error( );
/**/}

	return(
		Self.create(
			'design',      design,
			'facets',      design.facets,
			'resolution',  resolution,
			'systemFocus', systemFocus,
			'transform',   transform,
			'traceV',      traceV,
			'zone',        design.zone,
		)
	);
};

/*
| Checkboxes are focusable.
*/
def.proto.focusable = true;

/*
| The widget's glint.
*/
def.lazy.glint =
	function( )
{
	if( !this.visible ) return undefined;

	const facet =
		this.facets.getFacet(
			'hover', !!this.hover,
			'focus', !!this.mark && this.systemFocus,
		);

	const list = [ facet.glint( this._tZone )];

	if( this.checked )
	{
		list.push( this.design.iconChecked.facet.glint( this._iconCheckedT ) );
	}

	return ListGlint.Array( list );
};

/*
| Toggles the checkbox.
*/
def.proto.toggle =
	function( )
{
	Shell.toggleCheckbox( this.traceV );
};

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	if( !this.visible ) return undefined;

	if( this._tZone.within( p ) )
	{
		this.toggle( );
		return false;
	}
	else
	{
		return undefined;
	}
};

/*
| Any normal key for a checkbox triggers it to flip
*/
def.proto.input =
	function( string )
{
	this.toggle( );
	return true;
};

/*
| Mouse hover.
*/
def.proto.pointingHover =
	function( p )
{
	if( !this.visible || !this._tZone.within( p ) ) return undefined;

	return Hover.CursorPointer.create( 'traceV', this.traceV );
};

/*
| Special keys for buttons having focus
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	switch( key )
	{
		case 'down':
			Shell.cycleFocus( 1 );
			return;

		case 'up':
			Shell.cycleFocus( -1 );
			return;

		case 'enter':
			this.toggle( );
			return;
	}
};

/*
| The check icon of the check box.
*/
def.lazy._iconCheckedT =
	function( )
{
	return(
		this.design.iconChecked.shape
		.transform( this.transform.setOffset( this._tZone.pc ) )
	);
};

/*
| The transformed zone.
*/
def.lazy._tZone =
	function( )
{
	return this.zone.transform( this.transform );
};
