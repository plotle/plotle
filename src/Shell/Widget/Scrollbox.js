/*
| A scrollbox.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// component hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// scroll position
	scrollPos: { type: 'gleam:Point' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// the widgets
	widgets: { type: 'twig@<Shell/Widget/Types' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};

import { Self as DesignScrollbar } from '{Shell/Design/Scrollbar}';
import { Self as DesignScrollbox } from '{Shell/Design/Widget/Scrollbox}';
import { Self as GlintPane       } from '{gleam:Glint/Pane}';
import { Self as GlintTransform  } from '{gleam:Glint/Transform}';
import { Self as GlintWindow     } from '{gleam:Glint/Window}';
import { Self as ListGlint       } from '{list@<gleam:Glint/Types}';
import { Self as Point           } from '{gleam:Point}';
import { Self as SeamFormRoot    } from '{Shell/Seam/Form/Root}';
import { Self as Shell           } from '{Shell/Self}';
import { Self as Size            } from '{gleam:Size}';
import { Self as TransformNormal } from '{gleam:Transform/Normal}';
import { Self as TwigWidget      } from '{twig@<Shell/Widget/Types}';
import { Self as WidgetBase      } from '{Shell/Widget/Base}';
import { Self as WidgetScrollbar } from '{Shell/Widget/Scrollbar}';

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	p =
		Point.XY(
			p.x - this._zone.pos.x + this.scrollPos.x,
			p.y - this._zone.pos.y + this.scrollPos.y
		);

	for( let widget of this.widgets )
	{
		const bubble = widget.click( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}
	// otherwise
	return undefined;
};

/*
| Starts an operation with the pointing device held down.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const sbary = this.scrollbarY;

	if( sbary )
	{
		const bubble = sbary.dragStart( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	p =
		Point.XY(
			p.x - this._zone.pos.x + this.scrollPos.x,
			p.y - this._zone.pos.y + this.scrollPos.y
		);

	for( let widget of this.widgets )
	{
		const bubble = widget.click( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}
	// otherwise
	return undefined;
};

/*
| Returns a fixed scrollPos if current is out of bonds.
| Used by parents transformative getter.
*/
/*
def.lazy.fixScrollPos =
	function( )
{
	const y = this.scrollPos.y;
	const maxY = this.innerSize.height - this.zone.height;
	if( maxY < 0 ) maxY = 0;
	if( y <= maxY ) return pass;
	return this.scrollPos.create( 'y', maxY );
};
*/

/*
| Creates an actual widget from a design.
|
| ~design:      widget design
| ~light:       true if light color scheme
| ~traceV:      traceV of the widget
| ~resolution:  current screen resolution
| ~systemFocus: true if having the system focus
| ~transform:   transform of the widget
*/
def.static.FromDesign =
	function( design, light, traceV, resolution, systemFocus, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 6 ) throw new Error( );
/**/	if( design.ti2ctype !== DesignScrollbox ) throw new Error( );
/**/}

	const twig = { };
	for( let wKey of design.widgets.keys )
	{
		twig[ wKey ] =
			WidgetBase.FromDesign(
				design.widgets.get( wKey ),
				traceV.add( 'widgets', wKey ),
				resolution,
				systemFocus,
				transform,
			);
	}

	return(
		Self.create(
			'light',       light,
			'resolution',  resolution,
			'scrollPos',   Point.zero,
			'systemFocus', systemFocus,
			'transform',   transform,
			'traceV',      traceV,
			'widgets',     TwigWidget.create( 'twig:init', twig, design.widgets.keys ),
			'zone',        design.zone,
		)
	);
};

/*
| The widget's glint.
*/
def.lazy.glint =
	function( )
{
	const widgets = this.widgets;
	const list = [ ];
	for( let r = widgets.length - 1; r >= 0; r-- )
	{
		const w = widgets.atRank( r );
		const sg = w.glint;
		if( sg ) list.push( sg );
	}

	const zone = this._zone;
	let glint =
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint',
						// TODO this is an akward hack to make it appear correctly.
						GlintTransform.create(
							'transform',
								TransformNormal.setOffset(
									Point.XY(
										0,
										-this.scrollPos.y * this.transform.scale
										* this.resolution.superSampling,
									)
								),
							'glint', ListGlint.Array( list )
						),
					'resolution', this.resolution,
					'size', zone.size,
				),
			'pos', zone.pos
		);

	const sbary = this.scrollbarY;
	if( sbary ) glint = ListGlint.Elements( glint, sbary.glint );

	return glint;
};

/*
| Is true when the scrollbox has a vertical bar.
*/
def.lazy.hasScrollbarY =
	function( )
{
	return this.innerSize.height > this.zone.height;
};

/*
| The widget's inner height and width.
*/
def.lazy.innerSize =
	function( )
{
	let h = 0, w = 0;
	for( let r = this.widgets.length - 1; r >= 0; r-- )
	{
		const widget = this.widgets.atRank( r );
		const pse = widget.zone.pse;
		if( pse.x > w ) w = pse.x;
		if( pse.y > h ) h = pse.y;
	}

	return Size.WH( w, h );
};

/*
| Mouse wheel is being turned.
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	if( !this._zone.within( p ) ) return undefined;

	p =
		Point.XY(
			p.x - this._zone.pos.x + this.scrollPos.x,
			p.y - this._zone.pos.y + this.scrollPos.y
		);

	for( let widget of this.widgets )
	{
		const bubble = widget.mousewheel( p, dir, shift, ctrl );
		if( bubble ) return bubble;
	}

	let y = this.scrollPos.y - dir * config.textWheelSpeed;

	if( y < 0 ) y = 0;

	// TODO: this is a dirty workaround
	//       make a traceSeam for the widget.
	const formName = this.traceV.forward( 'form' ).last.key;
	Shell.alter(
		SeamFormRoot.traceSeam.add( formName ).add( 'scrollPos' ),
		this.scrollPos.create( 'y', y )
	);

	return true;
};

/*
| User is hovering his/her pointer (mouse add).
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const sbary = this.scrollbarY;
	if( sbary )
	{
		const bubble = sbary.pointingHover( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	p =
		Point.XY(
			p.x - this._zone.pos.x + this.scrollPos.x,
			p.y - this._zone.pos.y + this.scrollPos.y
		);

	for( let widget of this.widgets )
	{
		const bubble = widget.pointingHover( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	// otherwise
	return undefined;
};

/*
| Prepares the scroll position to fit innerSize/zone parameters
*/
def.static.prepareScrollPos =
	function( scrollPos, innerHeight, zone )
{
	if( scrollPos === undefined || innerHeight <= zone.height ) return Point.zero;

	if( scrollPos.x < 0 || scrollPos.y < 0 )
	{
		scrollPos =
			scrollPos.XY(
				Math.max( 0, this.scrollPos.x ),
				Math.max( 0, this.scrollPos.y )
			);
	}

	if( innerHeight > zone.height
		&& scrollPos.y > innerHeight - zone.height
	)
	{
		scrollPos = scrollPos.create( 'y', innerHeight - zone.height );
	}

	return scrollPos;
};

/*
| The scrollbar widgt.
*/
def.lazy.scrollbarY =
	function( )
{
	if( !this.hasScrollbarY )
	{
		return undefined;
	}

	const innerSize = this.innerSize;
	const zone = this.zone;

	return(
		WidgetScrollbar.create(
			'anchorAlign', 'right',
			'aperture',     zone.height,
			'ellipseA',     DesignScrollbar.ellipseA,
			'ellipseB',     DesignScrollbar.ellipseB,
			'facet',        DesignScrollbar.facet( this.light ),
			'max',          innerSize.height,
			'minHeight',    DesignScrollbar.minHeight,
			'pos',          zone.pos.add( zone.width, 0 ),
			'resolution',   this.resolution,
			'scrollPos',    this.scrollPos.y,
			'size',         zone.height,
			'strength',     DesignScrollbar.strength,
			'transform',    this.transform,
			'traceV',       this.traceV.add( 'widgets', 'scrollbarY' ),
		)
	);
};

/*
| Exta checking
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const sp = this.scrollPos;
/**/	if( sp.x < 0 || sp.y < 0 ) throw new Error( );
/**/}
};

/*
| The transformed zone.
*/
def.lazy._zone =
	function( )
{
	return this.zone.transform( this.transform );
};
