/*
| A board widget is just an empty slab.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// style facets
	facets: { type: 'Shell/Facet/List' },

	// shape of the board
	shape:
	{
		type:
		[
			'gleam:Ellipse',
			'gleam:RectRound',
			'gleam:RectRoundExt',
			'string',
		],
	},

	zone: { type: 'gleam:Rect' },
};

import { Self as Ellipse     } from '{gleam:Ellipse}';
import { Self as DesignBoard } from '{Shell/Design/Widget/Board}';
import { Self as Point       } from '{gleam:Point}';
import { Self as RectRound   } from '{gleam:RectRound}';

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	return undefined;
};

/*
| Start an operation with the poiting device button held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	return undefined;
};

/*
| Stops an operation with the poiting device button held down.
*/
def.proto.dragStop =
	function( )
{
	// nothing
};

/*
| Boards are not focusable.
*/
def.proto.focusable = false;

/*
| Creates an actual widget from a design.
|
| ~design:      widget design
| ~light:       true if light color scheme
| ~traceV:      traceV of the widget
| ~resolution:  current screen resolution
| ~systemFocus: true if having the system focus
| ~transform:   transform of the widget
*/
def.static.FromDesign =
	function( design, light, traceV, resolution, systemFocus, transform )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 6 ) throw new Error( );
/**/	if( design.ti2ctype !== DesignBoard ) throw new Error( );
/**/}

	return(
		Self.create(
			'facets',     design.facets,
			'resolution', resolution,
			'shape',      design.shape,
			'traceV',     traceV,
			'transform',  transform,
			'visible',    design.visible,
			'zone',       design.zone,
		)
	);
};

/*
| The widget's glint.
*/
def.lazy.glint =
	function( )
{
	if( !this.visible ) return undefined;

	const facet = this.facets.getFacet( );
	return facet.glint( this._tzShape.add( this._tZone.pos ) );
};


/*
| Any normal key for a button having focus triggers a push.
*/
def.proto.input =
	function( string )
{
	return true;
};

/*
| Mouse wheel is being turned.
*/
def.proto.mousewheel =
	function( p, shift, ctrl )
{
	// nothing
};

/*
| Mouse hover.
*/
def.proto.pointingHover =
	function( p )
{
	return undefined;
//	if( !this.within( p ) ) return undefined;
//
//	return Hover.CursorPointer.create( 'traceV', this.traceV );
};

/*
| Special keys for buttons having focus
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	/*
	switch( key )
	{
		case 'down':
			Root.cycleFocus( 1 );
			return;

		case 'up':
			Root.cycleFocus( -1 );
			return;

		case 'enter':
			Root.pushButton( this.traceV, false, false );
			return;
	}
	*/
};

/*
| Returns true if p is within the board.
*/
def.proto.within =
	function( p )
{
	return(
		this.visible
		&& this._tZone.within( p )
		&& this._tzShape.within( p.sub( this._tZone.pos ) )
	);
};

/*
| The transformed zone of the button.
*/
def.lazy._tZone =
	function( )
{
	return this.zone.transform( this.transform );
};

/*
| The shape of the button
| transformed and zero positioned.
*/
def.lazy._tzShape =
	function( )
{
	switch( this.shape )
	{
		case 'Ellipse':
		{
			const tZone = this._tZone;
			return Ellipse.PosWidthHeight( Point.zero, tZone.width - 1, tZone.height - 1 );
		}

		case 'RectRound':
		{
			const tZone = this._tZone;
			const ab = ( tZone.height - 1 ) / 3;
			return(
				RectRound.create(
					'pos', Point.zero,
					'width', tZone.width - 1,
					'height', tZone.height - 1,
					'a', ab,
					'b', ab
				)
			);
		}

		default:
		{
			return this.shape.transform( this.transform.scaleOnly );
		}
	}
};
