/*
| Abstract parent of widgets.
*/
def.abstract = true;

def.attributes =
{
	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// trace of the widget
	traceV: { type: 'ti2c:Trace' },

	// transform
	transform: { type: [ '<gleam:Transform/Types' ] },

	// if false the widget is hidden
	visible: { type: 'boolean', defaultValue: 'true' },
};

import { Self as DesignBoard     } from '{Shell/Design/Widget/Board}';
import { Self as DesignButton    } from '{Shell/Design/Widget/Button}';
import { Self as DesignCheckbox  } from '{Shell/Design/Widget/Checkbox}';
import { Self as DesignInput     } from '{Shell/Design/Widget/Input}';
import { Self as DesignLabel     } from '{Shell/Design/Widget/Label}';
import { Self as DesignScrollbox } from '{Shell/Design/Widget/Scrollbox}';
import { Self as DesignSelect    } from '{Shell/Design/Widget/Select}';
import { Self as SysMode         } from '{Shell/Result/SysMode}';
import { Self as WidgetBoard     } from '{Shell/Widget/Board}';
import { Self as WidgetButton    } from '{Shell/Widget/Button}';
import { Self as WidgetCheckbox  } from '{Shell/Widget/Checkbox}';
import { Self as WidgetInput     } from '{Shell/Widget/Input}';
import { Self as WidgetLabel     } from '{Shell/Widget/Label}';
import { Self as WidgetScrollbox } from '{Shell/Widget/Scrollbox}';
import { Self as WidgetSelect    } from '{Shell/Widget/Select}';

/*
| Widgets default don't have carets.
*/
def.proto.caretable = false;

/*
| Handles a potential dragStart event.
|
| ~p:     point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl(or meta) was held down
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );
};

/*
| Widgets are by default not focusable.
*/
def.proto.focusable = false;

/*
| Creates an actual widget from a design.
|
| ~design:      widget design
| ~light:       true if light color scheme
| ~trace:       trace of the widget
| ~resolution:  current screen resolution
| ~systemFocus: currently have systemFocus (show Caret)
| ~transform:   transform to apply
*/
def.static.FromDesign =
	function( design, light, trace, resolution, systemFocus, transform )
{
/**/if( CHECK && arguments.length !== 6 ) throw new Error( );

	const widget = Self._designMap.get( design.ti2ctype );

	return widget.FromDesign( design, light, trace, resolution, systemFocus, transform );
};

/*
| Mouse wheel is being turned.
|
| ~p:     screen position of the wheel event
| ~dir:   direction of the wheel
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl(or meta) was held down
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	// default to nothing
};

/*
| SysMode, see shell/root.
*/
def.lazy.sysMode =
	( ) =>
	SysMode.Blank;

/*
| Mapping of widget designs to widget objects.
*/
def.staticLazy._designMap =
	function( )
{
	const map = new Map( );
	map.set( DesignBoard,     WidgetBoard     );
	map.set( DesignButton,    WidgetButton    );
	map.set( DesignCheckbox,  WidgetCheckbox  );
	map.set( DesignInput,     WidgetInput     );
	map.set( DesignLabel,     WidgetLabel     );
	map.set( DesignScrollbox, WidgetScrollbox );
	map.set( DesignSelect,    WidgetSelect    );
	return Object.freeze( map );
};
