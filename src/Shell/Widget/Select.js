/*
| A select field.
*/
def.extend = 'Shell/Widget/Base';

def.attributes =
{
	// widget design, XXX remove
	design: { type: 'Shell/Design/Widget/Select' },

	// style facets
	facets: { type: 'Shell/Facet/List' },

	// colorized font
	fontColor: { type: 'gleam:Font/Color' },

	// if defined highlighting this option in the popup
	// if undefined highlights the current value
	highlight: { type: [ 'undefined', 'number' ] },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// true if open
	open: { type: 'boolean' },

	// the selectable options
	options: { type: 'list@string' },

	// if true the input is readonly.
	readonly: { type: 'boolean', defaultValue: 'false' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// seam trace
	traceSeam: { type: [ 'undefined', 'ti2c:Trace' ] },

	// the selected value
	value: { type: 'number' },

	// designed zone
	zone: { type: 'gleam:Rect' },
};

import { Self as Color        } from '{gleam:Color}';
import { Self as Font         } from '{gleam:Font/Root}';
import { Self as GlintFigure  } from '{gleam:Glint/Figure}';
import { Self as GlintPane    } from '{gleam:Glint/Pane}';
import { Self as GlintString  } from '{gleam:Glint/String}';
import { Self as GlintWindow  } from '{gleam:Glint/Window}';
import { Self as Hover        } from '{Shell/Result/Hover}';
import { Self as DesignSelect } from '{Shell/Design/Widget/Select}';
import { Self as ListGlint    } from '{list@<gleam:Glint/Types}';
import { Self as ListString   } from '{list@string}';
import { Self as MarkWidget   } from '{Shared/Mark/Widget}';
import { Self as Path         } from '{gleam:Path}';
import { Self as Point        } from '{gleam:Point}';
import { Self as Rect         } from '{gleam:Rect}';
import { Self as RectRound    } from '{gleam:RectRound}';
import { Self as RectRoundExt } from '{gleam:RectRoundExt}';
import { Self as Shell        } from '{Shell/Self}';
import { Self as SysMode      } from '{Shell/Result/SysMode}';

/*
| User clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	const zoneT = this._zoneT;

	if( zoneT.within( p ) )
	{
		const pp = p.sub( zoneT.pos );
		if( this._tzShape.within( pp ) )
		{
			if( this.readonly ) return false;

			Shell.alter(
				this.traceSeam.add( 'open' ), !this.open,
				'mark', MarkWidget.TraceV( this.traceV ),
			);
			return true;
		}
	}

	const zoneOptionsT = this._zoneOptionsT;
	if( zoneOptionsT.within( p ) )
	{
		const line = Math.floor( ( p.y - zoneOptionsT.pos.y ) / this._lineHeightOptionsT );

		// close the options window if something was clicked
		Shell.alter(
			this.traceSeam.add( 'open' ), !this.open,
			'mark', MarkWidget.TraceV( this.traceV ),
		);

		if( line !== this.value )
		{
			Shell.widgetSelectChanged( this.traceV, line );
		}
		return true;
	}

	return undefined;
};

/*
| Creates an actual widget from a design.
|
| ~design:      widget design
| ~light:       true if light color scheme
| ~traceV:      traceV of the widget
| ~resolution:  current screen resolution
| ~systemFocus: true if having the system focus
| ~transform:   transform of the widget
*/
def.static.FromDesign =
	function( design, light, traceV, resolution, systemFocus, transform, )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 6 ) throw new Error( );
/**/	if( design.ti2ctype !== DesignSelect ) throw new Error( );
/**/}

	return(
		Self.create(
			'design',       design,
			'facets',       design.facets,
			'fontColor',    design.font,
			'open',         false,
			'options',      ListString.Elements( '' ),
			'resolution',   resolution,
			'systemFocus',  systemFocus,
			'transform',    transform,
			'value',        0,
			'traceV',       traceV,
			'zone',         design.zone,
		)
	);
};

/*
| Inputs are focusable
*/
def.proto.focusable = true;

/*
| The widget's glint.
*/
def.lazy.glint =
	function( )
{
	if( !this.visible ) return undefined;

	const zone = this._zoneT;
	const zoneE1 = zone.envelope( 1 );
	const list = [ ];
	const resolution = this.resolution;

	list.push(
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint', this._glintInner,
					'resolution', resolution,
					'size', zoneE1.size,
				),
			'pos', zoneE1.pos
		)
	);

	if( this.open )
	{
		list.push(
			GlintWindow.create(
				'pane', this._paneOptions,
				'pos', this._zoneOptionsT.pos,
			),
		);
	}

	return ListGlint.Array( list );
};

/*
| User input.
|
| ~string: string inputed
*/
def.proto.input =
	function( string )
{
	/*
	const mark = this.mark;
	const value = this.value;
	const at = mark.traceVCaret.last.at;
	const maxlen = this.maxlen;
	// cuts of string if larger than this maxlen
	if( maxlen > 0 && value.length + value.length > maxlen )
	{
		string = string.substring( 0, maxlen - value.length );
	}
	*/
};
/*
| Default distance of characters.
*/
def.staticLazy.pitch =
	( ) =>
	Point.XY( 8, 3 );

/*
| Pointing hover.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	if(
		this._zoneT.within( p )
		&& this._tzShape.within( p.sub( this._zoneT.pos ) )
	)
	{
		return Hover.CursorDefault.create( 'traceV', this.traceV );
	}

	const zoneOptionsT = this._zoneOptionsT;
	if( this.open && zoneOptionsT.within( p ) )
	{
		const line = Math.floor( ( p.y - zoneOptionsT.pos.y ) / this._lineHeightOptionsT );
		return Hover.CursorDefault.create( 'traceV', this.traceV.add( 'options', line ) );
	}

	return undefined;
};

/*
| User pressed a special key
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	switch( key )
	{
		case 'down':
			this._keyDown( );
			return;

		case 'up':
			this._keyUp( );
			return;
	}
};

/*
| SysMode, see shell/root.
*/
def.lazy.sysMode =
	function( )
{
	const mark = this.mark;
	if( !mark )
	{
		return SysMode.Blank;
	}

	return SysMode.create( 'attention', this._zoneT.pc.y );
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const traceV = this.traceV;
/**/
/**/	// if hover is defined it has to affect this widget.
/**/	const hover = this.hover;
/**/	if( hover && !hover.hasTrace( traceV ) ) throw new Error( );
/**/
/**/	// if mark is defined it has to affect this widget.
/**/	const mark = this.mark;
/**/	if( mark && !mark.encompasses( traceV ) ) throw new Error( );
/**/}
};

/*
| The widget's facet.
*/
def.lazy._facet =
	function( )
{
	return(
		this.facets.getFacet(
			'hover', !!this.hover,
			'focus', !!this.mark && this.systemFocus && !this.open
		)
	);
};

/*
| Transformed font size.
*/
def.lazy._fontColor =
	function( )
{
	return this.fontColor.transform( this.transform );
};

/*
| Returns the inner glint of the widget.
*/
def.lazy._glintInner =
	function( )
{
	const design     = this.design;
	const pitch      = this._pitchT;
	const value      = this.value;
	const options    = this.options;
	const resolution = this.resolution;
	const list       = [ this._facet.glintFill( this._tzShape ) ];
	const fontColor  = this._fontColor;
	const string     = options.get( value );

	list.push(
		GlintString.create(
			'fontColor', fontColor,
			'resolution', resolution,
			'string', string,
			'p', Point.XY( pitch.x, fontColor.size + pitch.y )
		)
	);

	{
		// makes the arrow on right side
		const tZone0 = this._zoneT.zeroPos;
		const t = this.transform;
		const dx = t.d( 5 );
		const dy = t.d( 5 );

		const pArrowCenter = tZone0.pe.add( -t.d( 15 ), dy / 2 );

		list.push(
			// XXX
			design.facetArrow.glint(
				Path.Plan(
					'pc',    pArrowCenter,
					'start', pArrowCenter.add( -dx, -dy ),
					'line',  pArrowCenter,
					'line',  pArrowCenter.add(  dx, -dy ),
					'line',  'close',
				),
				true, // nogrid
			),
		);
	}

	list.push( this._facet.glintBorder( this._tzShape ) );
	return ListGlint.Array( list );
};

/*
| User pressed up key.
*/
def.proto._keyUp =
	function( )
{
	// nada
};

/*
| They line height in the options popup.
*/
def.lazy._lineHeightOptionsT =
	function( )
{
	return this._fontColor.size * ( 1 + Font.bottomBox );
};

/*
| Returns the inner glint of the options popup.
*/
def.lazy._paneOptions =
	function( )
{
	const design     = this.design;
	const zone       = this._zoneOptionsT;
	const options    = this.options;
	const pitchT     = this._pitchT;
	const resolution = this.resolution;
	const fontColor  = this._fontColor;

	const lineHeightT = this._lineHeightOptionsT;
	let highlight = this.value;
	const hover = this.hover;
	if( hover && hover.last.name === 'options' )
	{
		highlight = hover.last.at;
	}

	const list = [ ];
	list.push(
		design.facetOptions.glint(
			Rect.PosSize( Point.zero, zone.size )
		)
	);

	let p = Point.XY( pitchT.x, fontColor.size );
	for( let a = 0, alen = options.length; a < alen; a++ )
	{
		if( a === highlight )
		{
			list.push(
				GlintFigure.create(
					'fill', Color.RGB( 170, 170, 255 ),
					'figure',
						Rect.PosWidthHeight(
							Point.XY( 1, p.y - fontColor.size ),
							zone.width - 1, lineHeightT
						),
				)
			);
		}

		list.push(
			GlintString.create(
				'fontColor',  fontColor,
				'p',          p,
				'resolution', resolution,
				'string',     options.get( a ),
			)
		);
		p = p.add( 0, lineHeightT );
	}

	return(
		GlintPane.create(
			'glint', ListGlint.Array( list ),
			'resolution', this.resolution,
			'size', zone.size.add( 2 ),
		)
	);
};


/*
| Transformed pitch.
*/
def.lazy._pitchT =
	function( )
{
	const s = this.transform.scale;
	const p = Self.pitch;

	return Point.XY( p.x * s, p.y * s );
};

/*
| The transformed shape of the button
| positioned at zero.
*/
def.lazy._tzShape =
	function( )
{
	const tZone = this._zoneT;
	const t = this.transform;

	if( this.open )
	{
		return(
			RectRoundExt.PosWidthHeightHVNeNw(
				Point.zero,
				tZone.width, tZone.height,
				7 * t.scale, 3 * t.scale,
			)
		);
	}
	else
	{
		return(
			RectRound
			.create(
				'pos', Point.zero,
				'width', tZone.width,
				'height', tZone.height,
				'a', 7 * t.scale,
				'b', 3 * t.scale,
			)
		);
	}
};

/*
| The transformed zone of the options popup.
*/
def.lazy._zoneOptionsT =
	function( )
{
	const tZone = this._zoneT;
	return(
		Rect.PosWidthHeight(
			this._zoneT.psw.add( -1, -1 ),
			tZone.width,
			this.options.length * this._lineHeightOptionsT
		)
	);
};

/*
| The transformed zone of the button.
*/
def.lazy._zoneT =
	function( )
{
	return this.zone.transform( this.transform );
};
