/*
| The user is moving a document handle.
*/
def.extend = 'Shell/Action/Base';

def.attributes =
{
	// the visual item trace hovered upon.
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// if set this joint will be removed from the path.
	removing: { type: [ 'undefined', 'number' ] },

	// the joint number of DocPath being moved.
	jointNr: { type: 'number' },

	// move to this point.
	pointMove: { type: [ 'undefined', 'gleam:Point' ] },

	// mouse down point on drag creation.
	pointStart: { type: 'gleam:Point' },

	// the document to modify.
	vDoc: { type: 'Shell/View/Space/Item/DocPath' },

	// the joint being inserted.
	_insertItemJoint: { type: [ 'undefined', 'Shared/Fabric/Item/DocPath/Joint/Self' ] },
};

import { Self as ChangeListMove     } from '{ot:Change/List/Move}';
import { Self as FabricDocPathJoint } from '{Shared/Fabric/Item/DocPath/Joint/Self}';
import { Self as Sequence           } from '{ot:Change/Sequence}';

/*
| The changes this action applies onto the fabric tree.
|
| ~transient: if true includes transient changes.
*/
def.lazyFunc.changes =
	function( transient )
{
	const vDoc = this.vDoc;
	const removing = this.removing;
	const list = [ ];

	if( removing !== undefined )
	{
		list.push(
			ChangeListMove.create(
				'traceFrom', vDoc.ancillary.trace.add( 'joints', removing ),
				'val', vDoc.ancillary.joints.get( removing ).asFabric,
			)
		);
	}

	const iij = this._insertItemJoint;
	if( iij )
	{
		list.push(
			ChangeListMove.create(
				'traceTo', vDoc.ancillary.trace.add( 'joints', this.jointNr ),
				'val', iij,
			)
		);
	}
	else if( transient )
	{
		const mp = this.pointMove;
		if( mp )
		{
			list.push(
				ChangeListMove.create(
					'traceTo', vDoc.ancillary.trace.add( 'joints', this.jointNr ),
					'val', FabricDocPathJoint.create( 'pos', mp ),
				)
			);
		}
	}

	return Sequence.Array( list );
};

/*
| The changes this action applies on the fabric tree.
*/
def.lazy.changesTransient =
	function( )
{
	return this.changes( true );
};

