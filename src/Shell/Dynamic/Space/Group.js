/*
| A group of space dynamics.
*/
def.extend = 'group@Shell/Dynamic/Space/Self';

import { Self as ASpace            } from '{Shared/Ancillary/Space}';
import { Self as GroupASpace       } from '{group@Shared/Ancillary/Space}';
import { Self as GroupFabricSpace  } from '{group@Shared/Fabric/Space}';

/*
| The locals as ancillaries.
*/
def.lazy.ancillaries =
	function( )
{
	const locals = this.locals;

	let group = { };
	for( let uid of locals.keys )
	{
		group[ uid ] = ASpace.FromFabric( locals.get( uid ), uid );
	}

	return GroupASpace.Table( group );
};

/*
| Turns the dynamics into local stage fabrics.
*/
def.lazy.locals =
	function( )
{
	const group = { };

	for( let uid of this.keys )
	{
		const local = this.get( uid ).local;
		if( local )
		{
			group[ uid ] = local;
		}
	}

	return GroupFabricSpace.Table( group );
};
