/*
| Result of a system mode call.
|
| The system probes if it should show a keyboard.
| If yes, where the attention center should be.
| And possible when input mode (e.g. password) it should be.
*/
def.attributes =
{
	// the attention offset from top
	attention: { type: [ 'undefined', 'number' ] },

	// if true show a keyboard
	keyboard: { type: 'boolean', defaultValue: 'false' },

	// if true put the input into password mode
	password: { type: 'boolean', defaultValue: 'false' },
};

/*
| Shortcut.
*/
def.static.Keyboard =
	( attention ) =>
	Self.create(
		'attention', attention,
		'keyboard', true,
	);

/*
| Shortcut.
*/
def.staticLazy.Blank =
	( ) =>
	Self.create( );

