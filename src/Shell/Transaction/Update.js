/*
| Requesting updates on dynamics.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/Update' },
};

import { Self as Plan          } from '{Shared/Trace/Plan}';
import { Self as ReplyError    } from '{Shared/Reply/Error}';
import { Self as ReplyUpdate   } from '{Shared/Reply/Update}';
import { Self as RequestUpdate } from '{Shared/Request/Update}';
import { Self as UserCreds     } from '{Shared/User/Creds}';
import { Self as ListMoment    } from '{list@<Shared/Dynamic/Moment/Types}';

/*
| Starts a transaction.
*/
def.static.start =
	function( userCreds, moments )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( moments.ti2ctype !== ListMoment ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestUpdate.create(
					'userCreds', userCreds,
					'moments', moments,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplyUpdate.FromJson( json, Plan.update );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};

