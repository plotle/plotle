/*
| Requesting for a user to change their password.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/ChangePwd' },
};

import { Self as ReplyChangePwd   } from '{Shared/Reply/ChangePwd}';
import { Self as ReplyError       } from '{Shared/Reply/Error}';
import { Self as RequestChangePwd } from '{Shared/Request/ChangePwd}';
import { Self as UserCreds        } from '{Shared/User/Creds}';

/*
| Starts a transaction.
*/
def.static.start =
	function( userCreds, passhashNew )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( typeof( passhashNew ) !== 'string' ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestChangePwd.create(
					'userCreds', userCreds,
					'passhashNew', passhashNew,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplyChangePwd.FromJson( json );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};
