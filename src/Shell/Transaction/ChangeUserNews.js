/*
| Requesting for a user to change the newsletter opt-in.
*/
def.extend = 'Shell/Transaction/Base';

def.attributes =
{
	// the request shared between shell and server
	request: { type: 'Shared/Request/ChangeUserNews' },
};

import { Self as ReplyChangeUserNews   } from '{Shared/Reply/ChangeUserNews}';
import { Self as ReplyError            } from '{Shared/Reply/Error}';
import { Self as RequestChangeUserNews } from '{Shared/Request/ChangeUserNews}';
import { Self as UserCreds             } from '{Shared/User/Creds}';

/*
| Starts a transaction.
*/
def.static.start =
	function( userCreds, newsletter )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( userCreds.ti2ctype !== UserCreds ) throw new Error( );
/**/	if( typeof( newsletter ) !== 'boolean' ) throw new Error( );
/**/}

	const t = (
		Self.create(
			'request',
				RequestChangeUserNews.create(
					'userCreds',  userCreds,
					'newsletter', newsletter,
				),
		)
	);
	t._send( );
	return t;
};

/*
| Converts a json reply to a ti2c object.
*/
def.proto._replyJson2Ti2c =
	function( json )
{
	try
	{
		return ReplyChangeUserNews.FromJson( json );
	}
	catch( e )
	{
		return ReplyError.Message( 'received invalid reply' );
	}
};
