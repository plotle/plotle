/*
| An ongoing transaction.
*/
def.attributes =
{
	// the mutable data
	_mutable: { type: 'protean', defaultValue: '{ }' },
};

def.abstract = true;

import { Self as ReplyError } from '{Shared/Reply/Error}';
import { Self as Shell      } from '{Shell/Self}';
import { Self as System     } from '{Shell/System}';

/*
| Aborts the request if it is active.
|
| Returns true if the request has been aborted.
*/
def.proto.abort =
	function( )
{
	const mutable = this._mutable;

	if( !mutable.aborted )
	{
		mutable.aborted = true;
		mutable.controller.abort( );
		return true;
	}
	else
	{
		return false;
	}
};

/*
| A request has reponded.
*/
def.static._gotResponse =
	async function( response )
{
	const mutable = this._mutable;
	if( mutable.aborted ) return;

	if( response.status !== 200 )
	{
		Shell.onTransactionReply( this, ReplyError.Message( 'Lost server connection' ) );
	}

	let json;
	try
	{
		json = await response.json( );
	}
	catch( e )
	{
		Shell.onTransactionReply( this, ReplyError.Message( 'Server answered no json!' ) );
		return;
	}

	if( json.$type === ReplyError.$type )
	{
		Shell.onTransactionReply( this, ReplyError.FromJson( json ) );
	}
	else
	{
		Shell.onTransactionReply( this, this._replyJson2Ti2c( json ) );
	}
};

def.static._fetchFail =
	function( )
{
	const mutable = this._mutable;
	if( mutable.aborted ) return;
	System.failScreen( 'Lost server connection' );
};

/*
| Sends the request.
*/
def.proto._send =
	function( )
{
	const mutable = this._mutable;

/**/if( CHECK && mutable.controller ) throw new Error( );

	const controller = mutable.controller = new  AbortController( );

	fetch(
		'/x',
		{
			method: 'POST',
			headers:
			{
				'Content-Type': 'application/json',
			},
			body: this.request.jsonfy( ),
			signal: controller.signal,
		},
	)
	.then( ( response ) => Self._transmitterGotResponse.call( this, response ) )
	.catch( ( err ) => Self._transmitterFetchFail.call( this ) );
};

/*
| The error catcher for onReply events.
*/
def.staticLazy._transmitterGotResponse =
	( ) =>
	System.transmitter( Self._gotResponse, true );

/*
| The error catcher for onReply events.
*/
def.staticLazy._transmitterFetchFail =
	( ) =>
	System.transmitter( Self._fetchFail, true );
