/*
| Data to be jsonfied into clipboard.
*/
def.attributes =
{
	// the to be pasted items
	items: { type: [ 'undefined', 'twig@<Shared/Fabric/Item/Types' ], json: true },

	// center of the copy/paste
	center: { type: 'gleam:Point', json: true },
};

def.fromJsonArgs = [ 'plan' ];
def.json = true;

import { Self as GroupString } from '{group@string}';
import { Self as Uid         } from '{Shared/Session/Uid}';

/*
| Creates a mapping of keys to new uids.
*/
def.proto.newUidsMapping =
	function( )
{
	const jKeys = this.items.keys;
	const group = { };
	for( let key of jKeys )
	{
		group[ key ] = Uid.newUid( );
	}
	return GroupString.Table( group );
};
