/*
| A paragraphs pane is independend of its position.
*/
def.attributes =
{
	// the flow
	flow: { type: 'Shared/Flow/Block' },

	// the font
	fontColor: { type: 'gleam:Font/Color' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// scaling of the para
	scale: { type: [ 'gleam:Transform/Normal', 'gleam:Transform/Scale' ] },
};

import { Self as GlintPane   } from '{gleam:Glint/Pane}';
import { Self as GlintString } from '{gleam:Glint/String}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';
import { Self as Font        } from '{gleam:Font/Root}';
import { Self as Point       } from '{gleam:Point}';
import { Self as Size        } from '{gleam:Size}';

/*
| The glint of the pane.
*/
def.lazy.glint =
	function( )
{
	const flow = this.flow;
	const scale = this.scale;
	const fontColor = this.tFontColor;
	const resolution = this.resolution;

	const a = [ ];
	for( let line of flow.lines )
	{
		for( let token of line.tokens )
		{
			a.push(
				GlintString.create(
					'fontColor', fontColor,
					'resolution', resolution,
					'string', token.word,
					'p', Point.XY( token.x, line.y ).transform( scale ),
				)
			);
		}
	}

	return(
		GlintPane.create(
			'glint', ListGlint.Array( a ),
			'resolution', resolution,
			'size',
				Size.WH( flow.width, this.height )
				.transform( scale )
				.add( 1, 1 ),
		)
	);
};

/*
| The height of the para.
*/
def.lazy.height =
	function( )
{
	return(
		this.flow.height
		+ Math.round( this.fontColor.size * Font.bottomBox )
	);
};

/*
| The transformed font with family, size and color.
*/
def.lazy.tFontColor =
	function( )
{
	return this.fontColor.transform( this.scale );
};
