/*
| Not existing space form seam.
*/
def.attributes =
{
	// typed in confirmation
	confirmation: { type: 'string' },

	// error message
	errorMessage: { type: 'string' },
};

import { Self as SeamFormRoot } from '{Shell/Seam/Form/Root}';

/*
| Alters visual data.
*/
def.proto.alter =
	function( command, value )
{
	const base = command.get( 4 );

	switch( base.name )
	{
		case 'confirmation':
			return this.create( 'confirmation', value );

		case 'errorMessage':
			return this.create( 'errorMessage', value );

		default:
			throw new Error( );
	}
};

/*
| A clean form.
*/
def.staticLazy.Clean =
	function( )
{
	return(
		Self.create(
			'confirmation', '',
			'errorMessage', '',
		)
	);
};

/*
| Matching data trace.
*/
def.staticLazy.traceSeam =
	function( )
{
	return SeamFormRoot.traceSeam.add( 'deleteSpace' );
};

