/*
| The tab on spaces.
|
| Also doubles as zoom panel.
*/
def.extend = 'Shell/Panel/Base';

def.attributes =
{
	// # of the window this is on.
	atWindow: { type: 'number' },

	// true if light color scheme
	light: { type: 'boolean' },

	// current mode
	mode: { type: [ '<Shell/Mode/Types' ] },

	// the reference of current space
	refSpace: { type: 'Shared/Ref/Space' },

	// the window shown
	window: { type: '<Shell/Window/Types' },
};

import { Self as ActionZoomButton } from '{Shell/Action/ZoomButton}';
import { Self as Angle            } from '{gleam:Angle}';
import { Self as Color            } from '{gleam:Color}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as DesignBoard      } from '{Shell/Design/Widget/Board}';
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as Facet            } from '{Shell/Facet/Self}';
import { Self as FacetBorder      } from '{Shell/Facet/Border}';
import { Self as FacetList        } from '{Shell/Facet/List}';
import { Self as GlintPane        } from '{gleam:Glint/Pane}';
import { Self as GlintWindow      } from '{gleam:Glint/Window}';
import { Self as GroupBoolean     } from '{group@boolean}';
import { Self as IconZoom         } from '{Shell/Design/Icon/Zoom}';
import { Self as IconZoomAll      } from '{Shell/Design/Icon/ZoomAll}';
import { Self as IconZoomHome     } from '{Shell/Design/Icon/ZoomHome}';
import { Self as IconZoomIn       } from '{Shell/Design/Icon/ZoomIn}';
import { Self as IconZoomOut      } from '{Shell/Design/Icon/ZoomOut}';
import { Self as ListGlint        } from '{list@<gleam:Glint/Types}';
import { Self as ModeForm         } from '{Shell/Mode/Form}';
import { Self as ModeNormal       } from '{Shell/Mode/Normal}';
import { Self as ModeZoom         } from '{Shell/Mode/Zoom}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as RectRoundExt     } from '{gleam:RectRoundExt}';
import { Self as SeamFormSpace    } from '{Shell/Seam/Form/Space}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as Size             } from '{gleam:Size}';
import { Self as TwigWidget       } from '{twig@<Shell/Widget/Types}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';
import { Self as WidgetBase       } from '{Shell/Widget/Base}';
import { Self as WidgetBoard      } from '{Shell/Widget/Board}';
import { Self as WidgetButton     } from '{Shell/Widget/Button}';

/*
| Move during a dragging operation.
*/
def.proto.dragMove =
	function( p, shift, ctrl, action )
{
	return(
		action.ti2ctype !== ActionZoomButton
		? undefined
		: false
	);
};

/*
| A button has been dragStarted.
*/
def.proto.dragStartButton =
	function( traceV )
{
/**/if( CHECK )
/**/{
/**/	const key = traceV.backward( 'panels' ).last.key;
/**/	if( !key.startsWith( 'tabSpace' ) ) throw new Error( );
/**/}

	const keyButton = traceV.backward( 'widgets' ).last.key;
	const atWindow = this.atWindow;

	switch( keyButton )
	{
		case 'zoomIn' :
		{
			Shell.alter(
				'action', ActionZoomButton.createZoom( atWindow, traceV, 1 )
			);
			Shell.changeSpaceTransformCenter( atWindow, 1 );
			return;
		}

		case 'zoomOut' :
		{
			Shell.alter(
				'action', ActionZoomButton.createZoom( atWindow, traceV, -1 )
			);
			Shell.changeSpaceTransformCenter( atWindow, -1 );
			return;
		}
	}
};

/*
| Stop of a dragging operation.
*/
def.proto.dragStop =
	function( p, shift, ctrl, action )
{
	if( action.ti2ctype !== ActionZoomButton ) return;

	Shell.alter( 'action', undefined );
};

/*
| The panel's glint.
*/
def.lazy.glint =
	function( )
{
	const resolution = this.resolution;
	const list = [ ];

	for( let widget of this.widgets )
	{
		const g = widget.glint;
		if( g ) list.push( g );
	}

	const zone = this.tZone;
	const zoneE1 = zone.envelope( 1 );

	return(
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint', ListGlint.Array( list ),
					'resolution', resolution,
					'size', zoneE1.size,
				),
			'pos', zoneE1.pos,
		)
	);
};

/*
| The pointing device just went down.
| Probes if the system ought to wait if it's
| a click or can initiate a drag right away.
*/
def.proto.probeClickDrag =
	function( p, shift, ctrl )
{
	const tZone = this.tZone;

	// shortcut if p is not near the panel
	if( !tZone.within( p ) )
	{
		return undefined;
	}

	const pp = p.sub( tZone.pos );
	// if pp is not on the panel
	if( !this._tOutline.within( pp ) )
	{
		return undefined;
	}

	if(
		this.widgets.get( 'zoomIn' ).within( pp )
		|| this.widgets.get( 'zoomOut' ).within( pp )
	)
	{
		return 'drag';
	}
	else
	{
		return 'atween';
	}
};

/*
| A button of the main panel has been pushed.
|
| ~traceV: traceV of the button
| ~shift: true if shift was held
| ~ctrl: true if ctrl/meta was held
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK )
/**/{
/**/	const key = traceV.backward( 'panels' ).last.key;
/**/	if( !key.startsWith( 'tabSpace' ) ) throw new Error( );
/**/}

	const keyButton = traceV.backward( 'widgets' ).last.key;

	switch( keyButton )
	{
		case 'space':
			Shell.alter(
				SeamFormSpace.traceSeam, SeamFormSpace.Clean,
				'mode', ModeForm[ keyButton ],
			);
			break;

		case 'zoom':
			Shell.alter(
				'mode',
					this._hasZoom
					? ModeNormal.singleton
					: ModeZoom.AtWindow( this.atWindow ),
			);
			break;

		case 'zoomAll':
			Shell.changeSpaceTransformAll( this.atWindow );
			return;

		case 'zoomHome':
			Shell.changeSpaceTransformHome( this.atWindow );
			return;

		default: throw new Error( );
	}
};

/*
| The panel's transformed zone.
*/
def.lazy.tZone =
	function( )
{
	const scale = this.transform.scale;
	const widgets = this.widgets;
	let w = 0, h = 0;
	for( let widget of widgets )
	{
		const pse = widget.zone.pse;
		w = Math.max( w, pse.x );
		h = Math.max( h, pse.y );
	}

	return(
		// TODO get the 20 from somewhere?
		Rect.PosWidthHeight(
			this.rectView.pos.add( 0, 20 * scale ),
			w * scale,
			h * scale,
		)
	);
};

/*
| Builds the widgets.
*/
def.lazy.widgets =
	function( )
{
	const light         = this.light;
	const hover         = this.hover;
	const designWidgets = Self._designWidgets( light );
	const mark          = this.mark;
	const mode          = this.mode;
	const refSpace      = this.refSpace;
	const resolution    = this.resolution;
	const systemFocus   = this.systemFocus;
	const transform     = this.transform;
	const traceV        = this.traceV;
	const hasZoom       = this._hasZoom;

	let twig = { };
	for( let key of designWidgets.keys )
	{
		const traceW = traceV.add( 'widgets', key );

		const widget =
			WidgetBase.FromDesign(
				designWidgets.get( key ),
				light, traceW, resolution, systemFocus, transform,
			);

		switch( widget.ti2ctype )
		{
			case WidgetBoard:
			{
				twig[ key ] = widget.create( 'visible', hasZoom );
				break;
			}

			case WidgetButton:
			{
				let string = pass;
				let visible = pass;
				let down;

				switch( key )
				{
					case 'space':
						string = refSpace.fullname;
						visible = true;
						down = mode.ti2ctype === ModeForm && mode.formName === 'space';
						break;

					case 'zoom':
						visible = true;
						down = hasZoom;
						break;

					case 'zoomAll':
					case 'zoomIn':
					case 'zoomHome':
					case 'zoomOut':
						visible = hasZoom;
						break;

					default:
						visible = true;
						down = false;
						break;
				}

				let wHover = hover;
				if( wHover && !wHover.hasTrace( traceW ) ) wHover = undefined;

				let wMark = mark;
				if( wMark && !wMark.encompasses( traceW ) ) wMark = undefined;

				twig[ key ] =
					widget.create(
						'down', down,
						'hover', wHover,
						'mark', wMark,
						'string', string,
						'visible', visible,
					);
				break;
			}

			default: throw new Error( );
		}
	}

	return TwigWidget.create( 'twig:init', twig, designWidgets.keys );
};

/*
| Widgets design.
*/
def.staticLazyFunc._designWidgets =
	( light ) =>
{
	const facetsButton =
		FacetList.Elements(
			// default state.
			Facet.create(
				'border', FacetBorder.Color( Color.RGB( 225, 185, 122 ) ),
				'fill', Color.RGB( 253, 253, 220 ),
				'specs', GroupBoolean.Empty,
			),
			// hover
			Facet.create(
				'border', FacetBorder.Color( Color.RGB( 225, 185, 122 ) ),
				'fill', Color.RGB( 253, 239, 199 ),
				'specs', GroupBoolean.Table( { hover: true } ),
			),
			// down
			Facet.create(
				'border', FacetBorder.Color( Color.RGB( 225, 185, 122 ) ),
				'fill', Color.RGB( 255, 188, 88 ),
				'specs', GroupBoolean.Table( { down: true } ),
			),
			// down and hover
			Facet.create(
				'border', FacetBorder.Color( Color.RGB( 225, 185, 122 ) ),
				'fill', Color.RGB( 255, 188, 88 ),
				'specs', GroupBoolean.Table( { down: true, hover: true } ),
			)
		);

	const facetsBoard =
		FacetList.Elements(
			// default state.
			Facet.create(
				'border', FacetBorder.Color( Color.RGB( 225, 185, 122 ) ),
				'fill', Color.RGB( 247, 246, 176 ),
				'specs', GroupBoolean.Empty,
			),
		);

	const facetsButtonZoom =
		FacetList.Elements(
			// default state.
			Facet.create( ),
			// hover
			Facet.create(
				'border', FacetBorder.Color( Color.RGBA( 196, 94, 44, 0.4 ) ),
				'fill', Color.RGBA( 255, 235, 210, 0.7 ),
				'specs', GroupBoolean.Table( { hover: true } ),
			),
			// down
			Facet.create(
				'border', FacetBorder.Color( Color.RGBA( 196, 94, 44, 0.4 ) ),
				'fill', Color.RGB( 255, 188, 88 ),
				'specs', GroupBoolean.Table( { down: true } ),
			),
			// down and hover
			Facet.create(
				'fill', Color.RGB( 255, 188, 88 ),
				'border', FacetBorder.Color( Color.RGBA( 196, 94, 44, 0.4 ) ),
				'specs', GroupBoolean.Table( { down: true, hover: true } ),
			)
		);

	const posSpaceButton = Point.XY( 0, 0 );
	const sizeSpaceButton = Size.WH( 33, 250 );
	const ab = sizeSpaceButton.width / 2;

	const sizeZoomBoard = Size.WH( 54, 220 );

	const sizeZoomButton = Size.WH( 33, 33 );
	const sizeZBoardButton = Size.WH( 44, 44 );

	const posZoomBoard = Point.XY( 0, sizeSpaceButton.height - sizeZoomButton.height + 1 );
	const posZoomAll   = posZoomBoard.add( 6, 40 );
	const posZoomIn    = posZoomAll.add( 0, 44 );
	const posZoomOut   = posZoomIn .add( 0, 44 );
	const posZoomHome  = posZoomOut.add( 0, 44 );

	return(
		TwigDesignWidget.Grow(
			'space',
			DesignButton.create(
				'facets', facetsButton,
				'font', DesignFont.NameSizeLight( 'panel', 14, light ),
				'shape',
					RectRoundExt.PosWidthHeightHVNese(
						Point.XY( 0, 1 ),
						sizeSpaceButton.width,
						sizeSpaceButton.height,
						ab, ab,
					),
				'string', '',
				'stringRotation', Angle.n,
				'zone',
					Rect.PosWidthHeight(
						posSpaceButton,
						sizeSpaceButton.width + 4,
						sizeSpaceButton.height + 2,
					),
			),
			'boardZoom',
			DesignBoard.create(
				'facets', facetsBoard,
				'shape',
					RectRoundExt.PosWidthHeightHVNese(
						Point.XY( 0, 1 ),
						sizeZoomBoard.width,
						sizeZoomBoard.height,
						ab, ab,
					),
				'visible', false,
				'zone',
					Rect.PosWidthHeight(
						posZoomBoard,
						sizeZoomBoard.width + 4,
						sizeZoomBoard.height + 2,
					),
			),
			'zoom',
			DesignButton.create(
				'facets', facetsButton,
				'icon', IconZoom.design( light ),
				'iconNoGrid', true,
				'shape',
					RectRoundExt.PosWidthHeightHVSe(
						Point.XY( 0, 1 ),
						sizeZoomButton.width,
						sizeZoomButton.height,
						ab, ab,
					),
				'zone',
					Rect.PosWidthHeight(
						posZoomBoard,
						sizeZoomButton.width + 4,
						sizeZoomButton.height + 2,
					),
			),
			'zoomAll',
			DesignButton.create(
				'facets', facetsButtonZoom,
				'font', DesignFont.Size( 16 ),
				'icon', IconZoomAll.design( light ),
				'iconNoGrid', true,
				'shape', 'RectRound',
				'visible', false,
				'zone', Rect.PosSize( posZoomAll, sizeZBoardButton )
			),
			'zoomIn',
			DesignButton.create(
				'facets', facetsButtonZoom,
				'font', DesignFont.Size( 16 ),
				'icon', IconZoomIn.design( light ),
				'shape', 'RectRound',
				'visible', false,
				'zone', Rect.PosSize( posZoomIn, sizeZBoardButton )
			),
			'zoomOut',
			DesignButton.create(
				'facets', facetsButtonZoom,
				'font', DesignFont.Size( 16 ),
				'icon', IconZoomOut.design( light ),
				'shape', 'RectRound',
				'visible', false,
				'zone', Rect.PosSize( posZoomOut, sizeZBoardButton )
			),
			'zoomHome',
			DesignButton.create(
				'facets', facetsButtonZoom,
				'font', DesignFont.Size( 16 ),
				'icon', IconZoomHome.design( light ),
				'iconNoGrid', true,
				'shape', 'RectRound',
				'visible', false,
				'zone', Rect.PosSize( posZoomHome, sizeZBoardButton )
			)
		)
	);
};


/*
| True if the zoom subpanel is shown.
*/
def.lazy._hasZoom =
	function( )
{
	const mode = this.mode;
	return mode.ti2ctype === ModeZoom && mode.atWindow === this.atWindow;
};

/*
| The panel's transformed outline.
*/
def.lazy._tOutline =
	function( )
{
	return this.tZone.zeroPos;
};

