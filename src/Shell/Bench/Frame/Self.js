/*
| The frame bench.
|
| Allows moving and resizing items.
*/
def.extend = 'Shell/Bench/Base';

def.attributes =
{
	// content of the frame
	content: { type: 'Shell/View/Space/Item/Set' },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// should show guides
	showGuides: { type: 'boolean' },

	// current view size
	sizeView: { type: 'gleam:Size' },
};

import { Self as ActionMoveItems   } from '{Shell/Action/MoveItems}';
import { Self as ActionResizeItems } from '{Shell/Action/ResizeItems}';
import { Self as Angle             } from '{gleam:Angle}';
import { Self as BenchBase         } from '{Shell/Bench/Base}';
import { Self as Color             } from '{gleam:Color}';
import { Self as DesignButton      } from '{Shell/Design/Widget/Button}';
import { Self as FigureList        } from '{gleam:Figure/List}';
import { Self as GlintFigure       } from '{gleam:Glint/Figure}';
import { Self as GlintMask         } from '{gleam:Glint/Mask}';
import { Self as GlintPane         } from '{gleam:Glint/Pane}';
import { Self as GlintWindow       } from '{gleam:Glint/Window}';
import { Self as Hover             } from '{Shell/Result/Hover}';
import { Self as IconXSymbol       } from '{Shell/Design/Icon/XSymbol}';
import { Self as IconZoomIn        } from '{Shell/Design/Icon/ZoomIn}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as Margin            } from '{gleam:Margin}';
import { Self as Path              } from '{gleam:Path}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as RectRound         } from '{gleam:RectRound}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as Size              } from '{gleam:Size}';
import { Self as Segment           } from '{gleam:Segment}';
import { Self as TransformNormal   } from '{gleam:Transform/Normal}';
import { Self as TwigWidget        } from '{twig@<Shell/Widget/Types}';
import { Self as WidgetBase        } from '{Shell/Widget/Base}';

/*
| Checks if the frame has been clicked.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl  key was pressed
*/
def.proto.click =
	function( p, shift, ctrl )
{
	// ctrl-clicks are not swallowed.
	if( ctrl )
	{
		return undefined;
	}

	// not even near anything?
	if( !this._zoneWindowT.within( p ) )
	{
		return undefined;
	}

	// in the mask? pass through
	if( this._mask.within( p ) )
	{
		return undefined;
	}

	const pz = p.sub( this._zoneWindowT.pos );


	// anything of the frame is clicked? absorbs it
	if(
		this._zoneContentTP.within( pz )
		|| this._shapeHandleResizeNw.within( pz )
		|| this._shapeHandleResizeN .within( pz )
		|| this._shapeHandleResizeNe.within( pz )
		|| this._shapeHandleResizeE .within( pz )
		|| this._shapeHandleResizeSe.within( pz )
		|| this._shapeHandleResizeS .within( pz )
		|| this._shapeHandleResizeSw.within( pz )
		|| this._shapeHandleResizeW .within( pz )
	)
	{
		return true;
	}

	// this is on the panel
	for( let widget of this._widgets.reverse( ) )
	{
		const bubble = widget.click( pz, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	// absorbs clicks on the grab area
	if( this._shapeAreaGrab.within( pz ) )
	{
		return true;
	}

	return undefined;
};

/*
| Starts an operation with the pointing device held down.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const window = this.window;

	if( window.access !== 'rw' )
	{
		return undefined;
	}

	const zoneC = this.content.zone;

	// if not even near or within the mask, pass through
	if( !this._zoneWindowT.within( p ) || this._mask.within( p ) )
	{
		return undefined;
	}

	const pz = p.sub( this._zoneWindowT.pos );

	let com, pBase;
	if( this._shapeHandleResizeNw.within( pz ) )
	{
		com = Angle.nw;
		pBase = zoneC.pse;
	}
	else if( this._shapeHandleResizeNe.within( pz ) )
	{
		com = Angle.ne;
		pBase = zoneC.psw;
	}
	else if( this._shapeHandleResizeSe.within( pz ) )
	{
		com = Angle.se;
		pBase = zoneC.pos;
	}
	else if( this._shapeHandleResizeSw.within( pz ) )
	{
		com = Angle.sw;
		pBase = zoneC.pne;
	}
	else if( this._shapeHandleResizeN.within( pz ) )
	{
		com = Angle.n;
		pBase = zoneC.ps;
	}
	else if( this._shapeHandleResizeE.within( pz ) )
	{
		com = Angle.e;
		pBase = zoneC.pw;
	}
	else if( this._shapeHandleResizeS.within( pz ) )
	{
		com = Angle.s;
		pBase = zoneC.pn;
	}
	else if( this._shapeHandleResizeW.within( pz ) )
	{
		com = Angle.w;
		pBase = zoneC.pe;
	}
	else if( this._shapeAreaGrab.within( pz ) )
	{
		// dradding the inner body starts to drag the items
		const dp = p.detransform( window.transform );

		Shell.alter(
			'action',
				ActionMoveItems.create(
					'items',      this.content,
					'moveBy',     Point.zero,
					'pointStart', dp,
					'startZone',  zoneC,
					'traceV',     this.traceV,
				)
		);
		return true;
	}
	else
	{
		return undefined;
	}

	const content = this.content;
	Shell.alter(
		'action',
		ActionResizeItems.create(
			'items',        content,
			'pBase',        pBase,
			'proportional', content.proportional,
			'resizeDir',    com,
			'scaleX',       1,
			'scaleY',       1,
			'pointStart',   p.detransform( window.transform ),
			'startZone',    zoneC,
			'startZones',   content.zones,
			'traceV',       this.traceV,
		),
	);

	return true;
};

/*
| The frames glint.
*/
def.lazy.glint =
	function( )
{
	const pos = this._zoneWindowT.pos;

	const list =
	[
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane', this._glintPaneFrame,
			'pos',  pos,
		),
	];

	{
		const colorHandleStroke =
			Color.RGBA( 255, 220, 157, 0.6 );

		list.push(
			GlintFigure.FigureFill( this._shapeHandleStrokeE,  colorHandleStroke ),
			GlintFigure.FigureFill( this._shapeHandleStrokeN,  colorHandleStroke ),
			GlintFigure.FigureFill( this._shapeHandleStrokeNE, colorHandleStroke ),
			GlintFigure.FigureFill( this._shapeHandleStrokeS,  colorHandleStroke ),
			GlintFigure.FigureFill( this._shapeHandleStrokeW,  colorHandleStroke ),
		);
	}

	if( this.showGuides )
	{
		list.push( this._glintGuides );
	}

	{
		// adds the handleStroke after the guides so they are on top
		const colorIconStroke = Color.RGB( 96, 0, 0 );
		const icon = IconZoomIn.design( this.light ).shape;
		const ratio = this.resolution.ratio;
		const pe  = this._shapeHandleStrokeE.pc;
		const pn  = this._shapeHandleStrokeN.pc;
		const ps  = this._shapeHandleStrokeS.pc;
		const pw  = this._shapeHandleStrokeW.pc;
		const pne = this._shapeHandleStrokeNE.pc;

		list.push(
			GlintFigure.FigureFill( icon.add( pe.x + 14 * ratio, pe.y ), colorIconStroke ),
			GlintFigure.FigureFill( icon.add( pn.x, pn.y - 14 * ratio ), colorIconStroke ),
			GlintFigure.FigureFill( icon.add( ps.x, ps.y + 14 * ratio ), colorIconStroke ),
			GlintFigure.FigureFill( icon.add( pw.x - 14 * ratio, pw.y ), colorIconStroke ),

			GlintFigure.FigureFill( icon.add( pne.x - 14 * ratio, pne.y ), colorIconStroke ),
		);
	}

	return(
		GlintMask.create(
			'glint',    ListGlint.Array( list ),
			'outline',  this._mask,
			'reversed', true
		)
	);
};

/*
| Pointing device hover.
|
| Returns true if the pointing device hovers over anything.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const zwt = this._zoneWindowT;

	// if not in the windows zone
	// or within the mask
	if( !zwt.within( p ) || this._mask.within( p ) )
	{
		return undefined;
	}

	const pz = p.sub( zwt.pos );

	if( this._shapeHandleResizeNw.within( pz ) )
	{
		return Hover.CursorNwResize;
	}

	if( this._shapeHandleResizeNe.within( pz ) )
	{
		return Hover.CursorNeResize;
	}

	if( this._shapeHandleResizeSe.within( pz ) )
	{
		return Hover.CursorSeResize;
	}

	if( this._shapeHandleResizeSw.within( pz ) )
	{
		return Hover.CursorSwResize;
	}

	if( this._shapeHandleResizeN.within( pz ) || this._shapeHandleResizeS.within( pz ) )
	{
		return Hover.CursorNsResize;
	}

	if( this._shapeHandleResizeE.within( pz ) || this._shapeHandleResizeW.within( pz ) )
	{
		return Hover.CursorEwResize;
	}

	for( let widget of this._widgets.reverse( ) )
	{
		const bubble = widget.pointingHover( pz, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	if( this._shapeAreaGrab.within( pz ) )
	{
		return Hover.CursorGrab;
	}

	return undefined;
};

/*
| A button has been pushed.
|
| ~traceV: traceV of the button
| ~shift:  true if shift was held
| ~ctrl:   true if ctrl/meta was held
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const buttonKey = traceV.backward( 'widgets' ).last.key;

	switch( buttonKey )
	{
		case 'buttonRemove':
			Shell.removeItems( this.content.traceVs );
			break;

		default: throw new Error( );
	}
};

/*
| Dstance corner extra width.
*/
def.lazy._dCornerExt =
	function( )
{
	const zct = this._zoneContentTP;
	const ratio = this.resolution.ratio;

	// distance corner extra width
	const dce = 25 * ratio;

	// minimum handle distance
	const dhm = 30 * ratio;

	let dcex = dce;
	let dcey = dce;

	if( zct.width - 2 * dcex < dhm )
	{
		dcex = ( zct.width - dhm ) / 2;
		if( dcex < 0 )
		{
			return 0;
		}
	}

	if( zct.height - 2 * dcey < dhm )
	{
		dcey = ( zct.height - dhm ) / 2;
		if( dcey < 0 )
		{
			return 0;
		}
	}

	return Math.min( dcex, dcey );
};

/*
| The guides
*/
def.lazy._glintGuides =
	function( )
{
	const zcta = this.content.zone.transform( this.window.transform );
	const sv = this.sizeView;

	const colorGuideInner = Color.RGBA( 255, 229, 181, 0.9 );
	const colorGuideOuter = Color.RGBA( 255, 220, 157, 0.9 );

	const pn  = zcta.pn;
	const pos = zcta.pos;
	const pse = zcta.pse;
	const pw  = zcta.pw;

	return(
		ListGlint.Elements(
			// north guide
			GlintFigure.FigureColor(
				Segment.P0P1(
					pos.create( 'x', 0 ),
					pos.create( 'x', sv.width ),
				),
				colorGuideOuter,
			),
			// south guide
			GlintFigure.FigureColor(
				Segment.P0P1(
					pse.create( 'x', 0 ),
					pse.create( 'x', sv.width ),
				),
				colorGuideOuter,
			),
			// west guide
			GlintFigure.FigureColor(
				Segment.P0P1(
					pos.create( 'y', 0 ),
					pos.create( 'y', sv.height ),
				),
				colorGuideOuter,
			),
			// east guide
			GlintFigure.FigureColor(
				Segment.P0P1(
					pse.create( 'y', 0 ),
					pse.create( 'y', sv.height ),
				),
				colorGuideOuter,
			),
			// vertical guide
			GlintFigure.FigureColor(
				Segment.P0P1(
					pn.create( 'y', 0 ),
					pn.create( 'y', sv.height ),
				),
				colorGuideInner,
			),
			// horizontal guide
			GlintFigure.FigureColor(
				Segment.P0P1(
					pw.create( 'x', 0 ),
					pw.create( 'x', sv.width ),
				),
				colorGuideInner,
			)
		)
	);
};

/*
| The frame glint holds all unmasked stuff.
*/
def.lazy._glintPaneFrame =
	function( )
{
	const colorHandleCorner =
		Color.RGBA( 255, 220, 157, 0.9 );

	const colorHandleSide =
		this.content.proportional
		? Color.RGBA( 255, 245, 200, 0.9 )
		: Color.RGBA( 255, 240, 194, 0.9 );

	const list =
	[
		GlintFigure.FigureColorFill(
			this._shapeAreaGrab,
			Color.RGBA( 255, 220, 157, 0.9 ),
			this.light
			? Color.RGBA( 255, 250, 230, 0.85 )
			: Color.RGBA( 176, 165, 125, 0.85 ),
		),
		GlintFigure.FigureFill( this._shapeHandleResizeNe, colorHandleCorner ),
		GlintFigure.FigureFill( this._shapeHandleResizeNw, colorHandleCorner ),
		GlintFigure.FigureFill( this._shapeHandleResizeSe, colorHandleCorner ),
		GlintFigure.FigureFill( this._shapeHandleResizeSw, colorHandleCorner ),
		GlintFigure.FigureFill( this._shapeHandleResizeE,  colorHandleSide ),
		GlintFigure.FigureFill( this._shapeHandleResizeN,  colorHandleSide ),
		GlintFigure.FigureFill( this._shapeHandleResizeS,  colorHandleSide ),
		GlintFigure.FigureFill( this._shapeHandleResizeW,  colorHandleSide ),
	];

	for( let widget of this._widgets )
	{
		const g = widget.glint;
		if( g )
		{
			list.push( g );
		}
	}

	return(
		GlintPane.create(
			'glint',      ListGlint.Array( list ),
			'resolution', this.resolution,
			'size',       this._zoneWindowT.size.add( 2 ),
		)
	);

	//return ListGlint.Array( list );
};

/*
| Margin of the grab area.
*/
def.staticLazy._marginAreaGrab =
	( ) =>
	Margin.NESW( 72, 72, 72, 91 );

/*
| The figure used as mask for the inner contents of the frame.
*/
def.lazy._mask =
	function( )
{
	const list = [ ];

	for( let ca of this.content )
	{
		const cam = ca.tMask;
		if( cam.ti2ctype === FigureList )
		{
			for( let cama of cam )
			{
				list.push( cama.envelope( 1 ) );
			}
		}
		else
		{
			list.push( cam.envelope( 1 ) );
		}
	}

	return FigureList.Array( list );
};

/*
| Shape of grabbing handle.
*/
def.lazy._shapeAreaGrab =
	function( )
{
	const zwt = this._zoneWindowT;
	const rounding = 78 * this.resolution.ratio;

	return(
		RectRound.create(
			'a',       rounding,
			'b',       rounding,
			'height',  zwt.height,
			'pos',     Point.zero,
			'width',   zwt.width,
		)
	);
};

/*
| Stroke handle in north.
*/
def.lazy._shapeHandleStrokeE =
	function( )
{
	const zct   = this._zoneContentT;
	const ratio = this.resolution.ratio;
	const pc    = zct.pe.add( Self._marginAreaGrab.e * ratio, 0 );
	const dy    = 29 * ratio;
	const dym   =  5 * ratio;
	const dx    = 29 * ratio;

	return(
		Path.Plan(
			'pc',    pc,
			'start', pc.add(  0, -dym - dy ),
			'qbend', pc.add( dx, -dym      ),
			'line',  pc.add( dx,  dym      ),
			'qbend', pc.add(  0,  dym + dy ),
			'line', 'close',
		)
	);
};

/*
| Stroke handle in north.
*/
def.lazy._shapeHandleStrokeN =
	function( )
{
	const zct   = this._zoneContentT;
	const ratio = this.resolution.ratio;
	const pc    = zct.pn.add( 0, -Self._marginAreaGrab.n * ratio );
	const dx    = 29 * ratio;
	const dxm   =  5 * ratio;
	const dy    = 29 * ratio;

	return(
		Path.Plan(
			'pc',    pc,
			'start', pc.add( -dx - dxm,   0 ),
			'qbend', pc.add(      -dxm, -dy ),
			'line',  pc.add(       dxm, -dy ),
			'qbend', pc.add(  dx + dxm,   0 ),
			'line', 'close',
		)
	);
};

/*
| Stroke handle in north east.
*/
def.lazy._shapeHandleStrokeNE =
	function( )
{
	const zct   = this._zoneContentT;
	const ratio = this.resolution.ratio;

	const pc    =
		zct.pne.add(
			(  Self._marginAreaGrab.e - 7 ) * ratio,
			( -Self._marginAreaGrab.n + 7 ) * ratio
		);

	return(
		Path.Plan(
			'pc',
				pc,
			'start',
				pc.add( -40 * ratio,           0 ),
			'bezier',
				pc.add( -36 * ratio, -8 * ratio ),
				pc.add( -26 * ratio, -12 * ratio ),
				pc.add( -19 * ratio, -12 * ratio ),
			'qbend',
				pc.add(  12 * ratio,  19 * ratio ),
			'bezier',
				pc.add(  12 * ratio,  27 * ratio ),
				pc.add(   8 * ratio,  36 * ratio ),
				pc.add(           0,  40 * ratio ),
			'qcurve',
				pc.add( -12 * ratio,  12 * ratio ),
				'close',
		)
	);
};

/*
| Stroke handle in south.
*/
def.lazy._shapeHandleStrokeS =
	function( )
{
	const zct   = this._zoneContentT;
	const ratio = this.resolution.ratio;
	const pc    = zct.ps.add( 0, Self._marginAreaGrab.n * ratio );
	const dx    = 29 * ratio;
	const dxm   =  5 * ratio;
	const dy    = 29 * ratio;

	return(
		Path.Plan(
			'pc',    pc,
			'start', pc.add(  dx + dxm,  0 ),
			'qbend', pc.add(       dxm, dy ),
			'line',  pc.add(      -dxm, dy ),
			'qbend', pc.add( -dx - dxm,  0 ),
			'line', 'close',
		)
	);
};

/*
| Stroke handle in west.
*/
def.lazy._shapeHandleStrokeW =
	function( )
{
	const zct   = this._zoneContentT;
	const ratio = this.resolution.ratio;
	const pc    = zct.pw.add( -Self._marginAreaGrab.w * ratio, 0 );
	const dy    = 29 * ratio;
	const dym   =  5 * ratio;
	const dx    = 29 * ratio;

	return(
		Path.Plan(
			'pc',    pc,
			'start', pc.add(   0,  dym + dy ),
			'qbend', pc.add( -dx,  dym      ),
			'line',  pc.add( -dx, -dym      ),
			'qbend', pc.add(   0, -dym - dy ),
			'line', 'close',
		)
	);
};

/*
| North handle shape.
*/
def.lazy._shapeHandleResizeN =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Rect.PosWidthHeight(
			Point.XY( zct.pos.x + dce, zrt.pos.y ),
			zct.width - 2 * dce,
			zct.pos.y - zrt.pos.y,
		)
	);
};

/*
| North-east handle shape.
*/
def.lazy._shapeHandleResizeNe =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Path.Plan(
			'pc',    zct.pne,
			'start', zct.pne.x - dce, zct.pne.y,
			'line',  zct.pne.x - dce, zrt.pne.y,
			'qbend', zrt.pne.x,       zct.pne.y + dce,
			'line',  zct.pne.x,       zct.pne.y + dce,
			'qbend', 'ccw', 'close',
		)
	);
};

/*
| North-west handle shape.
*/
def.lazy._shapeHandleResizeNw =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Path.Plan(
			'pc',    zct.pos,
			'start', zct.pos.x,       zct.pos.y + dce,
			'line',  zrt.pos.x,       zct.pos.y + dce,
			'qbend', zct.pos.x + dce, zrt.pos.y,
			'line',  zct.pos.x + dce, zct.pos.y,
			'qbend', 'ccw', 'close',
		)
	);
};

/*
| East handle shape.
*/
def.lazy._shapeHandleResizeE =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Rect.PosWidthHeight(
			Point.XY( zct.pne.x, zct.pne.y + dce ),
			zrt.pne.x - zct.pne.x,
			zct.height - dce * 2,
		)
	);
};

/*
| South handle shape.
*/
def.lazy._shapeHandleResizeS =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Rect.PosWidthHeight(
			Point.XY( zct.psw.x + dce, zct.psw.y ),
			zct.width - 2 * dce,
			zrt.psw.y - zct.psw.y,
		)
	);
};

/*
| South-east handle shape.
*/
def.lazy._shapeHandleResizeSe =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Path.Plan(
			'pc',    zct.pse,
			'start', zct.pse.x,       zct.pse.y - dce,
			'line',  zrt.pse.x,       zct.pse.y - dce,
			'qbend', zct.pse.x - dce, zrt.pse.y,
			'line',  zct.pse.x - dce, zct.pse.y,
			'qbend', 'ccw', 'close',
		)
	);
};

/*
| South-west handle shape.
*/
def.lazy._shapeHandleResizeSw =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Path.Plan(
			'pc',    zct.psw,
			'start', zct.psw.x + dce, zct.psw.y,
			'line',  zct.psw.x + dce, zrt.psw.y,
			'qbend', zrt.psw.x,       zct.psw.y - dce,
			'qbend', zct.psw.x,       zct.psw.y - dce,
			'qbend', 'ccw', 'close',
		)
	);
};

/*
| West handle shape.
*/
def.lazy._shapeHandleResizeW =
	function( )
{
	const zct = this._zoneContentTP;
	const zrt = this._zoneResizeT;
	const dce = this._dCornerExt;

	return(
		Rect.PosWidthHeight(
			Point.XY( zrt.pos.x, zct.pos.y + dce ),
			zct.pos.x - zrt.pos.x,
			zct.height - 2 * dce,
		)
	);
};

/*
| Builds the widgets.
*/
def.lazy._widgets =
	function( )
{
	const light       = this.light;
	const hover       = this.hover;
	const traceV      = this.traceV;
	const resolution  = this.resolution;
	const transform =
		TransformNormal.setScaleOffset(
			resolution.ratio,
			this._shapeAreaGrab.pw,
		);

	const posRemoveButton = Point.XY( 9, -60 );
	const sizeButton = Size.WH( 32, 32 );

	const twig = { };
	const keys = [ ];

	if( this.window.access === 'rw' )
	{
		const traceW = traceV.add( 'widgets', 'buttonRemove' );

		let button =
			WidgetBase.FromDesign(
				DesignButton.create(
					'facets',     BenchBase.facetsButton( light ),
					'icon',       IconXSymbol.design( light ),
					'iconNoGrid', true,
					'shape',     'RectRound',
					'zone',       Rect.PosSize( posRemoveButton, sizeButton )
				),
				light, traceW, this.resolution,
				false, // systemFocus
				transform,
			);

		let hoverW = hover;
		if( hoverW && !hoverW.hasTrace( traceW ) )
		{
			hoverW = undefined;
		}

		twig.buttonRemove = button.create( 'hover', hoverW );
		keys.push( 'buttonRemove' );
	}

	return TwigWidget.create( 'twig:init', twig, keys );
};

/*
| Transformed zone relative to pane.
*/
def.lazy._zoneContentT =
	function( )
{
	return this.content.zone.transform( this.window.transform );
};

/*
| Transformed zone relative to pane.
*/
def.lazy._zoneContentTP =
	function( )
{
	const zct   = this._zoneContentT;
	const mag   = Self._marginAreaGrab;
	const ratio = this.resolution.ratio;

	return(
		Rect.PosWidthHeight(
			Point.XY( mag.w * ratio, mag.n * ratio ),
			zct.width,
			zct.height,
		)
	);
};

/*
| Zone of the resizing handles.
*/
def.lazy._zoneResizeT =
	function( )
{
	const ratio = this.resolution.ratio;
	const mag = Self._marginAreaGrab;
	const wr = 42;

	const zct = this._zoneContentTP;

	return(
		Rect.PosWidthHeight(
			Point.XY( ( mag.w - wr ) * ratio, ( mag.n - wr ) * ratio ),
			zct.width  + 2 * wr * ratio,
			zct.height + 2 * wr * ratio,
		)
	);
};

/*
| The transformed zone of the window.
*/
def.lazy._zoneWindowT =
	function( )
{
	const mag   = Self._marginAreaGrab;
	const ratio = this.resolution.ratio;
	const zcta  = this.content.zone.transform( this.window.transform );

	return(
		Rect.PosWidthHeight(
			Point.XY( zcta.pos.x - mag.w * ratio, zcta.pos.y - mag.n * ratio ),
			zcta.width  + mag.x * ratio,
			zcta.height + mag.y * ratio,
		)
	);
};

