/*
| The document path bench.
*/
def.extend = 'Shell/Bench/Base';

def.attributes =
{
	// current action
	action: { type: [ 'undefined', '<Shell/Action/Types' ] },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// should show guides
	showGuides: { type: 'boolean' },

	// current view size
	sizeView: { type: 'gleam:Size' },

	// the visual doc to bench.
	vDoc: { type: 'Shell/View/Space/Item/DocPath' },
};

import { Self as ActionMoveDocPathHandle } from '{Shell/Action/MoveDocPathHandle}';
import { Self as BenchFrame              } from '{Shell/Bench/Frame/Self}';
import { Self as FigureList              } from '{gleam:Figure/List}';
import { Self as Handle                  } from '{Shell/Bench/DocPath/Handle}';
import { Self as Hover                   } from '{Shell/Result/Hover}';
import { Self as ListGlint               } from '{list@<gleam:Glint/Types}';
import { Self as ListHandle              } from '{list@Shell/Bench/DocPath/Handle}';
import { Self as Shell                   } from '{Shell/Self}';
import { Self as VSpaceItemSet           } from '{Shell/View/Space/Item/Set}';

/*
| Checks if the frame has been clicked.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	return this._benchFrame.click( p, shift, ctrl );
};

/*
| Starts an operation with the pointing device held down.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const window = this.window;

	if( window.access !== 'rw' )
	{
		return undefined;
	}

	const handles = this._handles;
	for( let handle of handles )
	{
		// TODO move this into handle code
		if( handle.within( p ) )
		{
			const dp = p.detransform( window.transform );
			const vDoc = this.vDoc;
			Shell.alter(
				'action',
					ActionMoveDocPathHandle.create(
						'jointNr',    handle.jointNr,
						'removing',   handle.removing,
						'pointStart', dp,
						'vDoc',       vDoc,
						'traceV',     vDoc.traceV,
					)
			);

			return true;
		}
	}

	return this._benchFrame.dragStart( p, shift, ctrl );
};

/*
| The frames glint.
*/
def.lazy.glint =
	function( )
{
	const list = [ this._benchFrame.glint ];
	const handles = this._handles;

	for( let handle of handles )
	{
		list.push( handle.glint );
	}

	return ListGlint.Array( list );
};

/*
| Mouse hover.
|
| Returns true if the pointing device hovers over anything.
*/
def.proto.pointingHover =
	function( p )
{
	for( let handle of this._handles )
	{
		if( handle.within( p ) )
		{
			return Hover.CursorMove;
		}
	}

	return this._benchFrame.pointingHover( p );
};

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	return this._benchFrame.pushButton( traceV, shift, ctrl );
};

/*
| Zone.
*/
def.lazy.zone =
	function( )
{
	return this.doc.ancillary.zone;
};

/*
| The frame of the vdoc itself.
*/
def.lazy._benchFrame =
	function( )
{
	return(
		BenchFrame.create(
			'content',    VSpaceItemSet.Elements( this.vDoc ),
			'hover',      this.hover,
			'light',      this.light,
			'resolution', this.resolution,
			'showGuides', this.showGuides,
			'sizeView',   this.sizeView,
			'traceV',     this.traceV.back.add( 'frame' ),
			'window',     this.window,
		)
	);
};

/*
| The list of handles.
*/
def.lazy._handles =
	function( )
{
	const list = [ ];
	const vDoc = this.vDoc;

	let ajnr;
	const action = this.action;
	if( action && action.ti2ctype === ActionMoveDocPathHandle )
	{
		ajnr = action.jointNr;
	}

	const connections = vDoc.pathConnections;
	const joints = vDoc.ancillary.joints;
	const transform = this.window.transform;

	if( ajnr !== undefined )
	{
		const seg = connections.get( ajnr );

		if( action.hover )
		{
			const joint = joints.get( ajnr );
			list.push(
				Handle.create(
					'center',    seg.p1,
					'light',     this.light,
					'jointNr',   ajnr,
					'outline',   joint.outline,
					'traceV',    this.traceV.add( 'handle', ajnr ).add( 'add' ),
					'transform', transform,
				)
			);

			if( ajnr + 1 < connections.length )
			{
				// it's inserting a joint, shows a handle on the other side of the
				// inserted item as well
				const seg2 = connections.get( ajnr + 1 );
				list.push(
					Handle.create(
						'center',    seg2.p0,
						'light',     this.light,
						'jointNr',   ajnr + 1,
						'outline',   joint.outline,
						'traceV',    this.traceV.add( 'handle', ajnr + 1 ).add( 'add' ),
						'transform', transform,
					)
				);
			}
		}
		else
		{
			list.push(
				Handle.create(
					'center',    seg.p1,
					'light',     this.light,
					'jointNr',   ajnr,
					'traceV',    this.traceV.add( 'handle', ajnr ).add( 'add' ),
					'transform', transform,
				)
			);
		}
	}
	else if( connections.length !== 0 )
	{
		for( let a = 0, alen = connections.length; a < alen; a++ )
		{
			const seg = connections.get( a );
			const pjoint = a > 0 ? joints.get( a - 1 ) : undefined;
			const joint = joints.get( a );

			if( pjoint )
			{
				list.push(
					Handle.create(
						'center',    seg.p0,
						'light',     this.light,
						'jointNr',   a - 1,
						'outline',   pjoint.outline,
						'removing',  a - 1,
						'traceV',    this.traceV.add( 'handle', a - 1 ).add( 'rem2' ),
						'transform', transform,
					)
				);
			}

			list.push(
				Handle.create(
					'center',    seg.pc,
					'light',     this.light,
					'jointNr',   a,
					'traceV',    this.traceV.add( 'handle', a ).add( 'add' ),
					'transform', transform,
				),
				Handle.create(
					'center',    seg.p1,
					'light',     this.light,
					'jointNr',   a,
					'outline',   joint.outline,
					'removing',  a,
					'traceV',    this.traceV.add( 'handle', a ).add( 'rem1' ),
					'transform', transform,
				)
			);
		}

		// the last handle to add to the path
		if( ajnr === undefined )
		{
			const seg = vDoc.pathZigZag.last;
			const joint = joints.last;
			list.push(
				Handle.create(
					'center',    joint.outline.intersectsPoint( seg.rayP1Mirror ),
					'light',     this.light,
					'jointNr',   connections.length,
					'outline',   joint.outline,
					'traceV',    this.traceV.add( 'handle', connections.length ).add( 'add' ),
					'transform', transform,
				)
			);
		}
	}
	else
	{
		list.push(
			Handle.create(
				'center',    vDoc.ancillary.zone.ps,
				'light',     this.light,
				'jointNr',   0,
				'outline',   vDoc.outline,
				'traceV',    this.traceV.add( 'handle', 0 ).add( 'add' ),
				'transform', transform,
			)
		);
	}

	return ListHandle.Array( list );
};

/*
| The shape used as mask for the inner contents of the frame.
*/
def.lazy._shapeMask =
	function( )
{
	const list = [ ];
	list.push( this.vDoc.tOutline.envelope( 1 ) );
	return FigureList.Array( list );
};
