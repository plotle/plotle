/*
| A handle on the document path bench.
*/
def.attributes =
{
	// center of the handle
	center: { type: 'gleam:Point' },

	// true if light color scheme
	light: { type: 'boolean' },

	// the jointNr
	jointNr: { type: 'number' },

	// the outline of the item this belongs to,
	outline: { type: [ 'undefined', '<gleam:Shape/Types' ] },

	// if set this joint will be removed from the path
	// (if this handle is used)
	removing: { type: [ 'undefined', 'number' ] },

	// the visual trace
	traceV: { type: 'ti2c:Trace' },

	// transform
	transform: { type: '<gleam:Transform/Types' },
};

import { Self as Color       } from '{gleam:Color}';
import { Self as Ellipse     } from '{gleam:Ellipse}';
import { Self as GlintFigure } from '{gleam:Glint/Figure}';
import { Self as GlintMask   } from '{gleam:Glint/Mask}';
import { Self as IconZoomIn  } from '{Shell/Design/Icon/ZoomIn}';
import { Self as IconZoomOut } from '{Shell/Design/Icon/ZoomOut}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';

/*
| Glint of the handle.
*/
def.lazy.glint =
	function( )
{
	const light = this.light;
	let glint =
		GlintFigure.FigureFillNogridName(
			this.shape,
			//Color.RGBA( 255, 220, 157, 0.9 ),
			Color.RGBA( 224, 210, 255, 0.9 ),
			this.traceV.asString,
		);

	let outline = this.outline;
	const transform = this.transform;

	const icon =
		(
			this.removing !== undefined
			? IconZoomOut.design( light )
			: IconZoomIn.design( light )
		)
		.shape
		.add( this.center.transform( transform ) );

	const glintIcon = GlintFigure.FigureFill( icon, Color.darkRed );

	if( !outline )
	{
		return ListGlint.Elements( glint, glintIcon );
	}
	else
	{
		outline = outline.transform( this.transform );

		return(
			ListGlint.Array( [
				GlintMask.create(
					'glint', glint,
					'outline', outline,
					'reversed', true
				),
				GlintMask.create(
					'glint',
						GlintFigure.FigureFillNogrid(
							this.shape,
							//Color.RGBA( 255, 240, 194, 0.9 ),
							Color.RGBA( 242, 236, 255, 0.85 ),
						),
					'outline', outline,
				),
				glintIcon,
			] )
		);
	}
};

/*
| The shape of the handle.
*/
def.lazy.shape =
	function( )
{
	return(
		Ellipse.CenterWidthHeight(
			this.center.transform( this.transform ),
			56, 56,
		)
	);
};

/*
| Returns true if a point is within
| the item.
*/
def.proto.within =
	function( p )
{
	return this.shape.within( p );
};
