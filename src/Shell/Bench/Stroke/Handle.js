/*
| A handle on the stroke bench.
*/
def.attributes =
{
	// the joint number
	jointNr: { type: 'number' },

	// the stroke for which the handle is made
	stroke: { type: 'Shell/View/Space/Item/Stroke' },

	// transform
	transform: { type: '<gleam:Transform/Types' },
};

import { Self as Color       } from '{gleam:Color}';
import { Self as Ellipse     } from '{gleam:Ellipse}';
import { Self as GlintFigure } from '{gleam:Glint/Figure}';
import { Self as GlintMask   } from '{gleam:Glint/Mask}';
import { Self as ListGlint   } from '{list@<gleam:Glint/Types}';

/*
| Glint of the handle.
*/
def.lazy.glint =
	function( )
{
	const stroke = this.stroke;
	const shape = this.shape;
	let glint = GlintFigure.FigureFill( shape, Color.RGBA( 255, 220, 157, 0.9 ) );

	const maskOutline = stroke.tOutlineOfJoint( this.jointNr );

	if( maskOutline )
	{
		return(
			ListGlint.Elements(
				GlintMask.create(
					'glint', glint,
					'outline', maskOutline,
					'reversed', true,
				),
				GlintMask.create(
					'glint', GlintFigure.FigureFill( shape, Color.RGBA( 255, 240, 194, 0.9 ) ),
					'outline', maskOutline,
				),
			)
		);
	}
	else
	{
		return glint;
	}
};

/*
| The shape of the handle.
*/
def.lazy.shape =
	function( )
{
	return(
		Ellipse.CenterWidthHeight(
			this.stroke
			.pointOfJoint( this.jointNr )
			.transform( this.transform ),
			56,
			56,
		)
	);
};

/*
| Returns true if a point is within
| the item.
*/
def.proto.within =
	function( p )
{
	return this.shape.within( p );
};
