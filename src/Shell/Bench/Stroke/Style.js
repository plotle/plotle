/*
| The style selection on the stroke bench.
*/
def.extend = 'Shell/Bench/Base';

def.attributes =
{
	// content of the frame
	content: { type: 'Shell/View/Space/Item/Set' },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// base point (from mother panel)
	pBase: { type: 'gleam:Point' },

	// seam data
	seam: { type: 'Shell/Seam/Space/Bench/Stroke' },

	// the visual trace of the view the bench is in
	traceV: { type: 'ti2c:Trace' },
};

import { Self as BenchBase        } from '{Shell/Bench/Base}';
import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as Color            } from '{gleam:Color}';
import { Self as DesignButton     } from '{Shell/Design/Widget/Button}';
import { Self as GlintFigure      } from '{gleam:Glint/Figure}';
import { Self as GlintPane        } from '{gleam:Glint/Pane}';
import { Self as GlintWindow      } from '{gleam:Glint/Window}';
import { Self as IconStyleArrow   } from '{Shell/Design/Icon/StyleArrow}';
import { Self as ListGlint        } from '{list@<gleam:Glint/Types}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as RectRoundExt     } from '{gleam:RectRoundExt}';
import { Self as Size             } from '{gleam:Size}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as TransformNormal  } from '{gleam:Transform/Normal}';
import { Self as TwigDesignWidget } from '{twig@<Shell/Design/Widget/Types}';
import { Self as TwigWidget       } from '{twig@<Shell/Widget/Types}';
import { Self as WidgetBase       } from '{Shell/Widget/Base}';

/*
| Checks if the frame has been clicked.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl  key was pressed
*/
def.proto.click =
	function( p, shift, ctrl )
{
	// ctrl-clicks are not swallowed.
	if( ctrl )
	{
		return undefined;
	}

	// not even near anything?
	if( !this._zonePanelT.within( p ) )
	{
		return undefined;
	}

	const pz = p.sub( this._zonePanelT.pos );

	// this is on the panel
	for( let widget of this._widgets.reverse( ) )
	{
		const bubble = widget.click( pz, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	// absorbs clicks on the grab area
	if( this._shapePanelTZ.within( pz ) )
	{
		return true;
	}

	return undefined;
};

/*
| Starts an operation with the pointing device held down.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	return undefined;
};

/*
| The frames glint.
*/
def.lazy.glint =
	function( )
{
	const list =
	[
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane', this._glintPanePanel,
			'pos',  this._zonePanelT.pos,
		),
	];

	return ListGlint.Array( list );
};

/*
| Mouse hover.
|
| Returns true if the pointing device hovers over anything.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const pz = p.sub( this._zonePanelT.pos );

	for( let widget of this._widgets.reverse( ) )
	{
		const bubble = widget.pointingHover( pz, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	return undefined;
};

/*
| A button has been pushed.
|
| ~traceV: traceV of the button
| ~shift:  true if shift was held
| ~ctrl:   true if ctrl/meta was held
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const buttonKey = traceV.backward( 'widgets' ).last.key;

	let styleBegin, styleEnd;
	switch( buttonKey )
	{
		case 'buttonStyleLine':
		{
			styleBegin = styleEnd = 'none';
			break;
		}

		case 'buttonStyleArrow':
		{
			styleBegin = 'none';
			styleEnd = 'arrow';
			break;
		}

		case 'buttonStyleArrowReverse':
		{
			styleBegin = 'arrow';
			styleEnd = 'none';
			break;
		}

		case 'buttonStyleArrowDual':
		{
			styleBegin = styleEnd = 'arrow';
			break;
		}

		default: throw new Error( );
	}

	const changes = [ ];
	for( let item of this.content )
	{
		const ancillary = item.ancillary;
		if( ancillary.styleBegin !== styleBegin )
		{
			changes.push(
				ChangeTreeAssign.create(
					'trace', ancillary.trace.add( 'styleBegin' ),
					'prev', ancillary.styleBegin,
					'val', styleBegin,
				),
			);
		}

		if( ancillary.styleEnd !== styleEnd )
		{
			changes.push(
				ChangeTreeAssign.create(
					'trace', ancillary.trace.add( 'styleEnd' ),
					'prev', ancillary.styleEnd,
					'val', styleEnd,
				),
			);
		}
	}

	Shell.change( this.window.uid, Sequence.Array( changes ) );
};

/*
| Zone.
*/
def.lazy.zone =
	function( )
{
	return this.content.zone;
};

/*
| The design
|
| ~light: if true light variant, otherwise dark.
*/
def.staticLazyFunc._designWidgets =
	( light ) =>
{
	const sizeButton                 = Size.WH( 32, 32 );
	const disButtons                 = Point.XY( 33, 0 );
	const posButtonStyleLine         = Point.XY(  9, 9 );
	const posButtonStyleArrow        = posButtonStyleLine.add( disButtons );
	const posButtonStyleArrowReverse = posButtonStyleArrow.add( disButtons );
	const posButtonStyleArrowDual    = posButtonStyleArrowReverse.add( disButtons );

	return(
		TwigDesignWidget.Grow(
			// line style button
			'buttonStyleLine',
			DesignButton.create(
				'facets',     BenchBase.facetsButton( light ),
				'icon',       IconStyleArrow.designLine( light ),
				'iconNoGrid', true,
				'shape',     'RectRound',
				'zone',       Rect.PosSize( posButtonStyleLine, sizeButton )
			),

			// arrow style button
			'buttonStyleArrow',
			DesignButton.create(
				'facets',     BenchBase.facetsButton( light ),
				'icon',       IconStyleArrow.designArrow( light ),
				'iconNoGrid', true,
				'shape',     'RectRound',
				'zone',       Rect.PosSize( posButtonStyleArrow, sizeButton )
			),

			// reverse arrow style button
			'buttonStyleArrowReverse',
			DesignButton.create(
				'facets',     BenchBase.facetsButton( light ),
				'icon',       IconStyleArrow.designArrowReverse( light ),
				'iconNoGrid', true,
				'shape',     'RectRound',
				'zone',       Rect.PosSize( posButtonStyleArrowReverse, sizeButton )
			),

			// dual arrow style button
			'buttonStyleArrowDual',
			DesignButton.create(
				'facets',     BenchBase.facetsButton( light ),
				'icon',       IconStyleArrow.designArrowDual( light ),
				'iconNoGrid', true,
				'shape',     'RectRound',
				'zone',       Rect.PosSize( posButtonStyleArrowDual, sizeButton )
			),
		)
	);
};

/*
| If all of the content has the same
| styleBegin and styleEnd, this is the name
| of the respective button to be highlighted.
*/
def.lazy._nameButtonStyleAllContent =
	function( )
{
	let styleBegin, styleEnd;
	let first = true;

	for( let item of this.content )
	{
		if( !first )
		{
			if(
				styleBegin !== item.ancillary.styleBegin
				|| styleEnd !== item.ancillary.styleEnd
			)
			{
				return undefined;
			}
		}
		else
		{
			first      = false;
			styleBegin = item.ancillary.styleBegin;
			styleEnd   = item.ancillary.styleEnd;
		}
	}

	return(
		styleBegin === 'none'
		? (
			styleEnd === 'none'
			? 'buttonStyleLine'
			: 'buttonStyleArrow'
		)
		: (
			styleEnd === 'none'
			? 'buttonStyleArrowReverse'
			: 'buttonStyleArrowDual'
		)
	);
};

/*
| The stroke only panel pane.
*/
def.lazy._glintPanePanel =
	function( )
{
	const light = this.light;

	const list =
	[
		GlintFigure.FigureColorFill(
			this._shapePanelTZ,
			Color.RGBA( 255, 220, 157, 0.9 ),
			light
			? Color.RGBA( 255, 250, 230, 0.85 )
			: Color.RGBA( 176, 165, 125, 0.85 ),
		),
	];

	for( let widget of this._widgets )
	{
		const g = widget.glint;
		if( g )
		{
			list.push( g );
		}
	}

	return(
		GlintPane.create(
			'glint',      ListGlint.Array( list ),
			'resolution', this.resolution,
			'size',       this._zonePanelT.size.add( 2 ),
		)
	);
};

/*
| The shape of panel transformed and relative to zero.
*/
def.lazy._shapePanelTZ =
	function( )
{
	const zpt   = this._zonePanelT;
	const ratio = this.resolution.ratio;
	const rpr   = 9 * ratio;

	return(
		RectRoundExt.PosWidthHeightHVNwsw(
			Point.zero,
			zpt.width,
			zpt.height,
			rpr, rpr,
		)
	);
};

/*
| Builds the widgets.
*/
def.lazy._widgets =
	function( )
{
	const light             = this.light;
	const designWidgets     = Self._designWidgets( light );
	const hover             = this.hover;
	const traceV            = this.traceV;
	const resolution        = this.resolution;
	const transform =
		TransformNormal.setScaleOffset(
			resolution.ratio,
			this._shapePanelTZ.pos,
		);

	const twig = { };

	for( let key of designWidgets.keys )
	{
		const traceW = traceV.add( 'widgets', key );

		let button =
			WidgetBase.FromDesign(
				designWidgets.get( key ),
				light, traceW, this.resolution,
				false, // systemFocus
				transform,
			);

		let hoverW = hover;
		if( hoverW && !hoverW.hasTrace( traceW ) )
		{
			hoverW = undefined;
		}

		let down = pass;
		const nbsac = this._nameButtonStyleAllContent;
		if( nbsac === key )
		{
			down = true;
		}

		twig[ key ] =
			button.create(
				'down',  down,
				'hover', hoverW,
			);
	}

	return TwigWidget.create( 'twig:init', twig, designWidgets.keys );
};

/*
| Zone of the panel.
*/
def.lazy._zonePanelT =
	function( )
{
	const ratio             = this.resolution.ratio;

	const pBase        = this.pBase;
	const posPanel     = pBase.add( -150 * ratio, 33 * ratio );

	return(
		Rect.PosWidthHeight(
			posPanel,
			150 * ratio,
			48  * ratio,
		)
	);
};
