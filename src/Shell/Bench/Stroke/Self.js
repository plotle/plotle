/*
| The stroke bench.
*/
def.extend = 'Shell/Bench/Base';

def.attributes =
{
	// content of the frame
	content: { type: 'Shell/View/Space/Item/Set' },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// seam data
	seam: { type: 'Shell/Seam/Space/Bench/Stroke' },
};

import { Self as ActionMoveStrokeHandle } from '{Shell/Action/MoveStrokeHandle}';
import { Self as BenchBase              } from '{Shell/Bench/Base}';
import { Self as BenchStrokeStyle       } from '{Shell/Bench/Stroke/Style}';
import { Self as Color                  } from '{gleam:Color}';
import { Self as DesignButton           } from '{Shell/Design/Widget/Button}';
import { Self as FigureList             } from '{gleam:Figure/List}';
import { Self as GlintFigure            } from '{gleam:Glint/Figure}';
import { Self as GlintMask              } from '{gleam:Glint/Mask}';
import { Self as GlintPane              } from '{gleam:Glint/Pane}';
import { Self as GlintWindow            } from '{gleam:Glint/Window}';
import { Self as Handle                 } from '{Shell/Bench/Stroke/Handle}';
import { Self as IconStyleArrow         } from '{Shell/Design/Icon/StyleArrow}';
import { Self as IconXSymbol            } from '{Shell/Design/Icon/XSymbol}';
import { Self as ListGlint              } from '{list@<gleam:Glint/Types}';
import { Self as ListHandle             } from '{list@Shell/Bench/Stroke/Handle}';
import { Self as Point                  } from '{gleam:Point}';
import { Self as Rect                   } from '{gleam:Rect}';
import { Self as RectRound              } from '{gleam:RectRound}';
import { Self as SeamSpaceBenchStroke   } from '{Shell/Seam/Space/Bench/Stroke}';
import { Self as Shell                  } from '{Shell/Self}';
import { Self as Size                   } from '{gleam:Size}';
import { Self as TransformNormal        } from '{gleam:Transform/Normal}';
import { Self as TwigDesignWidget       } from '{twig@<Shell/Design/Widget/Types}';
import { Self as TwigWidget             } from '{twig@<Shell/Widget/Types}';
import { Self as WidgetBase             } from '{Shell/Widget/Base}';

/*
| Checks if the frame has been clicked.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl  key was pressed
*/
def.proto.click =
	function( p, shift, ctrl )
{
	// ctrl-clicks are not swallowed.
	if( ctrl )
	{
		return undefined;
	}

	const panelStyle = this._panelStyle;
	if( panelStyle )
	{
		const bubble = panelStyle.click( p, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	// not even near anything?
	if( !this._zonePanelT.within( p ) )
	{
		return undefined;
	}

	const pz = p.sub( this._zonePanelT.pos );

	// this is on the panel
	for( let widget of this._widgets.reverse( ) )
	{
		const bubble = widget.click( pz, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	// absorbs clicks on the grab area
	if( this._shapePanelTZ.within( pz ) )
	{
		return true;
	}

	return undefined;
};

/*
| Starts an operation with the pointing device held down.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const window = this.window;

	if( window.access !== 'rw' )
	{
		return undefined;
	}
	const handleList = this._handleList;

	for( let handle of handleList )
	{
		if( handle.within( p ) )
		{
			const dp = p.detransform( window.transform );
			const stroke = handle.stroke;
			Shell.alter(
				'action',
					ActionMoveStrokeHandle.create(
						'jointNr', handle.jointNr,
						'pointStart', dp,
						'stroke', stroke,
						'traceV', stroke.traceV,
					)
			);

			return true;
		}
	}
	return undefined;
};

/*
| The frames glint.
*/
def.lazy.glint =
	function( )
{
	return(
		GlintMask.create(
			'glint',    this._glint,
			'outline',  this._mask,
			'reversed', true,
		)
	);
};

/*
| Mouse hover.
|
| Returns true if the pointing device hovers over anything.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const panelStyle = this._panelStyle;
	if( panelStyle )
	{
		const bubble = panelStyle.pointingHover( p, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	const pz = p.sub( this._zonePanelT.pos );
	for( let widget of this._widgets.reverse( ) )
	{
		const bubble = widget.pointingHover( pz, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	return undefined;
};

/*
| A button has been pushed.
|
| ~traceV: traceV of the button
| ~shift:  true if shift was held
| ~ctrl:   true if ctrl/meta was held
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	if( traceV.backward( 'style' ) )
	{
		this._panelStyle.pushButton( traceV, shift, ctrl );
		return;
	}

	const buttonKey = traceV.backward( 'widgets' ).last.key;
	switch( buttonKey )
	{
		case 'buttonRemove':
		{
			Shell.removeItems( this.content.traceVs );
			break;
		}

		case 'buttonStyle':
		{
			Shell.alter( SeamSpaceBenchStroke.traceSeam.add( 'openStyle' ), !this.seam.openStyle );
			break;
		}

		default: throw new Error( );
	}
};

/*
| Zone.
*/
def.lazy.zone =
	function( )
{
	return this.content.zone;
};

/*
| The design
|
| ~light: if true light variant, otherwise dark.
*/
def.staticLazyFunc._designWidgets =
	( light ) =>
{
	const sizeButton      = Size.WH( 32, 32 );
	const disButtons      = Point.XY( 0, 33 );
	const posButtonRemove = Point.XY( 9,  9 );
	const posButtonStyle  = posButtonRemove.add( disButtons );

	return(
		TwigDesignWidget.Grow(
			// remove button
			'buttonRemove',
			DesignButton.create(
				'facets',     BenchBase.facetsButton( light ),
				'icon',       IconXSymbol.design( light ),
				'iconNoGrid', true,
				'shape',     'RectRound',
				'zone',       Rect.PosSize( posButtonRemove, sizeButton )
			),

			// style button
			'buttonStyle',
			DesignButton.create(
				'facets',     BenchBase.facetsButton( light ),
				'icon',       IconStyleArrow.designArrow( light ),
				'iconNoGrid', true,
				'shape',     'RectRound',
				'zone',       Rect.PosSize( posButtonStyle, sizeButton )
			),
		)
	);
};

/*
| The unmasked glint.
*/
def.lazy._glint =
	function( )
{
	const list = [ ];

	const panelStyle = this._panelStyle;
	if( panelStyle )
	{
		list.push( panelStyle.glint );
	}

	list.push(
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane', this._glintPanePanel,
			'pos',  this._zonePanelT.pos,
		),
	);

	const handleList = this._handleList;
	for( let handle of handleList )
	{
		list.push( handle.glint );
	}

	return ListGlint.Array( list );
};

/*
| The stroke only panel pane.
*/
def.lazy._glintPanePanel =
	function( )
{
	const light = this.light;
	const list =
	[
		GlintFigure.FigureColorFill(
			this._shapePanelTZ,
			Color.RGBA( 255, 220, 157, 0.9 ),
			light
			? Color.RGBA( 255, 250, 230, 0.85 )
			: Color.RGBA( 176, 165, 125, 0.85 ),
		),
	];

	for( let widget of this._widgets )
	{
		const g = widget.glint;
		if( g )
		{
			list.push( g );
		}
	}

	return(
		GlintPane.create(
			'glint',      ListGlint.Array( list ),
			'resolution', this.resolution,
			'size',       this._zonePanelT.size.add( 2 ),
		)
	);
};

/*
| The list of handles.
*/
def.lazy._handleList =
	function( )
{
	const transform = this.window.transform;
	const list = [ ];

	for( let stroke of this.content )
	{
		list.push(
			Handle.create(
				'jointNr', 0,
				'stroke', stroke,
				'transform', transform,
			),
			Handle.create(
				'jointNr', 1,
				'stroke', stroke,
				'transform', transform,
			)
		);
	}

	return ListHandle.Array( list );
};

/*
| The figure used as mask for the inner contents of the frame.
*/
def.lazy._mask =
	function( )
{
	const list = [ ];
	for( let ca of this.content )
	{
		list.push( ca.tOutline.envelope( 1 ) );
	}
	return FigureList.Array( list );
};

/*
| The style panel popup.
*/
def.lazy._panelStyle =
	function( )
{
	const seam = this.seam;

	if( !seam.openStyle )
	{
		return undefined;
	}

	const resolution = this.resolution;
	let pBase = this._zonePanelT.pos;

	return(
		BenchStrokeStyle.create(
			'content',    this.content,
			'hover',      this.hover,
			'light',      this.light,
			'pBase',      pBase,
			'resolution', resolution,
			'seam',       seam,
			'traceV',     this.traceV.add( 'style' ),
			'window',     this.window,
		)
	);
};

/*
| The shape of panel transformed and relative to zero.
*/
def.lazy._shapePanelTZ =
	function( )
{
	const zpt   = this._zonePanelT;
	const rp    = 9 * this.resolution.ratio;

	return(
		RectRound.create(
			'a',       rp,
			'b',       rp,
			'height',  zpt.height,
			'pos',     Point.zero,
			'width',   zpt.width,
		)
	);
};

/*
| Builds the widgets.
*/
def.lazy._widgets =
	function( )
{
	const light         = this.light;
	const designWidgets = Self._designWidgets( this.light );
	const hover         = this.hover;
	const seam          = this.seam;
	const traceV        = this.traceV;
	const resolution    = this.resolution;
	const transform =
		TransformNormal.setScaleOffset(
			resolution.ratio,
			this._shapePanelTZ.pos,
		);

	const twig = { };

	for( let key of designWidgets.keys )
	{
		const traceW = traceV.add( 'widgets', key );

		let button =
			WidgetBase.FromDesign(
				designWidgets.get( key ),
				light,
				traceW,
				resolution,
				false, // systemFocus
				transform,
			);

		let hoverW = hover;
		if( hoverW && !hoverW.hasTrace( traceW ) )
		{
			hoverW = undefined;
		}

		let down = pass;

		switch( key )
		{
			case 'buttonStyle':
			{
				down = seam.openStyle;
				break;
			}
		}

		twig[ key ] =
			button.create(
				'down',  down,
				'hover', hoverW,
			);
	}

	return TwigWidget.create( 'twig:init', twig, designWidgets.keys );
};

/*
| Zone of the panel.
*/
def.lazy._zonePanelT =
	function( )
{
	const ratio = this.resolution.ratio;

	let pc;
	{
		const content = this.content;
		let pcx = 0, pcy = 0;
		for( let item of content )
		{
			const pci = item.segmentT.pc;
			pcx += pci.x;
			pcy += pci.y;
		}
		const sc = content.size;
		pc = Point.XY( pcx / sc, pcy / sc );
	}

	return(
		Rect.PosWidthHeight(
			pc.add( -66 * ratio, 0 ),
			48 * ratio,
			86 * ratio,
		)
	);
};
