/*
| Split screen.
*/
def.extend = 'Shell/Screen/Space';

def.attributes =
{
	// doc view seam
	docViewSeam: { type: [ 'undefined', 'Shell/Seam/DocView/Root' ] },

	// number 0..1 where the split happens
	ratioSplit: { type: 'number' },
};

import { Self as ActionMoveSplit  } from '{Shell/Action/MoveSplit}';
import { Self as ActionZoomButton } from '{Shell/Action/ZoomButton}';
import { Self as Color            } from '{gleam:Color}';
import { Self as GlintFigure      } from '{gleam:Glint/Figure}';
import { Self as GlintPane        } from '{gleam:Glint/Pane}';
import { Self as GlintWindow      } from '{gleam:Glint/Window}';
import { Self as Hover            } from '{Shell/Result/Hover}';
import { Self as ListGlint        } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret        } from '{Shared/Mark/Caret}';
import { Self as MarkItems        } from '{Shared/Mark/Items}';
import { Self as MarkRange        } from '{Shared/Mark/Range}';
import { Self as MarkWidget       } from '{Shared/Mark/Widget}';
import { Self as PanelTabSpace    } from '{Shell/Panel/TabSpace}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as ScreenBase       } from '{Shell/Screen/Base}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as Segment          } from '{gleam:Segment}';
import { Self as SysMode          } from '{Shell/Result/SysMode}';
import { Self as TraceRoot        } from '{Shared/Trace/Root}';
import { Self as TransformNormal  } from '{gleam:Transform/Normal}';
import { Self as ViewSpace        } from '{Shell/View/Space/Self}';
import { Self as WindowSpace      } from '{Shell/Window/Space}';

/*
| Changes the transform so that all items of the space are visible in the rectView.
*/
def.proto.changeSpaceTransformAll =
	function( atWindow )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( atWindow !== 0 && atWindow !== 1 ) throw new Error( );
/**/}

	let view;
	switch( atWindow )
	{
		case 0: view = this._view( 0 ); break;
		case 1: view = this._view( 1 ); break;
		default: throw new Error( );
	}

	view.changeSpaceTransformAll( );
};

/*
| Changes the zoom factor keeping current center.
|
| ~atWindow: which window to change
| ~dir:      direction of zoom change (+/- 1)
*/
def.proto.changeSpaceTransformCenter =
	function( atWindow, dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/	if( dir !== -1 && dir !== 1 ) throw new Error( );
/**/}

	let rectView;
	switch( atWindow )
	{
		case 0: rectView = this._view( 0 ).rectView; break;
		case 1: rectView = this._view( 1 ).rectView; break;
		default: throw new Error( );
	}

	Shell.changeSpaceTransformPoint( atWindow, dir, rectView.zeroPos.pc );
};

/*
| A click.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).click( p, shift, ctrl );
		if( bubble !== undefined )
		{
			return bubble;
		}
	}

	if( this._rectView( 0 ).within( p ) )
	{
		return this._view( 0 ).click( p, shift, ctrl );
	}

	const rv1 = this._rectView( 1 );
	if( rv1.within( p ) )
	{
		p = p.sub( rv1.pos );
		return this._view( 1 ).click( p, shift, ctrl );
	}

	return undefined;
};

/*
| The content the mark puts into the clipboard.
*/
def.lazy.clipboard =
	function( )
{
	const mark = this.mark;
	const v1 = this._view( 1 );

	if( mark && mark.encompasses( v1.traceV ) )
	{
		return this._view( 1 ).clipboard;
	}
	else
	{
		return this._view( 0 ).clipboard;
	}
};

/*
| Cycles focus.
|
| ~dir: +1 clockwise, -1 anticlockwise
*/
def.proto.cycleFocus =
	function( dir )
{
	this._view( 0 ).cycleFocus( dir );
};

/*
| Drag moves.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto.dragMove =
	function( p, shift, ctrl )
{
	const action = this.action;

	switch( action?.ti2ctype )
	{

		case undefined:
		case ActionZoomButton:
			return;

		case ActionMoveSplit:
		{
			Shell.alter( 'ratioSplit', p.x / this.sizeDisplay.width );
			return;
		}

		default:
		{
			const traceV = action.traceV;
			switch( traceV.forward( 'view' ).last.at )
			{
				case 0:
					this._view( 0 ).dragMove( p, shift, ctrl );
					return;

				case 1:
					p = p.sub( this._rectView( 1 ).pos );
					this._view( 1 ).dragMove( p, shift, ctrl );
					return;

				default: throw new Error( );
			}
		}
	}
};

/*
| Starts an operation with the pointing device held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	const action = this.action;

	if( this._nearSplit( p.x ) )
	{
		Shell.alter(
			'action',
				ActionMoveSplit.create(
					'pointStart', p,
					'traceV', TraceRoot.root.add( 'screen' ),
				)
			);
		return true;
	}

	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).dragStart( p, shift, ctrl, action );
		if( bubble !== undefined )
		{
			return bubble;
		}
	}

	if( this._rectView( 0 ).within( p ) )
	{
		return this._view( 0 ).dragStart( p, shift, ctrl );
	}

	const rv1 = this._rectView( 1 );
	if( rv1.within( p ) )
	{
		p = p.sub( rv1.pos );
		return this._view( 1 ).dragStart( p, shift, ctrl );
	}

	return undefined;
};

/*
| Stops an operation with the mouse button held down.
*/
def.proto.dragStop =
	function( p, shift, ctrl )
{
	const action = this.action;

	if( !action ) return;

	if( action.ti2ctype === ActionMoveSplit )
	{
		Shell.alter( 'action', undefined );
		return;
	}

	const traceV = action.traceV;
	const tv1 = traceV.get( 1 );
	switch( tv1.name )
	{
		case 'panels':
			this._getPanel( tv1.key ).dragStop( p, shift, ctrl, action );
			break;

		case 'view':
			switch( tv1.at )
			{
				case 0:
				{
					this._view( 0 ).dragStop( p, shift, ctrl );
					break;
				}

				case 1:
				{
					p = p.sub( this._rectView( 1 ).pos );
					this._view( 1 ).dragStop( p, shift, ctrl );
					break;
				}
			}
			break;

		default: throw new Error( );
	}
};

/*
| Exports current docPath as docx.
*/
def.proto.exportDocPath =
	function( )
{
	this._view( 1 ).exportDocPath( );
};

/*
| Filters a mark.
|
| TODO check if this cant be done more simply with encompasses( )
| TODO including all other filterMark functions
*/
def.staticLazyFunc.filterMark =
	function( mark )
{
	if( !mark ) return undefined;

	switch( mark.ti2ctype )
	{
		case MarkCaret:
		case MarkWidget:
			return(
				mark.traceV.backward( 'docView' )
				|| mark.traceV.backward( 'view' )
				? mark
				: undefined
			);

		case MarkItems:
			return mark;

		case MarkRange:
			return(
				mark.traceBegin.backward( 'docView' )
				|| mark.traceEnd.backward( 'docView' )
				|| mark.traceBegin.backward( 'view' )
				|| mark.traceEnd.backward( 'view' )
				? mark
				: undefined
			);

		default: throw new Error( );
	}
};

/*
| Return the space glint.
*/
def.lazy.glint =
	function( )
{
	const sl = this._splitLeft;
	const rv1 = this._rectView( 1 );

	const splitter =
		Segment.P0P1(
			Point.XY( sl, 0 ),
			Point.XY( sl, this.sizeDisplay.height ),
		);

	let glintSplitter;
	if( this._view( 1 ).ti2ctype === ViewSpace )
	{
		glintSplitter = GlintFigure.FigureColor( splitter, Color.black );
	}
	else
	{
		glintSplitter =
			ListGlint.Elements(
				GlintFigure.FigureColorWidth( splitter, Color.RGB( 255, 188, 87 ), 1.2 ),
				GlintFigure.FigureColor( splitter, Color.black ),
			);
	}

	const list =
	[
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint',      this._view( 0 ).glint,
					'resolution', this.resolution,
					'size',       this._rectView( 0 ).size,
				),
			'pos', Point.zero,
		),
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint',      this._view( 1 ).glint,
					'resolution', this.resolution,
					'size',       rv1.size,
				),
			'pos', rv1.pos,
		),
		glintSplitter,
	];

	for( let key of this._keysPanel.reverse( ) )
	{
		list.push( this._getPanel( key ).glint );
	}

	return ListGlint.Array( list );
};

/*
| User is inputting characters.
*/
def.proto.input =
	function( str )
{
	const mark = this.mark;

	const v1 = this._view( 1 );
	if( mark && mark.encompasses( v1.traceV ) )
	{
		return this._view( 1 ).input( str );
	}
	else
	{
		return this._view( 0 ).input( str );
	}
};

/*
| Mouse wheel.
|
| ~p:     cursor point
| ~dir:   wheel direction, >0 for down, <0 for up
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	if( this._rectView( 0 ).within( p ) )
	{
		return this._view( 0 ).mousewheel( p, dir, shift, ctrl );
	}

	const rv1 = this._rectView( 1 );
	if( rv1.within( p ) )
	{
		p = p.sub( rv1.pos );
		return this._view( 1 ).mousewheel( p, dir, shift, ctrl );
	}

	return undefined;
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.proto.paste =
	function( type, data )
{
	const mark = this.mark;

	if( data.startsWith( '//#*PLOTLE+-' ) )
	{
		// in case its plotle item data directly forward this to
		// visual even if docView has focus
		return this._view( 0 ).paste( type, data );
	}

	const v1 = this._view( 1 );
	if( mark && mark.encompasses( v1.traceV ) )
	{
		return this._view( 1 ).paste( type, data );
	}
	else
	{
		return this._view( 0 ).paste( type, data );
	}
};

/*
| Mouse hover.
| Returns a Hover result with hovering trace and cursor to show.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const action = this.action;
	switch( action?.ti2ctype )
	{
		case ActionMoveSplit:
		{
			return Hover.CursorEwResize;
		}

		case undefined:
		{
			if( this._nearSplit( p.x ) )
			{
				return Hover.CursorEwResize;
			}
			break;
		}
	}

	for( let key of this._keysPanel )
	{
		const hover = this._getPanel( key ).pointingHover( p, shift, ctrl );
		if( hover ) return hover;
	}

	if( this._rectView( 0 ).within( p ) )
	{
		return this._view( 0 ).pointingHover( p, shift, ctrl );
	}
	else
	{
		return this._view( 1 ).pointingHover( p.sub( this._rectView( 1 ).pos ), shift, ctrl );
	}
};

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const tv1 = traceV.get( 1 );

	if( tv1.name === 'panels' )
	{
		this._getPanel( tv1.key ).pushButton( traceV, shift, ctrl );
		return;
	}

/**/if( CHECK && tv1.name !== 'view' ) throw new Error( );

	switch( tv1.at )
	{
		case 0:
			this._view( 0 ).pushButton( traceV, shift, ctrl );
			return;

		case 1:
			this._view( 1 ).pushButton( traceV, shift, ctrl );
			return;

		default: throw new Error( );
	}
};

/*
| Performs some maintenance.
*/
def.proto.rectify =
	function( )
{
	this._view( 0 ).rectify( );
	this._view( 1 ).rectify( );
};

/*
| Scrolls current mark into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	const mark = this.mark;

	const v1 = this._view( 1 );
	if( mark && mark.encompasses( v1.traceV ) )
	{
		this._view( 1 ).scrollMarkIntoView( );
	}
	else
	{
		this._view( 0 ).scrollMarkIntoView( );
	}
};

/*
| User pressed a special key.
|
| ~key:   key being pressed
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
| ~inputWord: the last word (on mobile)
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
	const mark = this.mark;

	const v1 = this._view( 1 );
	if( mark && mark.encompasses( v1.traceV ) )
	{
		return this._view( 1 ).specialKey( key, shift, ctrl, inputWord );
	}
	else
	{
		return this._view( 0 ).specialKey( key, shift, ctrl, inputWord );
	}
};

/*
| The system mode.
*/
def.lazy.sysMode =
	function( )
{
	const mark = this.mark;
	if( !mark ) return SysMode.Blank;

	const v0 = this._view( 0 );
	const v1 = this._view( 1 );

	if( mark.encompasses( v0.traceV ) )
	{
		return v0.sysMode;
	}

	if( mark.encompasses( v1.traceV ) )
	{
		return v1.sysMode;
	}

	return SysMode.Blank;
};

/*
| Transforms a mark.
*/
def.proto.transformMark =
	function( mark, changeX )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	const tvc = mark?.traceVCaret;
	if( !tvc )
	{
		return mark;
	}

	switch( tvc.forward( 'view' ).last.at )
	{
		case 0:
			return this._view( 0 ).transformMark( mark, changeX );

		case 1:
			return this._view( 1 ).transformMark( mark, changeX );

		default:
			throw new Error( );
	}
};

/*
| Gets a panel.
*/
def.lazyFunc._getPanel =
	function( key )
{
	switch( key )
	{
		case 'tabSpace1':
		{
			let hover = this.hover;
			if( hover )
			{
				const h1 = hover.get( 1 );
				if( h1.name !== 'panels' || h1.key !== key )
				{
					hover = undefined;
				}
			}

			const traceV = TraceRoot.root.add( 'panels', key );
			const sizeDisplay = this.sizeDisplay;
			const win1 = this.windows.get( 1 );
			const resolution = this.resolution;
			const transform = TransformNormal.setScale( resolution.ratio );

			const xo = this._rectView( 1 ).pos.x;

			return(
				PanelTabSpace.create(
					'atWindow',    1,
					'hover',       hover,
					'mark',        this.mark,
					'mode',        this.mode,
					'rectView',
						Rect.PosWidthHeight(
							Point.XY( xo, 0 ),
							sizeDisplay.width - xo,
							sizeDisplay.height,
						),
					'refSpace',    win1.ref,
					'resolution',  this.resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
					'window',      win1,
				)
			);
		}

		default: return ScreenBase._getPanel.call( this, key );
	}
};

/*
| List of keys of visible panels.
|
| Order is foreground to background.
*/
def.lazy._keysPanel =
	function( )
{
	const listBase = ScreenBase._keysPanel.call( this );
	const win1 = this.windows.get( 1 );

	return(
		win1.ti2ctype === WindowSpace
		? listBase.append( 'tabSpace1' )
		: listBase
	);
};

/*
| Returns true if x is near the split.
*/
def.proto._nearSplit =
	function( x )
{
	const lcd = config.splitClickDistance;
	const sl = this._splitLeft;
	return x >= sl - lcd && x <= sl + lcd + 1;
};

/*
| The split starts here.
*/
def.lazy._splitLeft =
	function( )
{
	return Math.round( this.sizeDisplay.width * this.ratioSplit );
};
