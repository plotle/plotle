/*
| Showing the space only.
*/
def.extend = 'Shell/Screen/Base';

def.attributes =
{
	// current space fabric data
	ancillaries: { type: 'group@Shared/Ancillary/Space' },

	// local extra data
	seamSpace: { type: 'Shell/Seam/Space/Self' },

	// show space grid
	showGrid: { type: 'boolean' },

	// show space guides
	showGuides: { type: 'boolean' },

	// space has snapping
	snapping: { type: 'boolean' },
};

import { Self as ActionZoomButton     } from '{Shell/Action/ZoomButton}';
import { Self as ASpace               } from '{Shared/Ancillary/Space}';
import { Self as Color                } from '{gleam:Color}';
import { Self as DesignIconOpts       } from '{Shell/Design/Icon/Opts}';
import { Self as DesignIconSelect     } from '{Shell/Design/Icon/Select}';
import { Self as Facet                } from '{Shell/Facet/Self}';
import { Self as FacetBorder          } from '{Shell/Facet/Border}';
import { Self as FacetList            } from '{Shell/Facet/List}';
import { Self as FormSpaceDeleted     } from '{Shell/Form/SpaceDeleted}';
import { Self as FormLoading          } from '{Shell/Form/Loading}';
import { Self as FormNoAccessToSpace  } from '{Shell/Form/NoAccessToSpace}';
import { Self as FormNonExistingSpace } from '{Shell/Form/NonExistingSpace}';
import { Self as GlintPane            } from '{gleam:Glint/Pane}';
import { Self as GlintWindow          } from '{gleam:Glint/Window}';
import { Self as GroupBoolean         } from '{group@boolean}';
import { Self as ListFacetBorder      } from '{list@Shell/Facet/Border}';
import { Self as ListGlint            } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret            } from '{Shared/Mark/Caret}';
import { Self as MarkItems            } from '{Shared/Mark/Items}';
import { Self as MarkRange            } from '{Shared/Mark/Range}';
import { Self as MarkWidget           } from '{Shared/Mark/Widget}';
import { Self as ModeForm             } from '{Shell/Mode/Form}';
import { Self as ModeSelect           } from '{Shell/Mode/Select}';
import { Self as Point                } from '{gleam:Point}';
import { Self as Rect                 } from '{gleam:Rect}';
import { Self as Shell                } from '{Shell/Self}';
import { Self as TraceRoot            } from '{Shared/Trace/Root}';
import { Self as TransformNormal      } from '{gleam:Transform/Normal}';
import { Self as ViewDoc              } from '{Shell/View/Doc/Self}';
import { Self as ViewSpace            } from '{Shell/View/Space/Self}';
import { Self as WidgetButton         } from '{Shell/Widget/Button}';
import { Self as WindowDeleted        } from '{Shell/Window/Deleted}';
import { Self as WindowDoc            } from '{Shell/Window/DocView}';
import { Self as WindowLoading        } from '{Shell/Window/Loading}';
import { Self as WindowNoAccess       } from '{Shell/Window/NoAccess}';
import { Self as WindowNonExisting    } from '{Shell/Window/NonExisting}';
import { Self as WindowSpace          } from '{Shell/Window/Space}';

/*
| Changes the transform so that all items of the space are visible in the rectView.
*/
def.proto.changeSpaceTransformAll =
	function( atWindow )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( atWindow !== 0 ) throw new Error( );
/**/}

	this._view( 0 ).changeSpaceTransformAll( );
};

/*
| Changed the zoom to 1 and pans home.
*/
def.proto.changeSpaceTransformHome =
	function( atWindow )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/}

	Shell.changeTransformTo(
		atWindow,
		0,
		TransformNormal.singleton.setScaleOffset( this.resolution.ratio, Point.zero ),
		120, // zoomAllHomeTime
	);
};

/*
| Changes the zoom factor keeping current center.
|
| ~atWindow: which window to change
| ~dir:      direction of zoom change (+/- 1)
*/
def.proto.changeSpaceTransformCenter =
	function( atWindow, dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( atWindow ) !== 'number' ) throw new Error( );
/**/	if( dir !== -1 && dir !== 1 ) throw new Error( );
/**/}

	const rectView = this._view( 0 ).rectView;

	Shell.changeSpaceTransformPoint( atWindow, dir, rectView.zeroPos.pc );
};

/*
| Removes empty labels in case they lost the users mark.
|
| TODO take care of view1
|
| ~oldMark: the old mark.
*/
def.proto.checkEmptyLabels =
	function( oldMark )
{
	if( this.windows.get( 0 ) !== WindowSpace )
	{
		return;
	}

	const mark = this.mark;
	if( !mark || !mark.hasCaret )
	{
		return;
	}

	const mtvc = mark.traceVCaret;
	if( !mtvc || mtvc.forward( 'view' ).last.at !== 0 )
	{
		return;
	}

	const view = this._view( 0 );

	const oim = oldMark.itemsMark;
	if( oim )
	{
		const nim = mark && mark.itemsMark;

		for( let itemTraceV of oim.traces )
		{
			if( nim && nim.containsItemTraceV( itemTraceV ) )
			{
				continue;
			}

			// TODO sometimes its key?
			const item = view.items.get( itemTraceV.last.key );

			if( item )
			{
				item.markLost( );
			}
		}
	}
};

/*
| A click.
*/
def.proto.click =
	function( p, shift, ctrl )
{
	{
		const bubble = this._buttonOpts.click( p, shift, ctrl );
		if( bubble !== undefined )
		{
			Shell.alter( 'mode', ModeForm.opts );
			return true;
		}
	}
	{
		const bubble = this._buttonSelect.click( p, shift, ctrl );
		if( bubble !== undefined )
		{
			Shell.alter( 'mode', ModeSelect.create( 'fallbackMode', this.mode ) );
			return true;
		}
	}

	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).click( p, shift, ctrl );
		if( bubble !== undefined )
		{
			return bubble;
		}
	}

	return this._view( 0 ).click( p, shift, ctrl );
};

/*
| The content the mark puts into the clipboard.
*/
def.lazy.clipboard =
	function( )
{
	return this._view( 0 ).clipboard;
};

/*
| Cycles focus.
|
| ~dir: +1 clockwise, -1 anticlockwise
*/
def.proto.cycleFocus =
	function( dir )
{
	this._view( 0 ).cycleFocus( dir );
};

/*
| Drag moves.
|
| ~p:     point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.dragMove =
	function( p, shift, ctrl )
{
	const action = this.action;
	if( !action ) return;

	switch( this.action.ti2ctype )
	{
		case ActionZoomButton:
			return;

		default:
			this._view( 0 ).dragMove( p, shift, ctrl );
			return;
	}
};

/*
| Starts an operation with the pointing device held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	const action = this.action;
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).dragStart( p, shift, ctrl, action );
		if( bubble !== undefined )
		{
			return bubble;
		}
	}

	return this._view( 0 ).dragStart( p, shift, ctrl );
};

/*
| A button has been drag-started.
*/
def.proto.dragStartButton =
	function( trace )
{
	const t1 = trace.get( 1 );
	switch( t1.name )
	{
		case 'panels':
			return this._getPanel( t1.key ).dragStartButton( trace, false, false );

		default:
			return false;
	}
};

/*
| Stops an operation with the mouse button held down.
*/
def.proto.dragStop =
	function( p, shift, ctrl )
{
	const action = this.action;
	if( !action ) return;

	const traceV = action.traceV;

	//if( traceV.length < 2 ) return;

	const tv1 = traceV.get( 1 );
	switch( tv1.name )
	{
		case 'view':
			// TODO check if key is 0
			this._view( 0 ).dragStop( p, shift, ctrl );
			break;

		case 'panels':
			this._getPanel( tv1.key ).dragStop( p, shift, ctrl, action );
			break;

		default: throw new Error( );
	}
};

/*
| Filters a mark.
|
| TODO check if this can't be done more simply with encompasses( )
| TODO including all other filterMark functions
*/
def.staticLazyFunc.filterMark =
	function( mark )
{
	if( !mark ) return undefined;

	switch( mark.ti2ctype )
	{
		case MarkCaret:
		case MarkWidget:
		{
			return(
				mark.traceV.forward( 'view' )
				? mark
				: undefined
			);
		}

		case MarkItems:
		{
			return mark;
		}

		case MarkRange:
		{
			return(
				mark.traceBegin.forward( 'view' )
				|| mark.traceEnd.forward( 'view' )
				? mark
				: undefined
			);
		}

		default: throw new Error( );
	}
};

/*
| Return the space glint.
*/
def.lazy.glint =
	function( )
{
	const list = [ ];

	list.push(
		GlintWindow.create(
			'name', TraceRoot.root.add( 'view', 0 ).asString,
			'pane',
				GlintPane.create(
					'glint',      this._view( 0 ).glint,
					'resolution', this.resolution,
					'size',       this.sizeDisplay,
				),
			'pos', Point.zero,
		),
		this._buttonOpts.glint,
		this._buttonSelect.glint,
	);

	for( let key of this._keysPanel.reverse( ) )
	{
		list.push( this._getPanel( key ).glint );
	}

	return ListGlint.Array( list );
};

/*
| User is inputting characters.
*/
def.proto.input =
	function( str )
{
	this._view( 0 ).input( str );
};

/*
| Mouse wheel.
|
| ~p:     cursor point
| ~dir:   wheel direction, >0 for down, <0 for up
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).mousewheel( p, dir, shift, ctrl );
		if( bubble )
		{
			return;
		}
	}

	this._view( 0 ).mousewheel( p, dir, shift, ctrl );
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.proto.paste =
	function( type, data )
{
	this._view( 0 ).paste( type, data );
};

/*
| Mouse hover.
| Returns a ResultHover with hovering trace and cursor to show.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	{
		const hover = this._buttonOpts.pointingHover( p, shift, ctrl );
		if( hover )
		{
			return hover;
		}
	}
	{
		const hover = this._buttonSelect.pointingHover( p, shift, ctrl );
		if( hover )
		{
			return hover;
		}
	}

	for( let key of this._keysPanel )
	{
		const hover = this._getPanel( key ).pointingHover( p, shift, ctrl );
		if( hover )
		{
			return hover;
		}
	}

	return this._view( 0 ).pointingHover( p, shift, ctrl );
};

/*
| The pointing device just went down.
| Probes if the system ought to wait if it's
| a click or can initiate a drag right away.
*/
def.proto.probeClickDrag =
	function( p, shift, ctrl )
{
	for( let key of this._keysPanel )
	{
		const bubble = this._getPanel( key ).probeClickDrag( p, shift, ctrl );
		if( bubble !== undefined )
		{
			return bubble;
		}
	}

	// future probe vSpace
	return 'atween';
};

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const tv1 = traceV.get( 1 );
	switch( tv1.name )
	{
		case 'panels':
		{
			this._getPanel( tv1.key ).pushButton( traceV, shift, ctrl );
			return;
		}

		case 'float':
		{
			return;
		}

		case 'view':
		{
			this._view( 0 ).pushButton( traceV, shift, ctrl );
			return;
		}

		default: throw new Error( );
	}
};

/*
| Performs some maintenance.
*/
def.proto.rectify =
	function( )
{
	this._view( 0 ).rectify( );
};

/*
| Scrolls current mark into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	this._view( 0 ).scrollMarkIntoView( );
};

/*
| User pressed a special key.
|
| ~key:   key being pressed
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
| ~inputWord: the last word (on mobile)
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
	this._view( 0 ).specialKey( key, shift, ctrl, inputWord );
};

/*
| The system mode.
*/
def.lazy.sysMode =
	function( )
{
	return this._view( 0 ).sysMode;
};

/*
| Transforms a mark.
*/
def.proto.transformMark =
	function( mark, changeX )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	return this._view( 0 ).transformMark( mark, changeX );
};

/*
| Converts a visual trace to a fabric trace.
*/
def.proto.traceV2F =
	function( trace )
{
	const atWindow = trace.forward( 'view' ).last.at;

	return this._view( atWindow ).traceV2F( trace );
};

/*
| The rectangle of a view.
*/
def.lazyFunc._rectView =
	function( atWindow )
{
	const sv = this.sizeDisplay;

	if( atWindow === 0 )
	{
		const windows = this.windows;

		if( windows.length === 1 )
		{
			return sv.zeroRect;
		}
		else
		{
/**/		if( CHECK && windows.length !== 2 ) throw new Error( );

			return Rect.PosWidthHeight( Point.zero, this._splitLeft, sv.height );
		}
	}
	else
	{
/**/	if( CHECK && atWindow !== 1 ) throw new Error( );

		const splitLeft = this._splitLeft;

		return(
			Rect.PosWidthHeight(
				Point.XY( splitLeft + 2, 0 ),
				sv.width - splitLeft,
				sv.height
			)
		);
	}
};

/*
| Builds a visual space view.
|
| ~atWindow: window # it is at
*/
def.lazyFunc._view =
	function( atWindow )
{
	const windows = this.windows;
	const window = windows.get( atWindow );
	let traceV = TraceRoot.root.add( 'view', atWindow );

	switch( window.ti2ctype )
	{
		case WindowDeleted:     traceV = traceV.add( 'form', 'deleted'     ); break;
		case WindowDoc:         traceV = traceV.add( 'docView'             ); break;
		case WindowLoading:     traceV = traceV.add( 'form', 'loading'     ); break;
		case WindowNoAccess:    traceV = traceV.add( 'form', 'noAccess'    ); break;
		case WindowNonExisting: traceV = traceV.add( 'form', 'nonExisting' ); break;
		case WindowSpace:       traceV = traceV.add( 'space', window.uid   ); break;
		default: throw new Error( );
	}

	let mark = this.mark;
	if( mark && !mark.encompasses( traceV ) )
	{
		mark = undefined;
	}

	let hover = this.hover;
	if( hover && !hover.hasTrace( traceV ) )
	{
		hover = undefined;
	}

	let action = this.action;
	if( action && !action.traceV.hasTrace( traceV ) )
	{
		action = undefined;
	}

	const sizeDisplay = this.sizeDisplay;
	const rectView = this._rectView( atWindow );

	const light = this.light;

	switch( window.ti2ctype )
	{
		case WindowDeleted:
		{
			const transform =
				TransformNormal.setScaleOffset(
					this.resolution.ratio,
					rectView.zeroPos.pc,
				);

			return(
				FormSpaceDeleted.create(
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
					'window',      window,
				)
			);
		}

		case WindowDoc:
		{
			const win0 = windows.get( 0 );
			const win1 = windows.get( 1 );

			return(
				ViewDoc
				.create(
					'access',      win0.access,
					'ancillary',   this.ancillaries.get( win0.uid ),
					'action',      action,
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'resolution',  this.resolution,
					'seam',        this.docViewSeam,
					'systemFocus', this.systemFocus,
					'traceV',      traceV,
					'viewClip',    rectView,
					'window',      win1,
				)
			);
		}

		case WindowLoading:
		{
			const transform =
				TransformNormal.setScaleOffset(
					this.resolution.ratio,
					rectView.zeroPos.pc,
				);

			return(
				FormLoading.create(
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'refSpace',    window.ref,
					'resolution',  this.resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
				)
			);
		}

		case WindowNoAccess:
		{
			const transform =
				TransformNormal.setScaleOffset(
					this.resolution.ratio,
					rectView.zeroPos.pc,
				);

			return(
				FormNoAccessToSpace.create(
					'atWindow',    atWindow,
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
					'window',      window,
				)
			);
		}

		case WindowNonExisting:
		{
			const transform =
				TransformNormal.setScaleOffset(
					this.resolution.ratio,
					rectView.zeroPos.pc,
				);

			return(
				FormNonExistingSpace.create(
					'atWindow',    atWindow,
					'hover',       hover,
					'light',       light,
					'mark',        mark,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'sizeDisplay', sizeDisplay,
					'systemFocus', this.systemFocus,
					'transform',   transform,
					'traceV',      traceV,
					'window',      window,
				)
			);
		}

		case WindowSpace:
		{
			const uid = window.uid;
			let ancillary = this.ancillaries.get( uid );

			if( action )
			{
				// applies ancillaries changes of the action on the visual space.
				const chgsAT = action.changesTransient;
				if( chgsAT )
				{
					ancillary = ASpace.FromFabric( chgsAT.changeTree( ancillary.asFabric ), uid );

					for(;;)
					{
						const chgs = ancillary.changes( chgsAT.affectedTwigItems );
						if( !chgs )
						{
							break;
						}

						ancillary = ASpace.FromFabric( chgs.changeTree( ancillary.asFabric ), uid );
					}
				}
			}

			return(
				ViewSpace.create(
					'action',      action,
					'ancillary',   ancillary,
					'hover',       this.hover,
					'light',       light,
					'mark',        mark,
					'mode',        this.mode,
					'rectView',    rectView,
					'resolution',  this.resolution,
					'seam',        this.seamSpace,
					'showGrid',    this.showGrid,
					'showGuides',  this.showGuides,
					'snapping',    this.snapping,
					'systemFocus', this.systemFocus,
					'traceV',      traceV,
					'userCreds',   this.userCreds,
					'window',      window,
				)
			);
		}

		default: throw new Error( );
	}
};

/*
| Facets for floating buttons.
*/
def.lazy._facetsButton =
	function( )
{
	const light = this.light;

	return(
		FacetList.Elements(
			// default state.
			Facet.create(
				'border',
					light
					? FacetBorder.Color( Color.RGBA(   0,   0,   0, 0.5 ) )
					: FacetBorder.Color( Color.RGBA( 251, 251, 251, 0.5 ) ),
				'fill',
					light
					? Color.RGBA( 251, 251, 251, 0.5 )
					: Color.RGBA(  50,  50,  50, 0.5 ),
				'specs', GroupBoolean.Empty,
			),
			// hover
			Facet.create(
				'border',
					light
					? FacetBorder.Color( Color.RGBA(   0,   0,   0, 0.5 ) )
					: FacetBorder.Color( Color.RGBA( 251, 251, 251, 0.5 ) ),
				'fill',
					light
					? Color.RGBA( 255, 255, 180, 0.955 )
					: Color.RGBA(  60,  56,  47, 0.955 ),
				'specs', GroupBoolean.Table( { hover: true } ),
			),
			// down
			Facet.create(
				'border',
					light
					? FacetBorder.Color( Color.RGBA(   0,   0,   0, 0.5 ) )
					: FacetBorder.Color( Color.RGBA( 251, 251, 251, 0.5 ) ),
				'fill',
					light
					? Color.RGB( 255, 188,  88 )
					: Color.RGB( 255, 188,  88 ),
				'specs', GroupBoolean.Table( { down: true } ),
			),
			// down and hover
			Facet.create(
				'border',
					light
					? FacetBorder.Color( Color.RGBA(   0,   0,   0, 0.5 ) )
					: FacetBorder.Color( Color.RGBA( 251, 251, 251, 0.5 ) ),
				'fill',
					light
					? Color.RGB( 255, 188, 188 )
					: Color.RGB( 255, 188, 188 ),
				'specs', GroupBoolean.Table( { down: true, hover: true } ),
			),
		)
	);
};

/*
| Facets for icons on floating buttons.
*/
def.lazy._facetsButtonIcon =
	function( )
{
	const light = this.light;

	return(
		Facet.create(
			'border',
			light
			? ListFacetBorder.Elements(
				FacetBorder.ColorWidth( Color.RGBA(   0,   0,   0, 0.10 ), 2 ),
				FacetBorder.ColorWidth( Color.RGBA(   0,   0,   0, 0.50 ), 1 ),
			)
			: ListFacetBorder.Elements(
				FacetBorder.ColorWidth( Color.RGBA( 170, 170, 170, 0.10 ), 2 ),
				FacetBorder.ColorWidth( Color.RGBA( 170, 170, 170, 0.50 ), 1 ),
			)
		)
	);
};

/*
| The "opts" floating button.
*/
def.lazy._buttonOpts =
	function( )
{
	const resolution = this.resolution;
	const ratio = resolution.ratio;
	const sizeDisplay = this.sizeDisplay;
	const zone =
		Rect.PosWidthHeight(
			Point.XY( sizeDisplay.width - ( 14 + 40 ) * ratio, 14 * ratio ),
			40 * ratio,
			40 * ratio,
		);

	const traceV = TraceRoot.root.add( 'float', 'opts' );
	let hover = this.hover;
	if( hover && !hover.hasTrace( traceV ) )
	{
		hover = undefined;
	}

	return(
		WidgetButton.create(
			'facets',      this._facetsButton,
			'hover',       hover,
			'iconFacet',   this._facetsButtonIcon,
			'iconNoGrid',  true,
			'iconShape',   DesignIconOpts.figure,
			'resolution',  resolution,
			'shape',      'Ellipse',
			'systemFocus', this.systemFocus,
			'traceV',      traceV,
			'transform',   TransformNormal.setScale( ratio ),
			'zone',        zone.detransform( TransformNormal.setScale( ratio ) ),
		)
	);
};

/*
| The "select" floating button.
*/
def.lazy._buttonSelect =
	function( )
{
	const resolution = this.resolution;
	const ratio = resolution.ratio;
	const sizeDisplay = this.sizeDisplay;
	const zone =
		Rect.PosWidthHeight(
			Point.XY( sizeDisplay.width - ( 14 + 2 * 40 + 10 ) * ratio, 14 * ratio ),
			40 * ratio,
			40 * ratio,
		);

	const traceV = TraceRoot.root.add( 'float', 'select' );
	let hover = this.hover;
	if( hover && !hover.hasTrace( traceV ) )
	{
		hover = undefined;
	}

	const mode = this.mode;
	const down = mode && mode.ti2ctype === ModeSelect;

	return(
		WidgetButton.create(
			'down',        down,
			'facets',      this._facetsButton,
			'hover',       hover,
			'iconFacet',   this._facetsButtonIcon,
			'iconNoGrid',  true,
			'iconShape',   DesignIconSelect.figure,
			'resolution',  resolution,
			'shape',      'Ellipse',
			'systemFocus', this.systemFocus,
			'traceV',      traceV,
			'transform',   TransformNormal.setScale( ratio ),
			'zone',        zone.detransform( TransformNormal.setScale( ratio ) ),
		)
	);
};
