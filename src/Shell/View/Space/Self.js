/*
| A space view.
*/
def.attributes =
{
	// current action
	action: { type: [ 'undefined', '<Shell/Action/Types' ] },

	// the ancillary fabric of the space
	ancillary: { type: 'Shared/Ancillary/Space' },

	// the widget hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// current mode
	mode: { type: [ 'undefined', '<Shell/Mode/Types' ] },

	// rectangle of view window
	rectView: { type: 'gleam:Rect' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// extra data
	seam: { type: 'Shell/Seam/Space/Self' },

	// show space grid
	showGrid: { type: 'boolean' },

	// show space guides
	showGuides: { type: 'boolean' },

	// space has snapping
	snapping: { type: 'boolean' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// the visual trace of the space view
	traceV: { type: [ 'undefined', 'ti2c:Trace' ] },

	// currently logged in user
	userCreds: { type: [ 'undefined', 'Shared/User/Creds' ] },

	// the window this view is for
	window: { type: 'Shell/Window/Space' },
};

import { Self as AActuator               } from '{Shared/Ancillary/Item/Actuator}';
import { Self as ADocPath                } from '{Shared/Ancillary/Item/DocPath/Self}';
import { Self as ALabel                  } from '{Shared/Ancillary/Item/Label}';
import { Self as ANote                   } from '{Shared/Ancillary/Item/Note}';
import { Self as AStroke                 } from '{Shared/Ancillary/Item/Stroke/Self}';
import { Self as ASubspace               } from '{Shared/Ancillary/Item/Subspace}';
import { Self as ActionMoveDocPathHandle } from '{Shell/Action/MoveDocPathHandle}';
import { Self as ActionMoveItems         } from '{Shell/Action/MoveItems}';
import { Self as ActionMoveStrokeHandle  } from '{Shell/Action/MoveStrokeHandle}';
import { Self as ActionNewItem           } from '{Shell/Action/NewItem}';
import { Self as ActionPan               } from '{Shell/Action/Pan}';
import { Self as ActionResizeItems       } from '{Shell/Action/ResizeItems}';
import { Self as ActionScrolly           } from '{Shell/Action/Scrolly}';
import { Self as ActionSelectRange       } from '{Shell/Action/SelectRange}';
import { Self as ActionSelectRect        } from '{Shell/Action/SelectRect}';
import { Self as ActionZoomButton        } from '{Shell/Action/ZoomButton}';
import { Self as AngleN                  } from '{gleam:Angle/N}';
import { Self as AngleNe                 } from '{gleam:Angle/Ne}';
import { Self as AngleNw                 } from '{gleam:Angle/Nw}';
import { Self as AngleE                  } from '{gleam:Angle/E}';
import { Self as AngleS                  } from '{gleam:Angle/S}';
import { Self as AngleSe                 } from '{gleam:Angle/Se}';
import { Self as AngleSw                 } from '{gleam:Angle/Sw}';
import { Self as AngleW                  } from '{gleam:Angle/W}';
import { Self as BenchDocPath            } from '{Shell/Bench/DocPath/Self}';
import { Self as BenchFrame              } from '{Shell/Bench/Frame/Self}';
import { Self as BenchNewItem            } from '{Shell/Bench/NewItem}';
import { Self as BenchStroke             } from '{Shell/Bench/Stroke/Self}';
import { Self as ChangeTreeGrow          } from '{ot:Change/Tree/Grow}';
import { Self as Color                   } from '{gleam:Color}';
import { Self as ExportItems             } from '{Shell/Export/V1/Items}';
import { Self as FabricDocPathJoint      } from '{Shared/Fabric/Item/DocPath/Joint/Self}';
import { Self as FabricStroke            } from '{Shared/Fabric/Item/Stroke/Self}';
import { Self as FabricStrokeJoint       } from '{Shared/Fabric/Item/Stroke/Joint/Self}';
import { Self as GlintFigure             } from '{gleam:Glint/Figure}';
import { Self as Hover                   } from '{Shell/Result/Hover}';
import { Self as ListGlint               } from '{list@<gleam:Glint/Types}';
import { Self as ListFabricStrokeJoint   } from '{Shared/Fabric/Item/Stroke/Joint/List}';
import { Self as MarkCaret               } from '{Shared/Mark/Caret}';
import { Self as MarkRange               } from '{Shared/Mark/Range}';
import { Self as MarkItems               } from '{Shared/Mark/Items}';
import { Self as MarkWidget              } from '{Shared/Mark/Widget}';
import { Self as ModeNormal              } from '{Shell/Mode/Normal}';
import { Self as ModeSelect              } from '{Shell/Mode/Select}';
import { Self as ModeZoom                } from '{Shell/Mode/Zoom}';
import { Self as Plan                    } from '{Shared/Trace/Plan}';
import { Self as Point                   } from '{gleam:Point}';
import { Self as Rect                    } from '{gleam:Rect}';
import { Self as ReUndo                  } from '{Shell/ReUndo/Stack}';
import { Self as SeamSpaceTextItem       } from '{Shell/Seam/Space/TextItem}';
import { Self as Sequence                } from '{ot:Change/Sequence}';
import { Self as SetVItem                } from '{Shell/View/Space/Item/Set}';
import { Self as SetTrace                } from '{set@ti2c:Trace}';
import { Self as Shell                   } from '{Shell/Self}';
import { Self as SysMode                 } from '{Shell/Result/SysMode}';
import { Self as TraceRoot               } from '{Shared/Trace/Root}';
import { Self as TransformNormal         } from '{gleam:Transform/Normal}';
import { Self as TwigFabricItem          } from '{twig@<Shared/Fabric/Item/Types}';
import { Self as TwigAItem               } from '{Shared/Ancillary/Item/Twig}';
import { Self as TwigVSpaceItem          } from '{twig@<Shell/View/Space/Item/Types}';
import { Self as VActuator               } from '{Shell/View/Space/Item/Actuator}';
import { Self as VDocPath                } from '{Shell/View/Space/Item/DocPath}';
import { Self as VGrid                   } from '{Shell/View/Space/Grid}';
import { Self as VLabel                  } from '{Shell/View/Space/Item/Label}';
import { Self as VNote                   } from '{Shell/View/Space/Item/Note}';
import { Self as VStroke                 } from '{Shell/View/Space/Item/Stroke}';
import { Self as VSubspace               } from '{Shell/View/Space/Item/Subspace}';

/*
| A mark of all items.
*/
def.lazy.allItemsMark =
	function( )
{
	const set = new Set( );
	for( let item of this.items )
	{
		set.add( item.traceV );
	}

	return MarkItems.TraceVSet( set );
};

/*
| The zone encompassing all items.
*/
def.lazy.allItemsZone =
	function( )
{
	let first = true;
	let wx, ny, ex, sy;

	for( let item of this.items )
	{
		const zone = item.ancillary.zone;
		const pos = zone.pos;
		if( first )
		{
			wx = pos.x;
			ny = pos.y;
			ex = wx + zone.width;
			sy = ny + zone.height;
			first = false;
		}
		else
		{
			if( pos.x < wx )
			{
				wx = pos.x;
			}

			if( pos.y < ny )
			{
				ny = pos.y;
			}

			if( pos.x + zone.width > ex )
			{
				ex = pos.x + zone.width;
			}

			if( pos.y + zone.height > sy )
			{
				sy = pos.y + zone.height;
			}
		}
	}

	if( !first )
	{
		return Rect.PosWidthHeight( Point.XY( wx, ny ), ex - wx, sy - ny );
	}
	else
	{
		return false;
	}
};

/*
| The window this space is on.
*/
def.lazy.atWindow =
	function( )
{
	return this.traceV.forward( 'view' ).last.at;
};

/*
| The current bench.
| Benches are tools for the user to alter items.
*/
def.lazy.bench =
	function( bench )
{
	const action = this.action;
	const window = this.window;

	if( action && action.ti2ctype === ActionNewItem )
	{
		const traceVBN = this.traceV.add( 'bench' ).add( 'newItem' );

		let hoverB = this.hover;
		if( hoverB && !hoverB.hasTrace( traceVBN ) )
		{
			hoverB = undefined;
		}

		return(
			BenchNewItem.create(
				'action',     this.action,
				'hover',      hoverB,
				'light',      this.light,
				'resolution', this.resolution,
				'sizeView',   this.rectView.size,
				'traceV',     traceVBN,
				'window',     window,
			)
		);
	}

	const mark = this.mark;
	if( !mark || !mark.itemsMark )
	{
		return undefined;
	}

	const content = this.getSet( mark.itemsMark.traces );
	if( !content )
	{
		return undefined;
	}

	if( content.docOnly )
	{
		const traceVBP = this.traceV.add( 'bench' ).add( 'docPath' );
		const traceVBF = this.traceV.add( 'bench' ).add( 'frame' );

		let hoverB = this.hover;
		if(
			hoverB
			&& !hoverB.hasTrace( traceVBP )
			&& !hoverB.hasTrace( traceVBF )
		)
		{
			hoverB = undefined;
		}

		return(
			BenchDocPath.create(
				'action',     this.action,
				'hover',      hoverB,
				'light',      this.light,
				'resolution', this.resolution,
				'showGuides', this.showGuides,
				'sizeView',   this.rectView.size,
				'traceV',     traceVBP,
				'vDoc',       content.trivial,
				'window',     window,
			)
		);
	}
	else if( content.onlyStrokes )
	{
		const traceVBS = this.traceV.add( 'bench' ).add( 'stroke' );

		let hoverB = this.hover;
		if( hoverB && !hoverB.hasTrace( traceVBS ) )
		{
			hoverB = undefined;
		}

		return(
			BenchStroke.create(
				'content',    content,
				'hover',      hoverB,
				'light',      this.light,
				'resolution', this.resolution,
				'seam',       this.seam.bench.stroke,
				'traceV',     traceVBS,
				'window',     window,
			)
		);
	}
	else
	{
		const traceVB = this.traceV.add( 'bench' ).add( 'frame' );

		let hoverB = this.hover;
		if( hoverB && !hoverB.hasTrace( traceVB ) )
		{
			hoverB = undefined;
		}

		return(
			BenchFrame.create(
				'content',    content,
				'hover',      hoverB,
				'light',      this.light,
				'resolution', this.resolution,
				'showGuides', this.showGuides,
				'sizeView',   this.rectView.size,
				'traceV',     traceVB,
				'window',     window,
			)
		);
	}
};

/*
| A click.
*/
def.proto.click =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const bench = this.bench;
	if( bench && bench.click( p, shift, ctrl ) )
	{
		return true;
	}

	// clicked some item?
	const mark = this.mark;
	for( let item of this.items )
	{
		// FIXME no need to pass mark
		if( item.click( p, shift, ctrl, mark ) )
		{
			return true;
		}
	}

	// otherwise ...

	if( this.window.access === 'rw' )
	{
		if( this.action?.ti2ctype === ActionNewItem )
		{
			// if there is already a NewItem bench, just cancel it
			Shell.alter( 'action', undefined );
			return true;
		}

		// creates a NewItem bench

		// calculates what new items would be
		let label, note, path, stroke, subspace;

		// target value for the new item
		const width  = 280;
		const height = 173;

		{
			// note
			let pos = p.add( -width / 2, -height / 2 );
			let pse = pos.add( width, height );
			pos = this._point2SpaceRS( pos, true );
			pse = this._point2SpaceRS( pse, true );
			let zone = Rect.PosPse( pos, pse );
			zone = zone.ensureMinSize( VNote.sizeMin );
			note = VNote.fabricModel.create( 'zone', zone );
		}

		{
			// label
			const labelM = VLabel.model( this.light );

			let pn = p.add( 0, -height / 2 );
			let ps = p.add( 0,  height / 2 );
			pn = this._point2SpaceRS( pn, true );
			ps = this._point2SpaceRS( ps, true );

			const fs = labelM.text.fontSize * ( ps.y - pn.y ) / labelM.ancillary.zone.height;

			let labelA = labelM.ancillary;
			labelA =
				ALabel.FromFabric(
					labelA.asFabric.create( 'fontSize', fs ),
					labelA.trace,
				);
			const size = labelA.sizeComputed;
			let pos = this._point2SpaceRS( p, true ).add( -size.width / 2, -size.height / 2 );
			let pse = pos.add( size.width, size.height );

			label =
				labelA.asFabric.create(
					'zone', Rect.PosPse( pos, pse ),
				);
		}

		{
			// path
			const wp = height / VDocPath.proportion;
			let pos = p.add( -wp / 2, -height / 2 );
			let pse = pos.add( wp, height );
			pos = this._point2SpaceRS( pos, true );
			pse = this._point2SpaceRS( pse, true );
			let zone = Rect.PosPse( pos, pse );

			const heightMin = VDocPath.sizeMin.height;
			if( zone.height < heightMin )
			{
				zone =
					zone.create(
						'height', heightMin,
						'width',  heightMin / VDocPath.proportion,
					);
			}
			path = VDocPath.fabricModel.create( 'zone', zone );
		}

		{
			// subspace
			let pos = p.add( -height / 2, -height / 2 );
			let pse = pos.add( height, height );
			pos = this._point2SpaceRS( pos, true );
			pse = this._point2SpaceRS( pse, true );
			subspace =
				VSubspace.fabricModel.create(
					'zone', Rect.PosPse( pos, pse )
				);
		}

		{
			// stroke
			let pos = p.add( -width / 2, -height / 2 );
			let psw = pos.add( 0, height );
			let pne = pos.add( width, 0 );
			psw = this._point2SpaceRS( psw, true );
			pne = this._point2SpaceRS( pne, true );

			stroke =
				FabricStroke.create(
					'joints',
						ListFabricStrokeJoint.Elements(
							FabricStrokeJoint.create( 'pos', psw ),
							FabricStrokeJoint.create( 'pos', pne )
						),
					'styleBegin', 'none',
					'styleEnd', 'arrow',
				);
		}

		Shell.alter(
			'action',
				ActionNewItem.create(
					'pCenterV',  p,
					'traceV',    this.traceV,
					'label',     label,
					'note',      note,
					'path',      path,
					'stroke',    stroke,
					'subspace',  subspace,
				),
			'mark', undefined,
		);
	}
	else
	{
		Shell.alter( 'mark', undefined );
	}

	return true;
};

/*
| The content the mark puts into the clipboard.
*/
def.lazy.clipboard =
	function( )
{
	const mark = this.mark;

	if( !mark )
	{
		return '';
	}

	switch( mark.ti2ctype )
	{
		case MarkRange:
			return this.rangeContent( mark );

		case MarkItems:
		{
			const preamble = '//#*PLOTLE+-\n';

			let items = TwigFabricItem.create( );
			const tItems = this.items;
			for( let key of tItems.keys )
			{
				const item = tItems.get( key );
				if( mark.traces.has( item.traceV ) )
				{
					items = items.create( 'twig:add', key, item.ancillary.asFabric );
				}
			}

			const cdata =
				ExportItems.create(
					'items', items,
					'center', this.rectView.zeroPos.pc.detransform( this.window.transform ),
				);

			return preamble + cdata.jsonfy( '  ' );
		}

		default: return '';
	}
};

/*
| Changes the transform so that all items of the space are visible in the rectView.
*/
def.proto.changeSpaceTransformAll =
	function( )
{
	const allItemsZone = this.allItemsZone;
	if( !allItemsZone ) return;

	const resolution = this.resolution;
	const ratio = resolution.ratio;

	// center
	const aizPc = allItemsZone.pc;
	const cx = aizPc.x;
	const cy = aizPc.y;
	const aizPos = allItemsZone.pos;
	const wx = aizPos.x;
	const ny = aizPos.y;
	const aizPse = allItemsZone.pse;
	const ex = aizPse.x;
	const sy = aizPse.y;
	const rectView = this.rectView;
	const vsx2 = rectView.width / ( 2 * ratio );
	const vsy2 = rectView.height / ( 2 * ratio );
	const zoomMin = config.zoomMin;

	let exp, z;
	for( exp = config.zoomMax; exp > zoomMin; exp-- )
	{
		z = Math.pow( 1.1, exp );
		const extra = 10 / z;
		if( ex + extra > cx + vsx2 / z ) continue;
		if( wx - extra < cx - vsx2 / z ) continue;
		if( sy + extra > cy + vsy2 / z ) continue;
		if( ny - extra < cy - vsy2 / z ) continue;
		break;
	}

	Shell.changeTransformTo(
		this.atWindow,
		exp,
		TransformNormal.setScaleOffset(
			z * resolution.ratio,
			//Point.XY( vsx2 - cx * z + rectView.pos.x, vsy2 - cy * z )
			Point.XY( vsx2 - cx * z, vsy2 - cy * z )
		),
		120, // zoomAllHomeTime
	);
};

/*
| Cycles the focus.
*/
def.proto.cycleFocus =
	function( dir )
{
	const focus = this.focus;
	if( !focus ) return;

	focus.cycleFocus( dir );
};

/*
| Drag moves.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto.dragMove =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const action = this.action;
	switch( action.ti2ctype )
	{
		case ActionMoveDocPathHandle:
			this._moveDocPathHandleDragMove( p, shift, ctrl );
			return;

		case ActionMoveItems:
			this._moveItemsDragMove( p, shift, ctrl );
			return;

		case ActionMoveStrokeHandle:
			this._moveStrokeHandleDragMove( p, shift, ctrl );
			return;

		case ActionPan:
			this._panningDragMove( p, shift, ctrl );
			return;

		case ActionResizeItems:
			this._resizeItemsDragMove( p, shift, ctrl );
			return;

		case ActionScrolly:
			this._scrollyDragMove( p, shift, ctrl );
			return;

		case ActionSelectRange:
			this._selectRangeDragMove( p, shift, ctrl );
			return;

		case ActionSelectRect:
			this._selectRectDragMove( p, shift, ctrl );
			return;

		default: throw new Error( );
	}
};

/*
| Starts an operation with the pointing device held down.
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
	const mode = this.mode;

	switch( mode.ti2ctype )
	{
		case ModeSelect:
		{
			const ps = this._point2SpaceRS( p, !ctrl );
			Shell.alter(
				'action',
					ActionSelectRect.create(
						'pointStart', ps,
						'pointTo', ps,
						'traceV', this.traceV,
					)
			);
			return;
		}

		case ModeNormal:
		case ModeZoom:
		{
			this._normalDragStart( p, shift, ctrl );
			return;
		}
	}
};

/*
| Stops a drag.
|
| ~p:      point of stop
| ~shift:  true if shift key was pressed
| ~ctrl:   true if ctrl key was pressed
*/
def.proto.dragStop =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const action = this.action;

	switch( action.ti2ctype )
	{
		case ActionMoveDocPathHandle:
			this._moveDocPathHandleDragStop( p, shift, ctrl );
			return;

		case ActionMoveItems:
			this._moveItemsDragStop( p, shift, ctrl );
			return;

		case ActionMoveStrokeHandle:
			this._moveStrokeHandleDragStop( p, shift, ctrl );
			return;

		case ActionPan:
			this._panningDragStop( p, shift, ctrl );
			return;

		case ActionResizeItems:
			Shell.alter( 'action', undefined );
			Shell.change( this.window.uid, action.changesTransient );
			return;

		case ActionScrolly:
			Shell.alter( 'action', undefined );
			return;

		case ActionSelectRange:
			// range is already set.
			Shell.alter( 'action', undefined );
			return;

		case ActionSelectRect:
			this._selectRectDragStop( p, shift, ctrl );
			return;

		default: throw new Error( );
	}
};

/*
| Determines the focused item.
*/
def.lazy.focus =
	function( )
{
	const mark = this.mark;
	if( !mark ) return undefined;

	const im = mark.itemsMark;
	if( !im ) return undefined;

	const traces = im.traces;
	if( im.traces.size !== 1 ) return undefined;

	const traceV = traces.trivial;
	return this.items.get( traceV.last.key );
};

/*
| Returns a set of items by an itemsMark.
*/
def.proto.getSet =
	function( traces )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( traces.ti2ctype !== SetTrace ) throw new Error( );
/**/	if( traces.size === 0 ) throw new Error( );
/**/}

	const items = this.items;
	const set = new Set( );

	for( let trace of traces )
	{
/**/	if( CHECK && trace.last.name !== 'items' ) throw new Error( );

		const item = items.get( trace.last.key );
		if( item )
		{
			set.add( item );
		}
	}

	return(
		set.size > 0
		? SetVItem.create( 'set:init', set )
		: undefined
	);
};

/*
| Return the space glint.
*/
def.lazy.glint =
	function( )
{
	const action = this.action;
	const window = this.window;
	const transform = window.transform;
	const list = [ ];

	if( this.showGrid && window.access === 'rw' )
	{
		list.push( this._grid.glint );
	}

	const items = this.items;
	for( let item of items.reverse( ) )
	{
		list.push( item.glint );
	}

	const bench = this.bench;
	if( bench )
	{
		list.push( bench.glint );
	}

	if( action?.ti2ctype === ActionSelectRect && action.zone )
	{
		list.push(
			GlintFigure.FigureColorWidth(
				action.zone.transform( transform ),
				Color.RGBA( 255, 215, 114, 0.9 ),
				2,
			)
		);
	}

	return ListGlint.Array( list );
};

/*
| User is inputting characters.
*/
def.proto.input =
	function( string )
{
	const mark = this.mark;
	if( !mark || !mark.hasCaret ) return;

	const item = this.items.get( mark.traceVCaret.backward( 'items' ).last.key );
	if( item ) item.input( string );
};

/*
| Builds the visual items.
*/
def.lazy.items =
	function( )
{
	const light = this.light;
	const aItems = this.ancillary.items;
	const twig = { };

	const window      = this.window;
	const access      = window.access;
	const action      = this.action;
	const keys        = aItems.keys;
	const mark        = this.mark;
	const hover       = this.hover;
	const resolution  = this.resolution;
	const systemFocus = this.systemFocus;
	const transform   = window.transform;

	const traceFSpace = TraceRoot.space( window.uid );
	const traceVSpace = this.traceV;

	for( let rank = 0, rlen = keys.length; rank < rlen; rank++ )
	{
		const key = keys[ rank ];
		const aItem = aItems.get( key );
		const trace  = traceFSpace.add( 'items', key );
		const traceV = traceVSpace.add( 'items', key );
		let highlight = !!( mark?.encompasses( traceV ) );

		let hoverI = hover;
		if( hoverI && !hoverI.hasTrace( traceV ) )
		{
			hoverI = undefined;
		}

		let markI = mark;
		if( markI && !markI.encompasses( traceV ) )
		{
			markI = undefined;
		}

		if( !highlight )
		{
			// checks if current action highlights the item
			switch( action?.ti2ctype )
			{
				case ActionMoveDocPathHandle:
				{
					if( action.hover?.hasTrace( traceV ) )
					{
						highlight = true;
					}
					break;
				}

				case ActionMoveStrokeHandle:
				{
					if( action.fHover?.hasTrace( trace ) )
					{
						highlight = true;
					}
					break;
				}

				case ActionSelectRect:
				{
					const aZone = action.zone;
					if( aZone && aZone.within( aItem.zone ) )
					{
						highlight = true;
					}
				}
			}
		}

		switch( aItem.ti2ctype )
		{
			case AActuator:
			{
				twig[ key ] =
					VActuator.create(
						'access',       access,
						'ancillary',    aItem,
						'highlight',    highlight,
						'hover',        hoverI,
						'isSpaceOwner', window.isOwner,
						'light',        light,
						'mark',         markI,
						'rank',         rank,
						'resolution',   resolution,
						'systemFocus',  systemFocus,
						'transform',    transform,
						'traceV',       traceV,
						'userCreds',    this.userCreds,
					);
				break;
			}

			case ADocPath:
			{
				twig[ key ] =
					VDocPath.create(
						'access',      access,
						'ancillary',   aItem,
						'highlight',   highlight,
						'hover',       hoverI,
						'light',       light,
						'mark',        markI,
						'rank',        rank,
						'resolution',  resolution,
						'systemFocus', systemFocus,
						'transform',   transform,
						'traceV',      traceV,
					);
				break;
			}

			case ALabel:
			{
				twig[ key ] =
					VLabel.create(
						'access',      access,
						'ancillary',   aItem,
						'highlight',   highlight,
						'hover',       hoverI,
						'light',       light,
						'mark',        markI,
						'rank',        rank,
						'resolution',  resolution,
						'systemFocus', systemFocus,
						'transform',   transform,
						'traceV',      traceV,
					);
				break;
			}

			case ANote:
			{
				twig[ key ] =
					VNote.create(
						'access',      access,
						'ancillary',   aItem,
						'highlight',   highlight,
						'hover',       hoverI,
						'light',       light,
						'mark',        markI,
						'rank',        rank,
						'resolution',  resolution,
						'seam',        this.seam.textItems.get( key ) || SeamSpaceTextItem.Clean,
						'systemFocus', systemFocus,
						'transform',   transform,
						'traceV',      traceV,
					);
				break;
			}

			case AStroke:
			{
				twig[ key ] =
					VStroke.create(
						'access',      access,
						'ancillary',   aItem,
						'highlight',   highlight,
						'hover',       hoverI,
						'light',       light,
						'mark',        markI,
						'rank',        rank,
						'resolution',  resolution,
						'systemFocus', systemFocus,
						'transform',   transform,
						'traceV',      traceV,
					);
				break;
			}

			case ASubspace:
			{
				twig[ key ] =
					VSubspace.create(
						'access',      access,
						'ancillary',   aItem,
						'highlight',   highlight,
						'hover',       hoverI,
						'light',       light,
						'mark',        markI,
						'rank',        rank,
						'resolution',  resolution,
						'systemFocus', systemFocus,
						'transform',   transform,
						'traceV',      traceV,
					);
				break;
			}

			default:
				throw new Error( );
		}
	}

	return TwigVSpaceItem.create( 'twig:init', twig, keys );
};

/*
| Mouse wheel.
|
| ~p:     cursor point
| ~dir:   wheel direction, >0 for down, <0 for up
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( p.ti2ctype !== Point ) throw new Error( );
/**/	if( dir !== -1 && dir !== 1 ) throw new Error( );
/**/	if( typeof( shift ) !== 'boolean' ) throw new Error( );
/**/	if( typeof( ctrl ) !== 'boolean' ) throw new Error( );
/**/}

	for( let item of this.items )
	{
		if( item.mousewheel( p, dir, shift, ctrl ) )
		{
			return true;
		}
	}

	// otherwhise
	Shell.alter( 'action', undefined );
	Shell.changeSpaceTransformPoint( this.atWindow, dir, p );
	return true;
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.proto.paste =
	function( type, data )
{
	if( data.startsWith( '//#*PLOTLE+-' ) )
	{
		const ion = data.indexOf( '\n' );
		if( ion >= 0 )
		{
			data = data.substr( ion + 1 );
			this._pasteItems( data );
			return;
		}
	}

	this.input( data );
};

/*
| Mouse hover.
|
| Returns a Hover result with hovering trace and cursor to show.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const action = this.action;
	switch( action?.ti2ctype )
	{
		case undefined:
			break;

		case ActionMoveDocPathHandle:
		{
			return this._moveDocPathHandlePointingHover( p, shift, ctrl );
		}

		case ActionMoveItems:
		case ActionPan:
		{
			return Hover.CursorGrabbing;
		}

		case ActionMoveStrokeHandle:
		{
			return this._moveStrokeHandlePointingHover( p, shift, ctrl );
		}

		case ActionNewItem:
		{
			break;
		}

		case ActionResizeItems:
		{
			return this._resizeItemsPointingHover( p, shift, ctrl );
		}

		case ActionScrolly:
		{
			return Hover.CursorNsResize;
		}

		case ActionSelectRange:
		{
			return Hover.CursorText;
		}

		case ActionSelectRect:
		{
			return Hover.CursorCrosshair;
		}

		case ActionZoomButton:
		{
			return Hover.CursorDefault;
		}

		default: throw new Error( );
	}

	const bench = this.bench;
	const mode = this.mode;

	if( bench && mode.ti2ctype !== ModeSelect )
	{
		const bubble = bench.pointingHover( p, shift, ctrl );

		if( bubble )
		{
			return bubble;
		}
	}

	for( let item of this.items )
	{
		const bubble = item.pointingHover( p, mode );

		if( bubble )
		{
			return bubble;
		}
	}

	return(
		mode.ti2ctype === ModeSelect
		? Hover.CursorCrosshair
		: Hover.CursorPointer
	);
};

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const traceVS = traceV.forward( 'space' );
	const stepC = traceV.get( traceVS.length );

	switch( stepC.name )
	{
		case 'items':
		{
			const item = this.items.get( stepC.key );
			return item.pushButton( traceV, shift, ctrl );
		}

		case 'bench':
		{
			return this.bench.pushButton( traceV, shift, ctrl );
		}

		default: throw new Error( );
	}
};

/*
| Performs some maintenance.
|
| Notes scrollPos are checked to be within limits.
*/
def.proto.rectify =
	function( )
{
	for( let item of this.items )
	{
		if( !item.rectify ) continue;
		item.rectify( );
	}
};

/*
| Returns the content of a range.
*/
def.lazyFunc.rangeContent =
	function( range )
{
	if( range.ti2ctype !== MarkRange ) throw new Error( );
	const item = this.items.get( range.traceBegin.forward( 'items' ).last.key );
	return item.text.rangeContent( range );
};

/*
| Tries to scroll the focused item to move
| the mark into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	const focus = this.focus;
	if( focus && focus.scrollMarkIntoView )
	{
		focus.scrollMarkIntoView( );
	}
};

/*
| User pressed a special key.
|
| ~key:   key being pressed
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
| ~inputWord: the last word (on mobile)
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
	if( ctrl )
	{
		switch( key )
		{
			case 'z':
				// TODO this should catched by root
				ReUndo.undo( );
				return;

			case 'y':
				ReUndo.redo( );
				// TODO this should catched by root
				return;

			case ',':
				Shell.changeSpaceTransformCenter( 1 );
				return;

			case '.':
				Shell.changeSpaceTransformCenter( -1 );
				return;
		}
	}

	const mark = this.mark;
	if( mark?.ti2ctype === MarkItems && key === 'del' )
	{
		Shell.removeItems( mark.traces );
		return;
	}

	if( mark && mark.hasCaret )
	{
		const item = this.items.get( mark.traceVCaret.backward( 'items' ).last.key );
		if( item ) item.specialKey( key, shift, ctrl, inputWord );
		return;
	}

	if( ctrl )
	{
		switch( key )
		{
			case 'a':
				// selects all items in this space
				Shell.alter( 'mark', this.allItemsMark );
				return;
		}
		return;
	}
};

/*
| The system mode.
*/
def.lazy.sysMode =
	function( )
{
	const focus = this.focus;
	if( !focus ) return SysMode.Blank;

	const sm = focus.sysMode;
	return sm;
};

/*
| Converts a fabric trace to a visual trace.
|
| See also traceV2F.
*/
def.proto.traceF2V =
	function( trace )
{
/**/if( CHECK && trace.get( 0 ).name !== 'space' ) throw new Error( );

	const step1 = trace.get( 1 );
	if( step1.name === 'items' )
	{
		return this.items.get( step1.key ).traceF2V( trace );
	}

	let traceV = this.traceV;
	for( let a = 1, alen = trace.length; a < alen; a++ )
	{
		const step = trace.get( a );
		traceV = traceV.addStep( step );
	}
	return traceV;
};

/*
| Converts a visual trace to a fabric trace.
| See also traceF2V.
*/
def.proto.traceV2F =
	function( traceV )
{
/**/if( CHECK )
/**/{
/**/	if( traceV.get( 0 ).name !== 'root'  ) throw new Error( );
/**/	if( traceV.get( 1 ).name !== 'view'  ) throw new Error( );
/**/	if( traceV.get( 2 ).name !== 'space' ) throw new Error( );
/**/}

	const step3 = traceV.get( 3 );
	if( step3.name === 'items' )
	{
		return this.items.get( step3.key )?.traceV2F( traceV );
	}

	let trace = TraceRoot.space( this.window.uid );
	for( let a = 3, alen = traceV.length; a < alen; a++ )
	{
		const step = traceV.get( a );
		trace = trace.addStep( step );
	}
	return trace;
};

/*
| Transforms a mark in visual space.
*/
def.proto.transformMark =
	function( mark, changeX )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	switch( mark.ti2ctype )
	{
		case MarkCaret:
		{
			let traceV = mark.traceV;
			let trace = this.traceV2F( traceV );
			if( !trace ) return undefined;
			trace = changeX.transform( trace );

			// caret vanished?
			if( !trace ) return undefined;

			return mark.create( 'traceV', this.traceF2V( trace ) );
		}

		case MarkRange:
		{
			const traceBegin = changeX.transform( this.traceV2F( mark.traceBegin ) );
			const traceEnd = changeX.transform( this.traceV2F( mark.traceEnd ) );

			// trange vanished?
			if( !traceBegin || !traceEnd )
			{
				return undefined;
			}

			// range became a caret only?
			if( traceBegin === traceEnd )
			{
				return MarkCaret.create( 'traceV', this.traceF2V( traceBegin ) );
			}

			return(
				mark.create(
					'traceBegin', this.traceF2V( traceBegin ),
					'traceEnd', this.traceF2V( traceEnd ),
				)
			);
		}

		case MarkItems:
		{
			const set = new Set( );
			for( let iTrace of mark.traces )
			{
				let trace = this.traceV2F( iTrace );
				trace = trace && changeX.transform( trace );
				if( trace )
				{
					set.add( this.traceF2V( trace ) );
				}
			}

			return(
				set.size === 0
				? undefined
				: MarkItems.TraceVSet( set )
			);
		}

		case MarkWidget:
			return mark;

		default: throw new Error( );
	}
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const traceV = this.traceV;
/**/	if( traceV.length !== 3 ) throw new Error( );
/**/}
};

/*
| Drag moves during moving a doc path handle.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._moveDocPathHandleDragMove =
	function( p, shift, ctrl )
{
	const action = this.action;
	const psrs = this._point2SpaceRS( p, false );

	Shell.alter( 'action', action.create( 'pointMove', psrs ) );
};

/*
| Drag stops during moving a doc path handle.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._moveDocPathHandleDragStop =
	function( p, shift, ctrl )
{
	const action = this.action;

	Shell.alter( 'action', undefined );
	Shell.change( this.window.uid, action.changes( false ) );
};

/*
| Mouse hover during move a doc path handle.
|
| ~p:      cursor point
| ~shift:  true if shift key was pressed
| ~ctrl:   true if ctrl key was pressed
|
| ~return: a HoverResult.
*/
def.proto._moveDocPathHandlePointingHover =
	function( p, shift, ctrl )
{
	const action = this.action;
	const hover = action.hover;
	const joints = action.vDoc.ancillary.joints;

	for( let item of this.items )
	{
		// only certain items can be in a docPath
		switch( item.ti2ctype )
		{
			case VLabel:
			case VNote:
			//case VSubspace:
				break;

			default:
				continue;
		}

		if( item.within( p ) )
		{
			// ignores items already in the doc path
			const jointNr = joints.getByTrace( item.ancillary.trace );

			if( jointNr >= 0 && jointNr !== action.removing ) continue;

			const traceV = item.traceV;
			if( !hover || hover !== traceV )
			{
				Shell.alter(
					'action',
						action.create(
							'hover', traceV,
							'_insertItemJoint',
								FabricDocPathJoint.create(
									'outline', item.outline,
									'pos', item.ancillary.trace,
								),
						),
				);
			}

			return Hover.CursorDefault;
		}
	}

	// if nothing hit and hover is set, unsets hover
	if( hover )
	{
		Shell.alter(
			'action',
				action.create(
					'hover', undefined,
					'_insertItemJoint', undefined,
				)
			);
	}

	return Hover.CursorGrabbing;
};

/*
| Drag moves during moving items.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._moveItemsDragMove =
	function( p, shift, ctrl )
{
	const action      = this.action;
	const pointStart  = action.pointStart;
	const transform   = this.window.transform;
	const startPos    = action.startZone.pos;
	const dp          = p.detransform( transform );
	let movedStartPos = startPos.add( dp ).sub( pointStart );

	// TODO decomplicated this.
	movedStartPos = this._point2SpaceRS( movedStartPos.transform( transform ), !ctrl );
	Shell.alter( 'action', action.create( 'moveBy', movedStartPos.sub( startPos ) ) );
};

/*
| Drag stops during moving items.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._moveItemsDragStop =
	function( p, shift, ctrl )
{
	const action = this.action;

	Shell.alter(	'action', undefined );
	Shell.change( this.window.uid, action.changesTransient );
};

/*
| Drag moves during moving a stroke handle.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._moveStrokeHandleDragMove =
	function( p, shift, ctrl )
{
	const action = this.action;
	const psrs = this._point2SpaceRS( p, !ctrl );

	Shell.alter( 'action', action.create( 'movedPoint', psrs ) );
};

/*
| Drag stops during moving a stroke handle.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._moveStrokeHandleDragStop =
	function( p, shift, ctrl )
{
	const action = this.action;

	Shell.alter( 'action', undefined );
	const ct = action.changesTransient;
	if( ct )
	{
		Shell.change( this.window.uid, ct );
	}
};

/*
| Mouse hover during mosing a stroke handle.
|
| ~p:      cursor point
| ~shift:  true if shift key was pressed
| ~ctrl:   true if ctrl key was pressed
|
| ~return: a HoverResult.
*/
def.proto._moveStrokeHandlePointingHover =
	function( p, shift, ctrl )
{
	const action = this.action;

	const hover = action.hover;
	for( let item of this.items )
	{
		if( item.ti2ctype === VStroke ) continue;

		if( item.within( p ) )
		{
			// TODO actually block the hover in case hovering over opposite
			if( item.ancillary.trace === action.oppositeStrokeJoint.pos ) continue;

			const traceV = item.traceV;
			if( !hover || hover !== traceV )
			{
				Shell.alter(
					'action',
						action.create(
							'hover', traceV,
							'fHover', this.traceV2F( traceV )
						)
				);
			}

			return Hover.CursorDefault;
		}
	}

	// if nothing hit and hover is set, unsets hover
	if( hover )
	{
		Shell.alter(
			'action',
				action.create(
					'hover', undefined,
					'fHover', undefined,
				)
		);
	}

	return Hover.CursorGrabbing;
};

/*
| Drag moves during panning.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._panningDragMove =
	function( p, shift, ctrl )
{
	const action = this.action;
	const pd = p.sub( action.pointStart );

	Shell.setWindowTransform(
		this.traceV.forward( 'view' ).last.at,
		this.window.transform.setOffset( action.offset.add( pd ) )
	);
};

/*
| Drag stops during panning.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._panningDragStop =
	function( p, shift, ctrl )
{
	Shell.alter( 'action', undefined );
};

/*
| Drag moves during item resizing.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._resizeItemsDragMove =
	function( p, shift, ctrl )
{
	const action = this.action;
	const transform = this.window.transform;

	const pBase = action.pBase;
	const proportional = action.proportional;
	const resizeDir = action.resizeDir;
	const pointStart = action.pointStart;
	const startZone = action.startZone;
	const dp = p.detransform( transform );

	let scaleX, scaleY;
	{
		// start zone reference point
		const rp = resizeDir.from( startZone );
		// end zone point
		let ezp = dp.add( rp ).sub( pointStart );

		// TODO decomplicate
		ezp = this._point2SpaceRS( ezp.transform( transform ), !ctrl );

		if( resizeDir.hasY )
		{
			scaleY = ( pBase.y - ezp.y ) / ( pBase.y - rp.y );
			if( isNaN( scaleY ) ) scaleY = 0;
			else if( scaleY < 0 ) scaleY = 0;
		}

		if( resizeDir.hasX )
		{
			scaleX = ( pBase.x - ezp.x ) / ( pBase.x - rp.x );
			if( isNaN( scaleX ) ) scaleX = 0;
			if( scaleX < 0 ) scaleX = 0;
		}
	}

	if( proportional )
	{
		if( scaleX === undefined ) scaleX = scaleY;
		else if( scaleY === undefined ) scaleY = scaleX;
	}
	else
	{
		if( scaleX === undefined ) scaleX = 1;
		if( scaleY === undefined ) scaleY = 1;
	}

	const startZones = action.startZones;
	for( let item of action.items )
	{
		const key = item.traceV.last.key;
		const startZone = startZones.get( key );
		let min = item.minScaleX( startZone );
		if( scaleX < min ) scaleX = min;
		min = item.minScaleY( startZone );
		if( scaleY < min ) scaleY = min;
	}

	if( proportional )
	{
		scaleX = scaleY = Math.max( scaleX, scaleY );
	}

	Shell.alter(
		'action', action.create( 'scaleX', scaleX, 'scaleY', scaleY )
	);
};

/*
| Mouse hover during an item resize.
|
| ~p:      cursor point
| ~shift:  true if shift key was pressed
| ~ctrl:   true if ctrl key was pressed
|
| ~return: a HoverResult.
*/
def.proto._resizeItemsPointingHover =
	function( p, shift, ctrl )
{
	switch( this.action.resizeDir.ti2ctype )
	{
		case AngleN:  return Hover.CursorNsResize;
		case AngleNe: return Hover.CursorNeResize;
		case AngleNw: return Hover.CursorNwResize;
		case AngleE:  return Hover.CursorEwResize;
		case AngleS:  return Hover.CursorNsResize;
		case AngleSe: return Hover.CursorSeResize;
		case AngleSw: return Hover.CursorSwResize;
		case AngleW:  return Hover.CursorEwResize;
		default: throw new Error( );
	}
};

/*
| Drag moves during scrolling.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._scrollyDragMove =
	function( p, shift, ctrl )
{
	const action = this.action;
	const traceV = action.traceV;

/**/if( CHECK )
/**/{
/**/	if( traceV.get( 1 ).name !== 'view' ) throw new Error( );
/**/	if( traceV.last.name !== 'items' ) throw new Error( );
/**/}

	const t = this.window.transform;
	const itemV = this.items.get( traceV.last.key );
	const dy = ( p.y - action.pointStart.y ) / t.scale;
	let spos = action.startPos + itemV.scrollbarY.scale( dy );
	if( spos < 0 ) spos = 0;

	Shell.alter(
		itemV.traceSeam.add( 'scrollPos' ),
			itemV.scrollPos.create( 'y', spos )
	);
};

/*
| Drag moves during selecting a range.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._selectRangeDragMove =
	function( p, shift, ctrl )
{
	const action = this.action;
	const itemV = this.items.get( action.traceV.forward( 'items' ).last.key );
	const mark = itemV.markForPoint( p, true );

	Shell.alter( 'mark', mark );
	itemV.scrollMarkIntoView( );
};

/*
| Drag moves during selecting with a rect.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._selectRectDragMove =
	function( p, shift, ctrl )
{
	Shell.alter(
		'action', this.action.create( 'pointTo', this._point2SpaceRS( p, false ) )
	);
};

/*
| Drag stops during selecting with a rect.
|
| ~p: point, viewbased point
| ~shift: true if shift key was pressed
| ~ctrl: true if ctrl key was pressed
*/
def.proto._selectRectDragStop =
	function( p, shift, ctrl )
{
	let action = this.action;

	action = action.create( 'pointTo', this._point2SpaceRS( p, false ) );
	let traceVs = new Set( );
	for( let item of this.items )
	{
		if( action.affectsItem( item ) )
		{
			traceVs.add( item.traceV );
		}
	}

	let mark = pass;
	if( traceVs.size > 0 )
	{
		mark = MarkItems.TraceVSet( traceVs );
		if( ctrl )
		{
			const tMark = this.mark;
			if( tMark )
			{
				mark = mark.combine( tMark.itemsMark );
			}
		}
	}

	const mode = this.mode;

	Shell.alter(
		'action', undefined,
		'mark', mark,
		'mode', shift ? mode : mode.fallbackMode,
	);
};

/*
| The zoomGrid glint for this space.
*/
def.lazy._grid =
	function( )
{
	return(
		VGrid.create(
			'light',    this.light,
			'size',     this.rectView.size,
			'spacing',  Self._standardSpacing,
			'transform', this.window.transform,
		)
	);
};

/*
| Drag start in normal mode.
*/
def.proto._normalDragStart =
	function( p, shift, ctrl )
{
	// sees if the bench was targeted
	const bench = this.bench;
	const window = this.window;

	if(
		window.access === 'rw'
		&& bench
		&& bench.dragStart( p, shift, ctrl )
	)
	{
		return;
	}

	// see if one item was targeted
	for( let item of this.items )
	{
		if( item.dragStart( p, shift, ctrl, this ) )
		{
			return;
		}
	}

	// otherwise starts panning
	Shell.alter(
		'action',
			ActionPan.create(
				'offset', this.window.transform.offset,
				'pointStart', p,
				'traceV', this.traceV,
			)
	);
};

/*
| The user pasted items.
|
| ~jstr: json string.
*/
def.proto._pasteItems =
	function( jstr )
{
	const ex = ExportItems.FromJson( JSON.parse( jstr ), Plan.exportItems );
	const trace = TraceRoot.space( this.window.uid );
	const itemsA = TwigAItem.FromFabric( ex.items, trace );

	const ancillary = this.ancillary;
	const transform = this.window.transform;

	// pastes to the center of the current view
	const center = this.rectView.zeroPos.pc.detransform( transform );
	let offset = center.sub( ex.center );
	if( this.snapping )
	{
		offset = this._grid.snap( offset.transform( transform ) ).detransform( transform );
	}

	const mapping = ex.newUidsMapping( );
	let restart;
	let changes;

	do
	{
		restart = false;
		changes = [ ];
		let rank = 0;
		for( let key of itemsA.keys )
		{
			const item =
				itemsA.get( key )
				.paste( mapping, offset, ancillary.items );

			if( !item )
			{
				// there was a collision, restart with shifted offset.
				offset = offset.add( this._grid.gVisualSpacingDetransform );
				restart = true;
				break;
			}

			changes.push(
				ChangeTreeGrow.create(
					'trace', trace.add( 'items', mapping.get( key ) ),
					'val', item.asFabric,
					'rank', rank++,
				)
			);
		}
	}
	while( restart );

	Shell.change( this.window.uid, Sequence.Array( changes ) );
};

/*
| (De)transforms a point from visual reference system (VisualRS) to
| space reference system (SpaceRS).
|
| ~p:    the point in visual RS
| ~snap: if true snap is enabled for space
*/
def.proto._point2SpaceRS =
	function( p, snap )
{
	const t = this.window.transform;

	return(
		!snap || !this.snapping
		? p.detransform( t )
		: this._grid.snap( p ).detransform( t )
	);
};

/*
| Standard grid spacing.
*/
def.staticLazy._standardSpacing =
	( ) =>
	Point.XY( 15, 15 );

