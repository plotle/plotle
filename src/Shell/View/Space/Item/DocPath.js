/*
| A document path.
*/
def.extend = 'Shell/View/Space/Item/Base';

def.attributes =
{
	// the item's zone
	ancillary: { type: 'Shared/Ancillary/Item/DocPath/Self' },
};

import { Self as ADocPath          } from '{Shared/Ancillary/Item/DocPath/Self}';
import { Self as ChangeTreeGrow    } from '{ot:Change/Tree/Grow}';
import { Self as Color             } from '{gleam:Color}';
import { Self as DesignFont        } from '{Shell/Design/Font}';
import { Self as DesignHighlight   } from '{Shell/Design/Highlight}';
import { Self as Facet             } from '{Shell/Facet/Self}';
import { Self as FacetBorder       } from '{Shell/Facet/Border}';
import { Self as FacetList         } from '{Shell/Facet/List}';
import { Self as FigureList        } from '{gleam:Figure/List}';
import { Self as FDocPath          } from '{Shared/Fabric/Item/DocPath/Self}';
import { Self as GlintFigure       } from '{gleam:Glint/Figure}';
import { Self as GlintMask         } from '{gleam:Glint/Mask}';
import { Self as GlintPane         } from '{gleam:Glint/Pane}';
import { Self as GlintWindow       } from '{gleam:Glint/Window}';
import { Self as GradientColorStop } from '{gleam:Gradient/ColorStop}';
import { Self as GradientRadial    } from '{gleam:Gradient/Radial}';
import { Self as GroupBoolean      } from '{group@boolean}';
import { Self as Hover             } from '{Shell/Result/Hover}';
import { Self as ListFacetBorder   } from '{list@Shell/Facet/Border}';
import { Self as ListFDocPathJoint } from '{Shared/Fabric/Item/DocPath/Joint/List}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as MarkItems         } from '{Shared/Mark/Items}';
import { Self as Path              } from '{gleam:Path}';
import { Self as PathParts         } from '{gleam:Path/Parts}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as Resolution        } from '{gleam:Display/Canvas/Resolution}';
import { Self as Segment           } from '{gleam:Segment}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as Size              } from '{gleam:Size}';
import { Self as SysMode           } from '{Shell/Result/SysMode}';
import { Self as Trace             } from '{ti2c:Trace}';
import { Self as TraceRoot         } from '{Shared/Trace/Root}';
import { Self as TransformNormal   } from '{gleam:Transform/Normal}';
import { Self as TwigWidget        } from '{twig@<Shell/Widget/Types}';
import { Self as Uid               } from '{Shared/Session/Uid}';
import { Self as VSpaceWidgetInput } from '{Shell/View/Space/Widget/Input}';
import { Self as WidgetButton      } from '{Shell/Widget/Button}';

/*
| The zone is directly affected by actions.
*/
def.proto.actionAffects = 'zone';

/*
| Sees if the item is being clicked.
|
| ~p:     point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
| ~mark:  current user mark
*/
def.proto.click =
	function( p, shift, ctrl, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	if( this.within( p ) )
	{
		if( ctrl )
		{
			return this._ctrlClick( p, shift, mark );
		}

		const ps = p.sub( this.tZone.pos );
		for( let widget of this.widgets )
		{
			const bubble = widget.click( ps, shift, ctrl );
			if( bubble )
			{
				return bubble;
			}
		}

		// if none of the widgets were clicked
		// just focus the item itself
		Shell.alter(
			'action' , undefined,
			'mark', MarkItems.TraceVs( this.traceV ),
		);
		return true;
	}

	// checks through the path zig zag.
	const access = this.access;
	const pcons = this.pathConnections;
	const lineClickDistance = config.lineClickDistance;
	const transform = this.transform;

	for( let part of pcons )
	{
		if( !part.transform( transform ).withinDistance( p, lineClickDistance ) )
		{
			continue;
		}

		if( access !== 'rw' )
		{
			return false;
		}

		Shell.alter(
			'action', undefined,
			'mark',
				!mark || !ctrl
				? MarkItems.TraceVs( this.traceV )
				: mark.itemsMark.toggle( this.traceV )
		);

		return true;
	}

	// otherwise neither the vDoc start not the zizag were clicked.
	return undefined;
};

/*
| Cycles the focus.
|
| ~dir: +1 clockwise, -1 anticlockwise
|
| ~return: true, the item swallowed the cycle, false cycle on parent.
*/
def.proto.cycleFocus =
	function( dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( dir !== 1 && dir !== -1 ) throw new Error( );
/**/}

	return true;
};

/*
| The item model.
*/
def.staticLazy.fabricModel =
	( ) =>
	FDocPath.create(
		'joints', ListFDocPathJoint.Empty,
		'name',   'begin',
		'zone', Rect.zero,
	);

/*
| Returns the focused widget.
*/
def.lazy.focusedWidget =
	function( )
{
	const mark = this.mark;
	if( !mark )
	{
		return undefined;
	}

	const tvw = mark.traceVWidget;
	if( !tvw )
	{
		return undefined;
	}

	return this.widgets.get( tvw.last.key );
};

/*
| User wants to create a new docPath.
|
| ~action: the create action
| ~dp: the detransform point the create stopped at.
*/
def.static.Generic =
	function( action, dp )
{
	let zone = Rect.Arbitrary( action.pointStart, dp );
	zone = zone.ensureMinSize( Self.sizeMin );
	zone = zone.create( 'width', zone.height / Self.proportion );

	const ati = action.transientItem;
	const ancillary = ati.ancillary.create( 'zone', zone );
	const uidSpace = action.uidSpace;
	const key = Uid.newUid( );

	const trace = TraceRoot.space( uidSpace ).add( 'items', key );

	const traceV =
		action.transientItem.traceV
		.forward( 'space' )
		.add( 'items', key );

	Shell.change(
		uidSpace,
		ChangeTreeGrow.create(
			'trace', trace,
			'rank', 0,
			'val', ancillary.asFabric,
		)
	);

	Shell.alter(	'mark', MarkItems.TraceVs( traceV ) );
};

/*
| The item's glint.
*/
def.lazy.glint =
	function( )
{
	const zone = this.tZone;
	const list =
	[
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane',
				GlintPane.create(
					'glint', this._innerGlint,
					'size', zone.size.add( 2 ),
					'resolution', this.resolution,
				),
			'pos', zone.pos,
		)
	];

	if( this.highlight )
	{
		const facet = Self._facets.getFacet( 'highlight', true );
		list.push( facet.glint( this.tOutline ) );
	}

	const joints = this.ancillary.joints;
	const pathZZ = this.pathZigZag;
	const transform = this.transform;

/**/if( CHECK && pathZZ.length !== joints.length ) throw new Error( );

	let prevOutline = this.tOutline;
	const colorBorder    = Color.RGBA( 137, 123, 176, 0.6 );
	const widthBorder    = 1.5;
	const widthPath      = 11;
	const colorArrowFill = Color.RGBA( 137, 123, 176, 0.6 );
	const colorFill      = Color.RGBA( 224, 210, 255, 0.9 );
	const lenArrow       = 10;
	// distance of path arrows (from tip to tip)
	const stepArrow      = 22;

	const tw05 = widthPath / Math.sqrt( 2 );

	for( let a = 0, alen = joints.length; a < alen; a++ )
	{
		const joint = joints.get( a );
		const pp = pathZZ.get( a );

		const thickPath = pp.thickPath( widthPath ).transform( transform );
		const outlines = [ ];

		if( prevOutline )
		{
			outlines.push( prevOutline );
		}

		if( joint.pos.ti2ctype === Trace )
		{
			prevOutline = joint.outline.transform( transform );
			outlines.push( prevOutline );
		}

		// glint list for stuff in this connection.
		const list2 =
		[
			GlintFigure.create(
				'color',  colorBorder,
				'figure', thickPath,
				'fill',   colorFill,
				'width',  widthBorder,
			),
		];

		const plen = pp.length;
		const p0 = pp.p0;
		const angle = pp.angle;
		const acw = angle.cw08;
		const accw = angle.ccw08;

		// creates the little arrows on the line
		for( let la = stepArrow / 2; la < plen; la += stepArrow )
		{
			const lb = la + lenArrow;
			const pa = p0.add( angle.cos * la, - angle.sin * la );
			const pb = p0.add( angle.cos * lb, - angle.sin * lb );

			const path =
				Path.Plan(
					'start', pa.add( -acw.cos * tw05, acw.sin * tw05 ),
					'line', pa,
					'line', pa.add( -accw.cos * tw05, accw.sin * tw05 ),
					'line', pb.add( -accw.cos * tw05, accw.sin * tw05 ),
					'line', pb,
					'line', pb.add( -acw.cos * tw05, acw.sin * tw05 ),
					'line', 'close',
				);

			list2.push(
				GlintFigure.create(
					'figure', path.transform( transform ),
					'fill', colorArrowFill,
					'nogrid', true,
				)
			);
		}

		// masks the path line with beginning and ending shape
		list.push(
			GlintMask.create(
				'glint', ListGlint.Array( list2 ),
				'outline', FigureList.Array( outlines ),
				'reversed', true,
			)
		);
	}

	return ListGlint.Array( list );
};

/*
| User inputs some characters.
*/
def.proto.input =
	function( string )
{
	const widget = this.focusedWidget;
	if( widget )
	{
		widget.input( string );
	}
};

/*
| The mask for the benches.
*/
def.lazy.mask =
	function( )
{
	const list = [ this.outline ];
	const pzz = this.pathZigZag;

	for( let segment of pzz )
	{
		list.push( segment.thickPath( 11 ) );
	}

	return FigureList.Array( list );
};

/*
| Returns the minimum x-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleX =
	function( zone )
{
	return Self.sizeMin.width / zone.width;
};

/*
| Returns the minimum y-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleY =
	function( zone )
{
	return Self.sizeMin.height / zone.height;
};

/*
| Minimum size.
*/
def.lazy.sizeMin =
def.staticLazy.sizeMin =
	( ) =>
	Size.WH( 0, 80 );

/*
| The item model.
*/
def.staticLazyFunc.model =
	function( light )
{
	const ancillary =
		ADocPath.FromFabric(
			Self.fabricModel,
			TraceRoot.space( ':transient' ).add( 'items', ':transient' ),
		);

	return(
		Self.create(
			'access',     'rw',
			'ancillary',   ancillary,
			'highlight',   false,
			'light',       light,
			'rank',        0,
			'resolution',  Resolution.Identity,
			'systemFocus', true,
			'transform',   TransformNormal.singleton,
		)
	);
};

/*
| Mouse wheel turned.
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	return this.within( p );
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	return this.ancillary.outline;
};

/*
| The connections of the path.
|
| While pathZigZag goes through the item centers,
| pathConnections begins and ends on the outlines.
| It is used for the handles.
*/
def.lazy.pathConnections =
	function( )
{
	let list = [ ];
	const joints = this.ancillary.joints;
	let prevJoint;

	for( let joint of joints )
	{
		if( prevJoint )
		{
			const po = prevJoint.outline || prevJoint.pos;
			const to = joint.outline || joint.pos;
			list.push( Segment.Connection( po, to ) );
		}
		else
		{
			const po = this.ancillary.zone.ps;
			const to = joint.outline || joint.pos;
			list.push( Segment.Connection( po, to ) );
		}
		prevJoint = joint;
	}

	return PathParts.Array( list );
};

/*
| The zigzag of the path going through the centers.
*/
def.lazy.pathZigZag =
	function( )
{
	let list = [ ];
	const joints = this.ancillary.joints;
	let prevJoint;

	for( let joint of joints )
	{
		const pp =
			prevJoint
			? prevJoint.pc
			: this.ancillary.zone.ps;
		list.push( Segment.P0P1( pp, joint.pc ) );
		prevJoint = joint;
	}

	return PathParts.Array( list );
};

/*
| User is hovering his/her pointing device.
| Checks if this item reacts on this.
|
| ~p: point hovered upon
| ~mode: space mode
*/
def.proto.pointingHover =
	function( p, mode )
{
	// not on the item?
	if( !this.within( p ) )
	{
		return undefined;
	}

	const ps = p.sub( this.tZone.pos );
	const widgets = this.widgets;
	for( let w of widgets )
	{
		const bubble = w.pointingHover( ps );
		if( bubble )
		{
			return bubble;
		}
	}

	return Hover.CursorDefault.create( 'traceV', this.traceV );
};

/*
| Portals are positioned by their zone.
*/
def.static.positioning =
def.proto.positioning =
	'zone';

/*
| How high pins should be in respect to their width.
*/
def.static.proportion = 1.30;

/*
| The doc path icon keeps it's proportions.
*/
def.proto.proportional = true;

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK && traceV.forward( 'items' ) !== this.traceV ) throw new Error( );


	Shell.alter( 'action', undefined );

	Shell.showDocView(
		this.traceV.forward( 'view' ).last.at,
		this.ancillary.trace.last.key
	);
};

/*
| User pressed a special key.
|
| ~key: key pressed.
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	const widget = this.focusedWidget;
	if( !widget )
	{
		return;
	}

	if( key === 'tab' )
	{
		Shell.cycleFocus( shift ? -1 : 1 );
		return;
	}

	widget.specialKey( key, shift, ctrl );
};

/*
| Returns the system mode.
*/
def.lazy.sysMode =
	function( )
{
	const focus = this.focusedWidget;

	return(
		focus
		? focus.sysMode
		: SysMode.Blank
	);
};

/*
| Converts a fabric trace to a visual trace.
| See also traceV2F.
*/
def.proto.traceF2V =
	function( trace )
{
/**/if( CHECK && trace.forward( 'items' ) !== this.ancillary.trace ) throw new Error( );

	const traceFItems = trace.forward( 'items' );

	let traceV = this.traceV;

	let a; // continues plain convert here.
	if( traceFItems )
	{
		a = traceFItems.length;
		if( trace.length > a )
		{
			const stepField = trace.get( a++ );
			switch( stepField.name )
			{
				case 'name':
					traceV = traceV.add( 'widgets', 'name' ).add( 'value' );
					break;
				default: throw new Error( );
			}
		}
	}
	else
	{
		a = this.ancillary.trace.length;
	}

	for( let alen = trace.length; a < alen; a++ )
	{
		const step = trace.get( a );
		traceV = traceV.addStep( step );
	}

	return traceV;
};

/*
| Converts a visual trace to a fabric trace.
| See also traceF2V.
*/
def.proto.traceV2F =
	function( traceV )
{
/**/if( CHECK && traceV.forward( 'items' ) !== this.traceV ) throw new Error( );

	let trace = this.ancillary.trace;
	const traceVWidgets = traceV.forward( 'widgets' );

	let a; // continues plain convert here.
	if( traceVWidgets )
	{
/**/	if( CHECK && traceV.get( traceVWidgets.length ).name !== 'value' ) throw new Error( );

		a = traceVWidgets.length + 1;
		switch( traceVWidgets.last.key )
		{
			case 'name':
				trace = trace.add( 'name' );
				break;

			default: throw new Error( );
		}
	}
	else
	{
		a = this.traceV.length;
	}

	for( let alen = traceV.length; a < alen; a++ )
	{
		const step = traceV.get( a );
		trace = trace.addStep( step );
	}

	return trace;
};

/*
| Builds the widgets.
*/
def.lazy.widgets =
	function( )
{
	const keys = [ 'name', 'docView' ];
	const twig =
	{
		name:    this._widgetInputName,
		docView: this._widgetButtonDocView,
	};
	return TwigWidget.create( 'twig:init', twig, keys );
};

/*
| Colors.
*/
def.staticLazy._colorBorder =
	( ) =>
	Color.RGB( 214, 200, 255 );

/*
| Facets of the item. FIXME remove.
|
| ~light: if true light variant, otherwise dark.
*/
def.staticLazy._facets =
	( ) =>
	FacetList.Elements(
		// default
		Facet.create(
			'fill',
				GradientRadial.Elements(
					GradientColorStop.OffsetColor( 0, Color.RGBA( 255, 242, 255, 0.955 ) ),
					GradientColorStop.OffsetColor( 1, Color.RGBA( 224, 210, 255, 0.955 ) ),
				),
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistanceWidth( Self._colorBorder, -3, 6 ),
					FacetBorder.simpleBlack,
				),
		),
		DesignHighlight.facet,
	);

/*
| Creates the item's inner glint.
*/
def.lazy._innerGlint =
	function( )
{
	const transform = this.transform.scaleOnly;
	const facet = Self._facets.getFacet( );

	const widgets = this.widgets;
	const list = [ ];
	for( let widget of widgets )
	{
		list.push( widget.glint );
	}
	const tzo =
		this.outline.sub( this.ancillary.zone.pos )
		.transform( transform );

	return(
		ListGlint.Elements(
			facet.glintFill( tzo ),
			// masks the content
			GlintMask.create( 'glint', ListGlint.Array( list ), 'outline', tzo ),
			// puts the border on top of everything else
			facet.glintBorder( tzo )
		)
	);
};

/*
| Prepares the button widget for "doc view".
*/
def.lazy._widgetButtonDocView =
	function( )
{
	const zone = this.ancillary.zone;
	const transform = this.transform;
	const traceV = this.traceV.add( 'widgets', 'docView' );

	const sizeWidth  = 50;
	const sizeHeight = 20;

	const zoneW =
		Rect.PosWidthHeight(
			Point.XY(
				( zone.width - sizeWidth ) / 2,
				( zone.width - sizeHeight ) / 2 + 22,
			),
			sizeWidth, sizeHeight
		);

	let wHover = this.hover;
	if( wHover && !wHover.hasTrace( traceV ) )
	{
		wHover = undefined;
	}

	const colorBorderDarker = Color.RGB( 137, 123, 176 );
	const colorBorderEvenDarker = Color.RGB( 108, 93, 151 );

	const facets =
		FacetList.Elements(
			// default state.
			Facet.create(
				'fill',
					Color.RGBA( 237, 210, 255, 0.5 ),
				'border',
					FacetBorder.Color( colorBorderDarker )
			),
			// hover
			Facet.create(
				'border',
					FacetBorder.ColorWidth( colorBorderDarker, 1.5 ),
				'fill',
					Color.RGBA( 188, 88, 255, 0.7 ),
				'specs',
					GroupBoolean.Table( { hover: true } ),
			),
			// focus
			Facet.create(
				'border',
					FacetBorder.ColorDistanceWidth( colorBorderEvenDarker, -1, 1.5 ),
				'fill',
					Color.RGBA( 237, 210, 255, 0.5 ),
				'specs',
					GroupBoolean.Table( { focus: true } ),
			),
			// focus and hover
			Facet.create(
				'border',
					FacetBorder.ColorDistanceWidth( colorBorderEvenDarker, -1, 1.5 ),
				'fill',
					Color.RGBA( 188, 88, 255, 0.7 ),
				'specs',
					GroupBoolean.Table( { focus: true, hover: true } ),
			),
		);

	return(
		WidgetButton.create(
			'facets',      facets,
			'fontColor',   DesignFont.Size( 13 ),
			'hover',       wHover,
			'resolution',  this.resolution,
			'shape',      'RectRound',
			'string',     'view',
			'systemFocus', this.systemFocus,
			'transform',   transform.scaleOnly,
			'traceV',      traceV,
			'zone',        Rect.zero,
			'zone',        zoneW,
		)
	);
};

/*
| Prepares the button widget for "doc view".
*/
def.lazy._widgetInputName =
	function( )
{
	const zone = this.ancillary.zone;
	const transform = this.transform;

	const fontColor = DesignFont.Size( 13 );
	const name = this.ancillary.name;
	const pitch = VSpaceWidgetInput.pitch;

	const width = fontColor.StringNormal( name ).advanceWidth + 2 * pitch.x;
	const height = fontColor.size + 2 * pitch.y + 2;

	const traceV = this.traceV.add( 'widgets', 'name' );

	const pos =
		Point.XY(
			( zone.width - width ) / 2,
			( zone.width - height ) / 2 - 10
		);

	let wHover = this.hover;
	if( wHover && !wHover.hasTrace( traceV ) )
	{
		wHover = undefined;
	}

	let wMark = this.mark;
	if( wMark && !wMark.encompasses( traceV ) )
	{
		wMark = undefined;
	}

	const facets =
		FacetList.Elements(
			// default state
			Facet.create(
				'fill', Color.white,
				'border', FacetBorder.Color( Self._colorBorder )
			)
		);

	return(
		VSpaceWidgetInput.create(
			'facets',       facets,
			'fontColor',    fontColor,
			'hover',        wHover,
			'mark',         wMark,
			'readonly',     this.access !== 'rw',
			'resolution',   this.resolution,
			'traceFString', this.ancillary.trace.add( 'name' ),
			'systemFocus',  this.systemFocus,
			'transform',    transform.scaleOnly,
			'value',        name,
			'traceV',       traceV,
			'zone',         Rect.PosWidthHeight( pos, width, height ),
		)
	);
};
