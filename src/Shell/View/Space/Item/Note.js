/*
| A fix sized text item.
|
| Has potentionaly a scrollbar.
*/
def.extend = 'Shell/View/Space/Item/TextItem';

def.attributes =
{
	// the labels ancillary fabric
	ancillary: { type: 'Shared/Ancillary/Item/Note' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// text item seam
	seam: { type: 'Shell/Seam/Space/TextItem' },
};

import { Self as ANote             } from '{Shared/Ancillary/Item/Note}';
import { Self as Caret             } from '{Shared/Mark/Caret}';
import { Self as ChangeTreeGrow    } from '{ot:Change/Tree/Grow}';
import { Self as Color             } from '{gleam:Color}';
import { Self as DesignHighlight   } from '{Shell/Design/Highlight}';
import { Self as DesignScrollbar   } from '{Shell/Design/Scrollbar}';
import { Self as Facet             } from '{Shell/Facet/Self}';
import { Self as FacetBorder       } from '{Shell/Facet/Border}';
import { Self as FacetList         } from '{Shell/Facet/List}';
import { Self as FigureList        } from '{gleam:Figure/List}';
import { Self as FontRoot          } from '{gleam:Font/Root}';
import { Self as FNote             } from '{Shared/Fabric/Item/Note}';
import { Self as FPara             } from '{Shared/Fabric/Text/Para}';
import { Self as FText             } from '{Shared/Fabric/Text/Self}';
import { Self as GlintMask         } from '{gleam:Glint/Mask}';
import { Self as GlintPane         } from '{gleam:Glint/Pane}';
import { Self as GlintWindow       } from '{gleam:Glint/Window}';
import { Self as GradientAskew     } from '{gleam:Gradient/Askew}';
import { Self as GradientColorStop } from '{gleam:Gradient/ColorStop}';
import { Self as ListFacetBorder   } from '{list@Shell/Facet/Border}';
import { Self as ListFPara         } from '{list@Shared/Fabric/Text/Para}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as SeamSpaceRoot     } from '{Shell/Seam/Space/Self}';
import { Self as Size              } from '{gleam:Size}';
import { Self as TraceRoot         } from '{Shared/Trace/Root}';
import { Self as Uid               } from '{Shared/Session/Uid}';
import { Self as VSpacePara        } from '{Shell/View/Space/Para}';
import { Self as VSpaceText        } from '{Shell/View/Space/Text}';
import { Self as WidgetScrollbar   } from '{Shell/Widget/Scrollbar}';


/*
| The zone is directly affected by actions.
*/
def.proto.actionAffects = 'zone';

/*
| The item model.
*/
def.staticLazy.fabricModel =
	( ) =>
	FNote.create(
		'fontSize', 13,
		'text',
			FText.create(
				'paras', ListFPara.Elements( FPara.create( 'string', '' ) ),
			),
		'zone', Rect.zero,
	);

/*
| User wants to create a new note.
|
| ~action: the create action
| ~dp:     the detransform point the create stopped at.
*/
def.static.Generic =
	function( action, dp )
{
	let zone = Rect.Arbitrary( action.pointStart, dp );
	zone = zone.ensureMinSize( Self.sizeMin );

	const tItem = action.transientItem;
	const aNote = tItem.ancillary.create( 'zone', zone );
	const key = Uid.newUid( );

	const uidSpace = action.uidSpace;
	const trace = TraceRoot.space( uidSpace ).add( 'items', key );

	const traceV =
		action.transientItem.traceV
		.forward( 'space' )
		.add( 'items', key )
		.add( 'text' ).add( 'paras', 0 )
		.add( 'string' ).add( 'offset', 0 );

	Shell.change(
		uidSpace,
		ChangeTreeGrow.create(
			'rank', 0,
			'trace', trace,
			'val', aNote.asFabric,
		)
	);

	Shell.alter( 'mark', Caret.TraceV( traceV ) );
};

/*
| The item's glint.
*/
def.lazy.glint =
	function( )
{
	const light = this.light;
	const tZone = this.tZone;

	const list =
	[
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane',
				GlintPane.create(
					'glint', this._innerGlint,
					'resolution', this.resolution,
					'size', tZone.size.add( 2 ),
				),
			'pos', tZone.pos
		)
	];

	if( this.highlight )
	{
		const facet = Self._facets( light ).getFacet( 'highlight', true );
		list.push( facet.glint( this.tOutline ) );
	}

	const sbary = this.scrollbarY;
	if( sbary )
	{
		list.push( sbary.glint );
	}

	return ListGlint.Array( list );
};

/*
| The note mask can include scrollbars.
*/
def.lazy.mask =
	function( )
{
	const sy = this.scrollbarY;
	if( !sy )
	{
		return this.outline;
	}

	return FigureList.Elements( this.outline, sy.outline );
};

/*
| Returns the minimum x-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleX =
	function( zone )
{
	return Self.sizeMin.width / zone.width;
};

/*
| Returns the minimum y-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleY =
	function( zone )
{
	return Self.sizeMin.height / zone.height;
};

/*
| Minimum size.
*/
def.lazy.sizeMin =
def.staticLazy.sizeMin =
	( ) =>
	Size.WH( 30, 30 );

/*
| Mouse wheel turned.
|
| ~p,
| ~dir
*/
def.proto.mousewheel =
	function( p, dir )
{
	if( !this.within( p ) ) return false;

	const scrollPos = this.scrollPos;
	let y = scrollPos.y - dir * config.textWheelSpeed;
	if( y < 0 ) y = 0;

	Shell.alter( this.traceSeam.add( 'scrollPos' ), scrollPos.create( 'y', y ) );

	return true;
};

/*
| The note's outline.
*/
def.lazy.outline =
	function( )
{
	return this.ancillary.outline;
};

/*
| Notes use zone for positioning
*/
def.static.positioning =
def.proto.positioning =
	'zone';

/*
| Rectify checks if the scrollpos is valid.
*/
def.proto.rectify =
	function( )
{
	const scrollPos = this.scrollPos;
	const sy = scrollPos.y;
	const rsy = this._rectifyScrollY( sy );

	if( sy !== rsy )
	{
		Shell.alter( this.traceSeam.add( 'scrollPos' ), scrollPos.create( 'y', rsy ) );
	}
};

/*
| The vertical scrollbar.
*/
def.lazy.scrollbarY =
	function( )
{
	const dHeight = this.text.sizeFull.height;
	const zone = this.ancillary.zone;
	const aperture = zone.height - ANote.marginInner.y;

	const distanceVScrollbar = 5;

	if( dHeight <= aperture )
	{
		return undefined;
	}

	return(
		WidgetScrollbar.create(
			'anchorAlign', 'center',
			'aperture',     aperture,
			'ellipseA',     DesignScrollbar.ellipseA,
			'ellipseB',     DesignScrollbar.ellipseB,
			'facet',        DesignScrollbar.facet( this.light ),
			'max',          dHeight,
			'minHeight',    DesignScrollbar.minHeight,
			'pos',          zone.pos.add( zone.width, distanceVScrollbar ),
			'resolution',   this.resolution,
			'scrollPos',    this.scrollPos.y,
			'size',         zone.height - distanceVScrollbar * 2,
			'strength',     DesignScrollbar.strength,
			'transform',    this.transform,
			'traceV',       this.traceV.add( 'widgets', 'scrollbarY' ),
		)
	);
};

/*
| Scrolls the note so the caret comes into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	const mark = this.mark;
	if( !mark || !mark.hasCaret ) return;

	const scrollPos = this.scrollPos;
	let sy = scrollPos.y;
	const para = this.text.paras.get( mark.traceVCaret.backward( 'paras' ).last.at );

/**/if( CHECK && para.ti2ctype !== VSpacePara ) throw new Error( );

	const zone = this.ancillary.zone;
	const imargin = this.text.innerMargin;
	const fs = this.text.fontSize;
	const descend = fs * FontRoot.bottomBox;
	const p = para.locateOffsetPoint( mark.traceVCaret.last.at );
	const ppos = para.pos;
	const s = ppos.y + p.y + descend + imargin.s;
	const n = ppos.y + p.y - fs - imargin.n;

	if( n < 0 )
	{
		sy = sy + n;
	}
	else if( s > zone.height )
	{
		sy = sy + s - zone.height;
	}

	sy = this._rectifyScrollY( sy );
	if( sy !== scrollPos.y )
	{
		Shell.alter( this.traceSeam.add( 'scrollPos' ), scrollPos.create( 'y', sy ) );
	}
};

/*
| ScrollPos from notes is taken from seam.
*/
def.lazy.scrollPos =
	function( )
{
	return this.seam.scrollPos;
};

/*
| Builds the visual text.
*/
def.lazy.text =
	function( )
{
	const ancillary = this.ancillary;
	const zone = ancillary.zone;
	const traceV = this.traceV;

	return(
		VSpaceText.create(
			'ancillary',        ancillary.text,
			'clipSize',         zone.size,
			'colorFontDefault', Color.black,
			'fontSize',         ancillary.fontSize,
			'innerMargin',      ANote.marginInner,
			'light',            this.light,
			'mark',             this.mark,
			'paraSep',          Math.round( ancillary.fontSize / 1.5 ),
			'resolution',       this.resolution,
			'scale',            this.transform.scaleOnly,
			'scrollPos',        this.scrollPos,
			'systemFocus',      this.systemFocus,
			'traceV',           traceV?.add( 'text' ),
			'widthFlow',        zone.width - ANote.marginInner.x,
		)
	);
};

/*
| Seam trace of the item.
*/
def.lazy.traceSeam =
	function( )
{
	return SeamSpaceRoot.traceSeam.add( 'textItems', this.traceV.last.key );
};

/*
| Facets of the item.
|
| FIXME remove.
*/
def.staticLazyFunc._facets =
	( light ) =>
	FacetList.Elements(
		Facet.create(
			'fill',
				GradientAskew.Elements(
					GradientColorStop.OffsetColor( 0, Color.RGBA( 255, 255, 248, 0.955 ) ),
					GradientColorStop.OffsetColor( 1, Color.RGBA( 255, 255, 160, 0.955 ) ),
				),
			'border',
				ListFacetBorder.Elements(
					FacetBorder.ColorDistance( Color.RGB( 255, 188, 87 ), -1 ),
					FacetBorder.simpleBlack,
				),
		),
		DesignHighlight.facet,
	);


/*
| The notes inner glint.
*/
def.lazy._innerGlint =
	function( )
{
	const text = this.text;
	const facet = Self._facets( this.light ).getFacet( );
	const tOrthoShape = this.outlineZero.transform( this.transform.scaleOnly );

	return(
		ListGlint.Elements(
			facet.glintFill( tOrthoShape ),
			GlintMask.create( 'outline', tOrthoShape, 'glint', text.glint, ),
			facet.glintBorder( tOrthoShape )
		)
	);
};

/*
| Returns a valid vertical scroll position.
|
| ~sy: into scrollPosition to rectify
*/
def.proto._rectifyScrollY =
	function( sy )
{
	const aperture = this.ancillary.zone.height - ANote.marginInner.y;
	const dHeight = this.text.sizeFull.height;

	if( dHeight < aperture )
	{
		sy = 0;
	}
	else if( sy > dHeight - aperture )
	{
		sy = dHeight - aperture;
	}

	if( sy < 0 )
	{
		sy = 0;
	}

	return sy;
};

/*
| The item's outline anchored at zero.
*/
def.lazy.outlineZero =
	function( )
{
	return this.ancillary.outline.create( 'pos', Point.zero );
};
