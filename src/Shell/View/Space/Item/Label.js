/*
| An item with resizing text.
*/
def.extend = 'Shell/View/Space/Item/TextItem';

def.attributes =
{
	// the label's ancillary fabric
	ancillary: { type: 'Shared/Ancillary/Item/Label' },

	// the item is highlighted
	highlight: { type: [ 'undefined', 'boolean' ] },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },
};

import { Self as ALabel           } from '{Shared/Ancillary/Item/Label}';
import { Self as ChangeTreeShrink } from '{ot:Change/Tree/Shrink}';
import { Self as Color            } from '{gleam:Color}';
import { Self as DesignHighlight  } from '{Shell/Design/Highlight}';
import { Self as Facet            } from '{Shell/Facet/Self}';
import { Self as FacetList        } from '{Shell/Facet/List}';
import { Self as FLabel           } from '{Shared/Fabric/Item/Label}';
import { Self as FPara            } from '{Shared/Fabric/Text/Para}';
import { Self as FText            } from '{Shared/Fabric/Text/Self}';
import { Self as GlintPane        } from '{gleam:Glint/Pane}';
import { Self as GlintWindow      } from '{gleam:Glint/Window}';
import { Self as ListFPara        } from '{list@Shared/Fabric/Text/Para}';
import { Self as ListGlint        } from '{list@<gleam:Glint/Types}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Rect             } from '{gleam:Rect}';
import { Self as Resolution       } from '{gleam:Display/Canvas/Resolution}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as TransformNormal  } from '{gleam:Transform/Normal}';
import { Self as TraceRoot        } from '{Shared/Trace/Root}';
import { Self as VSpace           } from '{Shell/View/Space/Self}';
import { Self as VSpaceText       } from '{Shell/View/Space/Text}';

/*
| Position and fontSize are directly affected by actions.
*/
def.proto.actionAffects = 'posfs';

/*
| The item model.
*/
def.staticLazy.fabricModel =
	( ) =>
	FLabel.create(
		'fontSize', 13,
		'text',
			FText.create(
				'paras', ListFPara.Elements( FPara.create( 'string', 'Label' ) ),
			),
		'zone', Rect.zero,
	);

/*
| The item's glint.
*/
def.lazy.glint =
	function( )
{
	const light = this.light;
	const tZone = this.tZone;
	const list =
	[
		GlintWindow.create(
			'name', this.traceV.asString,
			'pane',
				GlintPane.create(
					'glint', this.text.glint,
					'resolution', this.resolution,
					'size', tZone.size.add( 1 ),
				),
			'pos', tZone.pos
		)
	];

	if( this.highlight )
	{
		const facet = Self._facets( light ).getFacet( 'highlight', true );
		list.push( facet.glint( this.tOutline ) );
	}

	return ListGlint.Array( list );
};

/*
| Nofication when the item lost the users mark.
*/
def.proto.markLost =
	function( )
{
	const ancillary = this.ancillary;

	if( ancillary.text.isBlank )
	{
		const trace = ancillary.trace;

		Shell.change(
			trace.first.key,
			ChangeTreeShrink.create(
				'trace', trace,
				//'prev', tl.pick( root ),
				'prev', ancillary.asFabric,
				'rank', this.rank,
			)
		);
	}
};

/*
| Returns the minimum x-scale factor this item could go through.
|
| While theoretically it can go as small as desired,
| zero turned out to cause issues.
*/
def.proto.minScaleX =
	( zone ) => 0.0001;

/*
| Returns the minimum y-scale factor this item could go through.
*/
def.proto.minScaleY =
	( zone ) => 0.0001;

/*
| The item model.
|
| ~light: true if light color scheme
*/
def.staticLazyFunc.model =
	function( light )
{
	const trace = TraceRoot.space( ':transient' ).add( 'items', ':transient' );

	let aLabel = ALabel.FromFabric( Self.fabricModel, trace );
	aLabel = aLabel.create( 'zone', Rect.zero.Size( aLabel.sizeComputed ) );

	return(
		Self.create(
			'access',     'rw',
			'ancillary',   aLabel,
			'highlight',   false,
			'light',       light,
			'rank',        0,
			'resolution',  Resolution.Identity,
			'systemFocus', true,
			'transform',   TransformNormal.singleton,
			'traceV',      VSpace.transTrace,
		)
	);
};

/*
| The mouse wheel turned.
|
| ~p: point where the whelling happeneded
| ~dir: direction of wheel (+1 or -1)
*/
def.proto.mousewheel =
	function( p, dir )
{
	// the label lets wheel events pass through it.
	return false;
};

/*
| Labels use zones for positioning
*/
def.static.positioning =
def.proto.positioning =
	'zone';

/*
| Labels resize proportional only.
*/
def.proto.proportional = true;

/*
| Dummy since a label does not scroll.
*/
def.proto.scrollMarkIntoView =
	( ) =>
	undefined;

/*
| ScrollPos for labels is always zero.
*/
def.lazy.scrollPos =
	function( )
{
	return Point.zero;
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	return this.ancillary.zone;
};

/*
| Builds the visual text.
*/
def.lazy.text =
	function( )
{
	const ancillary = this.ancillary;
	const fontSize = ancillary.fontSize;
	const traceV = this.traceV;

	return(
		VSpaceText.create(
			'ancillary',        ancillary.text,
			'colorFontDefault',
				this.light
				? Color.black
				: Color.RGB( 229, 229, 299 ),
			'fontSize',         fontSize,
			'innerMargin',      ALabel.marginInner,
			'light',            this.light,
			'mark',             this.mark,
			'paraSep',          Math.round( fontSize / 20 ),
			'resolution',       this.resolution,
			'scale',            this.transform.scaleOnly,
			'scrollPos',        Point.zero,
			'systemFocus',      this.systemFocus,
			'traceV',           traceV?.add( 'text' ),
			'widthFlow',        0,
		)
	);
};

/*
| Facets of the item.
|
| FIXME remove.
*/
def.staticLazyFunc._facets =
	( light ) =>
	FacetList.Elements(
		// default
		Facet.create( ),
		DesignHighlight.facet,
	);
