/*
| A subspace portal to another space.
*/
def.extend = 'Shell/View/Space/Item/Base';

def.attributes =
{
	// the subspace portal's ancillary fabric
	ancillary: { type: 'Shared/Ancillary/Item/Subspace' },
};

import { Self as ASubspace         } from '{Shared/Ancillary/Item/Subspace}';
import { Self as ChangeTreeGrow    } from '{ot:Change/Tree/Grow}';
import { Self as Color             } from '{gleam:Color}';
import { Self as DesignFont        } from '{Shell/Design/Font}';
import { Self as DesignHighlight   } from '{Shell/Design/Highlight}';
import { Self as Ellipse           } from '{gleam:Ellipse}';
import { Self as Facet             } from '{Shell/Facet/Self}';
import { Self as FacetBorder       } from '{Shell/Facet/Border}';
import { Self as FacetList         } from '{Shell/Facet/List}';
import { Self as FSubspace         } from '{Shared/Fabric/Item/Subspace}';
import { Self as GlintMask         } from '{gleam:Glint/Mask}';
import { Self as GlintPane         } from '{gleam:Glint/Pane}';
import { Self as GlintWindow       } from '{gleam:Glint/Window}';
import { Self as GradientColorStop } from '{gleam:Gradient/ColorStop}';
import { Self as GradientRadial    } from '{gleam:Gradient/Radial}';
import { Self as GroupBoolean      } from '{group@boolean}';
import { Self as Hover             } from '{Shell/Result/Hover}';
import { Self as ListFacetBorder   } from '{list@Shell/Facet/Border}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as MarkItems         } from '{Shared/Mark/Items}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as RectRoundExt      } from '{gleam:RectRoundExt}';
import { Self as Resolution        } from '{gleam:Display/Canvas/Resolution}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as Size              } from '{gleam:Size}';
import { Self as SysMode           } from '{Shell/Result/SysMode}';
import { Self as TraceRoot         } from '{Shared/Trace/Root}';
import { Self as TransformNormal   } from '{gleam:Transform/Normal}';
import { Self as TwigWidget        } from '{twig@<Shell/Widget/Types}';
import { Self as Uid               } from '{Shared/Session/Uid}';
import { Self as VSpaceWidgetInput } from '{Shell/View/Space/Widget/Input}';
import { Self as WidgetButton      } from '{Shell/Widget/Button}';

/*
| The zone is directly affected by actions.
*/
def.proto.actionAffects = 'zone';

/*
| Sees if this item is being clicked.
|
| ~p:     point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
| ~mark:  current user mark
*/
def.proto.click =
	function( p, shift, ctrl, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	if( !this.within( p ) )
	{
		return undefined;
	}

	if( ctrl )
	{
		return this._ctrlClick( p, shift, mark );
	}

	const ps = p.sub( this.tZone.pos );
	for( let widget of this.widgets )
	{
		const bubble = widget.click( ps, shift, ctrl );
		if( bubble )
		{
			return bubble;
		}
	}

	// if none of the widgets were clicked
	// just focus the item itself
	Shell.alter(
		'action', undefined,
		'mark',   MarkItems.TraceVs( this.traceV ),
	);

	return true;
};

/*
| Cycles the focus.
|
| ~dir: +1 clockwise, -1 anticlockwise
|
| ~return: true, the item swallowed the cycle, false cycle on parent.
*/
def.proto.cycleFocus =
	function( dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( dir !== 1 && dir !== -1 ) throw new Error( );
/**/}

	const mark = this.mark;
	if( !mark )
	{
		return false;
	}

	const traceV = mark.traceVCaret.backward( 'widgets' );
	const widgets = this.widgets;
	let rank = widgets.rankOf( traceV.last.key );
	rank += dir;

	if( rank < 0 )
	{
		rank += widgets.length;
	}
	else if( rank >= widgets.length )
	{
		rank -= widgets.length;
	}

	Shell.alter(
		'mark',
			MarkCaret.TraceV(
				this.traceV
				.add( 'widgets', widgets.getKey( rank ) )
				.add( 'value' ).add( 'offset', 0 )
			),
	);

	return true;
};

/*
| The item model.
*/
def.staticLazy.fabricModel =
	( ) =>
	FSubspace.create(
		'link', 'subspace',
		'zone',  Rect.zero,
	);

/*
| Returns the focused widget.
*/
def.lazy.focusedWidget =
	function( )
{
	const mark = this.mark;
	if( !mark )
	{
		return undefined;
	}

	const tvw = mark.traceVWidget;
	if( !tvw )
	{
		return undefined;
	}

	return this.widgets.get( tvw.last.key );
};

/*
| User wants to create a new subspace portal.
|
| ~action: the create action
| ~dp: the detransform point the create stopped at.
*/
def.static.Generic =
	function( action, dp )
{
	let zone = Rect.Arbitrary( action.pointStart, dp );
	zone = zone.ensureMinSize( Self.sizeMin );

	const ati = action.transientItem;
	const ancillary = ati.ancillary.create( 'zone', zone );
	const key = Uid.newUid( );
	const uidSpace = action.uidSpace;

	const trace = TraceRoot.space( uidSpace ).add( 'items', key );

	const traceV =
		action.transientItem.traceV
		.forward( 'space' )
		.add( 'items', key );

	Shell.change(
		uidSpace,
		ChangeTreeGrow.create(
			'trace', trace,
			'rank', 0,
			'val', ancillary.asFabric,
		),
	);

	Shell.alter(
		'mark',
		MarkCaret.TraceV(
			traceV
			.add( 'widgets', 'link' )
			.add( 'value' ).add( 'offset', 0 )
		),
	);
};

/*
| The item's glint.
*/
def.lazy.glint =
	function( )
{
	const zone = this.tZone;
	const list =
	[
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint', this._innerGlint,
					'size', zone.size.add( 2 ),
					'resolution', this.resolution,
				),
			'pos', zone.pos
		)
	];

	if( this.highlight )
	{
		const facet = Self._facets.getFacet( 'highlight', true );
		list.push( facet.glint( this.tOutline ) );
	}

	return ListGlint.Array( list );
};

/*
| User inputs some characters.
*/
def.proto.input =
	function( string )
{
	const widget = this.focusedWidget;
	if( widget )
	{
		widget.input( string );
	}
};

/*
| Returns the minimum x-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleX =
	function( zone )
{
	return Self.sizeMin.width / zone.width;
};

/*
| Returns the minimum y-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleY =
	function( zone )
{
	return Self.sizeMin.height / zone.height;
};

/*
| Minimum size
*/
def.lazy.sizeMin =
def.staticLazy.sizeMin =
	( ) =>
	Size.WH( 40, 40 );

/*
| The item model.
*/
def.staticLazyFunc.model =
	function( light )
{
	const ancillary =
		ASubspace.FromFabric(
			Self.fabricModel,
			TraceRoot.space( ':transient' ).add( 'items', ':transient' ),
		);

	return(
		Self.create(
			'access', 'rw',
			'ancillary', ancillary,
			'highlight', false,
			'rank', 0,
			'resolution', Resolution.Identity,
			'systemFocus', true,
			'transform', TransformNormal.singleton,
		)
	);
};

/*
| Mouse wheel turned.
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	return this.within( p );
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	return this.ancillary.outline;
};

/*
| User is hovering his/her pointing device.
| Checks if this item reacts on this.
|
| ~p: point hovered upon
| ~mode: space mode
*/
def.proto.pointingHover =
	function( p, mode )
{
	// not on the item?
	if( !this.within( p ) ) return undefined;

	const ps = p.sub( this.tZone.pos );
	const widgets = this.widgets;

	for( let w of widgets )
	{
		const bubble = w.pointingHover( ps );
		if( bubble ) return bubble;
	}

	return Hover.CursorDefault.create( 'traceV', this.traceV );
};

/*
| Subspace portals are positioned by their zone.
*/
def.static.positioning =
def.proto.positioning =
	'zone';

/*
| Subspace portals do not need to be resized proportionally.
*/
def.proto.proportional = false;

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK && traceV.forward( 'items' ) !== this.traceV ) throw new Error( );

	const atViewThis = this.traceV.forward( 'view' ).last.at;

	const ref =
		Shell.expandRefLink(
			this.traceV.forward( 'view' ).last.at,
			this.ancillary.link
		);

	switch( traceV.backward( 'widgets' ).last.key )
	{
		case 'openhere':
			Shell.openSpace( atViewThis, ref, 'addHistory' );
			break;

		case 'opensplit':
			Shell.openSpace( ( atViewThis + 1 ) % 2 , ref, 'addHistory' );
			break;

		default: throw new Error( );
	}
};

/*
| User pressed a special key.
|
| ~key: key pressed.
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	const widget = this.focusedWidget;
	if( !widget ) return;

	if( key === 'tab' )
	{
		Shell.cycleFocus( shift ? -1 : 1 );
		return;
	}

	widget.specialKey( key, shift, ctrl );
};

/*
| Returns the sysMode.
*/
def.lazy.sysMode =
	function( )
{
	const focus = this.focusedWidget;

	return(
		focus
		? focus.sysMode
		: SysMode.Blank
	);
};

/*
| Converts a fabric trace to a visual trace.
| See also traceV2F.
*/
def.proto.traceF2V =
	function( trace )
{
/**/if( CHECK && trace.forward( 'items' ) !== this.ancillary.trace ) throw new Error( );

	let traceV = this.traceV;

	let a; // continues plain convert here.
	const traceLink = trace.forward( 'link' );
	if( traceLink )
	{
		a = traceLink.length;
		traceV = traceV.add( 'widgets', 'link' ).add( 'value' );
	}
	else
	{
		a = this.ancillary.trace.length;
	}

	for( let alen = trace.length; a < alen; a++ )
	{
		const step = trace.get( a );
		traceV = traceV.addStep( step );
	}
	return traceV;
};

/*
| Converts a visual trace to a fabric trace.
| See also traceF2V.
*/
def.proto.traceV2F =
	function( traceV )
{
/**/if( CHECK && traceV.forward( 'items' ) !== this.traceV ) throw new Error( );

	let trace = this.ancillary.trace;
	const traceVWidgets = traceV.forward( 'widgets' );

	let a; // continues plain convert here.
	if( traceVWidgets )
	{
/**/	if( CHECK && traceV.get( traceVWidgets.length ).name !== 'value' ) throw new Error( );
		a = traceVWidgets.length + 1;
		switch( traceVWidgets.last.key )
		{
			case 'link':
				trace = trace.add( 'link' );
				break;

			default:
				throw new Error( );
		}
	}
	else
	{
		a = this.traceV.length;
	}

	for( let alen = traceV.length; a < alen; a++ )
	{
		const step = traceV.get( a );
		trace = trace.addStep( step );
	}

	return trace;
};

/*
| Builds the widgets.
*/
def.lazy.widgets =
	function( )
{
	return(
		TwigWidget.create(
			'twig:init',
			{
				link:      this._widgetInputLink,
				openhere:  this._widgetButtonOpenHere,
//				opensplit: this._widgetButtonOpenSplit,
			},
			[
				'link',
				'openhere',
//				'opensplit',
			],
		)
	);
};

/*
| Facets of the item.
|
| FIXME remove.
*/
def.staticLazy._facets =
	( ) =>
	FacetList.Elements(
			// default
			Facet.create(
				'fill',
					GradientRadial.Elements(
						GradientColorStop.OffsetColor( 0, Color.RGBA( 255, 255, 248, 0.955 ) ),
						GradientColorStop.OffsetColor( 1, Color.RGBA( 255, 255, 160, 0.955 ) )
					),
				'border',
					ListFacetBorder.Elements(
						FacetBorder.ColorDistanceWidth( Color.RGB( 255, 220, 128 ), -3, 6 ),
						FacetBorder.simpleBlack
					)
			),
			DesignHighlight.facet,
	);

/*
| Facets of buttons.
*/
def.staticLazy._facetsButton =
	( ) =>
	FacetList.Elements(
		// default state.
		Facet.create(
			'fill', Color.RGBA( 255, 237, 210, 0.5 ),
			'border', FacetBorder.Color( Color.RGB( 255, 141, 66 ) )
		),
		// hover
		Facet.create(
			'border', FacetBorder.ColorWidth( Color.RGB( 255, 141, 66 ), 1.5 ),
			'fill', Color.RGBA( 255, 188, 88, 0.7 ),
			'specs', GroupBoolean.Table( { hover: true } ),
		),
		// focus
		Facet.create(
			'border', FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 1.5 ),
			'fill', Color.RGBA( 255, 237, 210, 0.5 ),
			'specs', GroupBoolean.Table( { focus: true } ),
		),
		// focus and hover
		Facet.create(
			'border', FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 1.5 ),
			'fill', Color.RGBA( 255, 188, 88, 0.7 ),
			'specs', GroupBoolean.Table( { focus: true, hover: true } ),
		)
	);


/*
| Creates the subspace portal's inner glint.
*/
def.lazy._innerGlint =
	function( )
{
	const transform = this.transform.scaleOnly;
	const facet = Self._facets.getFacet( );

	const widgets = this.widgets;
	const list =
	[
		widgets.get( 'link' ).glint,
		widgets.get( 'openhere' ).glint,
//		widgets.get( 'opensplit' ).glint,
	];

	const tzo =
		Ellipse.PosSize( Point.zero, this.ancillary.zone )
		.transform( transform );

	return(
		ListGlint.Elements(
			facet.glintFill( tzo ),
			// masks the item's content
			GlintMask.create( 'glint', ListGlint.Array( list ), 'outline', tzo ),
			// puts the border on top of everything else
			facet.glintBorder( tzo )
		)
	);
};

/*
| Size of the "open here" button.
*/
def.staticLazy._sizeButtonOpenHere =
	( ) =>
	//Size.WH( 50, 20 );
	Size.WH( 100, 20 );

/*
| Size of the "open split" button.
*/
def.staticLazy._sizeButtonOpenSplit =
	( ) =>
	Size.WH( 50, 20 );

/*
| Prepares the button widget for "open here".
*/
def.lazy._widgetButtonOpenHere =
	function( )
{
	const light = this.light;
	const zoneA = this.ancillary.zone;
	const sizeOpenHere  = Self._sizeButtonOpenHere;
	//const sizeOpenSplit = Self._sizeButtonOpenSplit;
	const zoneW =
		Rect.PosSize(
			Point.XY(
				//( zoneA.width - ( sizeOpenHere.width + sizeOpenSplit.width ) ) / 2,
				( zoneA.width - sizeOpenHere.width ) / 2,
				( zoneA.height + 10 ) / 2
			),
			sizeOpenHere
		);
	const traceV = this.traceV.add( 'widgets', 'openhere' );

	let wHover = this.hover;
	if( wHover && !wHover.hasTrace( traceV ) )
	{
		wHover = undefined;
	}

	let wMark = this.mark;
	if( wMark && !wMark.encompasses( traceV ) )
	{
		wMark = undefined;
	}

	const transform = this.transform.scaleOnly;
	const hvr = ( zoneW.height - 1 ) / 3;

	return(
		WidgetButton.create(
			'facets',        Self._facetsButton,
			'fontColor',     DesignFont.NameSizeLight( 'standard', 13, light ),
			'hover',         wHover,
			'mark',          wMark,
			'resolution',    this.resolution,
			'shape',
				//'RectRound',
				RectRoundExt.PosWidthHeightHVNwsw(
					Point.zero,
					zoneW.width,
					zoneW.height,
					hvr, hvr,
				),
			'string',       'open',
			//'stringNewline', 15,
			'systemFocus',   this.systemFocus,
			'transform',     transform,
			'traceV',        traceV,
			'zone',          zoneW,
		)
	);
};

/*
| Prepares the button widget for "open split".
*/
def.lazy._widgetButtonOpenSplit =
	function( )
{
	const light = this.light;
	const zoneA = this.ancillary.zone;
	const sizeOpenHere  = Self._sizeButtonOpenHere;
	const sizeOpenSplit = Self._sizeButtonOpenSplit;
	const zoneW =
		Rect.PosSize(
			Point.XY(
				( zoneA.width - ( sizeOpenHere.width + sizeOpenSplit.width ) ) / 2 + sizeOpenHere.width,
				( zoneA.height + 10 ) / 2
			),
			sizeOpenSplit
		);
	const traceV = this.traceV.add( 'widgets', 'opensplit' );

	let wHover = this.hover;
	if( wHover && !wHover.hasTrace( traceV ) )
	{
		wHover = undefined;
	}

	let wMark = this.mark;
	if( wMark && !wMark.encompasses( traceV ) )
	{
		wMark = undefined;
	}

	const transform = this.transform.scaleOnly;
	const hvr = ( zoneW.height - 1 ) / 3;

	return(
		WidgetButton.create(
			'facets',        Self._facetsButton,
			'fontColor',     DesignFont.NameSizeLight( 'standard', 13, light ),
			'hover',         wHover,
			'mark',          wMark,
			'resolution',    this.resolution,
			'shape',
				RectRoundExt.PosWidthHeightHVNese(
					Point.zero,
					zoneW.width,
					zoneW.height,
					hvr, hvr,
				),
			'string',       'split',
			//'stringNewline', 15,
			'systemFocus',   this.systemFocus,
			'transform',     transform,
			'traceV',        traceV,
			'zone',          zoneW,
		)
	);
};

/*
| Prepares the input widget for the link value.
*/
def.lazy._widgetInputLink =
	function( )
{
	const light = this.light;
	const ancillary = this.ancillary;
	const value = ancillary.link;
	const zone = ancillary.zone;
	const fontColor = DesignFont.NameSizeLight( 'standard', 13, light );
	const width = fontColor.StringNormal( value ).advanceWidth;
	const pitch = VSpaceWidgetInput.pitch;
	const height = fontColor.size + 2 + pitch.y;

	const pos =
		Point.XY(
			( zone.width - width ) / 2 - pitch.x,
			zone.height / 2 - 10 - height
		);
	const transform = this.transform;
	const traceV = this.traceV.add( 'widgets', 'link' );

	let wHover = this.hover;
	if( wHover && !wHover.hasTrace( traceV ) )
	{
		wHover = undefined;
	}

	let wMark = this.mark;
	if( wMark && !wMark.encompasses( traceV ) )
	{
		wMark = undefined;
	}

	const facets =
		FacetList.Elements(
			// default state.
			Facet.create(
				'fill', Color.white,
				'border', FacetBorder.Color( Color.RGB( 255, 219, 165 ) )
			)
		);

	return(
		VSpaceWidgetInput.create(
			'facets',       facets,
			'fontColor',    fontColor,
			'hover',        wHover,
			'mark',         wMark,
			'readonly',     this.access !== 'rw',
			'resolution',   this.resolution,
			'traceFString', this.ancillary.trace.add( 'link' ),
			'systemFocus',  this.systemFocus,
			'transform',    transform.scaleOnly,
			'value',        ancillary.link,
			'traceV',       traceV,
			'zone',
				Rect.PosWidthHeight(
					pos,
					width  + 2 * pitch.x,
					height +     pitch.y
				),
		)
	);
};
