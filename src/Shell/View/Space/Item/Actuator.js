/*
| A subspace portal to another space.
*/
def.extend = 'Shell/View/Space/Item/Base';

def.attributes =
{
	// the ancillary fabric
	ancillary: { type: 'Shared/Ancillary/Item/Actuator' },

	// true if current user is an owner to the space
	isSpaceOwner: { type: 'boolean' },

	// currently logged in user
	userCreds: { type: 'Shared/User/Creds' },
};

import { Self as Color             } from '{gleam:Color}';
import { Self as DesignFont        } from '{Shell/Design/Font}';
import { Self as DesignHighlight   } from '{Shell/Design/Highlight}';
import { Self as Facet             } from '{Shell/Facet/Self}';
import { Self as FacetBorder       } from '{Shell/Facet/Border}';
import { Self as FacetList         } from '{Shell/Facet/List}';
import { Self as GlintPane         } from '{gleam:Glint/Pane}';
import { Self as GlintWindow       } from '{gleam:Glint/Window}';
import { Self as GroupBoolean      } from '{group@boolean}';
import { Self as Hover             } from '{Shell/Result/Hover}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as MarkItems         } from '{Shared/Mark/Items}';
import { Self as ModeForm          } from '{Shell/Mode/Form}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as Size              } from '{gleam:Size}';
import { Self as SysMode           } from '{Shell/Result/SysMode}';
import { Self as TwigWidget        } from '{twig@<Shell/Widget/Types}';
import { Self as WidgetButton      } from '{Shell/Widget/Button}';

/*
| The zone is directly affected by actions.
*/
def.proto.actionAffects = 'zone';

/*
| Sees if this item is being clicked.
|
| ~p:     point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
| ~mark:  current user mark
*/
def.proto.click =
	function( p, shift, ctrl, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	if( !this.within( p ) )
	{
		return undefined;
	}

	if( ctrl )
	{
		return this._ctrlClick( p, shift, mark );
	}

	if( !this.isSpaceOwner )
	{
		// for the admin / spaceOwner do not push the button

		const ps = p.sub( this.tZone.pos );
		for( let widget of this.widgets )
		{
			const bubble = widget.click( ps, shift, ctrl );
			if( bubble )
			{
				return bubble;
			}
		}
	}

	// if none of the widgets were clicked
	// just focus the item itself
	Shell.alter(
		'action', undefined,
		'mark',   MarkItems.TraceVs( this.traceV ),
	);

	return true;
};

/*
| Cycles the focus.
|
| ~dir: +1 clockwise, -1 anticlockwise
|
| ~return: true, the item swallowed the cycle, false cycle on parent.
*/
def.proto.cycleFocus =
	function( dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( dir !== 1 && dir !== -1 ) throw new Error( );
/**/}

	const mark = this.mark;
	if( !mark )
	{
		return false;
	}

	const traceV = mark.traceVCaret.backward( 'widgets' );
	const widgets = this.widgets;
	let rank = widgets.rankOf( traceV.last.key );
	rank += dir;

	if( rank < 0 )
	{
		rank += widgets.length;
	}
	else if( rank >= widgets.length )
	{
		rank -= widgets.length;
	}

	Shell.alter(
		'mark',
			MarkCaret.TraceV(
				this.traceV
				.add( 'widgets', widgets.getKey( rank ) )
				.add( 'value' ).add( 'offset', 0 )
			),
	);

	return true;
};

/*
| Returns the focused widget.
*/
def.lazy.focusedWidget =
	function( )
{
	const mark = this.mark;
	if( !mark )
	{
		return undefined;
	}

	const tvw = mark.traceVWidget;
	if( !tvw )
	{
		return undefined;
	}

	return this.widgets.get( tvw.last.key );
};

/*
| The item's glint.
*/
def.lazy.glint =
	function( )
{
	const name = this.ancillary.name;

	// the signup actuator is not visible for logged in users, except for admin/space owner.
	if( name === 'signup' && !this.userCreds.isVisitor && !this.isSpaceOwner )
	{
		return ListGlint.Empty;
	}

	const zone = this.tZone;
	const list =
	[
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint', this._innerGlint,
					'size', zone.size.add( 2 ),
					'resolution', this.resolution,
				),
			'pos', zone.pos
		)
	];

	if( this.highlight )
	{
		const facet = DesignHighlight.facet;
		list.push( facet.glint( this.tOutline ) );
	}

	return ListGlint.Array( list );
};

/*
| User inputs some characters.
*/
def.proto.input =
	function( string )
{
	const widget = this.focusedWidget;

	if( widget )
	{
		widget.input( string );
	}
};

/*
| Returns the minimum x-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleX =
	function( zone )
{
	return this.sizeMin.width / zone.width;
};

/*
| Returns the minimum y-scale factor this item could go through.
|
| ~zone: original zone
*/
def.proto.minScaleY =
	function( zone )
{
	return this.sizeMin.height / zone.height;
};

/*
| Minimum size
*/
def.lazy.sizeMin =
def.staticLazy.sizeMin =
	( ) =>
	Size.WH( 40, 40 );

/*
| Mouse wheel turned.
*/
def.proto.mousewheel =
	function( p, dir, shift, ctrl )
{
	return this.within( p );
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	return this.ancillary.outline;
};

/*
| User is hovering his/her pointing device.
| Checks if this item reacts on this.
|
| ~p: point hovered upon
| ~mode: space mode
*/
def.proto.pointingHover =
	function( p, mode )
{
	// not on the item?
	if( !this.within( p ) )
	{
		return undefined;
	}

	const ps = p.sub( this.tZone.pos );
	const widgets = this.widgets;

	for( let w of widgets )
	{
		const bubble = w.pointingHover( ps );
		if( bubble )
		{
			return bubble;
		}
	}

	return Hover.CursorDefault.create( 'traceV', this.traceV );
};

/*
| This item is positioned by it's zone.
*/
def.static.positioning =
def.proto.positioning =
	'zone';

/*
| This item does not need to be resized proportionally.
*/
def.proto.proportional = false;

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK && traceV.forward( 'items' ) !== this.traceV ) throw new Error( );

	const name = this.ancillary.name;

	// for space owner / admin dont do actuators.
	if( this.isSpaceOwner )
	{
		return;
	}

	switch( name )
	{
		case 'login':
			if( this.userCreds.isVisitor )
			{
				Shell.alter( 'mode', ModeForm.login );
			}
			else
			{
				Shell.logout( );
			}
			break;

		case 'signup':
			Shell.alter( 'mode', ModeForm.signUp );
			break;

		default: throw new Error( );
	}
};

/*
| User pressed a special key.
|
| ~key: key pressed.
| ~shift: true if shift key was held down
| ~ctrl:  true if ctrl or meta key was held down
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	const widget = this.focusedWidget;
	if( !widget )
	{
		return;
	}

	if( key === 'tab' )
	{
		Shell.cycleFocus( shift ? -1 : 1 );
		return;
	}

	widget.specialKey( key, shift, ctrl );
};

/*
| Returns the sysMode.
*/
def.lazy.sysMode =
	function( )
{
	const focus = this.focusedWidget;

	return(
		focus
		? focus.sysMode
		: SysMode.Blank
	);
};

/*
| Converts a fabric trace to a visual trace.
| See also traceV2F.
*/
def.proto.traceF2V =
	function( trace )
{
/**/if( CHECK && trace.forward( 'items' ) !== this.ancillary.trace ) throw new Error( );

	let traceV = this.traceV;

	let a; // continues plain convert here.
	const traceLink = trace.forward( 'link' );
	if( traceLink )
	{
		a = traceLink.length;
		traceV = traceV.add( 'widgets', 'link' ).add( 'value' );
	}
	else
	{
		a = this.ancillary.trace.length;
	}

	for( let alen = trace.length; a < alen; a++ )
	{
		const step = trace.get( a );
		traceV = traceV.addStep( step );
	}
	return traceV;
};

/*
| Converts a visual trace to a fabric trace.
| See also traceF2V.
*/
def.proto.traceV2F =
	function( traceV )
{
/**/if( CHECK && traceV.forward( 'items' ) !== this.traceV ) throw new Error( );

	let trace = this.ancillary.trace;
	const traceVWidgets = traceV.forward( 'widgets' );

	let a; // continues plain convert here.
	if( traceVWidgets )
	{
/**/	if( CHECK && traceV.get( traceVWidgets.length ).name !== 'value' ) throw new Error( );
		a = traceVWidgets.length + 1;
		switch( traceVWidgets.last.key )
		{
			case 'link':
				trace = trace.add( 'link' );
				break;

			default:
				throw new Error( );
		}
	}
	else
	{
		a = this.traceV.length;
	}

	for( let alen = traceV.length; a < alen; a++ )
	{
		const step = traceV.get( a );
		trace = trace.addStep( step );
	}

	return trace;
};

/*
| Builds the widgets.
*/
def.lazy.widgets =
	function( )
{
	return(
		TwigWidget.create(
			'twig:init',
			{ button:  this._widgetButton },
			[ 'button' ],
		)
	);
};

/*
| Facets of the actuator button.
*/
def.staticLazy._facetsButton =
	( ) =>
	FacetList.Elements(
		// default state.
		Facet.create(
			'fill', Color.RGBA( 255, 237, 210, 0.5 ),
			'border', FacetBorder.Color( Color.RGB( 255, 141, 66 ) )
		),
		// hover
		Facet.create(
			'border', FacetBorder.ColorWidth( Color.RGB( 255, 141, 66 ), 1.5 ),
			'fill', Color.RGBA( 255, 188, 88, 0.7 ),
			'specs', GroupBoolean.Table( { hover: true } ),
		),
		// focus
		Facet.create(
			'border', FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 1.5 ),
			'fill', Color.RGBA( 255, 237, 210, 0.5 ),
			'specs', GroupBoolean.Table( { focus: true } ),
		),
		// focus and hover
		Facet.create(
			'border', FacetBorder.ColorDistanceWidth( Color.RGB( 255, 99, 188 ), -1, 1.5 ),
			'fill', Color.RGBA( 255, 188, 88, 0.7 ),
			'specs', GroupBoolean.Table( { focus: true, hover: true } ),
		)
	),

/*
| Returns the inner glint.
*/
def.lazy._innerGlint =
	function( )
{
	const list =
	[
		this.widgets.get( 'button' ).glint,
	];

	return ListGlint.Array( list );
};

/*
| Prepares the button widget for "open here".
*/
def.lazy._widgetButton =
	function( )
{
	const zoneA = this.ancillary.zone;
	const zoneW =
		Rect.PosWidthHeight(
			Point.XY( 1, 1 ),
			zoneA.width - 2,
			zoneA.height - 2,
		);

	const traceV = this.traceV.add( 'widgets', 'button' );

	let wHover = this.hover;
	if( wHover && !wHover.hasTrace( traceV ) )
	{
		wHover = undefined;
	}

	let wMark = this.mark;
	if( wMark && !wMark.encompasses( traceV ) )
	{
		wMark = undefined;
	}

	const name = this.ancillary.name;
	let string;

	switch( name )
	{
		case 'login':
		{
			if( this.isSpaceOwner )
			{
				string = '[Log in]';
			}
			else
			{
				string =
					this.userCreds.isVisitor || this.isSpaceOwner
					? 'Log in'
					: 'Log out';
			}
			break;
		}

		case 'signup':
		{
			string =
				this.isSpaceOwner
				? '[Sign up]'
				: 'Sign up';
			break;
		}

		default: throw new Error( );
	}

	return(
		WidgetButton.create(
			'facets',        Self._facetsButton,
			'fontColor',     DesignFont.Size( 13 ),
			'hover',         wHover,
			'mark',          wMark,
			'resolution',    this.resolution,
			'shape',        'RectRound',
			'string',        string,
			'systemFocus',   this.systemFocus,
			'transform',     this.transform.scaleOnly,
			'traceV',        traceV,
			'zone',          zoneW,
		)
	);
};
