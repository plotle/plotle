/*
| DocView of a label.
*/
def.extend = 'Shell/View/Doc/Item/TextItem';

def.attributes =
{
	// the item's ancillary fabric
	ancillary: { type: [ 'undefined', 'Shared/Ancillary/Item/Label' ] },

	// point in north west
	pos: { type: [ 'undefined', 'gleam:Point' ] },
};

/*
| FontSize of the doc view item.
*/
def.lazy.fontSize =
	function( )
{
	return Math.round( this.resolution.ratio * 18 );
};
