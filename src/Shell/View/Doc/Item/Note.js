/*
| DocView of a note.
*/
def.extend = 'Shell/View/Doc/Item/TextItem';

def.attributes =
{
	// the item's ancillary fabric
	ancillary: { type: [ 'undefined', 'Shared/Ancillary/Item/Note' ] },
};

/*
| FontSize of the doc view item.
*/
def.lazy.fontSize =
	function( )
{
	return Math.round( 13 * this.resolution.ratio );
};

