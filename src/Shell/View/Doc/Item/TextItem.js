/*
| A doc view item with text.
*/
def.abstract = true;

def.extend = 'Shell/View/Doc/Item/Base';

def.attributes =
{
	// margins of text
	innerMargin: { type: 'gleam:Margin' },

	// point in north west
	pos: { type: 'gleam:Point' },

	// the width to be used.
	width: { type: 'number' },
};

import { Self as ActionSelectRange } from '{Shell/Action/SelectRange}';
import { Self as ALabel            } from '{Shared/Ancillary/Item/Label}';
import { Self as ANote             } from '{Shared/Ancillary/Item/Note}';
import { Self as Color             } from '{gleam:Color}';
import { Self as Dashed            } from '{gleam:Dashed}';
import { Self as DocViewPara       } from '{Shell/View/Doc/Para}';
import { Self as FontRoot          } from '{gleam:Font/Root}';
import { Self as GlintFigure       } from '{gleam:Glint/Figure}';
import { Self as ListDocViewPara   } from '{list@Shell/View/Doc/Para}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as MarkRange         } from '{Shared/Mark/Range}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as Segment           } from '{gleam:Segment}';
import { Self as Size              } from '{gleam:Size}';

/*
| Checks if the item is being clicked and reacts.
|
| ~p:     point where dragging starts.
| ~shift: true if shift key was held down.
| ~ctrl:  true if ctrl or meta key was held down.
| ~mark:  current users mark.
*/
def.proto.click =
	function( p, shift, ctrl, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	if( !this.within( p ) ) return undefined;

	if( this.access !== 'rw' ) return false;

	mark = this.markForPoint( p, shift ? mark : undefined );
	Shell.alter( 'mark', mark );

	return true;
};

/*
| Handles a potential dragStart event.
|
| ~p: point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl: true if ctrl or meta key was held down
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	if( !this.within( p ) )
	{
		return undefined;
	}

	// TODO actually selecting in readonly mode should work as well.
	if( this.access !== 'rw' )
	{
		return undefined;
	}

	let mark = this.markForPoint( p );
	Shell.alter(
		'action', ActionSelectRange.create( 'traceV', mark.traceV ),
		'mark', mark
	);

	return true;
};

/*
| Returns the paragraph at point.
*/
def.proto.getParaAtPoint =
	function( p )
{
	const paras = this.paras;
	for( let para of paras )
	{
		if( p.y < para.pos.y + para.flow.height ) return para;
	}
	return undefined;
};

/*
| The item's glint.
*/
def.lazy.glint =
	function( )
{
	const glints = [ ];
	const hv = this.viewClip.height;

	const paras = this.paras;
	for( let para of paras )
	{
		const yp = para.pos.y;

		if( yp + para.height <= 0 ) continue;
		if( yp > hv ) continue;

		glints.push( para.glint );
	}

	switch( this.typeNext )
	{
		case ALabel:
		case ANote:
		{
			const ysep = this.pos.y + this.sizeFull.height + this.paraSep;

			glints.push(
				GlintFigure.create(
					'figure',
						Segment.P0P1(
							Point.XY( 0, ysep ),
							Point.XY( this.viewClip.width, ysep ),
						),
					'color', Color.black,
					'dashed', Dashed.Elements( 1, 4 ),
					'width', 1,
				),
			);
		}
	}

	return ListGlint.Array( glints );
};

/*
| The user inputs some characters.
|
| ~string: string inputed
| ~mark:  current users mark.
*/
def.proto.input =
	function( dvRoot, string, mark )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const traceVCaret = mark.traceVCaret;
	if( !traceVCaret ) return;

	if( mark.ti2ctype === MarkRange )
	{
		dvRoot.removeRange( mark );
		// TODO this is akward but currently needed to transform the mark
		Shell.input( string );
		return;
	}

	const para = this.paras.get( mark.traceVCaret.backward( 'paras' ).last.at );
	para.input( dvRoot, this, string, mark );
};

/*
| Returns the mark for a point
|
| ~p:         the point to mark to
| ~rangeMark: if defined creates a range beginning with this mark.
*/
def.proto.markForPoint =
	function( p, rangeMark )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length < 1 || arguments.length > 2 ) throw new Error( );
/**/	if( p.ti2ctype !== Point ) throw new Error( );
/**/	if( rangeMark && rangeMark.ti2ctype !== MarkCaret && rangeMark.ti2ctype !== MarkRange )
/**/	{
/**/		throw new Error( );
/**/	}
/**/}

	let para = this.getParaAtPoint( p );
	let at;
	if( para )
	{
		at = para.getPointOffset( p.sub( para.pos ) );
	}
	else
	{
		// puts the mark at the end of text.
		para = this.paras.last;
		at = para.ancillary.string.length;
	}

	if( rangeMark )
	{
		return rangeMark.rangeMarkEnding( para.offsetTraceV( at ) );
	}
	else
	{
		return MarkCaret.TraceV( para.offsetTraceV( at ) );
	}
};

/*
| Builds the docView paras.
*/
def.lazy.paras =
	function( )
{
	const ancillary = this.ancillary;
	const imw = this.innerMargin.w;
	const list = [ ];
	const paras = ancillary.text.paras;
	const width = this.width;

	let y;
	let prev;
	const traceV = this.traceV;
	y = this.pos.y; // - this.scrollPos.y;

	for( let r = 0, rlen = paras.length; r < rlen; r++ )
	{
		const para = paras.get( r );

		if( prev )
		{
			y = prev.pos.y + prev.flow.height + this.paraSep;
		}

		prev =
			DocViewPara.create(
				'ancillary',  para,
				'fontSize',   this.fontSize,
				'pos',        Point.XY( imw, y ),
				'resolution', this.resolution,
				'traceV',     traceV.add( 'paras', r ),
				'widthFlow',  width,
			);

		list.push( prev );
	}

	return ListDocViewPara.Array( list );
};

/*
| Full size of the text.
*/
def.lazy.sizeFull =
	function( )
{
	let height = 0;
	let width = 0;
	const fs = this.fontSize;
	const paraSep = this.paraSep;

	let first = true;
	for( let para of this.paras )
	{
		const flow = para.flow;
		width = Math.max( width, flow.width );
		if( !first )
		{
			height += paraSep;
		}
		else
		{
			first = false;
		}
		height += flow.height;
	}

	height += Math.round( fs * FontRoot.bottomBox );
	return Size.WH( width, height );
};


/*
| Handles a special key.
*/
def.proto.specialKey =
	function( dvRoot, key, mark, shift, ctrl, inputWord )
{
/**/if( CHECK && arguments.length !== 6 ) throw new Error( );

	if( !mark?.hasCaret ) return;

	if( mark.ti2ctype === MarkRange )
	{
		switch( key )
		{
			case 'backspace':
			case 'del':
			{
				dvRoot.removeRange( mark );
				return;
			}

			case 'enter':
			{
				dvRoot.removeRange( mark );
				// TODO this is awkward but currently needed to transform the mark
				Shell.specialKey( key, shift, ctrl, inputWord );
				return;
			}
		}
	}

	const para = this.paras.get( mark.traceVCaret.backward( 'paras' ).last.at );
	para.specialKey( dvRoot, this, key, mark, shift, ctrl, inputWord );
};

/*
| Returns the system mode.
*/
def.lazyFunc.sysMode =
	function( mark )
{
	const at = mark.traceVCaret.backward( 'paras' ).last.at;
	return this.paras.get( at ).sysMode( mark );
};

/*
| Returns true if point 'p' is within the item.
*/
def.proto.within =
	function( p )
{
	const py = p.y;
	const posy = this.pos.y;
	return py >= posy && py <= posy + this.sizeFull.height + this.paraSep;
};

