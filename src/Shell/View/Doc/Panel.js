/*
| The docview panel.
*/
def.attributes =
{
	// users access to current space
	access: { type: 'string' },

	// the fabric of the docPath
	ancillaryDocPath: { type: 'Shared/Ancillary/Item/DocPath/Self' },

	// the thing hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// transform
	transform: { type: '<gleam:Transform/Types' },

	// current view clipping
	viewClip: { type: 'gleam:Rect' },

	// traceV of the panel
	traceV: { type: 'ti2c:Trace' },

	// the windows the docView is shown in
	window: { type: 'Shell/Window/DocView' },
};

import { Self as Color             } from '{gleam:Color}';
import { Self as Facet             } from '{Shell/Facet/Self}';
import { Self as FacetList         } from '{Shell/Facet/List}';
import { Self as FacetBorder       } from '{Shell/Facet/Border}';
import { Self as DesignButton      } from '{Shell/Design/Widget/Button}';
import { Self as DesignFont        } from '{Shell/Design/Font}';
import { Self as DesignIcon        } from '{Shell/Design/Icon/Self}';
import { Self as DesignInput       } from '{Shell/Design/Widget/Input}';
import { Self as GlintFigure       } from '{gleam:Glint/Figure}';
import { Self as GlintPane         } from '{gleam:Glint/Pane}';
import { Self as GlintWindow       } from '{gleam:Glint/Window}';
import { Self as GroupBoolean      } from '{group@boolean}';
import { Self as Hover             } from '{Shell/Result/Hover}';
import { Self as IconXSymbol       } from '{Shell/Design/Icon/XSymbol}';
import { Self as ListGlint         } from '{list@<gleam:Glint/Types}';
import { Self as MarkCaret         } from '{Shared/Mark/Caret}';
import { Self as MarkWidget        } from '{Shared/Mark/Widget}';
import { Self as Point             } from '{gleam:Point}';
import { Self as Rect              } from '{gleam:Rect}';
import { Self as Shell             } from '{Shell/Self}';
import { Self as Size              } from '{gleam:Size}';
import { Self as Segment           } from '{gleam:Segment}';
import { Self as SysMode           } from '{Shell/Result/SysMode}';
import { Self as TransformNormal   } from '{gleam:Transform/Normal}';
import { Self as TwigDesignWidget  } from '{twig@<Shell/Design/Widget/Types}';
import { Self as TwigWidget        } from '{twig@<Shell/Widget/Types}';
import { Self as VSpaceWidgetInput } from '{Shell/View/Space/Widget/Input}';
import { Self as WidgetBase        } from '{Shell/Widget/Base}';

/*
| Checks if the user clicked something on the panel.
|
| ~p:           cursor point
| ~shift:       true if shift key was pressed
| ~ctrl:        true if ctrl key was pressed
*/
def.proto.click =
	function( p, shift, ctrl )
{
	const tZone = this.tZone;

	// shortcut if p is not near the panel
	if( !tZone.within( p ) ) return undefined;

	const pp = p.sub( tZone.pos );

	// this is on the panel
	for( let widget of this.widgets )
	{
		const bubble = widget.click( pp, shift, ctrl );
		if( bubble ) return bubble;
	}

	return false;
};

/*
| Cycles the focus.
*/
def.proto.cycleFocus =
	function( dir )
{
	const widgets = this.widgets;
	const length = widgets.length;
	const traceV = this.mark.traceVWidget;

	if( !traceV )
	{
		return;
	}

	let rank = widgets.rankOf( traceV.last.key );
	const rs = rank;

	for(;;)
	{
		rank = ( rank + dir + length ) % length;
		if( rank === rs )
		{
			break;
		}
		const ve = widgets.atRank( rank );

		if( ve.focusable && ve.visible !== false )
		{
			Shell.alter(
				'mark',
					ve.caretable
					? MarkCaret.TraceV( ve.traceV.add( 'value' ).add( 'offset', 0 ) )
					: MarkWidget.create( 'traceV', ve.traceV )
			);
			break;
		}
	}
};

/*
| Designed size.
*/
def.staticLazy.designSize =
	( ) =>
	Size.WH( 600, 40 );

/*
| Returns the focused widget.
*/
def.lazy.focusedWidget =
	function( )
{
	const mark = this.mark;

	return(
		mark
		? this.widgets.get( mark.traceVWidget.last.key )
		: undefined
	);
};

/*
| The panel's glint.
*/
def.lazy.glint =
	function( )
{
	const resolution = this.resolution;

	const list =
	[
		GlintFigure.FigureFill(
			this.tZone,
			Color.RGBA( 224, 210, 255, 0.955 ),
		),
	];

	for( let widget of this.widgets )
	{
		const g = widget.glint;
		if( g )
		{
			list.push( g );
		}
	}

	const zone = this.tZone;
	const size = zone.size;

	list.push(
		GlintFigure.FigureColorWidth(
			Segment.P0P1(
				Point.XY( 0, zone.height ),
				Point.XY( zone.width + 1, zone.height )
			),
			Color.black,
			1
		)
	);

	return(
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'glint', ListGlint.Array( list ),
					'resolution', resolution,
					'size', size.create( 'height', size.height + 1 ),
				),
			'pos', zone.pos
		)
	);
};

/*
| User is inputing characters.
*/
def.proto.input =
	function( string )
{
	const widget = this.focusedWidget;
	if( widget )
	{
		widget.input( string );
	}
};

/*
| Returns true if point is on the panel.
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const tZone = this.tZone;

	// shortcut if p is not near the panel
	if( !tZone.within( p ) ) return undefined;

	const pp = p.sub( tZone.pos );
	if( !this.tOutline.within( pp ) ) return undefined;

	// it's on the panel
	for( let widget of this.widgets )
	{
		const bubble = widget.pointingHover( pp, shift, ctrl );
		if( bubble ) return bubble;
	}

	return Hover.CursorDefault;
};

/*
| A button of the panel has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
	const buttonName = traceV.backward( 'widgets' ).last.key;
	switch( buttonName )
	{
		case 'buttonClose':
		{
			Shell.hideDocView( );
			break;
		}

		case 'buttonExport':
		{
			Shell.exportDocPath( );
			break;
		}

		default: throw new Error( );
	}
};

/*
| User is pressing a special key.
*/
def.proto.specialKey =
	function( key, shift, ctrl )
{
	const widget = this.focusedWidget;
	if( !widget ) return;

	if( key === 'tab' )
	{
		this.cycleFocus( shift ? -1 : 1 );
		return;
	}

	widget.specialKey( key, shift, ctrl );
};

/*
| Returns the sysMode.
*/
def.lazy.sysMode =
	function( )
{
	const focus = this.focusedWidget;
	return(
		focus
		? focus.sysMode
		: SysMode.Blank
	);
};

/*
| The panel's transformed outline.
*/
def.lazy.tOutline =
	function( )
{
	return this.tZone;
};

/*
| The panel's transformed zone.
*/
def.lazy.tZone =
	function( )
{
	return(
		Rect.PosWidthHeight(
			Point.zero,
			this.viewClip.width + 1,
			this._cTransform.d( Self.designSize.height ),
		)
	);
};

/*
| Builds the widgets.
*/
def.lazy.widgets =
	function( )
{
	const light       = this.light;
	const designSize  = Self.designSize;
	const widgetsD    = Self._designWidgets( light );
	const resolution  = this.resolution;
	const systemFocus = this.systemFocus;
	const viewClip    = this.viewClip;
	const traceV      = this.traceV;
	const hover       = this.hover;
	const window      = this.window;

	const cTransform = this._cTransform;

	let twig = { };
	for( let key of widgetsD.keys )
	{
		const wTrace = traceV.add( 'widgets', key );

		let wHover = hover;
		if( wHover && !wHover.hasTrace( wTrace ) ) wHover = undefined;


		switch( key )
		{
			case 'buttonClose':
			{
				let widget =
					WidgetBase.FromDesign(
						widgetsD.get( key ),
						light, wTrace, resolution, systemFocus, cTransform,
					);

				let zone = widget.zone;
				const ratio = this.resolution.ratio;

				if( cTransform === this._rTransform )
				{
					// moves the close button the right side
					let pos = zone.pos;
					pos =
						pos.create(
							'x', viewClip.width / ratio - ( designSize.width - pos.x )
						);
					zone = zone.create( 'pos', pos );
				}

				twig[ key ] =
					widget.create(
						'down', false,
						'hover', wHover,
						'zone', zone,
					);
				break;
			}

			case 'buttonCollapse':
			case 'buttonExpand':
			case 'buttonExport':
			{
				let widget =
					WidgetBase.FromDesign(
						widgetsD.get( key ),
						light, wTrace, resolution, systemFocus, cTransform,
					);

				twig[ key ] =
					widget.create(
						'down', false,
						'hover', wHover,
					);
				break;
			}

			case 'name':
			{
				const design = widgetsD.get( key );

				let wMark = this.mark;
				if( wMark && !wMark.encompasses( traceV ) ) wMark = undefined;

				twig[ key ] =
					VSpaceWidgetInput.create(
						'facets',       design.facets,
						'fontColor',    design.font,
						'hover',        wHover,
						'mark',         wMark,
						'readonly',     this.access !== 'rw',
						'resolution',   this.resolution,
						'traceFString', window.traceBase.add( 'name' ),
						'systemFocus',  systemFocus,
						'transform',    cTransform,
						'value',        this.ancillaryDocPath.name,
						'traceV',       wTrace,
						'zone',         design.zone,
					);
				break;
			}

			default: throw new Error( );
		}
	}

	return TwigWidget.create( 'twig:init', twig, widgetsD.keys );
};

/*
| Corrected transform.
*/
def.lazy._cTransform =
	function( )
{
	const rTransform = this._rTransform;
	const rScale = rTransform.scale;
	const viewClip = this.viewClip;

	if( viewClip )
	{
		let scale =
			viewClip.width
			/ ( Self.designSize.width * this.transform.scale );

		if( scale > rScale )
		{
			scale = rScale;
		}

		return TransformNormal.setScale( scale );
	}
	else
	{
		return rTransform;
	}
};

/*
| Designed widgets.
*/
def.staticLazyFunc._designWidgets =
	( light ) =>
{
	const pn = Point.XY( 300, 0 ); // x has to be half of designed size.
	const colorBorderDarker = Color.RGB( 108, 93, 151 );

	const facetsName =
		FacetList.Elements(
			// default state.
			Facet.create(
				'fill', Color.white,
				'border', FacetBorder.Color( Color.RGB( 137, 123, 176 ) ),
			),
			// hover
			Facet.create(
				'border', FacetBorder.Color( Color.RGB( 137, 123, 176 ) ),
				'fill', Color.lightGray,
				'specs', GroupBoolean.Table( { hover: true } ),
			),
			// focus
			Facet.create(
				'border', FacetBorder.ColorDistanceWidth( Color.RGB( 108, 93, 151 ), 0, 2 ),
				'fill', Color.white,
				'specs', GroupBoolean.Table( { focus: true } ),
			),
			// focus and hover
			Facet.create(
				'border', FacetBorder.ColorDistanceWidth( Color.RGB( 108, 93, 151 ), 0, 2 ),
				'fill', Color.lightGray,
				'specs', GroupBoolean.Table( { focus: true, hover: true } ),
			)
		);

	const facetsButton =
		FacetList.Elements(
			// default state.
			Facet.create(
				'border',
					FacetBorder.Color( Color.RGB( 137, 123, 176 ) ),
				'fill',
					Color.RGBA( 255, 242, 255, 0.7 ),
				'specs', GroupBoolean.Empty,
			),
			// hover
			Facet.create(
				'border', FacetBorder.ColorWidth( Color.RGB( 137, 123, 176 ), 1.5 ),
				'fill', Color.RGBA( 188, 88, 255, 0.7 ),
				'specs', GroupBoolean.Table( { hover: true } ),
			),
			// focus
			Facet.create(
				'border', FacetBorder.ColorDistanceWidth( colorBorderDarker, -1, 1.5 ),
				'fill', Color.RGBA( 237, 210, 255, 0.5 ),
					'specs', GroupBoolean.Table( { focus: true } ),
			),
			// focus and hover
			Facet.create(
				'border', FacetBorder.ColorDistanceWidth( colorBorderDarker, -1, 1.5 ),
				'fill', Color.RGBA( 188, 88, 255, 0.7 ),
				'specs', GroupBoolean.Table( { focus: true, hover: true } ),
			)
		);

	return(
		TwigDesignWidget.Grow(
			'name',
			DesignInput.create(
				'facets', facetsName,
				'font', DesignFont.Size( 13 ),
				'zone',
					Rect.PosSize(
						Point.XY( 9, 9 ),
						Size.WH( 145, 23 )
					)
			),
			'buttonExport',
			DesignButton.create(
				'facets', facetsButton,
				'font', DesignFont.Size( 13 ),
				'shape', 'RectRound',
				'string', 'export .docx',
				'zone',
					Rect.PosSize(
						Point.XY( 162, 9 ),
						Size.WH( 100, 23 )
					)
			),
			'buttonClose',
			DesignButton.create(
				'facets', facetsButton,
				'icon',
					DesignIcon.create(
						'facet', Facet.create( 'fill', Color.black ),
						'shape',
							IconXSymbol.design( light )
							.shape
							.transform( TransformNormal.setScale( 0.75 ) ),
					),
				'iconNoGrid', true,
				'shape', 'RectRound',
				'zone',
					Rect.PosSize(
						pn.add( 250, 9 ),
						Size.WH( 27, 23 )
					)
			),
		)
	);
};

/*
| Ratio based transform.
*/
def.lazy._rTransform =
	function( )
{
	const ratio = this.resolution.ratio;
	return this.transform.scaleBy( ratio );
};
