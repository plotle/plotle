/*
| Root of a DocView.
*/
def.attributes =
{
	// users access to current space
	access: { type: 'string' },

	// the space ancillary fabric
	ancillary: { type: [ 'undefined', 'Shared/Ancillary/Space' ] },

	// current action
	action: { type: [ 'undefined', '<Shell/Action/Types' ] },

	// the widget hovered upon
	hover: { type: [ 'undefined', 'ti2c:Trace' ] },

	// true if light color scheme
	light: { type: 'boolean' },

	// the users mark
	mark: { type: [ 'undefined', '<Shared/Mark/Types'] },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// docView seam
	seam: { type: 'Shell/Seam/DocView/Root' },

	// have system focus (showing caret)
	systemFocus: { type: 'boolean' },

	// the visual trace of the doc view
	traceV: { type: 'ti2c:Trace' },

	// the clipped rect of the current split screen.
	viewClip: { type: 'gleam:Rect' },

	// the window the docView is shown in.
	window: { type: 'Shell/Window/DocView' },
};

import { Self as ALabel              } from '{Shared/Ancillary/Item/Label}';
import { Self as ANote               } from '{Shared/Ancillary/Item/Note}';
import { Self as ASubspace           } from '{Shared/Ancillary/Item/Subspace}';
import { Self as ActionScrolly       } from '{Shell/Action/Scrolly}';
import { Self as ActionSelectRange   } from '{Shell/Action/SelectRange}';
import { Self as ChangeListMove      } from '{ot:Change/List/Move}';
import { Self as ChangeStringMove    } from '{ot:Change/String/Move}';
import { Self as ChangeTreeShrink    } from '{ot:Change/Tree/Shrink}';
import { Self as Color               } from '{gleam:Color}';
import { Self as DesignScrollbar     } from '{Shell/Design/Scrollbar}';
import { Self as DesignSelect        } from '{Shell/Design/Select}';
import { Self as DocViewItemLabel    } from '{Shell/View/Doc/Item/Label}';
import { Self as DocViewItemNote     } from '{Shell/View/Doc/Item/Note}';
import { Self as DocViewItemSubspace } from '{Shell/View/Doc/Item/Subspace}';
import { Self as DocViewPanel        } from '{Shell/View/Doc/Panel}';
import { Self as EMath               } from '{Shared/Math/Self}';
import { Self as FabricPara          } from '{Shared/Fabric/Text/Para}';
import { Self as Facet               } from '{Shell/Facet/Self}';
import { Self as FigureList          } from '{gleam:Figure/List}';
import { Self as FontRoot            } from '{gleam:Font/Root}';
import { Self as GlintPane           } from '{gleam:Glint/Pane}';
import { Self as GlintWindow         } from '{gleam:Glint/Window}';
import { Self as Hover               } from '{Shell/Result/Hover}';
import { Self as ListDocViewItem     } from '{list@<Shell/View/Doc/Item/Types}';
import { Self as ListGlint           } from '{list@<gleam:Glint/Types}';
import { Self as Margin              } from '{gleam:Margin}';
import { Self as MarkCaret           } from '{Shared/Mark/Caret}';
import { Self as MarkRange           } from '{Shared/Mark/Range}';
import { Self as Path                } from '{gleam:Path}';
import { Self as Point               } from '{gleam:Point}';
import { Self as ReUndo              } from '{Shell/ReUndo/Stack}';
import { Self as Shell               } from '{Shell/Self}';
import { Self as SeamDocViewRoot     } from '{Shell/Seam/DocView/Root}';
import { Self as Segment             } from '{gleam:Segment}';
import { Self as Sequence            } from '{ot:Change/Sequence}';
import { Self as Size                } from '{gleam:Size}';
import { Self as SysMode             } from '{Shell/Result/SysMode}';
import { Self as Trace               } from '{ti2c:Trace}';
import { Self as TransformNormal     } from '{gleam:Transform/Normal}';
import { Self as WidgetScrollbar     } from '{Shell/Widget/Scrollbar}';

const paraSep = 13;

/*
| A click.
|
| ~p:     point where dragging starts.
| ~shift: true if shift key was held down.
| ~ctrl:  true if ctrl or meta key was held down.
*/
def.proto.click =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const panel = this._panel;
	if( panel.click( p, shift, ctrl ) )
	{
		return true;
	}

	// clicked some item?
	const mark = this.mark;
	for( let item of this.items )
	{
		if( item.click( p, shift, ctrl, mark ) )
		{
			return true;
		}
	}

	// otherwise ...
	Shell.alter( 'mark', undefined );
	return true;
};

/*
| The content the mark puts into the clipboard.
*/
def.lazy.clipboard =
	function( )
{
	const mark = this.mark;

	return(
		mark.ti2ctype === MarkRange
		? this.rangeContent( mark )
		: ''
	);
};

/*
| Drag moves.
|
| ~p:      point, viewbased point of stop
| ~shift:  true if shift key was pressed
| ~ctrl:   true if ctrl key was pressed
*/
def.proto.dragMove =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const action = this.action;
	switch( action.ti2ctype )
	{
		case ActionScrolly:
		{
			const traceV = action.traceV;

/**/		if( CHECK && traceV.forward( 'docView' ) !== this.traceV ) throw new Error( );

			const sbary = this.scrollbarY;
			const dy = p.y - action.pointStart.y;

			const maxY = this._sizeFull.height - this.innerHeight;

			const sy =
				EMath.limit(
					0,
					action.startPos + sbary.scale( dy ),
					maxY
				);

			Shell.alter(
				SeamDocViewRoot.traceSeam.add( 'scrollPos' ),
				this.seam.scrollPos.create( 'y', sy )
			);

			break;
		}

		case ActionSelectRange:
		{
			for( let item of this.items )
			{
				if( item.within( p ) && item.ti2ctype !== DocViewItemSubspace )
				{
					const mark = item.markForPoint( p, this.mark );
					Shell.alter( 'mark', mark );
					break;
				}
			}
		}
	}
};

/*
| Handles a potential dragStart event.
|
| ~p: point where dragging starts
| ~shift: true if shift key was held down
| ~ctrl: true if ctrl or meta key was held down
*/
def.proto.dragStart =
	function( p, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	// checks if it his the scrollbar
	const action = this.action;
	const sbary = this.scrollbarY;
	if( !action && sbary )
	{
		const bubble = sbary.dragStart( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	// clicked some item?
	for( let item of this.items )
	{
		if( item.dragStart( p, shift, ctrl ) ) return true;
	}

	return undefined;
};

/*
| Stops an operation with the poiting device button held down.
*/
def.proto.dragStop =
	function( p, shift, ctrl )
{
	Shell.alter( 'action', undefined );
};

/*
| Exports current docPath as docx.
*/
def.proto.exportDocPath =
	async function( )
{
	const xParas = [ ];
	const items = this.items;

	for( let item of items )
	{
		const ancillary = item.ancillary;
		const text = ancillary.text;
		const paras = text.paras;
		const isHeading = ancillary.ti2ctype === ALabel;

		for( let para of paras )
		{
			if( isHeading )
			{
				xParas.push(
					new docx.Paragraph({
						heading: docx.HeadingLevel.HEADING_1,
						children:
						[
							new docx.TextRun( { text: para.string } ),
						],
					}),
				);
			}
			else
			{
				xParas.push(
					new docx.Paragraph({
						children:
						[
							new docx.TextRun( { text: para.string } ),
						],
						style: 'normalPara',
					}),
				);
			}
		}
	}

	const sections = [ { children: xParas } ];
	const doc =
		new docx.Document( {
			sections: sections,
			styles: Self._docxStyles
		} );

	// packing
	const blob = await docx.Packer.toBlob( doc );
	const a = document.createElement( 'a' );
	const url = URL.createObjectURL( blob );
	a.href = url;
	let name = this._ancillaryDocpath.name;
	name = name.trim( );
	if( name === '' )
	{
		name = 'my doc';
	}
	a.download = name + '.docx';
	a.click( );
	URL.revokeObjectURL( url );
};

/*
| Returns the item at point.
*/
def.proto.getItemAtPoint =
	function( p )
{
	const items = this.items;
	for( let item of items )
	{
		if( p.y < item.pos.y + item.sizeFull.height )
		{
			return item;
		}
	}

	return undefined;
};

/*
| Return the doc view glint.
*/
def.lazy.glint =
	function( )
{
	return(
		GlintWindow.create(
			'pane',
				GlintPane.create(
					'background',
						Color.RGB( 251, 251, 251 ),
					'glint', this._glintInner,
					'resolution', this.resolution,
					'size', this.viewClip.size,
				),
			'pos', Point.zero,
		)
	);
};

/*
| The inner height (zone) of the docView.
*/
def.lazy.innerHeight =
	function( )
{
	const heightPanel = DocViewPanel.designSize.height;

	return this.viewClip.height - Self._innerMargin.y - heightPanel;
};

/*
| User is inputting characters.
|
| ~string: string inputed
*/
def.proto.input =
	function( string )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const mark = this.mark;
	if( !mark || !mark.hasCaret )
	{
		return;
	}

	const traceVCaret = mark.traceVCaret;
	const traceVCaretItems = traceVCaret.forward( 'items' );

	if( traceVCaretItems )
	{
		const item = this.items.get( traceVCaretItems.last.at );
		if( item )
		{
			item.input( this, string, mark );
		}
	}

	const traceVCaretPanel = traceVCaret.forward( 'panel' );
	if( traceVCaretPanel )
	{
		this._panel.input( string );
	}
};

/*
| Builds the doc view items.
*/
def.lazy.items =
	function( )
{
	const ancillary = this.ancillary;
	const items = ancillary.items;
	const viewClip = this.viewClip;
	const resolution = this.resolution;
	const width = viewClip.width - Self._innerMargin.x * resolution.ratio;
	const scrollPos = this.seam.scrollPos;
	const joints = this._ancillaryDocpath.joints;

	const list = [ ];
	const panel = this._panel;

	let y = ( panel ? panel.tZone.height : 0 ) + Self._innerMargin.n;

	const traces = [ ];
	for( let a = 0, alen = joints.length; a < alen; a++ )
	{
		const joint = joints.get( a );
		const trace = joint.pos;
		if( trace.ti2ctype !== Trace ) continue;
/**/	if( CHECK && trace.last.name !== 'items' ) throw new Error( );
		traces.push( trace );
	}

	for( let a = 0, alen = traces.length; a < alen; a++ )
	{
		const trace = traces[ a ];
		const traceFNext = traces[ a + 1 ];
		const traceFPrev = traces[ a - 1 ];

		const key = trace.last.key;
		const aItem = items.get( key );
		const aItemNext = traceFNext && items.get( traceFNext.last.key );
		const aItemPrev = traceFPrev && items.get( traceFPrev.last.key );
		const traceV = this.traceV.add( 'items', a );

		const pos = Point.XY( 0, y - scrollPos.y );

		let dvItem;
		switch( aItem.ti2ctype )
		{
			case ALabel:
				dvItem =
					DocViewItemLabel
					.create(
						'access', this.access,
						'ancillary', aItem,
						'innerMargin', Self._innerMargin,
						'paraSep', paraSep,
						'pos', pos,
						'resolution', resolution,
						'traceV', traceV,
						'typeNext', aItemNext?.ti2ctype,
						'typePrev', aItemPrev?.ti2ctype,
						'viewClip', viewClip,
						'width', width,
					);
				break;

			case ANote:
				dvItem =
					DocViewItemNote
					.create(
						'access', this.access,
						'ancillary', aItem,
						'innerMargin', Self._innerMargin,
						'paraSep', paraSep,
						'pos', pos,
						'resolution', resolution,
						'traceV', traceV,
						'typeNext', aItemNext?.ti2ctype,
						'typePrev', aItemPrev?.ti2ctype,
						'viewClip', viewClip,
						'width', width,
					);
				break;

			case ASubspace:
				dvItem =
					DocViewItemSubspace
					.create(
						'access', this.access,
						'ancillary', aItem,
						'innerMargin', Self._innerMargin,
						'paraSep', paraSep,
						'pos', pos,
						'resolution', resolution,
						'traceV', traceV,
						'typeNext', aItemNext?.ti2ctype,
						'typePrev', aItemPrev?.ti2ctype,
						'viewClip', viewClip,
						'width', width,
					);
				break;

			default: throw new Error( );
		}

		y += dvItem.sizeFull.height + paraSep * 2;
		list.push( dvItem );
	}

	return ListDocViewItem.Array( list );
};

/*
| Mouse wheel turned.
|
| ~p,
| ~dir
*/
def.proto.mousewheel =
	function( p, dir )
{
	if( !this.scrollbarY )
	{
		return true;
	}

	const maxY = this._sizeFull.height - this.innerHeight;
	const y =
		EMath.limit(
			0,
			this.seam.scrollPos.y - dir * config.textWheelSpeed,
			maxY
		);

	Shell.alter(
		SeamDocViewRoot.traceSeam.add( 'scrollPos' ),
		this.seam.scrollPos.create( 'y', y )
	);

	return true;
};

/*
| User pasted something.
|
| ~type: paste data type ('text/plain' )
| ~data: data pasted.
*/
def.proto.paste =
	function( type, data )
{
	this.input( data );
};

/*
| Mouse hover.
| Returns a Hover result with hovering trace and cursor to show.
|
| ~p:     cursor point
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
*/
def.proto.pointingHover =
	function( p, shift, ctrl )
{
	const panel = this._panel;
	{
		const bubble = panel.pointingHover( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	const sbary = this.scrollbarY;
	if( sbary )
	{
		const bubble = sbary.pointingHover( p, shift, ctrl );
		if( bubble !== undefined ) return bubble;
	}

	// TODO items
	return Hover.CursorText;
};

/*
| A button has been pushed.
*/
def.proto.pushButton =
	function( traceV, shift, ctrl )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	return this._panel.pushButton( traceV, shift, ctrl );
};

/*
| Returns the content of a range.
*/
def.lazyFunc.rangeContent =
	function( range )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( range.ti2ctype !== MarkRange ) throw new Error( );
/**/}

	let content;
	const items = this.items;
	const traceFront = range.traceFront;
	const traceBack = range.traceBack;
	const rankitemFront = traceFront.forward( 'items' ).last.at;
	const rankitemBack = traceBack.forward( 'items' ).last.at;

	// front item
	const itemFront = items.get( rankitemFront );
	const rankParaFront = traceFront.backward( 'paras' ).last.at;
	const rankParaBack = traceBack.backward( 'paras' ).last.at;

	if( rankitemBack === rankitemFront )
	{
		// range is within one item
		const paras = itemFront.paras;
		if( rankParaFront === rankParaBack )
		{
			// range is within one para
			return(
				paras.get( rankParaFront ).ancillary.string
				.substring( traceFront.last.at, traceBack.last.at )
			);
		}
		// range is over several paras, but within one item
		content =
			paras.get( rankParaFront ).ancillary.string
			.substr( traceFront.last.at );

		for( let r = rankParaFront + 1; r < rankParaBack; r++ )
		{
			content += '\n' + paras.get( r ).ancillary.string;
		}

		content +=
			'\n'
			+ paras.get( rankParaBack ).ancillary.string
			.substring( 0, traceBack.last.at );
	}

	// range goes over multiple items
	{
		// front item
		const paras = itemFront.paras;
		content =
			paras.get( rankParaFront ).ancillary.string
			.substr( traceFront.last.at );

		for( let r = rankParaFront + 1, rlen = paras.length; r < rlen; r++ )
		{
			content += '\n' + paras.get( r ).ancillary.string;
		}
	}

	// middle items
	for( let i = rankitemFront + 1; i < rankitemBack; i++ )
	{
		const item = items.get( i );
		const paras = item.paras;
		for( let r = 0, rlen = paras.length; r < rlen; r++ )
		{
			content += '\n' + paras.get( r ).ancillary.string;
		}
	}

	{
		// itemBack
		const itemBack = items.get( rankitemBack );
		const paras = itemBack.paras;
		for( let r = 0; r < rankParaBack; r++ )
		{
			content += '\n' + paras.get( r ).ancillary.string;
		}
		content +=
			'\n'
			+ paras.get( rankParaBack ).ancillary.string
			.substring( 0, traceBack.last.at );
	}

	return content;
};

/*
| Performs some maintenance.
|
| Notes scrollPos are checked to be within limits.
*/
def.proto.rectify =
	function( )
{
	const items = this.items;
	if( !items ) return this;
	let space = this;
	for( let item of items )
	{
		if( !item.rectify ) continue;
		const rItem = item.rectify( );
		if( rItem )
		{
			space = space.create( 'items', space.items.set( rItem.traceV.last.key, rItem ) );
		}
	}
	return space;
};

/*
| Removes a range.
*/
def.proto.removeRange =
	function( range )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( range.ti2ctype !== MarkRange ) throw new Error( );
/**/}

	const dItems = this.items;

	const traceFront = range.traceFront;
	const traceBack = range.traceBack;

/**/if( CHECK )
/**/{
/**/	if( traceFront.forward( 'docView' ) !== this.traceV ) throw new Error( );
/**/	if( traceBack.forward( 'docView' ) !== this.traceV ) throw new Error( );
/**/}

	const rankitemFront = traceFront.backward( 'items' ).last.at;
	const rankitemBack = traceBack.backward( 'items' ).last.at;

	const itemFront = dItems.get( rankitemFront );

	const traceparaFront = traceFront.backward( 'paras' );
	const traceparaBack = traceBack.backward( 'paras' );

	const rankParaFront = traceparaFront.last.at;
	const rankParaBack = traceparaBack.last.at;

	const ftraceparaFront = this.traceV2F( traceparaFront );
	const tb = this.window.traceBase;

	if( rankitemBack === rankitemFront )
	{
		// range is within one item
		const paras = itemFront.paras;

		if( rankParaFront === rankParaBack )
		{
			// range is within one para
			const val =
				paras.get( ftraceparaFront.last.at )
				.ancillary.string
				.substring( traceFront.last.at, traceBack.last.at );

			if( val !== '' )
			{
				Shell.change(
					tb.first.key,
					ChangeStringMove.create(
						'traceFrom',
							ftraceparaFront
							.add( 'string' )
							.add( 'offset', traceFront.last.at ),
						'val',
							paras.get( ftraceparaFront.last.at )
							.ancillary.string
							.substring( traceFront.last.at, traceBack.last.at )
					)
				);
			}

			return;
		}

		// range is over several paras, but within one item
		const changes = [ ];
		const traceFPivot = ftraceparaFront.back;

		{
			// removes the rest of line of begin of range
			const val =
				paras.get( rankParaFront )
				.ancillary.string
				.substring( traceFront.last.at );

			changes.push(
				ChangeStringMove.create(
					'traceFrom',
						ftraceparaFront
						.add( 'string' )
						.add( 'offset', traceFront.last.at ),
					'val', val,
				)
			);
		}

		// fully deletes all paras inbetween
		for( let r = rankParaFront + 1; r < rankParaBack; r++ )
		{
			changes.push(
				ChangeListMove.create(
					'traceFrom', traceFPivot.add( 'paras', rankParaFront + 1 ),
					'val', paras.get( r ).ancillary.asFabric,
				)
			);
		}

		const parasBacktring = paras.get( rankParaBack ).ancillary.string;
		// removes the top part of the last para of the range
		changes.push(
			ChangeStringMove.create(
				'traceFrom',
					traceFPivot
					.add( 'paras', rankParaFront + 1 )
					.add( 'string' )
					.add( 'offset', 0 ),
				'val',
					parasBacktring.substring( 0, traceBack.last.at ),
			)
		);

		// moves the rest of the last para onto the first para
		changes.push(
			ChangeStringMove.create(
				'traceFrom',
					traceFPivot
					.add( 'paras', rankParaFront + 1 )
					.add( 'string' )
					.add( 'offset', 0 ),
				'traceTo',
					ftraceparaFront
					.add( 'string' )
					.add( 'offset', traceFront.last.at ),
				'val',
					parasBacktring.substring( traceBack.last.at ),
				'slippyTo', true,
			)
		);

		// removes the last entry which is now empty.
		changes.push(
			ChangeListMove.create(
				'traceFrom', traceFPivot .add( 'paras', rankParaFront + 1 ),
				'val', FabricPara.create( 'string', '' ),
			)
		);

		Shell.change( tb.first.key, Sequence.Array( changes ) );
		return;
	}

	// range goes over several items.
	const changes = [ ];
	{
		// deletes the stuff on the front item
		const paras = itemFront.paras;

		{
			// removes the rest of line of begin of range
			const val =
				paras.get( rankParaFront )
				.ancillary.string
				.substring( traceFront.last.at );

			changes.push(
				ChangeStringMove.create(
					'traceFrom',
						ftraceparaFront
						.add( 'string' )
						.add( 'offset', traceFront.last.at ),
					'val', val,
				)
			);
		}

		// fully deletes remainint paras on first item
		const traceFPivot = ftraceparaFront.back;
		for( let r = rankParaFront + 1, rlen = paras.length; r < rlen; r++ )
		{
			changes.push(
				ChangeListMove.create(
					'traceFrom', traceFPivot.add( 'paras', rankParaFront + 1 ),
					'val', paras.get( r ).ancillary.asFabric,
				)
			);
		}
	}

	{
		// removes the items completely enclosed by the range
		const fItems = this.ancillary.items;
		const traceFBase = tb.back;

		for( let ii = rankitemFront + 1; ii < rankitemBack; ii++ )
		{
			const dItem = dItems.get( ii );
			const fRank = fItems.rankOf( dItem.ancillary.trace.last.key );
			const trace = traceFBase.add( 'items', fItems.getKey( fRank ) );
			changes.push(
				ChangeTreeShrink.create(
					'trace', trace,
					'prev', fItems.atRank( fRank ),
					'rank', fRank,
				)
			);
		}
	}

	{
		// removes stuff on the last item

		const itemBack = dItems.get( rankitemBack );
		// deletes the stuff on the back item
		const paras = itemBack.paras;
		const traceFparaBack = this.traceV2F( traceparaBack );
		const traceFPivot = traceFparaBack.back;

		// removes all paras on last item that are completly encolsed by the range
		for( let r = 0; r < rankParaBack; r++ )
		{
			changes.push(
				ChangeListMove.create(
					'traceFrom', traceFPivot.add( 'paras', rankParaFront + 1 ),
					'val', paras.get( r ).ancillary.asFabric,
				)
			);
		}

		{
			// removes the head part on the last para on last item
			const parasBacktring = paras.get( rankParaBack ).ancillary.string;

			changes.push(
				ChangeStringMove.create(
					'traceFrom',
						traceFPivot
						.add( 'paras', rankParaBack )
						.add( 'string' )
						.add( 'offset', 0 ),
					'val',
						parasBacktring.substring( 0, traceBack.last.at ),
				)
			);
		}
	}

	Shell.change( tb.first.key, Sequence.Array( changes ) );
};

/*
| The vertical scrollbar.
*/
def.lazy.scrollbarY =
	function( )
{
	const viewClip = this.viewClip;
	const dHeight = this._sizeFull.height;
	const panel = this._panel;
	const pHeight = panel.tZone.height;
	const aperture = viewClip.height - Self._innerMargin.y - DocViewPanel.designSize.height;

	if( dHeight <= aperture )
	{
		return false;
	}

	return(
		WidgetScrollbar.create(
			'anchorAlign', 'right',
			'aperture',    aperture,
			'ellipseA',    DesignScrollbar.ellipseA,
			'ellipseB',    DesignScrollbar.ellipseB,
			'facet',       DesignScrollbar.facet( this.light ),
			'max',         dHeight,
			'minHeight',   DesignScrollbar.minHeight,
			'pos',         Point.XY( viewClip.width, pHeight + 1 ),
			'resolution',  this.resolution,
			'scrollPos',   this.seam.scrollPos.y,
			'size',        viewClip.height - pHeight - 1,
			'strength',    DesignScrollbar.strength,
			'transform',   TransformNormal.singleton,
			'traceV',      this.traceV.add( 'scrollbarY' ),
		)
	);
};

/*
| Scrolls the docView so the caret comes into view.
*/
def.proto.scrollMarkIntoView =
	function( )
{
	const mark = this.mark;
	if( !mark || !mark.hasCaret )
	{
		return;
	}

	const scrollPos = this.seam.scrollPos;
	let sy = scrollPos.y;

	const traceVCaret = mark.traceVCaret;
	const traceVCaretItems = mark.traceVCaret.forward( 'items' );
	if( !traceVCaretItems )
	{
		return;
	}

	const item = this.items.get( traceVCaretItems.last.at );
	const para = item.paras.get( traceVCaret.backward( 'paras' ).last.at );

	const fs = item.fontSize;
	const descend = fs * FontRoot.bottomBox;

	const heightPanel = DocViewPanel.designSize.height;
	const imargin = Self._innerMargin;
	const p = para.locateOffsetPoint( traceVCaret.last.at );
	const ppos = para.pos;
	const s = ppos.y + p.y + descend - imargin.s - heightPanel;
	const ih = this.innerHeight;
	const n = ppos.y + p.y - fs - imargin.n - heightPanel;

	if( n < 0 ) sy = sy + n;
	else if( s > ih ) sy = sy + s - ih;

	const maxY = this._sizeFull.height - this.innerHeight;

	if( sy < 0 )
	{
		sy = 0;
	}
	else if( maxY >= 0 && sy > maxY )
	{
		sy = maxY;
	}

	if( sy !== scrollPos.y )
	{
		Shell.alter(
			SeamDocViewRoot.traceSeam.add( 'scrollPos' ),
			scrollPos.create( 'y', sy )
		);
	}
};

/*
| User pressed a special key.
|
| ~key:   key being pressed
| ~shift: true if shift key was pressed
| ~ctrl:  true if ctrl key was pressed
| ~inputWord: the last word (on mobile)
*/
def.proto.specialKey =
	function( key, shift, ctrl, inputWord )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	const mark = this.mark;

	if( mark?.traceVCaret.forward( 'panel' ) )
	{
		this._panel.specialKey( key, shift, ctrl );
		return;
	}

	if( ctrl )
	{
		switch( key )
		{
			// TODO this should be catched by root.
			case 'z': ReUndo.undo( ); return;
			case 'y': ReUndo.redo( ); return;
		}
	}

	if( ctrl )
	{
		switch( key )
		{
			case 'a':
			{
				// creates a range over all docView content
				const items = this.items;
				const para0 = items.first.paras.first;
				const para1 = items.last.paras.last;

				Shell.alter(
					'mark',
						MarkRange.create(
							'traceBegin', para0.offsetTraceV( 0 ),
							'traceEnd', para1.offsetTraceV( para1.ancillary.string.length ),
						)
				);
				return;
			}
		}
	}

	if( mark && mark.hasCaret )
	{
		const item = this.items.get( mark.traceVCaret.forward( 'items' ).last.at );
		if( item ) item.specialKey( this, key, mark, shift, ctrl, inputWord );
		return;
	}
};

/*
| Returns the system mode.
*/
def.lazy.sysMode =
	function( )
{
	const mark = this.mark;

	if( !mark || !mark.hasCaret ) return SysMode.Blank;

	const markItems = mark.traceVCaret.forward( 'items' );
	if( markItems )
	{
		const at = markItems.last.at;
		return this.items.get( at ).sysMode( mark );
	}

	const markPanel = mark.traceVCaret.forward( 'panel' );
	if( markPanel )
	{
		return this._panel.sysMode;
	}

	return SysMode.Blank;
};

/*
| Transforms a mark that is in docView.
*/
def.proto.transformMark =
	function( mark, changeX )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	switch( mark.ti2ctype )
	{
		case MarkCaret:
		{
			const ttraceV = this._transformTrace( mark.traceV, changeX );

			return(
				ttraceV
				? mark.create( 'traceV', ttraceV )
				: undefined
			);
		}

		case MarkRange:
		{
			const bTrace = this._transformTrace( mark.traceBegin, changeX );
			if( !bTrace )
			{
				return undefined;
			}

			const eTrace = this._transformTrace( mark.traceBegin, changeX );
			if( !eTrace )
			{
				return undefined;
			}

			if( bTrace === eTrace )
			{
				return MarkCaret.TraceV( bTrace );
			}
			return mark.create( 'traceBegin', bTrace, 'traceEnd', eTrace );
		}

		default: throw new Error( );
	}
};

/*
| Transform a visual trace to fabric trace.
|
| NOTE, for docViews the otherway is not easily available,
|       as the same fabric place could be multiple times in the doc path.
*/
def.proto.traceV2F =
	function( traceV )
{
	const traceVItems = traceV.forward( 'items' );
	const item = this.items.get( traceVItems.last.at );

	let trace = item.ancillary.trace.add( 'text' );
	for( let a = traceVItems.length, alen = traceV.length; a < alen; a++ )
	{
		trace = trace.addStep( traceV.get( a ) );
	}

	return trace;
};

/*
| Glint for the caret.
*/
def.lazy._caretGlint =
	function( )
{
	const traceVCaret = this.mark.traceVCaret;
	const traceVCaretItems = traceVCaret.forward( 'items' );

	if( !traceVCaretItems )
	{
		return undefined;
	}

	const at = traceVCaret.forward( 'items' ).last.at;
	const item = this.items.get( at );

	const fs = item.fontSize;
	const descend = fs * FontRoot.bottomBox;

	const pRank = traceVCaret.forward( 'paras' ).last.at;
	const para = item.paras.get( pRank );

	let p = para.locateOffsetPoint( traceVCaret.last.at );
	const ppos = para.pos.add( p.x, p.y - fs );

	// TODO don't use a facet
	return(
		Facet.blackStroke.glint(
			Segment.P0P1( ppos, ppos.add( 0, fs + descend ) )
		)
	);
};

/*
| Font color of the docView items.
*/
def.lazy._fontColor =
	function( )
{
	return Color.black;
};

/*
| Full size of the text.
|
| Disregards clipping in notes.
*/
def.lazy._sizeFull =
	function( )
{
	let height = 0;
	let width = 0;

	let first = true;
	const items = this.items;
	for( let item of items )
	{
		const ifs = item.sizeFull;
		width = Math.max( width, ifs.width );
		height += ifs.height;

		if( !first )
		{
			height += 2 * paraSep;
		}
		else
		{
			first = false;
		}
	}

	return Size.WH( width, height );
};

/*
| Returns the docView glint.
*/
def.lazy._glintInner =
	function( )
{
	const light = this.light;
	const items = this.items;
	const glints = [ ];
	const mark = this.mark;

	if( mark && this.systemFocus )
	{
		switch( mark.ti2ctype )
		{
			case MarkRange:
				glints.push(
					DesignSelect.facetSelectText( light ).glint( this._rangeShape )
				);
				break;

			case MarkCaret:
			{
				const cg = this._caretGlint;
				if( cg ) glints.push( cg );
				break;
			}
		}
	}

	const hv = this.viewClip.height;
	for( let item of items )
	{
		const yi = item.pos.y;

		if( yi + item.sizeFull.height <= 0 )
		{
			continue;
		}

		if( yi > hv )
		{
			continue;
		}

		glints.push( item.glint );
	}

	glints.push( this._panel.glint );

	const sbary = this.scrollbarY;
	if( sbary )
	{
		glints.push( sbary.glint );
	}

	return ListGlint.Array( glints );
};

/*
| Inner margin of the doc view.
*/
def.staticLazy._innerMargin =
	( ) =>
	Margin.NESW( 8, 16, 8, 8 );

/*
| Returns the shape for the selection range.
*/
def.lazy._rangeShape =
	function( )
{
	const mark = this.mark;
/**/if( CHECK && mark.ti2ctype !== MarkRange ) throw new Error( );

	const items = this.items;

	const traceFront = mark.traceFront;
	const traceBack = mark.traceBack;

	const rankItemFront = traceFront.forward( 'items' ).last.at;
	const rankItemBack = traceBack.forward( 'items' ).last.at;

	const itemFront = items.get( rankItemFront );
	const itemBack = items.get( rankItemBack );

	const rankParaFront = traceFront.backward( 'paras' ).last.at;
	const rankParaBack = traceBack.backward( 'paras' ).last.at;

	const parasFront = itemFront.paras;
	const parasBack = itemBack.paras;

	const paraFront = parasFront.get( rankParaFront );
	const paraBack = parasBack.get( rankParaBack );

	const posFront = paraFront.pos;
	const posBack = paraBack.pos;

	const fp = paraFront.locateOffsetPoint( traceFront.last.at ).add( posFront );
	const bp = paraBack.locateOffsetPoint( traceBack.last.at ).add( posBack );

	const fLine = paraFront.locateOffsetLine( traceFront.last.at );
	const bLine = paraBack.locateOffsetLine( traceBack.last.at );

	const frontFontSize = itemFront.fontSize;
	const frontDescend = frontFontSize * FontRoot.bottomBox;
	const frontAscend = frontFontSize;

	const backFontSize = itemBack.fontSize;
	const backDescend = backFontSize * FontRoot.bottomBox;
	const backAscend = backFontSize;

	const innerMargin = Self._innerMargin;
	const rx = this.viewClip.width - innerMargin.e;
	const lx = innerMargin.w;
	const frontFlow = paraFront.flow;
	const backFlow = paraBack.flow;

	// paraFront2, the para after the front para
	let paraFront2;
	if( rankParaFront + 1 < parasFront.length )
	{
		paraFront2 = parasFront.get( rankParaFront + 1 );
	}
	else if ( rankItemFront + 1 < items.length )
	{
		paraFront2 = items.get( rankItemFront + 1 ).paras.get( 0 );
	}

	if( paraFront === paraBack && fLine === bLine )
	{
		// fp o******o bp
		return(
			Path.Plan(
				'pc', ( fp.x + bp.x ) / 2, ( fp.y + bp.y ) / 2,
				'start', fp.add( 0, frontDescend ),
				'line', fp.add( 0, -frontAscend ),
				'line', bp.add( 0, -backAscend ),
				'line', bp.add( 0, backDescend ),
				'line', 'close'
			)
		);
	}
	else if(
		bp.x < fp.x
		&& (
			(
				paraFront === paraBack
				&& fLine + 1 === bLine
			)
			|| (
				paraFront2 === paraBack
				&& fLine + 1 >= frontFlow.lines.length
				&& bLine === 0
			)
		)
	)
	{
		//         fp o****
		// ****o bp
		return FigureList.Elements(
			Path.Plan(
				'pc', ( fp.x + rx ) / 2, ( 2 * fp.y - frontAscend + frontDescend ) / 2,
				'start', rx, fp.y - frontAscend,
				'line', fp.x, fp.y - frontAscend,
				'line', fp.x, fp.y + frontDescend,
				'line', rx, fp.y + frontDescend,
				'line', 'fly', 'close'
			),
			Path.Plan(
				'pc', ( fp.x + rx ) / 2, ( 2 * fp.y - backAscend + backDescend ) / 2,
				'start', lx, bp.y - backAscend,
				'line', bp.x, bp.y - backAscend,
				'line', bp.x, bp.y + backDescend,
				'line', lx, bp.y + backDescend,
				'line', 'fly', 'close'
			)
		);
	}
	else
	{
		//          6/7            8
		//        fp o------------+
		// fp2  +----+:::::::::::::
		//    5 :::::::::::::::::::
		//      ::::::::::::+-----+  bp2
		//      +-----------o bp   1
		//      4          2/3
		let f2y, b2y;
		if( fLine + 1 < frontFlow.lines.length )
		{
			f2y = frontFlow.lines.get( fLine + 1 ).y + posFront.y;
		}
		else
		{
			f2y = paraFront2.flow.lines.get( 0 ).y + paraFront2.pos.y;
		}

		if( bLine > 0 )
		{
			b2y = backFlow.lines.get( bLine - 1 ).y + posBack.y;
		}
		else
		{
			let paraBack2;
			if( rankParaBack > 0 )
			{
				paraBack2 = itemBack.paras.get( rankParaBack - 1 );
			}
			else
			{
				paraBack2 = items.get( rankItemBack - 1 ).paras.last;
			}

			b2y = paraBack2.flow.lines.last.y + paraBack2.pos.y;
		}

		if( traceFront.last.at > 0 )
		{
			return(
				Path.Plan(
					'pc', ( rx + lx ) / 2, ( b2y + backDescend + f2y - frontAscend ) / 2,
					'start', rx, b2y + backDescend,        // 1
					'line', bp.x, b2y + backDescend,       // 2
					'line', bp.x, bp.y + backDescend,      // 3
					'line', lx, bp.y + backDescend,        // 4
					'line', 'fly', lx, f2y - frontAscend,  // 5
					'line', fp.x, f2y - frontAscend,       // 6
					'line', fp.x, fp.y - frontAscend,      // 7
					'line', rx, fp.y - frontAscend,        // 8
					'line', 'fly', 'close'
				)
			);
		}
		else
		{
			return(
				Path.Plan(
					'pc', ( rx + lx ) / 2, ( b2y + backDescend + f2y - frontAscend ) / 2,
					'start', rx, b2y + backDescend,        // 1
					'line', bp.x, b2y + backDescend,       // 2
					'line', bp.x, bp.y + backDescend,      // 3
					'line', lx, bp.y + backDescend,        // 4
					'line', 'fly', lx, fp.y - frontAscend, // 7
					'line', rx, fp.y - frontAscend,        // 8
					'line', 'fly', 'close'
				)
			);
		}
	}
};

/*
| Transform a traceV on the docview by change(s).
|
| ~traceV: visual trace to transform.
| ~changeX: change(s) to transform upon.
*/
def.proto._transformTrace =
	function( traceV, changeX )
{
	const traceVItems = traceV.forward( 'items' );
	if( traceVItems )
	{
		const item = this.items.get( traceVItems.last.at );
		let trace = item.ancillary.trace.add( 'text' );
		for( let a = traceVItems.length, alen = traceV.length; a < alen; a++ )
		{
			trace = trace.addStep( traceV.get( a ) );
		}

		const traceFT = changeX.transform( trace );

		// trace transformed away?
		if( !traceFT )
		{
			return undefined;
		}

		const traceFTItems = traceFT.forward( 'items' );
		traceV = item.traceV;
		for( let a = traceFTItems.length + 1, alen = traceFT.length; a < alen; a++ )
		{
			traceV = traceV.addStep( traceFT.get( a ) );
		}
		return traceV;
	}

	const traceVPanel = traceV.forward( 'panel' );
	if( traceVPanel )
	{
		const traceVWidgets = traceV.forward( 'widgets' );
		if( traceVWidgets.last.key === 'name' )
		{
			let trace = this.window.traceBase.add( 'name' );
			for( let a = traceVWidgets.length + 1, alen = traceV.length; a < alen; a++ )
			{
				const step = traceV.get( a );
				trace = trace.addStep( step );
			}

			const traceFT = changeX.transform( trace );

			// trace transformed away?
			if( !traceFT )
			{
				return undefined;
			}

			const traceFTItems = traceFT.forward( 'items' );
			traceV = traceVWidgets.add( 'value' );
			for( let a = traceFTItems.length + 1, alen = traceFT.length; a < alen; a++ )
			{
				const step = traceFT.get( a );
				traceV = traceV.addStep( step );
			}
			return traceV;
		}
	}

	return traceV;
};

/*
| Default styles for docx.js.
*/
def.staticLazy._docxStyles =
	function( )
{
	return( {
		default:
		{
			heading1:
			{
				run:
				{
					font: 'Calibri',
					size: 52,
					bold: true,
					color: '000000',
				},
				paragraph:
				{
					//alignment: AlignmentType.CENTER,
					//spacing: { line: 340 },
				},
			},
		},
		paragraphStyles:
		[
			{
				id: 'normalPara',
				name: 'Normal Para',
				basedOn: 'Normal',
				next: 'Normal',
				quickFormat: true,
				run:
				{
					font: 'Calibri',
					size: 26,
				},
				paragraph:
				{
					spacing:
					{
						line: 276,
						before: 20 * 72 * 0.1,
						after: 20 * 72 * 0.05
					},
					rightTabStop: docx.TabStopPosition.MAX,
					leftTabStop: 453.543307087,
				},
			},
		],
	} );
};

/*
| The ancillary fabric of the docpath base.
*/
def.lazy._ancillaryDocpath =
	function( )
{
	return(
		this.ancillary.items.get(
			this.window.traceBase.forward( 'items' ).last.key
		)
	);
};

/*
| Builds the panel.
*/
def.lazy._panel =
	function( )
{
	const traceVPanel = this.traceV.add( 'panel' );

	let hoverPanel = this.hover;
	if( hoverPanel && !hoverPanel.hasTrace( traceVPanel ) )
	{
		hoverPanel = undefined;
	}

	let markPanel = this.mark;
	if( markPanel && !markPanel.encompasses( traceVPanel ) )
	{
		markPanel = undefined;
	}

	return(
		DocViewPanel.create(
			'access',           this.access,
			'ancillaryDocPath', this._ancillaryDocpath,
			'hover',            hoverPanel,
			'light',            this.light,
			'mark',             markPanel,
			'resolution',       this.resolution,
			'systemFocus',      this.systemFocus,
			'transform',        TransformNormal.singleton,
			'viewClip',         this.viewClip,
			'traceV',           traceVPanel,
			'window',           this.window,
		)
	);
};

