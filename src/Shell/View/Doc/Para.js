/*
| A doc view paragraph.
*/
def.attributes =
{
	// ancillary fabric of the para
	ancillary: { type: 'Shared/Ancillary/Text/Para' },

	// size of the font
	fontSize: { type: 'number' },

	// point in north west
	pos: { type: 'gleam:Point' },

	// display resolution
	resolution: { type: 'gleam:Display/Canvas/Resolution' },

	// the visual trace of the item
	traceV: { type: 'ti2c:Trace' },

	// the flow width that can be used.
	widthFlow: { type: 'number' },
};

import { Self as ChangeListMove   } from '{ot:Change/List/Move}';
import { Self as ChangeStringMove } from '{ot:Change/String/Move}';
import { Self as DesignFont       } from '{Shell/Design/Font}';
import { Self as FabricPara       } from '{Shared/Fabric/Text/Para}';
import { Self as FlowBlock        } from '{Shared/Flow/Block}';
import { Self as Font             } from '{gleam:Font/Root}';
import { Self as GlintWindow      } from '{gleam:Glint/Window}';
import { Self as MarkCaret        } from '{Shared/Mark/Caret}';
import { Self as MarkRange        } from '{Shared/Mark/Range}';
import { Self as ParaPane         } from '{Shell/Para/Pane}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Shell            } from '{Shell/Self}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as SysMode          } from '{Shell/Result/SysMode}';
import { Self as TransformNormal  } from '{gleam:Transform/Normal}';

/*
| Maps keys to handlers.
*/
const _keyMap =
	Object.freeze( {
		backspace : '_keyBackspace',
		del       : '_keyDel',
		down      : '_keyDown',
		end       : '_keyEnd',
		enter     : '_keyEnter',
		left      : '_keyLeft',
		pagedown  : '_keyPageDown',
		pageup    : '_keyPageUp',
		pos1      : '_keyPos1',
		right     : '_keyRight',
		up        : '_keyUp'
	} );

/*
| The para's flow, the position of all chunks.
*/
def.lazy.flow =
	function( )
{
/**/if( CHECK && this.fontSize === undefined ) throw new Error( );

	return(
		FlowBlock.create(
			'fontBland', this._fontBland,
			'string',    this.ancillary.string,
			'widthFlow', this.widthFlow,
		)
	);
};

/*
| Returns the offset closest to a point.
|
| ~point: the point to look for
*/
def.proto.getPointOffset =
	function( point )
{
	if( point.y < 0 ) return 0;
	const flow = this.flow;
	const lines = flow.lines;

	let line;
	for( line = 0; line < lines.length; line++ )
	{
		if( point.y <= lines.get( line ).y ) break;
	}

	if( line >= lines.length ) line--;
	return this._getOffsetAtLnX( line, point.x );
};

/*
| The para's glint.
*/
def.lazy.glint =
	function( )
{
	return(
		GlintWindow.create(
			'pane', this._pane.glint,
			'pos', this.pos,
		)
	);
};

/*
| The height of the para.
*/
def.lazy.height =
	function( )
{
	return this._pane.height;
};

/*
| A string has been inputed.
*/
def.proto.input =
	function( dvRoot, dvItem, string, mark )
{
/**/if( CHECK && arguments.length !== 4 ) throw new Error( );

	const changes = [ ];
	const reg = /([^\n]+)(\n?)/g;

	let trace = this.ancillary.trace;
	let at = mark.traceVCaret.last.at;
	let restOfLine;

	for( let rx = reg.exec( string ); rx; rx = reg.exec( string ) )
	{
		const line = rx[ 1 ];
		changes.push(
			ChangeStringMove.create(
				'traceTo', trace.add( 'string' ).add( 'offset', at ),
				'val', line,
			)
		);

		if( rx[ 2 ] )
		{
			if( restOfLine === undefined )
			{
				restOfLine = this.ancillary.string.substring( mark.traceVCaret.last.at );
			}

			const traceN = trace.back.add( 'paras', trace.last.at + 1 );

			changes.push(
				ChangeListMove.create(
					'traceTo', traceN,
					'val', FabricPara.create( 'string', '' ),
				),
				ChangeStringMove.create(
					'traceFrom', trace.add( 'string' ).add( 'offset', at + line.length ),
					'traceTo',   traceN.add( 'string' ).add( 'offset', 0 ),
					'val',       restOfLine,
				),
			);
			trace = traceN;
			at = 0;
		}
	}

	Shell.change( trace.first.key, Sequence.Array( changes ) );
	Shell.alter( 'clearRetainX', true );
};

/*
| Locates the line number of a given offset.
*/
def.lazyFunc.locateOffsetLine =
	function( offset )
{
	this._locateOffset( offset );
	// this is not recursive, it returns
	// the aheaded value set by _locateOffset
	return this.locateOffsetLine( offset );
};

/*
| Locates the line number of a given offset.
*/
def.lazyFunc.locateOffsetPoint =
	function( offset )
{
	this._locateOffset( offset );
	// this is not recursive, it returns
	// the aheaded value set by _locateOffset
	return this.locateOffsetPoint( offset );
};

/*
| Returns an offset trace into the string.
*/
def.proto.offsetTraceV =
	function( at )
{
	return this.traceV.add( 'string' ).add( 'offset', at );
};

/*
| Handles a special key.
*/
def.proto.specialKey =
	function( dvRoot, dvItem, key, mark, shift, ctrl, inputWord )
{
/**/if( CHECK && arguments.length !== 7 ) throw new Error( );

	/*
	if( ctrl && key === 'a' )
	{
		const paras = vText.paras;
		const v0 = paras.atRank( 0 );
		const v1 = paras.atRank( vText.paras.length - 1 );
		Shell.alter(
			'mark',
				MarkRange.create(
					'traceBegin', v0.offsetTraceV( 0 ),
					'traceEnd', v1.offsetTraceV( v1.fabric.string.length )
				)
		);
		return;
	}
	*/

	const keyHandler = _keyMap[ key ];
	if( keyHandler )
	{
		this[ keyHandler ]( dvRoot, dvItem, mark, shift, inputWord );
	}
};

/*
| The system mode.
*/
def.lazyFunc.sysMode =
	function( mark )
{
	const fs = this.fontSize;
	const descend = fs * Font.bottomBox;

	const p = this.locateOffsetPoint( mark.traceVCaret.last.at );
	const s = Math.round( p.y + descend );
	const n = s - Math.round( fs + descend );

	return SysMode.Keyboard( this.pos.y + n );
};

/*
| Font bland used.
*/
def.lazy._fontBland =
	function( )
{
	return this._fontColor.fontBland;
};

/*
| Font color used.
*/
def.lazy._fontColor =
	function( )
{
	return DesignFont.Size( this.fontSize );
};

/*
| Returns the offset by an x coordinate in a flow.
|
| ~ln: line number
| ~x: x coordinate
*/
def.proto._getOffsetAtLnX =
	function( ln, x )
{
	const fontBland = this._fontBland;
	const flow = this.flow;
	const line = flow.lines.get( ln );
	const tokens = line.tokens;
	const llen = tokens.length;
	let token, tn;
	for( tn = 0; tn < llen; tn++ )
	{
		token = tokens.get( tn );
		if( x <= token.x + token.width ) break;
	}

	if( tn >= llen && llen > 0 ) token = tokens.get( --tn );
	if( !token ) return 0;

	const dx = x - token.x;
	const word = token.word;
	let a, x1 = 0, x2 = 0;
	for( a = 0; a <= word.length; a++ )
	{
		x1 = x2;
		x2 = fontBland.StringBland( word.substr( 0, a ) ).advanceWidth;
		if( x2 >= dx ) break;
	}
	if( a > word.length ) a = word.length;
	if( dx - x1 < x2 - dx && a > 0 ) a--;
	return token.offset + a;
};

/*
| Backspace pressed.
*/
def.proto._keyBackspace =
	function( dvRoot, dvItem, mark, shift, inputWord )
{
	const at = mark.traceVCaret.last.at;
	const trace = this.ancillary.trace;

	if( at > 0 )
	{
		let n = 1;
		if( inputWord ) n = inputWord.length;
		if( n > at ) n = at;

		Shell.change(
			trace.first.key,
			ChangeStringMove.create(
				'traceFrom', trace.add( 'string' ).add( 'offset', at - n ),
				'val', this.ancillary.string.substring( at - n, at ),
			)
		);
		Shell.alter( 'clearRetainX', true );

		return;
	}

	const paras = dvItem.paras;
	const rank = trace.last.at;

	if( rank === 0 ) return;

	const ve = paras.get( rank - 1 );
	const changes =
	[
		ChangeStringMove.create(
			'traceFrom',
				trace.add( 'string' ).add( 'offset', 0  ),
			'traceTo',
				ve.ancillary.trace.add( 'string' ).add( 'offset', ve.ancillary.string.length ),
			'val', this.ancillary.string,
		),
		ChangeListMove.create(
			'traceFrom', trace,
			'val', FabricPara.create( 'string', '' ),
		),
	];

	Shell.change( trace.first.key, Sequence.Array( changes ) );
	Shell.alter( 'clearRetainX', true );
};

/*
| Del-key pressed.
*/
def.proto._keyDel =
	function( dvRoot, dvItem, mark, shift )
{
	const at = mark.traceVCaret.last.at;
	const trace = this.ancillary.trace;

	if( at < this.ancillary.string.length )
	{
		Shell.change(
			trace.first.key,
			ChangeStringMove.create(
				'traceFrom', trace.add( 'string' ).add( 'offset', at ),
				'val', this.ancillary.string.substring( at, at + 1 ),
			)
		);
		Shell.alter( 'clearRetainX', true );
		return;
	}

	const paras = dvItem.paras;
	const rank = trace.last.at;

	if( rank >= paras.length - 1 )
	{
		return;
	}

	const traceN = trace.back.add( 'paras', rank + 1 );
	const str = paras.get( traceN.last.at ).ancillary.string;

	Shell.change(
		trace.first.key,
		Sequence.Elements(
			ChangeStringMove.create(
				'traceFrom',
					traceN.add( 'string' ).add( 'offset', 0 ),
				'traceTo',
					trace.add( 'string' ).add( 'offset', this.ancillary.string.length ),
				'val', str,
				'slippyTo', true,
			),
			ChangeListMove.create(
				'traceFrom', traceN,
				'val', FabricPara.create( 'string', '' ),
			),
		)
	);

	Shell.alter( 'clearRetainX', true );
};

/*
| Down arrow pressed.
*/
def.proto._keyDown =
	function( dvRoot, dvItem, mark, shift )
{
	const flow = this.flow;
	let at = mark.traceVCaret.last.at;
	const cPosLine = this.locateOffsetLine( at );
	const cPosP = this.locateOffsetPoint( at );
	const x = mark.retainX ?? cPosP.x;

	if( cPosLine < flow.lines.length - 1 )
	{
		// stays within this para
		this._setMark( this._getOffsetAtLnX( cPosLine + 1, x ), mark, x, shift );
		return;
	}

	// goto next para
	const paras = dvItem.paras;
	const pRank = this.traceV.backward( 'paras' ).last.at;
	if( pRank < paras.length - 1 )
	{
		// on the same item
		const ve = paras.get( pRank + 1 );
		at = ve._getOffsetAtLnX( 0, x );
		ve._setMark( at, mark, x, shift );
	}
	else
	{
		// even on another item
		const items = dvRoot.items;
		const iRank = this.traceV.backward( 'items' ).last.at;
		if( iRank < items.length - 1 )
		{
			const item = items.get( iRank + 1 );
			const para = item.paras.get( 0 );
			at = para._getOffsetAtLnX( 0, x );
			para._setMark( at, mark, x, shift );
		}
	}
};

/*
| End-key pressed.
*/
def.proto._keyEnd =
	function( dvRoot, dvItem, mark, shift )
{
	this._setMark( this.ancillary.string.length, mark, undefined, shift );
};

/*
| Enter-key pressed.
*/
def.proto._keyEnter =
	function( dvRoot, dvItem, mark, shift )
{
	const trace = this.ancillary.trace;
	const offset = mark.traceVCaret.last.at;
	const traceN = trace.back.add( 'paras', this.ancillary.trace.last.at + 1 );

	const restOfLine = this.ancillary.string.substring( offset );

	Shell.change(
		trace.first.key,
		Sequence.Elements(
			ChangeListMove.create(
				'traceTo', traceN,
				'val', FabricPara.create( 'string', '' ),
			),
			ChangeStringMove.create(
				'traceFrom', trace.add( 'string' ).add( 'offset', offset ),
				'traceTo', traceN.add( 'string' ).add( 'offset', 0 ),
				'val', restOfLine,
			),
		),
	);
};

/*
| Left arrow pressed.
*/
def.proto._keyLeft =
	function( dvRoot, dvItem, mark, shift, inputWord )
{
	const at = mark.traceVCaret.last.at;

	if( at > 0 )
	{
		this._setMark( at - 1, mark, undefined, shift );
		return;
	}

	const paras = dvItem.paras;
	const pRank = this.traceV.backward( 'paras' ).last.at;

	if( pRank > 0 )
	{
		const ve = paras.get( pRank - 1 );
		ve._setMark( ve.ancillary.string.length, mark, undefined, shift );
	}
	else
	{
		const iRank = this.traceV.backward( 'items' ).last.at;
		if( iRank > 0 )
		{
			const vi = dvRoot.items.get( iRank - 1 );
			const vp = vi.paras.last;
			vp._setMark( vp.ancillary.string.length, mark, undefined, shift );
		}
	}
};

/*
| PageDown key pressed.
*/
def.proto._keyPageDown =
	function( dvRoot, dvItem, mark, shift )
{
	this._pageUpDown( 1, dvRoot, dvItem, mark, shift );
};

/*
| PageUp key pressed.
*/
def.proto._keyPageUp =
	function( dvRoot, dvItem, mark, shift )
{
	this._pageUpDown( -1, dvRoot, dvItem, mark, shift );
};

/*
| Pos1-key pressed.
*/
def.proto._keyPos1 =
	function( dvRoot, dvItem, mark, shift )
{
	this._setMark( 0, mark, undefined, shift );
};

/*
| Right arrow pressed.
*/
def.proto._keyRight =
	function( dvRoot, dvItem, mark, shift, inputWord )
{
	const at = mark.traceVCaret.last.at;

	if( at < this.ancillary.string.length )
	{
		this._setMark( at + 1, mark, undefined, shift );
		return;
	}

	const paras = dvItem.paras;
	const pRank = this.traceV.backward( 'paras' ).last.at;

	if( pRank < paras.length - 1 )
	{
		const ve = paras.get( pRank + 1 );
		ve._setMark( 0, mark, undefined, shift );
	}
	else
	{
		const items = dvRoot.items;
		const iRank = this.traceV.backward( 'items' ).last.at;
		if( iRank < items.length - 1 )
		{
			const vi = items.get( iRank + 1 );
			const vp = vi.paras.get( 0 );
			vp._setMark( 0, mark, undefined, shift );
		}
	}
};

/*
| Up arrow pressed.
*/
def.proto._keyUp =
	function( dvRoot, dvItem, mark, shift )
{
	let at = mark.traceVCaret.last.at;
	const cPosLine = this.locateOffsetLine( at );
	const cPosP = this.locateOffsetPoint( at );
	const x = mark.retainX ?? cPosP.x;

	if( cPosLine > 0 )
	{
		// stay within this para
		at = this._getOffsetAtLnX( cPosLine - 1, x );
		this._setMark( at, mark, x, shift );
		return;
	}

	// goto prev para
	const paras = dvItem.paras;
	const pRank = this.traceV.backward( 'paras' ).last.at;
	if( pRank > 0 )
	{
		const ve = paras.get( pRank - 1 );
		at = ve._getOffsetAtLnX( ve.flow.lines.length - 1, x );
		ve._setMark( at, mark, x, shift );
	}
	else
	{
		const items = dvRoot.items;
		const iRank = this.traceV.backward( 'items' ).last.at;
		if( iRank > 0 )
		{
			const vi = items.get( iRank - 1 );
			const vp = vi.paras.last;
			at = vp._getOffsetAtLnX( vp.flow.lines.length - 1, x );
			vp._setMark( at, mark, x, shift );
		}
	}
};

/*
| Sets the aheadValues for point and line of a given offset.
|
| ~offset: the offset to get the point from.
*/
def.proto._locateOffset =
	function( offset )
{
	const fontBland = this._fontBland;
	const string = this.ancillary.string;
	const flow = this.flow;
	const lines = flow.lines;
	// determines which line this offset belongs to
	let ln;
	const llen = lines.length - 1;
	for( ln = 0; ln < llen; ln++ )
	{
		if( lines.get( ln + 1 ).offset > offset ) break;
	}
	const line = lines.get( ln );
	const tokens = line.tokens;
	let tn;
	const lLen = tokens.length - 1;
	for( tn = 0; tn < lLen; tn++ )
	{
		if( tokens.get( tn + 1 ).offset > offset ) break;
	}
	let p;
	if( tn < tokens.length )
	{
		const token = tokens.get( tn );
		p =
			Point.XY(
				token.x
				+ fontBland.StringBland( string.substring( token.offset, offset ) ).advanceWidth,
				line.y
			);
	}
	else
	{
		p = Point.XY( 0, line.y );
	}

	ti2c.aheadFunction( this, 'locateOffsetLine', offset, ln );
	ti2c.aheadFunction( this, 'locateOffsetPoint', offset, p );
};

/*
| User pressed page up or down.
|
| FUTURE maintain relative scroll pos
| ~dir: +1 for down, -1 for up
*/
def.proto._pageUpDown =
	function( dir, dvRoot, dvItem, mark, shift )
{
	let at = mark.traceVCaret.last.at;
	const p = this.locateOffsetPoint( at );
	const height = dvRoot.innerHeight;
	const tp = this.pos.add( mark.retainX ?? p.x, p.y + dir * height );
	let tItem = dvRoot.getItemAtPoint( tp );
	let tPara;
	if( tItem ) tPara = tItem.getParaAtPoint( tp );

	if( !tPara )
	{
		if( dir > 0 ) tPara = dvRoot.items.last.paras.last;
		else tPara = dvRoot.items.first.paras.first;
	}

	const tPos = tPara.pos;
	at = tPara.getPointOffset( tp.sub( tPos ) );
	tPara._setMark( at, mark, mark.retainX, shift );
};

/*
| The para pane is independant of it's position.
*/
def.lazy._pane =
	function( )
{
	return(
		ParaPane.create(
			'flow', this.flow,
			'fontColor', this._fontColor,
			'resolution', this.resolution,
			'scale', TransformNormal.singleton,
		)
	);
};

/*
| Sets the users caret or range.
|
| ~at:         position to mark caret (or end of range).
| ~retainX:    retains this x position when moving up/down.
| ~traceBegin: begin trace when marking a range.
*/
def.proto._setMark =
	function( at, mark, retainX, shift )
{
	const traceV = this.offsetTraceV( at );

	Shell.alter(
		'mark',
			!shift
			? MarkCaret.create(
				'traceV', traceV,
				'retainX', retainX,
			)
			: MarkRange.create(
				'traceBegin', mark.traceBegin,
				'traceEnd', traceV,
				'retainX', retainX,
			)
	);
};

