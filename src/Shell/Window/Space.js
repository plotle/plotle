/*
| Window is showing a space.
*/
def.attributes =
{
	// current user's access to the space
	// "ro" or "rw"
	access: { type: 'string' },

	// transform zoom as power of 1.1
	exponent: { type: 'number' },

	// true if current user is an owner to the space
	isOwner: { type: 'boolean' },

	// reference of the space
	ref: { type: 'Shared/Ref/Space' },

	// current space transformation
	transform: { type: '<gleam:Transform/Types' },

	// the uid
	uid: { type: 'string' },
};

/*
| The fallback for a loaded space is it's own ref.
| This lazy attribute makes it compatible with WindowLoading.
*/
def.lazy.refFallback =
	function( )
{
	return this.ref;
};
