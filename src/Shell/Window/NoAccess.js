/*
| Window for user having tried to open a space they have no access for.
*/
def.attributes =
{
	// reference of the space having no access for.
	ref: { type: 'Shared/Ref/Space' },

	// reference of the space to be loaded as fallback.
	// if undefined, the window will be closed.
	refFallback: { type: [ 'undefined', 'Shared/Ref/Space'] },
};
