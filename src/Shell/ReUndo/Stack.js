/*
| The re/undo stack.
*/
def.abstract = true;

import { Self as Entry     } from '{Shell/ReUndo/Entry}';
import { Self as ListEntry } from '{list@Shell/ReUndo/Entry}';
import { Self as Shell     } from '{Shell/Self}';

/*
| The redo stack.
*/
let _redo;

/*
| The undo stack.
*/
let _undo;

/*
| Flushes the stacks.
*/
def.static.flush =
	function( )
{
	_undo = ListEntry.Empty;
	_redo = ListEntry.Empty;
};

/*
| Reverts undos from the redo stack.
*/
def.static.redo =
	function( )
{
	if( _redo.length === 0 ) return;

	const entry = _redo.last.reversed;

	_redo = _redo.remove( _redo.length - 1 );
	_undo = _undo.append( entry );

	Shell.change( entry.uidSpace, 'noTrack', entry.changes );
};

/*
| Reporting the tracker something has been altered.
|
| It will track it on the undo stack.
*/
def.static.track =
	function( uidSpace, changes )
{
	const entry = Entry.create( 'changes', changes, 'uidSpace', uidSpace );

	_undo = _undo.append( entry );

	if( _undo.length > config.maxUndo )
	{
		_undo = _undo.remove( 0 );
	}

	_redo = ListEntry.Empty;
};

/*
| Reverts an action from the undo stack.
*/
def.static.undo =
	function( )
{
	if( _undo.length === 0 ) return;

	const entry = _undo.last.reversed;

	_undo = _undo.remove( _undo.length - 1 );
	_redo = _redo.append( entry );

	Shell.change( entry.uidSpace, 'noTrack', entry.changes );
};

/*
| Received server updates.
|
| These contain updates from this sessions own changes
| now enriched with sequence ids as well as genuine updates
| from others.
*/
def.static.update =
	function( changeWrapList )
{
	// nothing to do?
	if( changeWrapList.length === 0 ) return;

	// adapts the tracker stacks
	for( let a = 0, alen = _undo.length; a < alen; a++ )
	{
		const entry = _undo.get( a );
		let changes = changeWrapList.transform( entry.changes );

		if( !changes )
		{
			// the changes vanished by transformation
			_undo = _undo.remove( a-- );
			alen--;
		}
		else
		{
			changes = changes.sequence;
			if( changes !== entry.changes )
			{
				_undo = _undo.set( a, entry.create( 'changes', changes ) );
			}
		}
	}

	for( let a = 0, alen = _redo.length; a < alen; a++ )
	{
		const entry = _redo.get( a );
		const changes = changeWrapList.transform( entry.changes );

		if( !changes )
		{
			// the changes vanished by transformation
			_redo = _redo.remove( a-- );
			alen--;
		}
		else
		{
			_redo = _redo.set( a, entry.create( 'change', changes ) );
		}
	}
};
