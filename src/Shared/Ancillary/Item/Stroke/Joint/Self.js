/*
| A stroke (with possible arrow heads)
*/
def.attributes =
{
	// ancillary outline of the joint
	outline: { type: [ 'undefined', '<gleam:Shape/Types' ] },

	// the joint position (point or item reference)
	pos: { type: [ 'gleam:Point', 'ti2c:Trace' ] },
};

import { Self as FabricJoint } from '{Shared/Fabric/Item/Stroke/Joint/Self}';
import { Self as Point       } from '{gleam:Point}';
import { Self as GroupString } from '{group@string}';
import { Self as Trace       } from '{ti2c:Trace}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	return( FabricJoint.create(
		'outline', this.outline,
		'pos', this.pos,
	) );
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( joint )
{
/**/if( joint.ti2ctype !== FabricJoint ) throw new Error( );

	return( Self.create(
		'outline', joint.outline,
		'pos',     joint.pos,
	) );
};

/*
| Pastes this item from an export.
|
| ~mapping: GroupString, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
*/
def.proto.paste =
	function( mapping, offset )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/}

	let pos = this.pos;
	let outline = this.outline;
	if( pos )
	{
		switch( pos.ti2ctype )
		{
			case Point:
				pos = pos.add( offset );
				break;
			case Trace:
			{
				if( pos.last.name !== 'items' ) throw new Error( );
				const mapKey = mapping.get( pos.last.key );
				if( mapKey )
				{
					pos = pos.back.add( 'items', mapKey );
				}
				break;
			}
			default: throw new Error( );
		}
	}

	if( outline ) outline = outline.add( offset );

	return(
		this.create(
			'pos', pos,
			'outline', outline,
		)
	);
};

/*
| Checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	if( this.pos.ti2ctype === Trace )
/**/	{
/**/		const pos0 = this.pos.get( 0 ).name;
/**/		if( pos0 !== 'space' && pos0 !== 'exportItems' ) throw new Error( );
/**/	}
/**/}
};
