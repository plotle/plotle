/*
| An actuator is a button on a space.
*/
def.extend = 'Shared/Ancillary/Item/Base';

def.attributes =
{
	// the actuator name
	name: { type: 'string', json: true },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },

	// the portals zone
	zone: { type: 'gleam:Rect' },
};

import { Self as FabricActuator } from '{Shared/Fabric/Item/Actuator}';
import { Self as GroupString    } from '{group@string}';
import { Self as Point          } from '{gleam:Point}';
import { Self as Trace          } from '{ti2c:Trace}';
import { Self as TwigItem       } from '{twig@<Shared/Ancillary/Item/Types}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	return( FabricActuator.create(
		'name', this.name,
		'zone', this.zone,
	) );
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( item, trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( item.ti2ctype !== FabricActuator ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/}

	return( Self.create(
		'name',     item.name,
		'trace',    trace,
		'zone',     item.zone,
	) );
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	// FIXME
	return this.zone;
};

/*
| Pastes this item from an export.
|
| ~mapping: GroupString, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigItem ) throw new Error( );
/**/}

	const zone = this.zone.add( offset );

	if( colItems )
	{
		for( let item of colItems )
		{
			if( item.ti2ctype !== Self ) continue;
			if( item.zone === zone ) return false;
		}
	}

	return this.create( 'zone', this.zone.add( offset ) );
};
