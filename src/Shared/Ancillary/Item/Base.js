/*
| Base of item ancillaries.
*/
def.abstract = true;

/*
| The changes needed for secondary data to adapt to primary.
*/
def.proto.changes =
	( ) =>
	undefined;
