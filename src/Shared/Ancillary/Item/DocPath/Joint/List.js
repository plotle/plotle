/*
| A list of joints.
*/
def.extend = 'list@Shared/Ancillary/Item/DocPath/Joint/Self';

import { Self as GroupString } from '{group@string}';
import { Self as Point       } from '{gleam:Point}';
import { Self as TwigItem    } from '{twig@<Shared/Ancillary/Item/Types}';

/*
| Returns the offset if a joint in the list that has the trace.
|
| TODO: Create a lazy .pos group.
*/
def.proto.getByTrace =
	function( trace )
{
	for( let a = 0, alen = this.length; a < alen; a++ )
	{
		if( this.get( a ).pos === trace )
		{
			return a;
		}
	}

	return -1;
};

/*
| Pastes this item from an export.
|
| ~mapping: GroupString, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigItem ) throw new Error( );
/**/}

	const list = [ ];
	for( let joint of this )
	{
		const pJoint = joint.paste( mapping, offset );
		if( !pJoint ) return false;
		list.push( pJoint );
	}
	return this.create( 'list:init', list );
};
