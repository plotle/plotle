/*
| A document path.
*/
def.attributes =
{
	// the name of the docpath
	name: { type: 'string' },

	// joints of the docpath
	joints: { type: 'Shared/Ancillary/Item/DocPath/Joint/List' },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },

	// the zone of the start marker
	zone: { type: 'gleam:Rect' },
};

import { Self as AJoint           } from '{Shared/Ancillary/Item/DocPath/Joint/Self}';
import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as ChangeListMove   } from '{ot:Change/List/Move}';
import { Self as FabricDocPath    } from '{Shared/Fabric/Item/DocPath/Self}';
import { Self as GroupString      } from '{group@string}';
import { Self as ListAJoint       } from '{Shared/Ancillary/Item/DocPath/Joint/List}';
import { Self as ListFabricJoint  } from '{Shared/Fabric/Item/DocPath/Joint/List}';
import { Self as Pin              } from '{gleam:Pin}';
import { Self as Point            } from '{gleam:Point}';
import { Self as Sequence         } from '{ot:Change/Sequence}';
import { Self as Trace            } from '{ti2c:Trace}';
import { Self as TwigItem         } from '{twig@<Shared/Fabric/Item/Types}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	const list = [ ];
	for( let joint of this.joints )
	{
		list.push( joint.asFabric );
	}

	return( FabricDocPath.create(
		'name',   this.name,
		'joints', ListFabricJoint.Array( list ),
		'zone',   this.zone,
	) );
};

/*
| The changes needed for secondary data to adapt to primary.
|
| ~space: ancillary space
*/
def.proto.changes =
	function( space )
{
	let chgs = Sequence.Empty;
	const joints = this.joints;

	// ac is the counter in the changed fabric
	for( let a = 0, ac = 0, alen = joints.length; a < alen; a++, ac++ )
	{
		const joint = joints.get( a );
		const pos = joint.pos;

		if( pos.ti2ctype !== Trace ) continue;

		const outlineJ = joint.outline;
		const item = space.items.get( pos.last.key );

		if( !item )
		{
			chgs =
				chgs.append(
					ChangeListMove.create(
						'traceFrom', this.trace.add( 'joints', ac-- ),
						'val', joint.asFabric,
					)
				);
		}
		else
		{
			const outlineI = item.outline;
			if( outlineJ !== outlineI )
			{
				chgs =
					chgs.append(
						ChangeTreeAssign.create(
							'trace', this.trace.add( 'joints', a ).add( 'outline' ),
							'prev',  outlineJ,
							'val',   outlineI,
						)
					);
			}
		}
	}

	return(
		chgs !== Sequence.Empty
		? chgs
		: undefined
	);
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( item, trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( item.ti2ctype !== FabricDocPath ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/}

	const joints = item.joints;
	const list = [ ];
	for( let joint of joints )
	{
		list.push( AJoint.FromFabric( joint ) );
	}

	return( Self.create(
		'name',   item.name,
		'joints', ListAJoint.Array( list ),
		'trace',  trace,
		'zone',   item.zone,
	) );
};

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	const zone = this.zone;

	return(
		Pin.create(
			'height', zone.height,
			'pos',    zone.pos,
			'width',  zone.width,
		)
	);
};

/*
| Pastes this item from an export.
|
| ~mapping: StringGroup, mapping to new Uids.
| ~offset: offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigItem ) throw new Error( );
/**/}

	const zone = this.zone.add( offset );

	// check for collisions of the starting marker zone.
	if( colItems )
	{
		for( let item of colItems )
		{
			if( item.ti2ctype !== Self ) continue;
			if( item.zone === zone ) return false;
		}
	}

	const joints = this.joints.paste( mapping, offset, colItems );
	if( !joints ) return false;

	return(
		this.create(
			'joints', joints,
			'zone', this.zone.add( offset )
		)
	);
};
