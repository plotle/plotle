/*
| A fix sized text item.
|
| Has potentionaly a scrollbar.
*/
def.extend = 'Shared/Ancillary/Item/Base';

def.attributes =
{
	// the fontsize of the note
	fontSize: { type: 'number' },

	// the notes text
	text: { type: 'Shared/Ancillary/Text/Self' },

	// the fabric trace
	trace: { type: 'ti2c:Trace' },

	// the notes zone
	zone: { type: 'gleam:Rect' },
};

import { Self as AText       } from '{Shared/Ancillary/Text/Self}';
import { Self as FabricNote  } from '{Shared/Fabric/Item/Note}';
import { Self as GroupString } from '{group@string}';
import { Self as Margin      } from '{gleam:Margin}';
import { Self as Point       } from '{gleam:Point}';
import { Self as RectRound   } from '{gleam:RectRound}';
import { Self as Trace       } from '{ti2c:Trace}';
import { Self as TwigItem    } from '{twig@<Shared/Ancillary/Item/Types}';

/*
| Returns the basic fabric.
*/
def.lazy.asFabric =
	function( )
{
	return( FabricNote.create(
		'fontSize', this.fontSize,
		'text',     this.text.asFabric,
		'zone',     this.zone,
	) );
};

/*
| Creates the ancillary from fabric.
*/
def.static.FromFabric =
	function( item, trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( item.ti2ctype !== FabricNote ) throw new Error( );
/**/	if( trace.ti2ctype !== Trace ) throw new Error( );
/**/}

	const zone = item.zone;
	const fontSize = item.fontSize;
	const widthFlow = zone.width - Self.marginInner.x;

	return( Self.create(
		'fontSize', fontSize,
		'text',
			AText.FromFabric( item.text, trace.add( 'text' ), fontSize, 1.5, widthFlow ),
		'trace',    trace,
		'zone',     zone,
	) );
};

/*
| Inner margins of the note.
*/
def.staticLazy.marginInner =
	( ) =>
	Margin.NESW( 4, 5, 4, 5 );

/*
| The item's outline.
*/
def.lazy.outline =
	function( )
{
	const zone = this.zone;
	const cornerRadius = 8;

	return(
		RectRound.create(
			'pos', zone.pos,
			'width', zone.width,
			'height', zone.height,
			'a', cornerRadius,
			'b', cornerRadius,
		)
	);
};

/*
| Pastes this item from an export.
|
| ~mapping:  GroupString, mapping to new Uids.
| ~offset:   offset of the paste (relative to exported items offset)
| ~colItems: if defined, checks if the pasted item collides
|            and returns false if so.
*/
def.proto.paste =
	function( mapping, offset, colItems )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( mapping.ti2ctype !== GroupString ) throw new Error( );
/**/	if( offset.ti2ctype !== Point ) throw new Error( );
/**/	if( colItems.ti2ctype !== TwigItem ) throw new Error( );
/**/}

	const zone = this.zone.add( offset );

	if( colItems )
	{
		for( let item of colItems )
		{
			if( item.ti2ctype !== Self ) continue;
			if( item.zone === zone ) return false;
		}
	}

	return this.create( 'zone', this.zone.add( offset ) );
};
