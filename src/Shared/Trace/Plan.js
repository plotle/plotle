/*
| Plan of traces.
*/
def.abstract = true;

import { Self as Ellipse            } from '{gleam:Ellipse}';
import { Self as FabricActuator     } from '{Shared/Fabric/Item/Actuator}';
import { Self as FabricDocPath      } from '{Shared/Fabric/Item/DocPath/Self}';
import { Self as FabricDocPathJoint } from '{Shared/Fabric/Item/DocPath/Joint/Self}';
import { Self as FabricLabel        } from '{Shared/Fabric/Item/Label}';
import { Self as FabricNote         } from '{Shared/Fabric/Item/Note}';
import { Self as FabricPara         } from '{Shared/Fabric/Text/Para}';
import { Self as FabricSpace        } from '{Shared/Fabric/Space}';
import { Self as FabricStroke       } from '{Shared/Fabric/Item/Stroke/Self}';
import { Self as FabricStrokeJoint  } from '{Shared/Fabric/Item/Stroke/Joint/Self}';
import { Self as FabricSubspace     } from '{Shared/Fabric/Item/Subspace}';
import { Self as FabricText         } from '{Shared/Fabric/Text/Self}';
import { Self as GroupPiece         } from '{group@ti2c:Plan/Piece}';
import { Self as Path               } from '{gleam:Path}';
import { Self as Pin                } from '{gleam:Pin}';
import { Self as Plan               } from '{ti2c:Plan}';
import { Self as Point              } from '{gleam:Point}';
import { Self as Rect               } from '{gleam:Rect}';
import { Self as RectRound          } from '{gleam:RectRound}';
import { Self as RefSpace           } from '{Shared/Ref/Space}';
import { Self as Trace              } from '{ti2c:Trace}';

/*
| Exported items.
*/
def.staticLazy.exportItems =
	( ) =>
	Plan.Build( {
		name: 'exportItems',
		subs: { items: Self.items },
	} );

/*
| Items of a space.
*/
def.staticLazy.items =
	( ) =>
	Plan.Build( {
		name: 'items',
		at: true,
		key: true,
		subs:
		{
			styleBegin: { },
			styleEnd:   { },
			fontSize:   { },
			joints:
			{
				at: true,
				subs:
				{
					outline: { types: [ Ellipse, Path, Pin, Rect, RectRound ] },
					pos: { types: [ Point, Trace ] },
				},
				types: [ FabricDocPathJoint, FabricStrokeJoint ],
			},
			link:
			{
				subs: { offset: { at: true }, }
			},
			name: { subs: { offset: { at: true } } },
			// for docView
			paras:
			{
				at: true,
				subs:
				{
					string:
					{
						subs:
						{
							offset: { at: true }
						},
					},
				},
			},
			pos: { types: [ Point ] },
			scrollPos: { types: [ Point ] },
			text:
			{
				subs:
				{
					paras:
					{
						at: true,
						subs: { string: { subs: { offset: { at: true } } } },
						types: [ FabricPara ],
					}
				},
				types: [ FabricText ],
			},
			widgets: Self.widgets,
			zone:
			{
				subs: { pos: { types: [ Point ] } },
				types: [ Rect ],
			},
		},
		types:
		[
			FabricActuator,
			FabricDocPath,
			FabricLabel,
			FabricNote,
			FabricStroke,
			FabricSubspace,
		],
	} );

/*
| The shell root.
*/
def.staticLazy.root =
	( ) =>
	Plan.Build( {
		name: 'root',
		subs:
		{
			docView: { },
			// floating buttons
			float: { key: true, },
			panels:
			{
				key: true,
				subs: { widgets: Self.widgets },
			},
			screen: { }, // used for split drag action
			seam:
			{
				subs:
				{
					docView:
					{
						subs: { scrollPos: { } },
					},
					forms:
					{
						subs:
						{
							changePwd:
							{
								subs:
								{
									errorMessage: { },
									passwordNew1: { },
									passwordNew2: { },
									passwordOld:  { },
								},
							},
							deleteSpace:
							{
								subs:
								{
									confirmation: { },
									errorMessage: { },
								},
							},
							done: { },
							loading:
							{
								subs:
								{
									refSpace: { },
								},
							},
							login:
							{
								subs:
								{
									errorMessage: { },
									password: { },
									username: { },
								},
							},
							moveTo:
							{
								subs: { scrollPos: { }, },
							},
							noAccessToSpace:
							{
								subs: { nonRefSpace: { }, },
							},
							nonExistingSpace:
							{
								subs: { nonRefSpace: { }, },
							},
							opts:
							{
								subs:
								{
									newsletter: { },
									widgets:
									{
										key: true,
										subs:
										{
											// open state of select widgets
											open: { },
										},
									},
								},
							},
							signUp:
							{
								subs:
								{
									email: { },
									errorMessage: { },
									newsletter: { },
									password: { },
									password2: { },
									username: { },
								},
							},
							spaceDeleted:
							{
								subs: { refSpace: { }, },
							},
							space:
							{
								subs:
								{
									widgets:
									{
										key: true,
										subs:
										{
											// open state of select widgets
											open: { },
										},
									},
								},
							},
							welcome: { },
						},
					},
					space:
					{
						subs:
						{
							bench:
							{
								subs:
								{
									stroke:
									{
										subs: { openStyle: { } },
									},
								},
							},
							textItems:
							{
								key: true,
								subs: { scrollPos: { }, },
							},
						},
					},
				},
			},
			view:
			{
				at: true,
				subs:
				{
					docView:
					{
						subs:
						{
							items: Self.items,
							panel:
							{
								subs: { widgets: Self.widgets },
							},
							scrollbarY: { },
						},
					},
					form:
					{
						key: true,
						subs:
						{
							widgets: Self.widgets,
						},
					},
					space:
					{
						key: true,
						subs:
						{
							bench:
							{
								subs:
								{
									docPath:
									{
										subs:
										{
											handle:
											{
												at: true,
												subs:
												{
													add:  { },
													rem1: { },
													rem2: { },
												},
											},
										},
									},
									frame:
									{
										subs:
										{
											widgets: Self.widgets,
											handle:
											{
												subs:
												{
													strokeN: { },
													strokeE: { },
													strokeS: { },
													strokeW: { },
												}
											},
										},
									},
									newItem:
									{
										subs:
										{
											label:    { },
											note:     { },
											path:     { },
											stroke:   { },
											subspace: { },
										}
									},
									stroke:
									{
										subs:
										{
											widgets: Self.widgets,
											style:
											{
												subs:
												{
													widgets: Self.widgets,
												}
											}
										},
									},
								},
							},
							items: Self.items,
						},
					},
				}
			},
			windows:
			{
				at: true,
				subs:
				{
					transform: { },
				},
			}
		},
	} );

/*
| A space.
*/
def.staticLazy.space =
	( ) =>
	Plan.Build( {
		name: 'space',
		key: true,
		subs:
		{
			publicReadable: { },
			items: Self.items,
		},
		types: [ FabricSpace ],
	} );

/*
| An update answer.
*/
def.staticLazy.update =
	( ) =>
	GroupPiece.Table( {
		'space':    Self.space,
		'userInfo': Self.userInfo,
	} );

/*
| TODO check if upgradeable into update( ).
*/
def.staticLazy.userInfo =
	( ) =>
	Plan.Build( {
		name: 'userInfo',
		subs:
		{
			newsletter: { },
			listSpacesOwned:
			{
				name: 'listSpacesOwned',
				at: true,
				subs:
				{
					tag: { subs: { offset: { at: true } } },
					username: { subs: { offset: { at: true } } },
				},
				types: [ RefSpace ],
			},
		},
	} );

/*
| Widgets on a form, panel or on visual items.
*/
def.staticLazy.widgets =
	( ) =>
	Plan.Build( {
		name: 'widgets',
		key: true,
		subs:
		{
			checked:   { },
			options:   { at: true },
			scrollPos: { types: [ Point ] },
			string:    { subs: { offset: { at: true } } },
			value:     { subs: { offset: { at: true } } },
			widgets:   ( ( ) => Self.widgets ),
		},
	} );
