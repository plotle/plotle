/*
| A client wants to delete a space.
*/
def.attributes =
{
	// confirmation, must be identical refSpace.fullname
	confirmation: { type: 'string', json: true },

	// uid of the space to delete
	uid: { type: 'string', json: true },

	// user requesting the space
	userCreds: { type: 'Shared/User/Creds', json: true },
};

def.json = true;
