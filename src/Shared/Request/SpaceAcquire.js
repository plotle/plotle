/*
| A client wants to acquire a space.
*/
def.attributes =
{
	// if true asks the server to create the space if it doesn't exist yet
	createMissing: { type: 'boolean', json: true },

	// reference of the space to acquire
	refSpace: { type: 'Shared/Ref/Space', json: true },

	// user requesting the space
	userCreds: { type: 'Shared/User/Creds', json: true },
};

def.json = true;
