/*
| A client requests spaces to be altered.
*/
def.attributes =
{
	// updates for spaces
	updates: { type: 'list@Shared/Dynamic/Update/Space', json: true },

	// user requesting the change
	userCreds: { type: 'Shared/User/Creds', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
