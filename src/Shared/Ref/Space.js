/*
| References a space.
*/
def.attributes =
{
	// name of the user the space belongs to
	username: { type: 'string', json: true },

	// tag of the space
	tag: { type: 'string', json: true },
};

def.json = true;

/*
| Creates the reference from a fullname
*/
def.static.Fullname =
	function( fullname )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( fullname ) !== 'string' ) throw new Error( );
/**/}

	const ar = fullname.split( ':' );
	if( ar.length !== 2 ) throw new Error( );
	return Self.UsernameTag( ar[ 0 ], ar[ 1 ] );
};

/*
| Creates the reference from a 'link' specifier.
|
| ~link: specifier to parse
| ~refFrom: reference from space.
*/
def.static.Link =
	function( link, refFrom )
{
	link = link.trim( );

	const ios = link.indexOf( ':' );
	if( ios >= 0 )
	{
		return(
			Self.create(
				'username', link.substring( 0, ios ),
				'tag', link.substring( ios + 1 ),
			)
		);
	}

	if( refFrom === undefined ) return undefined;

	if( link[ 0 ] === '/' )
	{
		return(
			Self.create(
				'username', refFrom.username,
				'tag', link.substring( 1 ),
			)
		);
	}

	return(
		Self.create(
			'username', refFrom.username,
			'tag', refFrom.tag + '/' + link,
		)
	);
};

/*
| Fullname of the space reference.
*/
def.lazy.fullname =
	function( )
{
	return this.username + ':' + this.tag;
};

/*
| Reference to plotles home space.
*/
def.staticLazy.PlotleHome =
	( ) =>
	Self.UsernameTag( 'plotle', 'home' );

/*
| Reference to plotle sandbox space.
*/
def.staticLazy.PlotleSandbox =
	( ) =>
	Self.UsernameTag( 'plotle', 'sandbox' );

/*
| Create Shortcut.
*/
def.static.UsernameTag =
	( username, tag ) =>
	Self.create( 'username', username, 'tag', tag );
