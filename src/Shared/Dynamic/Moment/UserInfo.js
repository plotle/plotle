/*
| A moment in a list of space references.
*/
def.attributes =
{
	// sequence number the dynamic is at
	seq: { type: 'integer', json: true },

	// the username the info is for
	username: { type: 'string', json: true },
};

def.json = true;

/*
| Shortcut.
*/
def.static.SeqUsername =
	function( seq, username )
{
	return(
		Self.create(
			'seq',      seq,
			'username', username,
		)
	);
};
