/*
| A change wrapped for client/server transport.
*/
def.attributes =
{
	// changes
	changes: { type: [ 'undefined', 'ot:Change/Sequence' ], json: true },

	// the dynamic has been deleted
	deleted: { type: [ 'undefined', 'boolean' ], json: true },

	// change id
	id: { type: 'string', json: true },

	// sequence number
	// the client sending changeWraps to the server are unnumbered.
	seq: { type: [ 'undefined', 'number' ], json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ChangeWrapList } from '{Shared/ChangeWrap/List}';
import { Self as Uid            } from '{Shared/Session/Uid}';

/*
| Custom shortcut.
*/
def.static.Wrapped =
	function( changes )
{
	return(
		Self.create(
			'changes', changes,
			'id', Uid.newUid( ),
		)
	);
};

/*
| Performes the wrapped changes on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	return this.changes.changeTree( tree );
};

/*
| Reversevly performes the wrapped changes on a tree.
*/
def.proto.changeTreeReversed =
	function( tree )
{
	return this.changes.reversed.changeTree( tree );
};

/*
| Transform another change.
*/
def.proto.transform =
	function( other )
{
	const changes = this.changes;

	switch( other.ti2ctype )
	{
		case ChangeWrapList:
		{
			const list = [ ];
			for( let cw of other )
			{
				list.push( cw.create( 'changes', changes.transform( cw.changes ).sequence ) );
			}
			return ChangeWrapList.Array( list );
		}

		case Self:
			return other.create( 'changes', changes.transform( other.changes ).sequence );

		default:
			return changes.transform( other );
	}
};

/*
| Additional checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	if( this.changes && this.deleted ) throw new Error( );
/**/	if( this.deleted === false ) throw new Error( );
/**/}
};
