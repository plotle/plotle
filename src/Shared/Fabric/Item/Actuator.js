/*
| An actuator is a button on a space.
*/
def.attributes =
{
	// the actuator name
	name: { type: 'string', json: true },

	// the zone
	zone: { type: 'gleam:Rect', json: true }
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
