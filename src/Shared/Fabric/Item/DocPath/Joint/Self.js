/*
| A joint of a document path.
*/
def.attributes =
{
	// the path position
	pos: { type: [ 'ti2c:Trace', 'gleam:Point', ], json: true },

	// ancillary outline of the joint
	outline: { type: [ 'undefined', '<gleam:Shape/Types' ], json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];
