/*
| A stroke (with possible arrow heads)
*/
def.attributes =
{
	// stroke joints
	joints: { type: 'Shared/Fabric/Item/Stroke/Joint/List', json: true },

	// "arrow" or "none"
	styleBegin: { type: 'string', json: true },

	// "arrow" or "none"
	styleEnd: { type: 'string', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as Segment     } from '{gleam:Segment}';

/*
| The basic connection of the stroke.
*/
def.lazy.segment =
	function( )
{
	const j0 = this.joints.get( 0 );
	const j1 = this.joints.get( 1 );

	return Segment.Connection( j0.outline || j0.pos, j1.outline || j1.pos );
};

/*
| The items zone.
*/
def.lazy.zone =
	function( )
{
	return this.segment.zone;
};
