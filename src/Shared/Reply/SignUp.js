/*
| The servers replies to a succesful clients sign up / register request.
*/
def.attributes =
{
	// additional userInfo
	userInfo: { type: 'Shared/Dynamic/Current/UserInfo', json: true },
};

def.json = true;
