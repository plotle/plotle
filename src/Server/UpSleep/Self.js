/*
| A client's update set to sleep.
*/
def.attributes =
{
	// the list of moments of dynamics the client is sleeping for
	moments: { type: 'list@<Shared/Dynamic/Moment/Types' },

	// the node result handler of the clients request
	result: { type: 'protean' },

	// the timer associated with this sleep
	timer: { type: 'protean' },
};
