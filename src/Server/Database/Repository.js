/*
| Database interface.
*/
def.abstract = true;

import { default as level } from 'level';
import { default as lexi  } from 'lexicographic-integer';

import { Self as ChangeSequence   } from '{ot:Change/Sequence}';
import { Self as ChangeSkid       } from '{Server/Database/ChangeSkid/Self}';
import { Self as ChangeTreeAssign } from '{ot:Change/Tree/Assign}';
import { Self as ChangeWrap       } from '{Shared/ChangeWrap/Self}';
import { Self as ChangeWrapList   } from '{Shared/ChangeWrap/List}';
import { Self as FabricSpace      } from '{Shared/Fabric/Space}';
import { Self as Log              } from '{Server/Log}';
import { Self as Plan             } from '{Shared/Trace/Plan}';
import { Self as RefSpace         } from '{Shared/Ref/Space}';
import { Self as TraceRoot        } from '{Shared/Trace/Root}';
import { Self as TwigItem         } from '{twig@<Shared/Fabric/Item/Types}';
import { Self as Uid              } from '{Shared/Session/Uid}';
import { Self as UserInfoSkid     } from '{Server/Database/UserInfoSkid}';

// the level instance
let _db;

/*
| Database version expected.
*/
const _dbVersion = '54';

/*
| Returns a repository object with an active connection.
*/
def.static.connect =
	async function( )
{
	let name = config.database.name;
	if( !name )
	{
		throw new Error( 'database name not configured' );
	}

	name = name.replaceAll( '${dbVersion}', _dbVersion );

	Log.log( 'opening database ' + name );

	_db = new level.Level( name );
	await _db.open( );
	const version = await _db.get( 'version' );

	if( version === undefined )
	{
		_db.close( );

		if( config.database.establish )
		{
			Log.log( 'not found, establishing a new one!' );
			await _establishRepository( name );
		}
		else
		{
			throw new Error( 'No database found' );
		}
	}
	else if( version !== _dbVersion )
	{
		throw new Error(
			'Wrong repository schema version, expected '
			+ _dbVersion
			+ ', but got ' + version
		);
	}
};

/*
| Deletes a space from the database.
|
| ~uid: space's unique identifier.
|
| ~return: true on success, error message on failure
*/
def.static.deleteSpace =
	async function( refSpace, uid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( refSpace.ti2ctype !== RefSpace ) throw new Error( );
/**/	if( typeof( uid ) !== 'string' ) throw new Error( );
/**/	if( uid.indexOf( ':' ) >= 0 ) throw new Error( );
/**/}

	const fullname = refSpace.fullname;
	Log.log( 'deleting space from repository', fullname, uid );

	let o = await _db.get( 'spaces:' + refSpace.fullname );
	if( o === undefined )
	{
		return 'notFound';
	}

	o = JSON.parse( o );

	// uid's must match!
	if( o.uid !== uid ) return 'wrongUid';

	// deletes the space from the "spaces" table
	await _db.del( 'spaces:' + refSpace.fullname );

	// clears the uids
	await _db.clear( { gt: 'changes:' + uid + ':', lt: 'changes:' + uid + ';' } );

	Log.log( 'deleted space ' + refSpace.fullname + ' (' + uid + ') from db' );

	return true;
};

/*
| Establishes a space in the database.
|
| ~refSpace: reference of the space
| ~fabric: space fabric to establish
|
| ~returns: it's Uid.
*/
def.static.establishSpace =
	async function( refSpace, fabric )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	//const refSpace = space.ref;

/**/if( CHECK && !refSpace ) throw new Error( );

	const uid = Uid.newUid( );

	const ops = [ ];
	// inserts the space in the "spaces" table
	ops.push( {
		type: 'put',
		key: 'spaces:' + refSpace.fullname,
		value:
			JSON.stringify( {
				username : refSpace.username,
				tag : refSpace.tag,
				uid : uid
			} )
	} );

	const changeAssign =
		ChangeTreeAssign.create(
			'trace', TraceRoot.space( uid ),
			'val', fabric,
			'prev', undefined,
		);

	const changeWrap =
		ChangeWrap.create(
			'id', Uid.newUid( ),
			'changes', ChangeSequence.Elements( changeAssign )
		);

	const changeSkid =
		ChangeSkid.ChangeWrap( changeWrap, ':init', Date.now( ) );

	ops.push( {
		type: 'put',
		key: 'changes:' + uid + ':' + lexi.pack( 1, 'hex' ),
		value: changeSkid.jsonfy( ),
	} );

	await _db.batch( ops );

	return uid;
};

/*
| Returns a space change.
*/
def.static.getSpaceChange =
	async function( spaceId, seqNr )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( spaceId ) !== 'string' ) throw new Error( );
/**/	if( typeof( seqNr ) !== 'number' ) throw new Error( );
/**/}

	const o = await _db.get( 'changes:' + spaceId + ':' + lexi.pack( seqNr, 'hex' ) );

	return(
		o !== undefined
		? ChangeSkid.FromJson( JSON.parse( o ), Plan.space )
		: 'notFound'
	);
};

/*
| Returns all space names.
*/
def.static.getSpaceNames =
	async function( )
{
	const names = [ ];
	const it = _db.iterator( { gt: 'spaces:', lt: 'spaces;' } );

	for await ( const [ key ] of it )
	{
		names.push( key );
	}

	return names;
};

/*
| Returns the userinfo for a username.
*/
def.static.getUser =
	async function( username )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const o = await _db.get( 'users:' + username );
	const duis = UserInfoSkid.FromJson( JSON.parse( o ) );

	return duis.asUser;
};

/*
| Returns all user names.
*/
def.static.getUserNames =
	async function( )
{
	const names = [ ];
	const it = _db.iterator( { gt: 'users:', lt: 'users;' } );
	for await ( const [ , value ] of it )
	{
		const o = JSON.parse( value );
		names.push( o.name );
	}
	return names;
};

/*
| Returns the meta data of a space by a name.
*/
def.static.getSpaceMeta =
	async function( name )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( !name.startsWith( 'spaces:' ) ) throw new Error( );
/**/}

	const json = await _db.get( name );

	return JSON.parse( json );
};

/*
| Server has seen the user.
| Updates the 'seen:' table.
|
| This returns immediately, so for the caller it's send & pray.
*/
def.static.seenUser =
	function( userInfo )
{
	// creates a changes view for this space
	_db.put(
		'seen:' + userInfo.name,
		Date.now( ),
		( data, err ) =>
		{
			if( err ) console.log( err );
		}
	);
};

/*
| Saves a user in the database.
*/
def.static.saveUser =
	async function( userInfo )
{
	// creates a changes view for this space
	await _db.put(
		'users:' + userInfo.name,
		UserInfoSkid.FromUserInfo( userInfo ).jsonfy( )
	);
};

/*
i| Sends a list of changeWraps into the database.
|
| This returns immediately, so for the caller it's send & pray.
|
| ~changeWrapList: change list to be sent
| ~spaceId:        space id
| ~username:       the user doing the change
| ~seqNr:          sequence number of the change
| ~date:           the timestamp for the changes
*/
def.static.sendChanges =
	function( changeWrapList, spaceId, username, seqNr, date )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 5 ) throw new Error( );
/**/	if( changeWrapList.ti2ctype !== ChangeWrapList ) throw new Error( );
/**/	if( typeof( spaceId ) !== 'string' ) throw new Error( );
/**/	if( typeof( username ) !== 'string' ) throw new Error( );
/**/	if( typeof( seqNr ) !== 'number' ) throw new Error( );
/**/	if( typeof( date ) !== 'number' ) throw new Error( );
/**/}

	const ops = [ ];
	for( let cw of changeWrapList )
	{
		if( !cw ) continue;

		const cs = ChangeSkid.ChangeWrap( cw, username, date );

		ops.push( {
			type: 'put',
			key: 'changes:' + spaceId + ':' + lexi.pack( seqNr++, 'hex' ),
			value: cs.jsonfy( ),
		} );
	}

	_db.batch(
		ops,
		( data, err ) =>
		{
			if( err ) console.log( err );
		}
	);
};

/*
| Initializes a new repository.
|
| ~name:       database name to use
| ~version:    repository version
*/
async function _establishRepository( name )
{
	_db = new level.Level( name );

	await _db.open( );
	await _db.put( 'version', _dbVersion );

	await Self.establishSpace(
		RefSpace.PlotleHome,
		FabricSpace.create(
			'items', TwigItem.create( ),
			'publicReadable', true,
		)
	);

	await Self.establishSpace(
		RefSpace.PlotleSandbox,
		FabricSpace.create(
			'items', TwigItem.create( ),
			'publicReadable', true,
		)
	);
}

