/*
| Starts the server.
*/
Error.stackTraceLimit = 15;

global.NODE = true;
global.CHECK = true;
global.TI2C_RETENTION = false;

import * as util from 'node:util';
util.inspect.defaultOptions.depth = null;

await import( 'ti2c' );

// ti2c packages
await import( 'ti2c-ot' );
await import( 'ti2c-web' );
await import( 'ti2c-gleam' );

const pkg =
	await ti2c.register(
		'name',    'plotle',
		'meta',    import.meta,
		'source',  'src/',
		'relPath', 'Server/Start',
		'codegen', 'codegen/'
	);
const Root = await pkg.import( 'Server/Root' );
Root.init( pkg.rootDir );
