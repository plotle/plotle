/*
| Holds and manages all spaces.
*/
def.abstract = true;

import { Self as GroupSpaceBox  } from '{group@Server/SpaceBox}';
import { Self as GroupString    } from '{group@string}';
import { Self as Log            } from '{Server/Log}';
import { Self as MomentSpace    } from '{Shared/Dynamic/Moment/Space}';
import { Self as MomentUserInfo } from '{Shared/Dynamic/Moment/UserInfo}';
import { Self as RefSpace       } from '{Shared/Ref/Space}';
import { Self as Repository     } from '{Server/Database/Repository}';
import { Self as SpaceBox       } from '{Server/SpaceBox}';
import { Self as UpSleeps       } from '{Server/UpSleep/Manager}';
import { Self as Users          } from '{Server/User/Manager}';

/*
| The spaceBoxes by uid.
*/
let	_boxes;

/*
| All spaces (reference to uid).
*/
let _uids;

/*
| Creates a new space.
*/
def.static.createSpace =
	async function( refSpace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( refSpace.ti2ctype !== RefSpace ) throw new Error( );
/**/}

	const spaceBox = await SpaceBox.createSpace( refSpace );
	_add( spaceBox );

	if( refSpace.username !== 'plotle' )
	{
		await Users.addUserRefSpace( refSpace );
		await UpSleeps.wake( MomentUserInfo, refSpace.username );
	}

	return spaceBox;
};

/*
| Deletes a space.
|
| ~uid: uid of space to delete.
|
| ~return: true on success, error message on failure
*/
def.static.deleteSpace =
	async function( uid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( uid ) !== 'string' ) throw new Error( );
/**/	if( uid.indexOf( ':' ) >= 0 ) throw new Error( );
/**/}

	// deletes the space from server data and database
	// gets the spaceBox
	let spaceBox = _boxes.get( uid );
	if( !spaceBox )
	{
		return 'notFound';
	}

	const refSpace = spaceBox.refSpace;

	// first marks the space as deleted (just to be sure)
	spaceBox = spaceBox.setDeleted( );
	_boxes = _boxes.set( uid, spaceBox );

	// removes the uid reference
	// but leaves it in spaces since some clients still may ask for updates
	// to be told it has been deleted
	_uids = _uids.remove( refSpace.fullname );

	// removes the space from the repository
	await Repository.deleteSpace( spaceBox.refSpace, uid );

	// tells connected clients to remove it from the list of userSpaces.
	await Users.delUserRefSpace( refSpace );

	// FUTURE do multiple waking at once
	await UpSleeps.wake( MomentSpace, uid );

	return true;
};

/*
| Gets a space by it's reference
*/
def.static.getByRef =
	function( ref )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( ref.ti2ctype !== RefSpace ) throw new Error( );
/**/}

	const uid = _uids.get( ref.fullname );

	return(
		uid
		? _boxes.get( uid )
		: undefined
	);
};

/*
| Gets a space by it's uid.
*/
def.static.getByUid =
	function( uid )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( uid ) !== 'string' ) throw new Error( );
/**/	if( uid.indexOf( ':' ) >= 0 ) throw new Error( );
/**/}

	return _boxes.get( uid );
};

/*
| Initializes the space manager.
| Loads all spaces and play back all changes from the database.
*/
def.static.init =
	async function( )
{
	_boxes = GroupSpaceBox.Empty;
	_uids = GroupString.Empty;

	Log.log( 'loading and replaying all spaces' );

	const names = await Repository.getSpaceNames( );

	for( let name of names )
	{
		const o = await Repository.getSpaceMeta( name );
		const refSpace = RefSpace.UsernameTag( o.username, o.tag );
		const uid = o.uid;
		Log.log( 'loading and replaying "' + refSpace.fullname + '" ' +  uid );
		const spaceBox = await SpaceBox.loadSpace( refSpace, uid );
		_add( spaceBox );
	}
};

/*
| Sets a spaceBox.
*/
def.static.set =
	function( spaceBox )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( spaceBox.ti2ctype !== SpaceBox ) throw new Error( );
/**/}

	_boxes = _boxes.set( spaceBox.uid, spaceBox );
};

/*
| Creates a sitemap.
*/
def.static.sitemap =
	function( )
{
	const data =
	[
		'<?xml version="1.0" encoding="UTF-8"?>',
		'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">',
		'<url>',
			'<loc>' + 'https://plotle.com/</loc>',
		'</url>',
	];

	for( let uid of _boxes.keys )
	{
		const spaceBox = _boxes.get( uid );
		if( !spaceBox.space.publicReadable ) continue;
		const refSpace = spaceBox.refSpace;
		data.push(
			'<url>',
				'<loc>'
				+ 'https://plotle.com/sp/' + refSpace.username
				+ '/' + refSpace.tag
				+ '/'
				+ '</loc>',
			'</url>',
		);
	}

	data.push( '</urlset>' );
	return data.join( '' );
};

/*
| Tests if the user has access to a space.
*/
def.static.testAccess =
	function( user, refSpace )
{
	if( refSpace.username === 'plotle' )
	{
		switch( refSpace.tag )
		{
			case 'sandbox':
				return 'rw';

			case 'home':
				return(
					user.name === config.admin
					? 'rw'
					: 'ro'
				);

			default:
				return(
					user.name === config.admin
					? 'rw'
					: 'no'
				);
		}
	}

	if( user.name === refSpace.username )
	{
		return 'rw';
	}

	const spaceBox = Self.getByRef( refSpace );
	if( spaceBox && spaceBox.space.publicReadable )
	{
		return 'ro';
	}

	return 'no';
};

/*
| Adds a space box.
*/
function _add( spaceBox )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( spaceBox.ti2ctype !== SpaceBox ) throw new Error( );
/**/}

	const uid = spaceBox.uid;
	const fullname = spaceBox.refSpace.fullname;

	if( _uids.get( fullname ) ) throw new Error( );
	if( _boxes.get( uid ) ) throw new Error( );

	_uids = _uids.set( fullname, uid );
	_boxes = _boxes.set( uid, spaceBox );
}

